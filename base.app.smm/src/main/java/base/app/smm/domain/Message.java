/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.smm.domain;

import base.app.smm.application.AvailableMachines;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author
 */
public class Message implements Serializable {

    private char version;
    private char code;
    private short id;
    private short data_length;
    private String raw_data;
    public static final char CODE_ACK = 150;
    public static final char CODE_NACK = 151;

    AvailableMachines av = new AvailableMachines();

    /**
     * Constructor for creating a Message
     *
     * @param version Version a char with value of the version
     * @param code code char representing the type of request/reply
     * @param id id short the Comunication Protocol ID of the machine
     * @param data_length data_length short containg the lenth of the raw data
     * string
     * @param raw_data raw_data String with the text of the message
     */
    public Message(char version, char code, short id, short data_length, String raw_data) {
        this.version = version;
        this.code = code;
        this.id = id;
        this.data_length = data_length;
        this.raw_data = raw_data;
    }

    /**
     * Creates a array[] with the correct off-set for each field
     *
     * @return array[] containing the message
     */
    public byte[] serialized() {
        int rawDataOffset = 6;
   
        ByteBuffer buffer = ByteBuffer.allocate(7 + this.data_length);
        buffer.order(ByteOrder.LITTLE_ENDIAN);
        buffer.putChar(0, version);
        buffer.putChar(1, code);
        buffer.putShort(2, id);
        buffer.putShort(4, data_length);

        for (int i = 0; i < this.data_length; i++) {
            buffer.putChar(rawDataOffset, this.raw_data.charAt(i));
            rawDataOffset++;
        }

        byte[] result = buffer.array();
        return result;

    }

    /**
     * Method that recieves a byte[] containing a message and converts to
     * readable text creates an instance of machine
     *
     * @param bytes array[] containing the message
     * @param machineIP the IP of the machine that sended the message
     * @return Instance of machine created
     * @throws java.lang.InterruptedException
     */
    public Machine deserialized(byte[] bytes, String machineIP) throws InterruptedException {

        ByteBuffer buffer = ByteBuffer.allocate(bytes.length);
        buffer.put(bytes);
        buffer.order(ByteOrder.LITTLE_ENDIAN);

        int j = 0;
        short machineProtocolID = buffer.getShort(2);
        short rawData_length = buffer.getShort(4);
        int replyCode = buffer.get(1) & 0xFF;

        if (replyCode == CODE_NACK) {
            return null;
        }
        /// raw data
        byte[] data = new byte[rawData_length];

        for (int i = 6; i < rawData_length + 6; i++) {
            data[j] = buffer.get(i);
            j++;
        }
        String rawData = new String(data);
        Machine machine = new Machine(machineProtocolID, rawData, machineIP);

        return machine;
    }

    /**
     * Creates a request type message
     *
     * @param requestCode
     * @return array[] containing the message
     */
    public static byte[] prepareRequest(char requestCode) {

        char version = 1, code = requestCode;
        String rawData = "";
        short id = 0, length = (short) rawData.length();

        Message m = new Message();
        m.setVersion(version);
        m.setCode(code);
        m.setId(id);
        m.setData_length(length);
        m.setRaw_data(rawData);

        return m.serialized();
    }
   
    public Message() {
    }

    /**
     * Set to change the version of the message
     *
     * @param version the new version to change
     */
    public void setVersion(char version) {
        this.version = version;
    }

    /**
     * Set to change the code of the message
     *
     * @param code the new code to change
     */
    public void setCode(char code) {
        this.code = code;
    }

    /**
     * Set to change the id of the message
     *
     * @param id the new id to change
     */
    public void setId(short id) {
        this.id = id;
    }

    /**
     * Set to change the data length of the message
     *
     * @param data_length the new data length to change
     */
    public void setData_length(short data_length) {
        this.data_length = data_length;
    }

    /**
     * Set to change the raw Data of the message
     *
     * @param raw_data the new raw Data to change
     */
    public void setRaw_data(String raw_data) {
        this.raw_data = raw_data;
    }

}

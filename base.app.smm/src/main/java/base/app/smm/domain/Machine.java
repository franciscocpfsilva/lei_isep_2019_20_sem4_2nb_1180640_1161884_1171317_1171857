/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.smm.domain;

import java.time.LocalDateTime;

/**
 *
 * @author joaomachado
 */
    public class Machine {

    private final short machineProtocolID;
    private final String data;
    private final LocalDateTime time;
    private final String ip;
    private String estado;

    /**
     * Constructor for creating a Machine It saves the time it was created and
     * sets the state as Available
     *
     * @param machineProtocolID the machineProtocolID
     * @param data the message sent from the machine
     * @param ip the IP of the machine
     */
    public Machine(final short machineProtocolID, final String data, final String ip) {
        this.machineProtocolID = machineProtocolID;
        this.data = data;
        this.time = LocalDateTime.now();
        this.ip = ip;
        this.estado = "Disponivel";
    }

    /**
     * Get the Machine Protocol ID from an instance
     *
     * @return returns the protocl ID
     */
    public short getMachineProtocolID() {
        return machineProtocolID;
    }

    /**
     * Get the last message from a machine
     *
     * @return the message from a machine
     */
    public String getData() {
        return data;
    }

    /**
     * Get the last time a machine sent a packet
     *
     * @return the last time machine sent a packet
     */
    public LocalDateTime getTime() {
        return time;
    }

    /**
     * Get the Machine IP
     *
     * @return returns IP of machine
     */
    public String getMachineIP() {
        return ip;
    }

    /**
     * Get the Machine State
     *
     * @return returns State of machine
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Updates the State of Machine
     *
     * @param estado recieves a new State
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }
    
       /**
     * Method that verifies equality between ride and another object
     *
     * @param obj, Object, another object to verify equality
     * @return true if the two are equal or false otherwise
     */
    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Machine other = (Machine) obj;

        return this.machineProtocolID == other.machineProtocolID;
    }
    

    /**
     * Custom implementation of toString
     *
     */
    @Override
    public String toString() {
        return "MachineID: " + getMachineProtocolID() + "\nLast Message: " + getData() + "\nState: " + this.estado + "\n Machine IP: " + getMachineIP();
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.smm.presentation;

import base.app.smm.application.SendRequest;
import static java.lang.Integer.parseInt;
import java.util.Scanner;

/**
 *
 * @author joaomachado
 */
public class Main {

    private static final int HELLO_TIMER = 30;
    private static final int MACHINE_STATUS_TIMER = 60;
    private static final Scanner in = new Scanner(System.in);
    private static final SendRequest server = new SendRequest();

    /**
     * Main method,validates if args were passed
     *
     * @param args
     * @throws java.lang.Exception
     */
    public static void main(String args[]) throws Exception {
        System.out.println("##############################################\n"
                + "### Sistema de Monitorização das Máquinas  ###\n"
                + "###                                        ###\n"
                + "##############################################");
        if (args.length == 0) {
            menu(HELLO_TIMER, MACHINE_STATUS_TIMER);

        } else if (args.length == 2) {
            if (args[0].equals("0")) {
                menu(HELLO_TIMER, parseInt(args[1]));
            } else {
                menu(parseInt(args[0]), parseInt(args[1]));
            }
        }

    }

    public static void menu(final int cadenciaEnvio, final int cadenciaUpdate) {
        server.sendHelloRequest(cadenciaEnvio);
        while (true) {
            System.out.printf("\nChoose an option: \n'1' to send Reset request \n'2' to see all Machines\n");
            String word = in.nextLine();
            try {
                switch (parseInt(word)) {
                    case 1:
                        server.sendResetRequest();
                        break;
                    case 2:
                        server.seeAllMachinesRequest(cadenciaUpdate);
                        break;
                    default:
                        System.out.println("\nChoose a valid option!\n");
                        break;
                }
            } catch (NumberFormatException ex) {
                System.out.println("\nError: Thtat is not a valid number\n");
            }
        }
    }
}

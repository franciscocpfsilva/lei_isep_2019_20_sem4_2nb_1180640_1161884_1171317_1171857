/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.smm.application;

import static base.app.smm.application.SendRequest.machines;
import base.app.smm.domain.Machine;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

/**
 *
 * @author joaomachado
 */
public class AvailableMachines {

    /**
     * Adds a machine to the map of discovered machines. Validates if there is
     * already a machine with the same ID and if te IP is the same
     *
     * @param machine Machine Instance to add
     * @return Machine Instance added
     */
    public Machine addMachine(Machine machine) {

        if (machines.containsKey(machine.getMachineProtocolID())) {
            Machine thatMachine = machines.get(machine.getMachineProtocolID());

            if (machine.getMachineIP().equals(thatMachine.getMachineIP())) {
               // System.out.println("\nUpdating Machine Comunication Protocol ID: " + machine.getMachineProtocolID());
                return machines.put(machine.getMachineProtocolID(), machine);

            } else {
               // System.out.println("\nError: Comunication Protocol ID with multiple IP's");
                return null;
            }

        } else {
            return machines.put(machine.getMachineProtocolID(), machine);
        }
    }

    /**
     * Updates the state of all machines with a time in seconds revieved. At the
     * same time prints all the machines in the map
     *
     * @param time time in seconds to consider a machine is available or not
     */
    public void seeAllMachines(int time) {
        System.out.println("\n---------Printing all Machines---------\n");
        machines.entrySet().forEach((entry) -> {
            Machine value = entry.getValue();

            long seconds = ChronoUnit.SECONDS.between(value.getTime(), LocalDateTime.now());
            if (seconds > time) {
                value.setEstado("Indisponivel");
            } else {
                value.setEstado("Disponivel");
            }
            System.out.println(entry.getValue().toString());
        });

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.smm.application;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

/**
 *
 * @author joaomachado
 */
public class ResetRequest {

    private static final SendRequest server = new SendRequest();

    /**
     * Method that sends and recieves Request UPD packets. Validates timeout and
     * displays the machines that were discovered
     *
     * @param sock DatagramSocket that contains the packets
     * @param udpPacket DatagramPacket that is send with the hello request
     * @param dataToSend byte[] with the message Hello
     * @param udpPacketR DatagramPacket that is recieved from machines
     * @param dataToReceive byte[] with the message reply
     */
    public void runResetRequest(final DatagramSocket sock, DatagramPacket udpPacket,
            DatagramPacket udpPacketR, byte[] dataToSend, byte[] dataToReceive, String ipAdress) {

        try {

            InetAddress targetIP = InetAddress.getByName(ipAdress);
            udpPacket.setAddress(targetIP);
            udpPacketR.setAddress(targetIP);

            udpPacket.setData(dataToSend);
            udpPacket.setLength(dataToSend.length);
            sock.send(udpPacket);
            udpPacketR.setData(dataToReceive);
            udpPacketR.setLength(dataToReceive.length);
            try {
                sock.receive(udpPacketR);
                String ip = udpPacketR.getAddress().getHostAddress();
                server.processPacket(dataToReceive, ip);
                System.out.println("\nReset reply recieved! \n");
            } catch (SocketTimeoutException ex) {
                System.out.println("\n Reset Request Timeout - " + ipAdress + "\n");
            }
        } catch (UnknownHostException ex) {
            System.out.println("\nError: " + ex);
        } catch (IOException ex) {
            System.out.println("\nError: " + ex);
        }
    }
}

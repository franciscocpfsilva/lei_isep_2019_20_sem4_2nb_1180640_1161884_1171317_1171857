/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.smm.application;

import base.app.smm.domain.Machine;
import base.app.smm.domain.Message;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.HashSet;
import java.util.Set;
import static base.app.smm.domain.Message.prepareRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author joaomachado
 */
public class SendRequest {

    private final ResetRequest resetRequest = new ResetRequest();
    private final AvailableMachines availableMachines = new AvailableMachines();

    public static Map<Short, Machine> machines = new HashMap<>();

    private static final int TIMEOUT_HELLO_REQUEST = 3;
    private static final int TIMEOUT_RESET_REQUEST = 30;
    public static final char CODE_HELLO = 0;
    public static final char CODE_RESET = 3;
    private final Set<String> IPList = new HashSet<>();

    static InetAddress targetIP;
    private static final Scanner in = new Scanner(System.in);

    /**
     * Run method that creates the byte[] with hello request to send.Set a
     * timout for the socket and creates the send and revieve packets
     *
     * @param cadenciaEnvio Value in seconds for repeating the Hello request
     */
    public void sendHelloRequest(final int cadenciaEnvio) {
        IPList.add("192.168.1.9"); // rede de testes (VM)
        IPList.add("192.168.1.2"); // rede de testes (VM)
        IPList.add("255.255.255.255"); //broadcast

        try {
            byte[] dataToSend = prepareRequest(CODE_HELLO);
            byte[] dataToReceive = new byte[300];

            DatagramSocket sock = new DatagramSocket();
            sock.setSoTimeout(1000 * TIMEOUT_HELLO_REQUEST);
            DatagramPacket udpPacket = new DatagramPacket(dataToSend, dataToSend.length, targetIP, 9999);
            DatagramPacket udpPacketR = new DatagramPacket(dataToReceive, dataToReceive.length, targetIP, 9999);

            HelloRequest r = new HelloRequest(cadenciaEnvio, sock,
                    udpPacket, udpPacketR, dataToSend, dataToReceive, IPList);

            new Thread(r).start();
        } catch (IOException ex) {
            System.out.println("\nError: " + ex);
        }
    }

    /**
     * Run method that creates the byte[] with Reset request to send.Set a
     * timout for the socket and creates the send and revieve packets
     *
     */
    public void sendResetRequest() {
        try {
            System.out.printf("\n Insert the IP adress to send RESET request\n");
            String ipAdress = in.nextLine();

            InetAddress resetIP = InetAddress.getByName(ipAdress);
            byte[] dataToSend = prepareRequest(CODE_RESET);
            byte[] dataToReceive = new byte[300];

            DatagramSocket sock = new DatagramSocket();
            sock.setSoTimeout(1000 * TIMEOUT_RESET_REQUEST);
            DatagramPacket udpPacket = new DatagramPacket(dataToSend, dataToSend.length, resetIP, 9999);
            DatagramPacket udpPacketR = new DatagramPacket(dataToReceive, dataToReceive.length, resetIP, 9999);

            resetRequest.runResetRequest(sock, udpPacket, udpPacketR, dataToSend,
                    dataToReceive, ipAdress);

        } catch (IOException ex) {
            System.out.println("\nError: " + ex);
        }
    }

    /**
     * Method that shows all available machines
     *
     * @param time alue in seconds for considering a machine unavailable
     */
    public void seeAllMachinesRequest(int time) {
        availableMachines.seeAllMachines(time);
    }

    /**
     * Method takes the byte[] sent by the machines and calls method to convert
     * to readable text. Add's the machine discovered
     *
     * @param receivedData
     * @param ip
     */
    public void processPacket(byte[] receivedData, String ip) {
        Message message = new Message();
        try {
            Machine myMachine = message.deserialized(receivedData, ip);
            if (myMachine == null) {
                return;
            }
            availableMachines.addMachine(myMachine);
        } catch (InterruptedException ex) {
            System.out.println("\nError: " + ex);
        }

    }

}

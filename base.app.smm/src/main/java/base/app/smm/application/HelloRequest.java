/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.smm.application;

import java.io.IOException;
import static java.lang.Thread.sleep;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Set;

/**
 *
 * @author joaomachado
 */
public class HelloRequest implements Runnable {

    private final int cadenciaEnvio;
    private final DatagramSocket sock;
    private final DatagramPacket udpPacket;
    private final DatagramPacket udpPacketR;
    private final byte[] dataToSend;
    private final byte[] dataToReceive;
    private final Set<String> IPList;

    private static final SendRequest server = new SendRequest();

    /**
     * Method that sends and recieves Hello UPD packets.Validates timeout and
     * displays the machines that were discovered
     *
     * @param cadenciaEnvio in seconds to repeat sending hellor request
     * @param sock DatagramSocket that contains the packets
     * @param udpPacket DatagramPacket that is send with the hello request
     * @param dataToSend byte[] with the message Hello
     * @param udpPacketR DatagramPacket that is recieved from machines
     * @param dataToReceive byte[] with the message reply
     * @param IPList List cointaing all IP's to send packets
     */
    public HelloRequest(final int cadenciaEnvio,final DatagramSocket sock, 
             DatagramPacket udpPacket, DatagramPacket udpPacketR,
            byte[] dataToSend, byte[] dataToReceive, Set<String> IPList) {
        this.cadenciaEnvio = cadenciaEnvio;
        this.sock = sock;
        this.udpPacket = udpPacket;
        this.udpPacketR = udpPacketR;
        this.dataToSend = dataToSend;
        this.dataToReceive = dataToReceive;
        this.IPList = IPList;

    }

    @Override
    public void run() {
        try {
            while (true) {
                for (String ipAdress : IPList) {

                    InetAddress targetIP = InetAddress.getByName(ipAdress);

                    udpPacket.setAddress(targetIP);
                    udpPacketR.setAddress(targetIP);

                    udpPacket.setData(dataToSend);
                    udpPacket.setLength(dataToSend.length);
                    sock.send(udpPacket);
                    udpPacketR.setData(dataToReceive);
                    udpPacketR.setLength(dataToReceive.length);
                    try {
                        sock.receive(udpPacketR);
                        String ip = udpPacketR.getAddress().getHostAddress();
                        server.processPacket(dataToReceive, ip);
                         System.out.println("\nA Machine was found in this network: " + ipAdress + "\n");
                    } catch (SocketTimeoutException ex) {
                       // System.out.println("\nNo reply from any machine in this network: " + ipAdress + "\n");
                    }
                }
                sleep(cadenciaEnvio * 1000);

            }
        } catch (UnknownHostException ex) {
            System.out.println("\nError: " + ex);
        } catch (IOException | InterruptedException ex) {
            System.out.println("\nError: " + ex);
        }
    }

}

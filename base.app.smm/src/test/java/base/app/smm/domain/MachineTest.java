/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.smm.domain;

import java.time.LocalDateTime;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertNotEquals;
import org.junit.Test;

/**
 *
 * @author joaomachado
 */
public class MachineTest {

    private static final short PROTOCOL_ID = 1;
    private static final String RAW_DATA = "Mensagem recebida";
    private static final String IP = "192.168.1.10";

    @Test
    public void ensureMachineWithProtocolIdRawDataAndIp() {
        Machine isntance = new Machine(PROTOCOL_ID, RAW_DATA, IP);
        assertTrue(true);
    }

    @Test
    public void TestGetMachineProtocolID() {
        System.out.println("getMachineProtocolID");
        Machine isntance = new Machine(PROTOCOL_ID, RAW_DATA, IP);
        short expResult = 1;
        short result = isntance.getMachineProtocolID();
        assertEquals(expResult, result);
    }

    @Test
    public void TestGetRawData() {
        System.out.println("getData");
        Machine isntance = new Machine(PROTOCOL_ID, RAW_DATA, IP);
        String expResult = "Mensagem recebida";
        String result = isntance.getData();
        assertEquals(expResult, result);
    }

    @Test
    public void TestGetTime() {
        System.out.println("getTime");
        LocalDateTime expResult;
        Machine isntance = new Machine(PROTOCOL_ID, RAW_DATA, IP);
        expResult = LocalDateTime.now();
        LocalDateTime result = isntance.getTime();
        assertTrue("Dates aren't close enough to each other!", (expResult.getSecond() - result.getSecond()) < 1);
    }

    @Test
    public void TestGetMachineIP() {
        System.out.println("getMachineIP");
        Machine isntance = new Machine(PROTOCOL_ID, RAW_DATA, IP);
        String expResult = "192.168.1.10";
        String result = isntance.getMachineIP();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class User.
     */
    @Test
    public void testSetEstado() {
        System.out.println("Test Set Estado");
        Machine instance = new Machine(PROTOCOL_ID, RAW_DATA, IP);
        String expResult = "Disponivel";
        instance.setEstado("Indisponivel");
        assertNotEquals(expResult,instance.getEstado());
    }
}

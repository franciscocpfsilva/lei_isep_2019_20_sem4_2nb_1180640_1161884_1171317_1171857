/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.smm.domain;

import base.app.smm.domain.Message;
import base.app.smm.domain.Machine;
import static base.app.smm.domain.Message.prepareRequest;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 *
 * @author joaomachado
 */
public class MessageTest {

    private static final char VERSION = 1;
    private static final char CODE = 0;
    private static final short ID = 0;
    private static final String RAW_DATA = "";
    private static final short DATA_LENGTH = (short) RAW_DATA.length();
    byte[] array = RAW_DATA.getBytes();

    @Test
    public void ensureMessageWithVersionCodeIdDataLengthAndRawData() {
        Message instance = new Message(VERSION, CODE, ID, DATA_LENGTH, RAW_DATA);
    }

    @Test
    public void ensureSerializedMessage() {
        byte[] arr = prepareRequest(CODE);
        Message instance = new Message(VERSION, CODE, ID, DATA_LENGTH, RAW_DATA);
        assertArrayEquals(instance.serialized(), arr);
    }

    @Test
    public void ensureDeserializeddMessageDoesNothingIfNACK() throws InterruptedException {
        char NACK = 151;
        byte[] arr = prepareRequest(NACK);

        Message message = new Message();
        Machine machine = message.deserialized(arr, "192.168.1.1");

        assertEquals(machine, null);
    }

    @Test
    public void ensureDeserializeddMessageUpdatesMachine() throws InterruptedException {
        char version = 1;
        char code = 150;
        short id = 10;
        String rawData = "";
        short dataLenght = (short) RAW_DATA.length();

        Message instance = new Message(version, code, id, dataLenght, rawData);
        byte[] arr = instance.serialized();

        Machine machine = instance.deserialized(arr, "192.168.1.1");
        Machine myMachine = new Machine(id, rawData, "192.168.1.1");
        
        assertEquals(machine, myMachine);
    }
}

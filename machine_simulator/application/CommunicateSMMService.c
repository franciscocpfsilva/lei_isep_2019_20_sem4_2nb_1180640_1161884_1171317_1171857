#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/socket.h>
#include <netdb.h>
#include "../communication/CommunicateWithSMM.h"
#include "../domain/MessageCode.h"
#include "../dto/MessageDTO.h"
#include "CommunicateSMMService.h"
#include "SendMessagesToCentralSystemService.h"
#include "../communication/CommunicateWithCentralSystem.h"
#include "../header.h"

/**
 * Function called by threads that waits for HELLO or RESET messages
 */
void CommunicateSMMService() {
	
	char cliIPtext[BUF_SIZE], cliPortText[BUF_SIZE], line[BUF_SIZE], code;
	int res;
	messageDto receiveMsg, sendMsg;
	
	result r = machine_udp_conection();
	
	int sock = r.sock;
	struct sockaddr_storage client;
	client = r.client;
	unsigned int adl = sizeof(client);
	
	while(1) {
	        recvfrom(sock,line,BUF_SIZE,0,(struct sockaddr *)&client,&adl);
		if(!getnameinfo((struct sockaddr *)&client,adl,
			cliIPtext,BUF_SIZE,cliPortText,BUF_SIZE,NI_NUMERICHOST|NI_NUMERICSERV)) 
			printf("Request from node %s, port number %s\n", cliIPtext, cliPortText);
		else puts("Got request, but failed to get client address");
		
		receiveMsg.msg = line;		
		code = extractCode(receiveMsg);
				
		switch(code){
			case (char)CODE_HELLO_REQUEST:
				pthread_mutex_lock(&mux);
				sendMsg = toDto(lastMsgFromScm);
				pthread_mutex_unlock(&mux);
				res = lastMsgFromScm.data_length + MIN_LENGTH;
				sendto(sock,sendMsg.msg,res,0,(struct sockaddr *)&client,adl);
				break;
			case (char)CODE_RESET_REQUEST:
				printf("Recebido pedido reset!\n");
                //Avisa as outras threads que tem de parar as suas atividades.
                pthread_mutex_lock(&mux);
				flag = 1;
				pthread_mutex_unlock(&mux);
				                
                //Envia mensagem HELLO para o SCMsendHelloRequestToScm()
                sendHelloRequestToScm();
                //Envia a resposta do SCM para o SMM
                pthread_mutex_lock(&mux);
				sendMsg = toDto(lastMsgFromScm);
				pthread_mutex_unlock(&mux);
				printf("A reeniciar...\n");
                sleep(20);
                res = lastMsgFromScm.data_length + MIN_LENGTH;
				sendto(sock,sendMsg.msg,res,0,(struct sockaddr *)&client,adl);
				//Avisa as outras threads que podem continuar as suas atividades.
                pthread_mutex_lock(&mux);
				flag = 0;
				pthread_mutex_unlock(&mux);
                printf("Maquina reeniciada com sucesso!\n");
                break;
		} 
	}
	
	pthread_exit(NULL);
}
#ifndef READ_FILE_CSV_H
#define READ_FILE_CSV_H

#include "../header.h"

#define FILE_NAME "./Mensagens.DD4"
#define MAX_SIZE_LINE 1024


/**
 * Global variable file pointer.
 */ 
FILE* fptr;

/**
 * Opens file and returns 1 if successful, saving the file pointer in the global variable fptr.
 * Otherwise returns 0.
 */ 
int openFile(char* file);

/**
 * Reads a message's raw data and returns pointer to chars with content read if successful.
 * Otherwise returns NULL.
 */ 
char* readMessageRawData();

/**
 * Checks if the file has ended. Returns non-zero value if has ended; 0 if not.
 */ 
int isEndOfFile();

/**
 * Closes file.
 */
void closeFile();

#endif
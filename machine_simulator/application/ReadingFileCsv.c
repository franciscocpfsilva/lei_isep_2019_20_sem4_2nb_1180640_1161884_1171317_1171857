#include "ReadingFileCsv.h"

/**
 * Opens file and returns 1 if successful, saving the file pointer in the global variable fptr.
 * Otherwise returns 0.
 */ 
int openFile(char* file) {
    if ((fptr = fopen(file, "r")) == NULL) {
        printf("Error! opening file\n");
        return 0;
    }
    return 1;
}

/**
 * Reads a message's raw data and returns pointer to chars with content read if successful.
 * Otherwise returns NULL.
 */ 
char* readMessageRawData() {
    size_t len = 0;
    ssize_t read;
    char* line;
    if ((read = getline(&line, &len, fptr)) == -1) {
        printf("Error while getting line.\n");
        return NULL;
    }
    
    // Processes line to take the '\n' character
    char* processedLine = malloc(read);
    int i;
    for (i = 0; i < read; i++) {
        if (*(line + i) != '\n') {
            *(processedLine + i) = *(line + i);
            if(i == read -1){
                *(processedLine + read) = '\0';
            }
        }else{
            *(processedLine + i) = '\0';
            break;
        }
    }
    
    return processedLine;
}

/**
 * Checks if the file has ended. Returns non-zero value if has ended; 0 if not.
 */ 
int isEndOfFile() {
    return feof(fptr);
}

/**
 * Closes file.
 */
void closeFile() {
    fclose(fptr);
}
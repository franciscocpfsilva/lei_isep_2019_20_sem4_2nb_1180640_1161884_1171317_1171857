#ifndef SEND_MESSAGES_TO_SCM_SERVICE_H
#define SEND_MESSAGES_TO_SCM_SERVICE_H

#define MAX_NUMBER_TRIES_TO_CONNECT 5

/**
 * Sends HELLO request to the Central System, expecting an ACK response to allow other requests in the future.
 * Returns 1 if successful; 0 if not.
 */ 
int sendHelloRequestToScm();

/**
 * Reads file with messages and tries to send each message to the Central System.
 */ 
void sendMessagesToScm(char* fileName);

#endif
#include "SendMessagesToCentralSystemService.h"
#include "ReadingFileCsv.h"
#include "../header.h"
#include "../domain/Message.h"
#include "../domain/MessageCode.h"
#include "../dto/MessageDTO.h"
#include "../communication/CommunicateWithCentralSystem.h"


int communicateWithScm(messageDto request);

/**
 * Sends HELLO request to the Central System, expecting an ACK response to allow other requests in the future.
 * Returns 1 if successful; 0 if not.
 */ 
int sendHelloRequestToScm() {
    // Creates message to send
    char empty[0] = "";
    char* rawData = empty;
    message* msgPtr = newMessage(
        (unsigned char) TCP_PROTOCOL_VERSION, 
        (unsigned char) CODE_HELLO_REQUEST, 
        machineId, 
        0,
        rawData
    );

    // Sends message 
    messageDto dto = toDto(*msgPtr);
    free(msgPtr);
    return communicateWithScm(dto);
}

/**
 * Reads file with messages and tries to send each message to the Central System.
 */ 
void sendMessagesToScm(char* fileName) {
    if (!openFile(fileName)) {
        printf("Problem opening file!\n");
        return;
    }
    int stop = 0;
    char* line;
    while (!isEndOfFile()) {
        do {
            pthread_mutex_lock(&mux);
			stop = flag;
			pthread_mutex_unlock(&mux);
        } while (stop);

        // Reads line of file (same as raw data of one message)
        line = readMessageRawData();
        
        if (line != NULL && strlen(line) > 0) {
            // Creates message
            message* msgPtr = newMessage(
                (unsigned char) TCP_PROTOCOL_VERSION, 
                (unsigned char) CODE_MSG_REQUEST, 
                machineId, 
                (unsigned short) strlen(line),
                line
            );

            if (msgPtr != NULL) {
                messageDto request = toDto(*msgPtr);
                free(msgPtr);

                communicateWithScm(request);
            }

            // Waits some seconds to send the next message
            sleep(secondsBetweenMessages);
        }
    }

    closeFile();
    printf("All messages sent.\n");
}

/**
 * Function to reuse the communication part with the Central System both
 * for HELLO requests and MSG requests. Treats the response received and 
 * tries to connect again if response is not ACK.
 */
int communicateWithScm(messageDto request) {
    unsigned int numTries = 0;
    unsigned char code = -1;
    do {
        // Sends message and gets response
        messageDto* resp = sendMessageToScmUsingTcp(request);
        if (resp != NULL) {
            message* respMsg = newMessage(
                extractVersion(*resp),
                extractCode(*resp),
                extractId(*resp),
                extractDataLength(*resp),
                extractRawData(*resp)
            );

            free(resp->msg);
            free(resp);
        
            // Saves and treats response
            if (respMsg != NULL) {
                pthread_mutex_lock(&mux);
                lastMsgFromScm = *respMsg;
                pthread_mutex_unlock(&mux);
                code = respMsg->code;

                if (numTries < (unsigned int) MAX_NUMBER_TRIES_TO_CONNECT && code != (unsigned char) CODE_ACK_RESPONSE) {
                    printf("Request has been refused and ignored by the server application.\nSTATUS: %s\n", respMsg->raw_data);
                }

                free(respMsg);
            } else {
                printf("Unknown response received from the server application.\n");
            }
        }
        
        numTries++;
        if (numTries < (unsigned int) MAX_NUMBER_TRIES_TO_CONNECT && code != (unsigned char) CODE_ACK_RESPONSE) {
            sleep(10);  // Try again in 10 seconds
        }
    } while (numTries < (unsigned int) MAX_NUMBER_TRIES_TO_CONNECT && code != (unsigned char) CODE_ACK_RESPONSE); // For treatment of errors

    if (code == (unsigned char) CODE_ACK_RESPONSE) {
        return 1;
    } else {
        return 0;
    }
}

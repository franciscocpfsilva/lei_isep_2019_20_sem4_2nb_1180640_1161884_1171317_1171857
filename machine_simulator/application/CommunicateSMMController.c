#include <stdio.h>
#include "CommunicateSMMController.h"
#include "CommunicateSMMService.h"

/**
 * Function called by threads that waits for HELLO or RESET messages
 * @args arguments, in this function we dont need them! 
 */
void* CommunicateSMMController(void* args) {
	
	CommunicateSMMService();
	return 0;
}

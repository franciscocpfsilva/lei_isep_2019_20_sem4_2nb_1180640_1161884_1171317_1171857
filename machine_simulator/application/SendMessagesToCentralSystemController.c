#include "SendMessagesToCentralSystemController.h"
#include "SendMessagesToCentralSystemService.h"

void simulateMachineSendingMessagesToScm(char* fileName) {
    if (sendHelloRequestToScm()) {
        sendMessagesToScm(fileName);
    }
}
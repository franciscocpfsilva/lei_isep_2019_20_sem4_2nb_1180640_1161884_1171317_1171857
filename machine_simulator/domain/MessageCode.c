#include "MessageCode.h"

int exists(unsigned char code) {
    if (code == CODE_HELLO_REQUEST
            || code == CODE_MSG_REQUEST
            || code == CODE_CONFIG_REQUEST
            || code == CODE_RESET_REQUEST
            || code == CODE_ACK_RESPONSE
            || code == CODE_NACK_RESPONSE) {

        return 1;
    }
    return 0;
}
#ifndef MESSAGE_CODE_H
#define MESSAGE_CODE_H

#define CODE_HELLO_REQUEST 0
#define CODE_MSG_REQUEST 1
#define CODE_CONFIG_REQUEST 2
#define CODE_RESET_REQUEST 3
#define CODE_ACK_RESPONSE 150
#define CODE_NACK_RESPONSE 151

int exists(unsigned char code);

#endif
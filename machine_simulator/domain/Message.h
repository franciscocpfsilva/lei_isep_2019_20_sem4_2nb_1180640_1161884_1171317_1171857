#ifndef MESSAGE_H
#define MESSAGE_H

#include "../dto/MessageDTO.h"

#define MIN_BYTES 6

typedef struct {
	unsigned char version;
	unsigned char code;
	unsigned short id;
	unsigned short data_length;
	char* raw_data;
} message;

#define SIZE_STRUCT_MESSAGE sizeof(message)

message* newMessage(unsigned char version, unsigned char code, unsigned short id, unsigned short data_length, char* raw_data);
message* newMessageWithoutRawData(unsigned char version, unsigned char code, unsigned short id);

messageDto toDto(message msg);

void toString(message msg);

#endif
#include "Message.h"
#include "../header.h"
#include "../dto/MessageDTO.h"
#include "MessageCode.h"

message* newMessage(unsigned char version, unsigned char code, unsigned short id, unsigned short data_length, char* raw_data) {
    if (!exists(code)) {
        return NULL;
    }
    
    message* msg = malloc(SIZE_STRUCT_MESSAGE);
    msg->version = version;
    msg->code = code;
    msg->id = id;
    msg->data_length = data_length;
    msg->raw_data = raw_data;

    return msg;
}

message* newMessageWithoutRawData(unsigned char version, unsigned char code, unsigned short id) {
    char* ptr = malloc(0);
    
    return newMessage(
        version,
        code,
        id,
        (unsigned short) 0,
        ptr
    );
}

/**
 * Method to transform a message into a DTO with a binary char array 
 *  @message struct type message
 */
messageDto toDto(message msg) {
    return create(msg.version, msg.code, msg.id, msg.data_length, msg.raw_data);
}

/**
 * Method that prints the message information
 *  @msg struct type message
 */
void toString(message msg){
	if (msg.data_length != 0) { 
        printf("VERSION: %d\nCODE: %d\nID: %d\nDATA LENGTH: %d\nRAW DATA: %s\n\n",
                msg.version, msg.code, msg.id, msg.data_length, msg.raw_data);
    } else {
        printf("VERSION: %d\nCODE: %d\nID: %d\nDATA LENGTH: %d\nRAW DATA:\n\n",
                msg.version, msg.code, msg.id, msg.data_length);
    }

}
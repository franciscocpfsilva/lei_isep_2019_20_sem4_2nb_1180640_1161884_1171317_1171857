#ifndef HEADER_H
#define HEADER_H

// Common 'includes' used throughout the application
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include "domain/Message.h"

// Constants
#define TCP_PROTOCOL_VERSION 6
#define UDP_VERSION_CONTROL 17

// Global variables
/**
 * ID of the machine working in the simulator
 */ 
unsigned short machineId;
/**
 * Time (in seconds) to wait from sending one message to send another
 */ 
unsigned int secondsBetweenMessages;
/**
 * Server's IP address for Central System
 */ 
char* serverIpAddress;
/**
 * Last message received from the Central System. Important for the communication with the Monitoring System.
 */ 
message lastMsgFromScm;
/**
 * Controls the access to the variable lastMsgFromScm
 */ 
pthread_mutex_t mux;
/**
 * Control variable, allows to pause the other threads when a reset request is requested
 */ 
int flag;

#endif
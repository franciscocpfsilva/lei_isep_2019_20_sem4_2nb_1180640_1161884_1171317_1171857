#include "../header.h"
#include "MachineAlgorithm.h"
#include "../application/SendMessagesToCentralSystemController.h"
#include "../application/CommunicateSMMController.h"
#include "../domain/MessageCode.h"
#include "../server.application/ReceiveTcpRequestController.h"

void* func_thread_sendMessagesToScm(void* arg);
void* func_thread_receiveTcpRequests(void* arg);

void simulateMachineWorking(char* fileName) {
	pthread_t thread_messages_to_scm, udp_thread, tcp_server_thread;

	// Initializes mutex
	pthread_mutex_init(&mux, NULL);

	// Creates necessary threads
	pthread_create(&thread_messages_to_scm, NULL, func_thread_sendMessagesToScm, (void*) fileName);
	pthread_create(&udp_thread, NULL, CommunicateSMMController, NULL);
	pthread_create(&tcp_server_thread, NULL, func_thread_receiveTcpRequests, NULL);

	// Waits for each thread
	pthread_join(thread_messages_to_scm, NULL);
	pthread_join(udp_thread, NULL);

	// Destroys mutex
	pthread_mutex_destroy(&mux);
}

void* func_thread_sendMessagesToScm(void* arg) {
	char* fileName = (char*) arg;
	simulateMachineSendingMessagesToScm(fileName);
	pthread_exit(NULL);
}

void* func_thread_receiveTcpRequests(void* arg) {
	startTcpServer();
	pthread_exit(NULL);
}



#include "../header.h"
#include "MachineAlgorithm.h"

#define NUM_ARGS_NEEDED 5

int main(int argc, char **argv) {

    // Verifies number of arguments
    if (argc != NUM_ARGS_NEEDED) {
        printf("ERROR: Invalid number of arguments!\n");
        printf("Run using: ./MainClass {MACHINE_ID} {TIME_BETWEEN_MESSAGES} {FILE_NAME} {SERVER_IP_ADDRESS}\n");
        exit(EXIT_FAILURE);
    }

    // Gives meaning to the arguments received
    sscanf(argv[1], "%hu", &machineId);
    sscanf(argv[2], "%u", &secondsBetweenMessages);
    char* fileName = malloc(strlen(argv[3]));
    strcpy(fileName, argv[3]);
    serverIpAddress = malloc(strlen(argv[4]));
    strcpy(serverIpAddress, argv[4]);

    // Calls machine simulation algorithm
    simulateMachineWorking(fileName);

    return 0;
}
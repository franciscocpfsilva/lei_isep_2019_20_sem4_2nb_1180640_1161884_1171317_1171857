#include "../header.h"
#include "CommunicateWithCentralSystem.h"
#include "../dto/MessageDTO.h"

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#define BUF_SIZE 30
#define SERVER_PORT "31401"
#define MIN_SIZE_RESPONSE 6

// read a string from stdin protecting buffer overflow
#define GETS(B,S) {fgets(B,S-2,stdin);B[strlen(B)-1]=0;}

/**
 * Creates a TCP connection with the server application and 
 * sends a request, returning its response
 */
messageDto* sendMessageToScmUsingTcp(messageDto request) {
	int err, sock;
	unsigned int i;
	struct addrinfo  req, *list;
   
	bzero((char *) &req, sizeof(req));
	// let getaddrinfo set the family depending on the supplied server address
	req.ai_family = AF_UNSPEC;
	req.ai_socktype = SOCK_STREAM;
	err = getaddrinfo(serverIpAddress, SERVER_PORT, &req, &list);
	if (err) {
        printf("Failed to get server address, error: %s\n", gai_strerror(err));
        return NULL; 
    }

	sock = socket(list->ai_family, list->ai_socktype, list->ai_protocol);
	if (sock == -1) {
        perror("Failed to open socket"); 
        freeaddrinfo(list); 
        return NULL;
    }

    // Tries to make the connection with the TCP server
	if ( connect(sock, (struct sockaddr*) list->ai_addr, list->ai_addrlen) == -1) {
        	perror("Failed connect"); 
            freeaddrinfo(list); 
            close(sock); 
            return NULL;
    }
	
    // Send request
    for (i = 0; i < request.sizeMsg; i++) {
        write(sock, (request.msg + i), 1); 
    }

    // Read response
    messageDto* resp = malloc(sizeof(messageDto));
    char* temp = malloc(MIN_SIZE_RESPONSE);
    unsigned int sizeMsg = MIN_SIZE_RESPONSE;
    for (i = 0; i < sizeMsg; i++) {
        read(sock, (temp + i), 1);
        if (i == MIN_SIZE_RESPONSE - 1) {
            unsigned short data_length;
	        memcpy(&data_length, temp + OFFSET_DATALENGTH, sizeof(short));
            sizeMsg += data_length;
            temp = realloc(temp, sizeMsg);
        }
    }
    *resp = createFromText(temp, sizeMsg);
    free(temp);

	close(sock);
	return resp;
}
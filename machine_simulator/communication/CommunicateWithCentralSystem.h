#ifndef COMMUNICATION_SCM_H
#define COMMUNICATION_SCM_H

#include "../dto/MessageDTO.h"

/**
 * Creates a TCP connection with the server application and 
 * sends a request, returning its response
 */
messageDto* sendMessageToScmUsingTcp(messageDto request);

#endif
#ifndef TEST_MESSAGE_H
#define TEST_MESSAGE_H

#define BASE_VERSION 0
#define BASE_CODE 0
#define BASE_ID 0
#define BASE_DATA_LENGTH 0

void verifyNewMessage();
void verifyVersionCanBe255();
void verifyCodeExists();

#endif
#include "../../header.h"
#include "MessageTest.h"
#include "../../domain/Message.h"

void verifyNewMessage() {
    printf("testingNewMessage\n");
    char temp = 0;
    char* charPtr = &temp;
    message* testMsg = newMessage(BASE_VERSION, BASE_CODE, BASE_ID, BASE_DATA_LENGTH, charPtr);
    int resultInvalid = (testMsg == NULL);
    free(testMsg);
    
    if (resultInvalid) {
        printf("FAILED\n");
        exit(EXIT_FAILURE);
    }
}

void verifyVersionCanBe255() {
    printf("testingVersionCanBe255\n");
    char temp = 0;
    char* charPtr = &temp;
    message* testMsg = newMessage(255, BASE_CODE, BASE_ID, BASE_DATA_LENGTH, charPtr);
    int resultInvalid = (testMsg == NULL);
    free(testMsg);
    
    if (resultInvalid) {
        printf("FAILED\n");
        exit(EXIT_FAILURE);
    }
}

void verifyCodeExists() {
    printf("testingCodeExists\n");
    char temp = 0;
    char* charPtr = &temp;
    message* testMsg = newMessage(BASE_VERSION, 199, BASE_ID, BASE_DATA_LENGTH, charPtr);
    int resultInvalid = (testMsg != NULL);
    free(testMsg);
    
    if (resultInvalid) {
        printf("FAILED\n");
        exit(EXIT_FAILURE);
    }
}

int main(void){
    
    printf("##################### RUN ALL TESTS #####################\n\n");
    // Run tests
    verifyNewMessage();
    verifyVersionCanBe255();
    verifyCodeExists();
    
    return 0;
}
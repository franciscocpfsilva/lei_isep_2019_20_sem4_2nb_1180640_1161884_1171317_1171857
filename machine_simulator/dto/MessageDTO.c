#include "../header.h"
#include "MessageDTO.h"

/**
 * Method to transform a message into a DTO with a binary char array 
 *  @message struct type message
 */
messageDto create(unsigned char version, unsigned char code, unsigned short id, unsigned short data_length, char* raw_data) {
	
	char *buf = malloc(data_length + MIN_LENGTH);
    
    memcpy(buf + OFFSET_VERSION, &version, sizeof(char)); //Escreve a VERSION
    memcpy(buf + OFFSET_CODE, &code, sizeof(char)); //Escreve o CODE
    memcpy(buf + OFFSET_ID, &id, sizeof(short)); // Escreve o ID
    memcpy(buf + OFFSET_DATALENGTH, &data_length, sizeof(short)); // Escreve o DATA_LENGTH
    memcpy(buf + MIN_LENGTH, raw_data, data_length * sizeof(char)); // Escreve o RAW_DATA
    
    messageDto m;
    m.msg = buf;
	m.sizeMsg = data_length + MIN_LENGTH;
    
	return m;
}

/**
 * Creates a MessageDTO from the passed message and its size
 */ 
messageDto createFromText(char* text, unsigned int size) {
	messageDto m;
	m.msg = malloc(size);
	memcpy(m.msg, text, size);
	m.sizeMsg = size;
	return m;
}

/**
 * Method that extracts the version of the message dto
 * @dto struct type messageDto
 */	
unsigned char extractVersion(messageDto dto) {
	unsigned char version;
	memcpy(&version, dto.msg + OFFSET_VERSION, sizeof(char));
	
	return version;
}

/**
 * Method that extracts the code of the message dto
 * @dto struct type messageDto
 */	
unsigned char extractCode(messageDto dto) {
	unsigned char code;
	memcpy(&code, dto.msg + OFFSET_CODE, sizeof(char));
	
	return code;
}

/**
 * Method that extracts the id of the message dto
 * @dto struct type messageDto
 */	
unsigned short extractId(messageDto dto) {
	unsigned short id;
	memcpy(&id, dto.msg + OFFSET_ID, sizeof(short));
	
	return id;	
}

/**
 * Method that extracts the data length of the message dto
 * @dto struct type messageDto
 */	
unsigned short extractDataLength(messageDto dto) {
	unsigned short data_length;
	memcpy(&data_length, dto.msg + OFFSET_DATALENGTH, sizeof(short));
	
	return data_length;
}

/**
 * Method that extracts the raw data of the message dto
 * @dto struct type messageDto
 */	
char* extractRawData(messageDto dto) {
	char* raw_data;
	short data_length = extractDataLength(dto);
	raw_data = malloc(data_length);
    memcpy(raw_data, dto.msg + MIN_LENGTH, data_length);
    
    return raw_data;
}

#ifndef MESSAGE_DTO_H
#define MESSAGE_DTO_H

#define OFFSET_VERSION 0
#define OFFSET_CODE 1
#define OFFSET_ID 2
#define OFFSET_DATALENGTH 4
#define OFFSET_RAWDATA 6
#define MIN_LENGTH 6

typedef struct {
	char* msg;
    unsigned int sizeMsg;
} messageDto;

messageDto create(unsigned char version, unsigned char code, unsigned short id, unsigned short data_length, char* raw_data);
messageDto createFromText(char* text, unsigned int size);

unsigned char extractVersion(messageDto dto);
unsigned char extractCode(messageDto dto);
unsigned short extractId(messageDto dto);
unsigned short extractDataLength(messageDto dto);
char* extractRawData(messageDto dto);

#endif
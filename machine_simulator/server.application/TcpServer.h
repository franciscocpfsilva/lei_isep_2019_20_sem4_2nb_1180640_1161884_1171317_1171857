#ifndef TCP_SERVER_H
#define TCP_SERVER_H

/**
 * Runs the TCP server -> listening for requests (ex: CONFIG requests).
 */ 
void runTcpServer();

#endif
#ifndef RECEIVE_TCP_REQUEST_CONTROLLER_H
#define RECEIVE_TCP_REQUEST_CONTROLLER_H

/**
 * Calls the server to make it run and start receiving requests.
 */ 
void startTcpServer();

#endif
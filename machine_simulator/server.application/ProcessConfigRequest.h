#ifndef PROCESS_CONFIG_REQUEST_SERVICE_H
#define PROCESS_CONFIG_REQUEST_SERVICE_H

#include "../domain/Message.h"

/**
 * Processes a CONFIG request, returning the response to send back.
 */ 
message processConfigRequest(message req);

#endif
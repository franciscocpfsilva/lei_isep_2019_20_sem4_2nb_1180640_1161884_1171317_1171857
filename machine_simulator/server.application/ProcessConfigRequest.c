#include "ProcessConfigRequest.h"
#include "../header.h"
#include "../domain/Message.h"
#include "../domain/MessageCode.h"
#include "WriteConfigFileTxt.h"

message processConfigRequest(message req) {
    // Validates ID in the request and returns NACK response if invalid
    if (req.id != machineId) {
        message *resp = newMessageWithoutRawData(
            (unsigned char) TCP_PROTOCOL_VERSION, 
            (unsigned char) CODE_NACK_RESPONSE, 
            machineId
        );
        return *resp;
    }

    // Write config in file
    writeConfigFileTxt(req.raw_data);

    // Prepares ACK response
    message *resp = newMessageWithoutRawData(
        (unsigned char) TCP_PROTOCOL_VERSION, 
        (unsigned char) CODE_ACK_RESPONSE, 
        machineId
    );

    return *resp;
}
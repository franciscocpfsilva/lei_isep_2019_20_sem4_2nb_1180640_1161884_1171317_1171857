#include "../header.h"
#include "ProcessRequestService.h"
#include "../dto/MessageDTO.h"
#include "../domain/Message.h"
#include "../domain/MessageCode.h"
#include "ProcessConfigRequest.h"

messageDto processTcpRequest(messageDto req) {
    
    // Creates message from dto
    message *msgReq = newMessage(
        extractVersion(req), 
        extractCode(req), 
        extractId(req),
        extractDataLength(req),
        extractRawData(req)
    );

    // Processes request and returns a response
    message msgResp;
    switch (msgReq->code) {
        case CODE_CONFIG_REQUEST:
            msgResp = processConfigRequest(*msgReq);
            break;
        default:
            msgResp = *newMessageWithoutRawData(
                (unsigned char) TCP_PROTOCOL_VERSION, 
                (unsigned char) CODE_NACK_RESPONSE, 
                machineId
            );
    }

    return toDto(msgResp);
}
#ifndef PROCESS_REQUEST_SERVICE_H
#define PROCESS_REQUEST_SERVICE_H

#include "../dto/MessageDTO.h"

/**
 * Processes a TCP request, returning the response to send back.
 */ 
messageDto processTcpRequest(messageDto req);

#endif
#include "../header.h"
#include "TcpServer.h"
#include "../dto/MessageDTO.h"
#include "ProcessRequestService.h"

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#define BUF_SIZE 300
#define SERVER_PORT "9999"
#define MIN_SIZE_RESPONSE 6

void runTcpServer() {
    struct sockaddr_storage from;
	int err, newSock, sock;
	unsigned int adl;
	unsigned long i;
	char cliIPtext[BUF_SIZE], cliPortText[BUF_SIZE];
	struct addrinfo  req, *list;

	bzero((char *)&req,sizeof(req));
	// requesting a IPv6 local address will allow both IPv4 and IPv6 clients to use it
	req.ai_family = AF_INET6;
	req.ai_socktype = SOCK_STREAM;
	req.ai_flags = AI_PASSIVE;      // local address

	err=getaddrinfo(NULL, SERVER_PORT , &req, &list);

	if (err) {
        printf("Failed to get local address, error: %s\n",gai_strerror(err)); 
        return; 
    }

	sock=socket(list->ai_family,list->ai_socktype,list->ai_protocol);
	if (sock==-1) {
        perror("Failed to open socket"); 
        freeaddrinfo(list); 
        return;
    }

	if (bind(sock,(struct sockaddr *)list->ai_addr, list->ai_addrlen) == -1) {
        perror("Bind failed");
        close(sock);
        freeaddrinfo(list);
        return;
    }

	freeaddrinfo(list);

	listen(sock,SOMAXCONN);

	puts("Accepting TCP connections (both over IPv6 or IPv4). Use CTRL+C to terminate the server");

	adl=sizeof(from);
	for (;;) {
        newSock=accept(sock,(struct sockaddr *)&from,&adl);
        
        int stop = 0;
        do {
            pthread_mutex_lock(&mux);
			stop = flag;
			pthread_mutex_unlock(&mux);
       } while (stop);

        if (!fork()) {
            close(sock);
            getnameinfo((struct sockaddr *) &from, adl, cliIPtext, BUF_SIZE, cliPortText, BUF_SIZE, NI_NUMERICHOST | NI_NUMERICSERV);
            printf("New connection from node %s, port number %s\n", cliIPtext, cliPortText);

            // Read request
            char* temp = malloc(MIN_LENGTH);
            unsigned int sizeMsg = MIN_LENGTH;
            for (i = 0; i < sizeMsg; i++) {
                read(newSock, (temp + i), 1);
                if (i == MIN_LENGTH - 1) {
                    unsigned short data_length;
                    memcpy(&data_length, temp + OFFSET_DATALENGTH, sizeof(short));
                    sizeMsg += (unsigned int) data_length;
                    temp = realloc(temp, sizeMsg);
                }
            }
            messageDto request = createFromText(temp, sizeMsg);
            free(temp);

            // Process request and prepare response
            messageDto resp = processTcpRequest(request);

            // Send response
            for (i = 0; i < resp.sizeMsg; i++) {
                write(newSock, (resp.msg + i), 1); 
            }
            
            close(newSock);
            printf("Connection from node %s, port number %s closed\n", cliIPtext, cliPortText);
            exit(0);
        }
        close(newSock);
    }
	close(sock);
}
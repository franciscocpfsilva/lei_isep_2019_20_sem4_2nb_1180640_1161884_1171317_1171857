#include "WriteConfigFileTxt.h"
#include "../header.h"

void writeConfigFileTxt(char* content) {
    FILE* fptr;

    // Defining name of file to write in
    char fileName[MAX_SIZE_FILE_NAME];
    snprintf(fileName, MAX_SIZE_FILE_NAME, "%s%hu%s", FILE_BASE_PATH_NAME, machineId, ".txt");

    // Opens file
    if ((fptr = fopen(fileName, "w")) == NULL) {
        printf("Error! opening file\n");
        return;
    }

    // Writes content in file
    fprintf(fptr, "CONFIGURATION FILE FOR MACHINE WITH ID %hu\n\n%s", machineId, content);

    // Closes file
    fclose(fptr);
}
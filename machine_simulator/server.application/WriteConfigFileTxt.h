#ifndef WRITE_CONFIG_FILE_TXT_H
#define WRITE_CONFIG_FILE_TXT_H

#include "../domain/Message.h"

#define FILE_BASE_PATH_NAME "../config_files/config_"
#define MAX_SIZE_FILE_NAME 100

/**
 * Writes the content passed in a text file.
 */ 
void writeConfigFileTxt(char* content);

#endif
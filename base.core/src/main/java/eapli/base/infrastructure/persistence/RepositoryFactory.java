/**
 *
 */
package eapli.base.infrastructure.persistence;

import eapli.base.clientusermanagement.repositories.ClientUserRepository;

import eapli.base.clientusermanagement.repositories.SignupRequestRepository;
import eapli.base.errormessage.repositories.ErrorMessagesRepository;
import eapli.base.communication.scm.repositories.CommunicationProtocolRepository;
import eapli.base.communication.scm.repositories.RequestConfigurationRepository;
import eapli.base.products.repositories.ProductsRepository;
import eapli.base.machinery.repositories.MachinesRepository;
import eapli.base.messageprocessing.repositories.ProductionLineProcessingStateRepository;
import eapli.base.productionline.repositories.ProductionLinesRepository;
import eapli.base.rawmaterials.repositories.RawMaterialCategoryRepository;
import eapli.base.storage.repositories.StorageUnitsRepository;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.authz.domain.repositories.UserRepository;
import eapli.base.rawmaterials.repositories.RawMaterialRepository;
import eapli.base.rawmessage.repositories.RawMessagesRepository;
import eapli.base.productionorder.repositories.ProductionOrderRepository;
import eapli.base.productionorderexecution.repositories.ProductionOrderExecutionRepository;

/**
 * @author Paulo Gandra Sousa
 *
 */
public interface RepositoryFactory {

    /**
     * factory method to create a transactional context to use in the
     * repositories
     *
     * @return
     */
    TransactionalContext newTransactionalContext();

    /**
     *
     * @param autoTx the transactional context to enrol
     * @return
     */
    UserRepository users(TransactionalContext autoTx);

    /**
     * repository will be created in auto transaction mode
     *
     * @return
     */
    UserRepository users();

    /**
     *
     * @param autoTx the transactional context to enroll
     * @return
     */
    ClientUserRepository clientUsers(TransactionalContext autoTx);

    /**
     * repository will be created in auto transaction mode
     *
     * @return
     */
    ClientUserRepository clientUsers();

    /**
     *
     * @param autoTx the transactional context to enroll
     * @return
     */
    SignupRequestRepository signupRequests(TransactionalContext autoTx);

    /**
     * repository will be created in auto transaction mode
     *
     * @return
     */
    SignupRequestRepository signupRequests();

    /**
     * repository will be created in auto transaction mode
     *
     * @return
     */
    ProductsRepository products();

    StorageUnitsRepository storageUnits();

    MachinesRepository machines();

    RawMaterialCategoryRepository rawMaterialCategories();

    RawMaterialRepository rawMaterial();

    ProductionLinesRepository productionLines();

    ProductionOrderRepository productionOrder();
    
    RawMessagesRepository rawMessages();
    
    ErrorMessagesRepository errorMessages();
    
    ProductionOrderExecutionRepository productionOrderExecution();

    CommunicationProtocolRepository communicationProtocols();
    
    RequestConfigurationRepository requestsConfiguration();
    
    ProductionLineProcessingStateRepository processingState();

}

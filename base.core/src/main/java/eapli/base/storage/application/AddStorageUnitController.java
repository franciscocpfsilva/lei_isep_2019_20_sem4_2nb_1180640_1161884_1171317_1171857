    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.storage.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.storage.domain.StorageUnit;
import eapli.base.storage.domain.StorageUnitID;
import eapli.base.storage.repositories.StorageUnitsRepository;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.application.UseCaseController;
import eapli.framework.general.domain.model.Description;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

/**
 * Controller for US3003 - Add Storage Unit.
 * 
 * @author
 */
@UseCaseController
public class AddStorageUnitController {
    
    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final StorageUnitsRepository storageUnitRepository = PersistenceContext.repositories().storageUnits();
    
    public StorageUnit addStorageUnit(final String identifier, final String description) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER, BaseRoles.SHOP_FLOOR_MANAGER);

        final StorageUnit newStorageUnit = new StorageUnit(StorageUnitID.valueOf(identifier), 
                Description.valueOf(description));
        
        return storageUnitRepository.save(newStorageUnit);
    }
    
}

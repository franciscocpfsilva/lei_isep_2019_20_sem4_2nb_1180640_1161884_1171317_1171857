/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.storage.repositories;

import eapli.base.storage.domain.StorageUnit;
import eapli.base.storage.domain.StorageUnitID;
import eapli.framework.domain.repositories.DomainRepository;

/**
 *
 * @author
 */
public interface StorageUnitsRepository extends DomainRepository<StorageUnitID, StorageUnit> {
    // empty
}

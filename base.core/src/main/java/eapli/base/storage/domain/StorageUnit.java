/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.storage.domain;

import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.DomainEntities;
import eapli.framework.general.domain.model.Description;
import eapli.framework.validations.Preconditions;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;

/**
 * Represents a storage unit.
 * 
 * Business rules:
 * - identifier and description can't be null or empty
 * - description can't have more than 40 characters
 * 
 * @author
 */
@Entity
public class StorageUnit implements AggregateRoot<StorageUnitID> {
    
    private static final long serialVersionUID = 1L;
    private static final int MAX_NUMBER_OF_CHARACTERS_FOR_DESCRIPTION = 40;
    
    @Id
    @GeneratedValue // (strategy = GenerationType.IDENTITY)
    private Long pk;
    @Version
    private Long version;
    
    // business id
    @AttributeOverride(name = "identifier", column = @Column(unique = true, nullable = false))
    private StorageUnitID identifier;
    private Description description;

    /**
     * StorageUnit constructor.
     *
     * @param identifier
     *            Mandatory
     * @param description
     *            Mandatory
     */
    public StorageUnit(final StorageUnitID identifier, final Description description) {
        Preconditions.noneNull(identifier, description);
        Preconditions.ensure(description.length() <= MAX_NUMBER_OF_CHARACTERS_FOR_DESCRIPTION, 
                "Description can't have more than 40 characters");
        
        this.identifier = identifier;
        this.description = description;
    }
    
    protected StorageUnit() {
        // for ORM
    }
    
    @Override
    public boolean equals(final Object o) {
        return DomainEntities.areEqual(this, o);
    }

    @Override
    public int hashCode() {
        return DomainEntities.hashCode(this);
    }

    @Override
    public boolean sameAs(final Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof StorageUnit)) {
            return false;
        }

        final StorageUnit that = (StorageUnit) other;
        return this.equals(that) && description.equals(that.description);
    }

    @Override
    public StorageUnitID identity() {
        return StorageUnitID.valueOf(identifier.identifier());
    }
    
    public Description description() {
        return Description.valueOf(description.toString());
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.storage.domain;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.util.HashCoder;
import eapli.framework.validations.Preconditions;
import eapli.framework.strings.util.StringPredicates;
import java.util.regex.Pattern;
import javax.persistence.Embeddable;

/**
 * Represents an identifier for a storage unit.
 * 
 * Business rules:
 * - can't be null
 * - can't be empty
 * - has a maximum of 10 characters
 * - is aplhanumeric (therefore can't have characters other than digits and 
 * capital or small letters)
 * 
 * @author
 */
@Embeddable
public class StorageUnitID implements ValueObject, Comparable<StorageUnitID> {
    
    private static final long serialVersionUID = 1L;
    private static final int MAX_NUMBER_OF_CHARACTERS = 10;
    private static final Pattern IS_ALPHANUMERIC_REGEX = Pattern.compile("^[0-9a-zA-Z]+$");
    
    private final String identifier;

    /**
     * StorageUnitID constructor.
     *
     * @param identifier
     *            Mandatory
     */
    public StorageUnitID(final String identifier) {
        Preconditions.nonEmpty(identifier, "Identifier can't be null nor empty");
        Preconditions.ensure(identifier.length() <= MAX_NUMBER_OF_CHARACTERS, 
                "Identifier can't have more than 10 characters");
        Preconditions.ensure(StringPredicates.isSingleWord(identifier), 
                "Identifier can't have more than 1 word");
        Preconditions.matches(IS_ALPHANUMERIC_REGEX, identifier, 
                "Identifier can only have digits and letters");
        
        this.identifier = identifier;
    }
    
    protected StorageUnitID() {
        // for ORM
        identifier = null;
    }
    
    /**
     * Factory method for obtaining StorageUnitID value objects.
     *
     * @param identifier
     * @return a new object corresponding to the value
     */
    public static StorageUnitID valueOf(final String identifier) {
        return new StorageUnitID(identifier);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof StorageUnitID)) {
            return false;
        }

        final StorageUnitID that = (StorageUnitID) o;
        return identifier.equals(that.identifier());
    }

    @Override
    public int hashCode() {
        return new HashCoder().with(identifier).code();
    }

   @Override
    public String toString() {
        return this.identifier;
    }

    public String identifier() {
        return this.identifier;
    }
    
    @Override
    public int compareTo(final StorageUnitID o) {
        return identifier.compareTo(o.identifier);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.productionorder.repositories;

import eapli.base.productionorder.domain.ProductionOrder;
import eapli.base.productionorder.domain.ProductionOrderID;
import eapli.framework.domain.repositories.DomainRepository;

/**
 *
 * @author joaomachado
 */
public interface ProductionOrderRepository extends DomainRepository<ProductionOrderID, ProductionOrder> {
    
}
 
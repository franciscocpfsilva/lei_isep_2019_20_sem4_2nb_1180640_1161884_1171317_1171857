/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.productionorder.domain;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.util.HashCoder;
import eapli.framework.validations.Preconditions;
import eapli.framework.strings.util.StringPredicates;
import java.util.regex.Pattern;
import javax.persistence.Column;

/**
 *
 * @author joaomachado
 */
public class OrderID implements ValueObject, Comparable<OrderID> {

    private static final int MAX_NUMBER_OF_CHARACTERS = 10;
    private static final Pattern IS_ALPHANUMERIC_REGEX = Pattern.compile("^[0-9a-zA-Z]+$");

    @Column(unique = true, nullable = false)
    private final String orderID;

    /**
     * Order constructor.
     *
     * @param orderID Mandatory id of Order
     */
    protected OrderID(final String orderID) {
        Preconditions.nonEmpty(orderID, "Order ID can't be null nor empty");
        Preconditions.ensure(orderID.length() <= MAX_NUMBER_OF_CHARACTERS,
                "Order ID can't have more than 10 characters");
        Preconditions.ensure(StringPredicates.isSingleWord(orderID),
                "Order ID can't have more than 1 word");
        Preconditions.matches(IS_ALPHANUMERIC_REGEX, orderID,
                "Order ID  can only have digits and letters");

        this.orderID = orderID;
    }

    /**
     * Order protected constructor for ORM.
     *
     */
    protected OrderID() {

        orderID = null;
    }

    /**
     * Factory method for obtaining Order value objects.
     *
     * @param orderID
     * @return a new object corresponding to the value
     */
    public static OrderID valueOf(final String orderID) {
        return new OrderID(orderID);
    }

    /**
     * Method that verifies if one Order is equal to another.
     *
     * @param o instance of other object
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OrderID)) {
            return false;
        }

        final OrderID that = (OrderID) o;
        return orderID.equalsIgnoreCase(that.orderID());
    }

    /**
     * Method that verifies returns hashcode of Order
     *
     */
    @Override
    public int hashCode() {
        return new HashCoder().with(orderID).code();
    }

    /**
     * Method that returns Order ID
     *
     * @return Order ID in strong
     */
    public String orderID() {
        return this.orderID;
    }

    /**
     * Method that verifies if one Order is equal to another.
     *
     * @param o instance of other Order
     */
    @Override
    public int compareTo(final OrderID o) {
        return orderID.compareTo(o.orderID);
    }

}

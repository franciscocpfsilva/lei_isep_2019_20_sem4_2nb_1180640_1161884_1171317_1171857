/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.productionorder.domain;

import eapli.base.domain.model.general.Quantity;
import eapli.base.products.domain.ProductID;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.DomainEntities;
import eapli.framework.validations.Preconditions;
import java.time.LocalDate;
import java.util.LinkedList;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Lob;

import javax.persistence.Version;

/**
 *
 * @author joaomachado
 */
@Entity
public class ProductionOrder implements AggregateRoot<ProductionOrderID> {

    @Version
    private Long version;
    private static final long serialVersionUID = 1L;

    @Column(unique = true, nullable = false)
    @EmbeddedId
    private ProductionOrderID productionOrderID;

    private LocalDate dateOfIssue;

    private LocalDate expectedExecutionDate;

    @AttributeOverride(name = "ID", column = @Column(name = "Product_ID"))
    private ProductID productId;

    @AttributeOverride(name = "ProductionOrderState", column = @Column(name = "Production_Order_State"))
    @Enumerated(EnumType.STRING)
    private ProductionOrderState productionOrderState;

    @Lob
    private LinkedList<OrderID> orderId = new LinkedList<>();
    @Lob
    private final Quantity quantity;

    /**
     * ExecutionOrder constructor.
     *
     * @param productionOrderID Id of Execution Order Mandatory
     * @param dateOfIssue Date of Issue of Execution Order Mandatory
     * @param expectedExecutionDate Expected Exeturion date of Execution Order
     * Mandatory
     * @param productId Product ID that Execution Order refers to Mandatory
     * @param orderId List of Id Orders the Execution Order refers to Mandatory
     * @param quantity Quantity to produce for Execution Order
     */
    public ProductionOrder(final ProductionOrderID productionOrderID, final LocalDate dateOfIssue,
            final LocalDate expectedExecutionDate, final ProductID productId, final LinkedList<OrderID> orderId, final Quantity quantity) {
        Preconditions.noneNull(productionOrderID, dateOfIssue, expectedExecutionDate, productId, orderId, quantity);
        Preconditions.ensure(expectedExecutionDate.isEqual(dateOfIssue) || expectedExecutionDate.isAfter(dateOfIssue),
                "Exepected Execution Date must be after the Date of Issue");

        this.productionOrderID = productionOrderID;
        this.dateOfIssue = dateOfIssue;
        this.expectedExecutionDate = expectedExecutionDate;
        this.productId = productId;
        this.productionOrderState = ProductionOrderState.PENDENTE;
        this.orderId = (LinkedList<OrderID>) orderId.clone();
        this.quantity = quantity;
    }

    /**
     * ProductionOrder protected constructor for ORM.
     *
     */
    protected ProductionOrder() {
        quantity = null;
    }

    /**
     * Method that verifies if one ProductionOrder is equal to another.
     *
     * @param o instance of other object
     */
    @Override
    public boolean equals(final Object o) {
        return DomainEntities.areEqual(this, o);
    }

    /**
     * Method that returns a hashCode for the ProductionOrder. Uses EAPLI
     * FrameWork
     */
    @Override
    public int hashCode() {
        return DomainEntities.hashCode(this);
    }

    /**
     * Method that compres one ProductionOrder to other using all atributes
     *
     * @param other other instace of RawaMaterial
     * @return Boolean True if equals
     */
    @Override
    public boolean sameAs(final Object other) {
        if (!(other instanceof ProductionOrder)) {
            return false;
        }

        final ProductionOrder that = (ProductionOrder) other;
        if (this == that) {
            return true;
        }

        return identity().equals(that.identity()) && dateOfIssue.equals(that.dateOfIssue)
                && expectedExecutionDate.equals(that.expectedExecutionDate) && productId.equals(that.productId) && productionOrderState.equals(that.productionOrderState)
                && orderId.equals(that.orderId) && quantity.equals(that.quantity);
    }

    /**
     * Method that returns the ProductionOrderID.
     *
     * @return ProductionOrderID
     */
    @Override
    public ProductionOrderID identity() {
        return ProductionOrderID.valueOf(productionOrderID.executionOrderID());
    }

    /**
     * Method that returns the ProductionOrderState
     *
     * @return productionOrderState
     */
    public ProductionOrderState productionOrderState() {
        return this.productionOrderState;
    }

    /**
     * Method that changes the status of the production order
     *
     * @param state
     */
    public void changeProductionOrderState(ProductionOrderState state) {
        this.productionOrderState = state;
    }

    /**
     * Return the product ID
     *
     * @return product ID
     */
    public ProductID productID() {
        return productId;
    }

    /**
     * Return the quantity
     *
     * @return quantity
     */
    public Quantity quantity() {
        return quantity;
    }

}

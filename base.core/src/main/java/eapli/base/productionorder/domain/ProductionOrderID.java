/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.productionorder.domain;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.util.HashCoder;
import eapli.framework.validations.Preconditions;
import eapli.framework.strings.util.StringPredicates;
import java.util.regex.Pattern;
import javax.persistence.Column;

/**
 *
 * @author joaomachado
 */
public class ProductionOrderID implements ValueObject, Comparable<ProductionOrderID> {

    private static final long serialVersionUID = 1L;
    private static final int MAX_NUMBER_OF_CHARACTERS = 10;
    private static final Pattern IS_ALPHANUMERIC_REGEX = Pattern.compile("^[0-9a-zA-Z]+$");

    @Column(unique = true, nullable = false)
    private final String productionOrderID;

    /**
     * ProductionOrderID constructor.
     *
     * @param productionOrderID Mandatory
     */
    protected ProductionOrderID(final String productionOrderID) {
        Preconditions.nonEmpty(productionOrderID, "Execution Order ID can't be null nor empty");
        Preconditions.ensure(productionOrderID.length() <= MAX_NUMBER_OF_CHARACTERS,
                "Production Order ID can't have more than 10 characters");
        Preconditions.ensure(StringPredicates.isSingleWord(productionOrderID),
                "Production Order ID can't have more than 1 word");
        Preconditions.matches(IS_ALPHANUMERIC_REGEX, productionOrderID,
                "Production Order ID can only have digits and letters");

        this.productionOrderID = productionOrderID;
    }

    /**
     * ProductionOrderID protected constructor for ORM.
     *
     */
    protected ProductionOrderID() {
        productionOrderID = null;
    }

    /**
     * Factory method for obtaining ProductionOrderID value objects.
     *
     * @param productionOrderID id of ProductionOrderID
     * @return a new object corresponding to the value
     */
    public static ProductionOrderID valueOf(final String productionOrderID) {
        return new ProductionOrderID(productionOrderID);
    }

    /**
     * Method that verifies if one ProductionOrderID is equal to another.
     *
     * @param o instance of other object
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProductionOrderID)) {
            return false;
        }

        final ProductionOrderID that = (ProductionOrderID) o;
        return productionOrderID.equalsIgnoreCase(that.executionOrderID());
    }

    /**
     * Method that verifies returns hashcode of ProductionOrderID
     *
     */
    @Override
    public int hashCode() {
        return new HashCoder().with(productionOrderID).code();
    }

    /**
     * Method that returns ProductionOrderID
     *
     * @return productionOrderID
     */
    public String executionOrderID() {
        return this.productionOrderID;
    }

    /**
     * Method that verifies if one ProductionOrderID is equal to another.
     *
     * @param o instance of other ProductionOrderID
     */
    @Override
    public int compareTo(final ProductionOrderID o) {
        return productionOrderID.compareTo(o.productionOrderID);

    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.productionorder.application;

import eapli.base.domain.model.general.Quantity;
import eapli.base.domain.model.general.UnitOfMeasurement;
import eapli.base.productionorder.domain.OrderID;
import eapli.base.productionorder.domain.ProductionOrder;
import eapli.base.productionorder.domain.ProductionOrderID;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.products.application.FindProductByIDService;
import eapli.base.products.domain.Product;
import eapli.base.products.domain.ProductID;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.application.UseCaseController;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.Optional;
import eapli.base.productionorder.repositories.ProductionOrderRepository;

/**
 *
 * @author joaomachado
 */
@UseCaseController
public class AddProductionOrderController {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final ProductionOrderRepository productionOrderRepository = PersistenceContext.repositories().productionOrder();
    private final FindProductByIDService svcFindProduct = new FindProductByIDService();

    /**
     * Method that creates a ProductionOrder and saves in DB
     *
     * @param productionOrderID Id of Execution Order Mandatory
     * @param dateIssue Date of Issue of Execution Order Mandatory
     * @param ExecutionDate Expected Exeturion date of Execution Order
     * @param productId Product ID that Execution Order refers to Mandatory
     * @param orderId List of Id Orders the Execution Order refers to Mandatory
     * @param quantity quantity to produce
     * @param unit the unit to produce
     * @return ProductionOrder Object
     */
    public ProductionOrder addExecutionOrder(final String productionOrderID, final String dateIssue,
            final String ExecutionDate, final String productId, final LinkedList<OrderID> orderId, final double quantity, final UnitOfMeasurement unit) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER,
                BaseRoles.PRODUCTION_MANAGER);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate dateOfIssue = LocalDate.parse(dateIssue, formatter);
        LocalDate expectedExecutionDate = LocalDate.parse(ExecutionDate, formatter);

        final ProductionOrder newProductionOrder = new ProductionOrder(ProductionOrderID.valueOf(productionOrderID),
                dateOfIssue, expectedExecutionDate, ProductID.valueOf(productId), orderId, Quantity.valueOf(quantity, unit));

        return this.productionOrderRepository.save(newProductionOrder);
    }

    /**
     * Mehtod that calls the service for creating a query to find a Product with
     * a specific ID and verify if has a ProductionSheet
     *
     * @param factory_ID The Id to search for
     * @return Product Object
     */
    public boolean findAndValidateProductByID(String factory_ID) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER,
                BaseRoles.PRODUCTION_MANAGER);

        Optional<Product> product = this.svcFindProduct.findByID(factory_ID);
        return (product.get().hasProductionSheet());
    }

}

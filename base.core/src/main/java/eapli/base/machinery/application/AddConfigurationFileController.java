/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.machinery.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.machinery.domain.ConfigurationFile;
import eapli.base.machinery.domain.Machine;
import eapli.base.machinery.repositories.MachinesRepository;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.general.domain.model.Description;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import java.util.Optional;

/**
 *
 * @author joaomachado
 */
public class AddConfigurationFileController {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final MachinesRepository machineRepository = PersistenceContext.repositories().machines();
    private final FindMachineByIDService svcFindMachine = new FindMachineByIDService();

    /**
     * Method that creates a ExecutionOrder and saves in DB
     *
     * @param machineID id of Machine to add Configuration File
     * @param configurationFileDescription Description for the configuration
     * file
     * @param configurationFile Byte[] with the text of the configuration file
     * @return ConfigurationFile Object
     */
    public Machine addConfigurationFile(final String machineID, final String configurationFileDescription, final byte[] configurationFile) {
        Machine me = findMachineByID(machineID);
        ConfigurationFile myConfigurationFile = createConfigurationFile(configurationFileDescription, configurationFile);
        me.addConfigurationFile(myConfigurationFile);

        return this.machineRepository.save(me);
    }

    /**
     * Mehtod that calls the service for creating a query to find a Machine with
     * a specific ID
     *
     * @param machineID The Id to search for
     * @return machine Object
     */
    public Machine findMachineByID(final String machineID) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER, BaseRoles.SHOP_FLOOR_MANAGER);

        Optional<Machine> myMachine = this.svcFindMachine.findByID(machineID);
        return myMachine.get();
    }

    /**
     * Mehtod that creates an instance of ConfigurationFile Obejct and returns
     * it
     *
     * @param configurationFileDescription Description for the configuration
     * file
     * @param configurationFile byte[] with the text of the configuration file
     * @return ConfigurationFile Object
     */
    public ConfigurationFile createConfigurationFile(final String configurationFileDescription, byte[] configurationFile) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER, BaseRoles.SHOP_FLOOR_MANAGER);

        ConfigurationFile myConfigurationFile = ConfigurationFile.valueOf(Description.valueOf(configurationFileDescription), configurationFile);
        return myConfigurationFile;
    }

}

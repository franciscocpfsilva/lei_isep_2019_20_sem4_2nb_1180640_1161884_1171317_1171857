/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.machinery.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.machinery.domain.CommunicationProtocolID;
import eapli.base.machinery.domain.Machine;
import eapli.base.machinery.domain.MachineID;
import eapli.base.machinery.repositories.MachinesRepository;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.application.UseCaseController;
import eapli.framework.general.domain.model.Description;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@UseCaseController
public class AddMachineController {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final MachinesRepository machinesRepository = PersistenceContext.repositories().machines();

    /**
     * Method that creates a new instance of machine and saves to persistence.
     *
     * @param machineID machineID of the machine
     * @param serialNumber serialNumber of the machine
     * @param machineDescription machineDescription of the machine
     * @param brand brand of the machine
     * @param model model of the machine
     * @param communicationProtocolID communicationProtocolID of the machine
     * @return
     */
    public Machine addMachine(final String machineID, final String serialNumber,
            final String machineDescription, final String installationDate, final String brand, final String model, final int communicationProtocolID) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER, BaseRoles.SHOP_FLOOR_MANAGER);
        
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate date = LocalDate.parse(installationDate, formatter);
        
        final Machine newMachine = new Machine(MachineID.valueOf(machineID), Description.valueOf(serialNumber),
                Description.valueOf(machineDescription), date,
                Description.valueOf(brand), Description.valueOf(model), CommunicationProtocolID.valueOf(communicationProtocolID));
        return machinesRepository.save(newMachine);
    }
}

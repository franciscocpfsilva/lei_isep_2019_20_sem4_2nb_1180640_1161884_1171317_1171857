/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.machinery.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.machinery.domain.Machine;
import eapli.base.machinery.repositories.MachinesRepository;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

/**
 *
 * @author
 */
public class FindMachineAvailableService {
    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final MachinesRepository machinesRepository = PersistenceContext.repositories().machines();

    /**
     * Method that return all the machines
     *
     * @return Machine Object
     */
    public Iterable<Machine> allAvailableMachines() {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER,
                BaseRoles.SHOP_FLOOR_MANAGER);

        return this.machinesRepository.findAll();
    }
}

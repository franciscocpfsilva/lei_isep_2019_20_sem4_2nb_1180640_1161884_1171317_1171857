/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.machinery.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.machinery.domain.Machine;
import eapli.base.machinery.domain.MachineID;
import eapli.base.machinery.repositories.MachinesRepository;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import java.util.Optional;

/**
 *
 * @author joaomachado
 */
public class FindMachineByIDService {

//    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final MachinesRepository machinesRepository = PersistenceContext.repositories().machines();

    /**
     * Method that querys the DB with a machine ID and returns a Machine Object
     * with that ID
     *
     * @param machineID Id of Machine to search for
     * @return Machine Object
     */
    public Optional<Machine> findByID(final String machineID) {
//        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER, BaseRoles.SHOP_FLOOR_MANAGER);

        final Optional<Machine> me = machinesRepository.findByID(MachineID.valueOf(machineID));
        return me;
    }
}

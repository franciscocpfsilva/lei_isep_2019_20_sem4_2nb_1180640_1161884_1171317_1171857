/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.machinery.domain;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.util.HashCoder;
import eapli.framework.validations.Preconditions;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author joaomachado
 */
@Embeddable
public class CommunicationProtocolID implements ValueObject, Comparable<CommunicationProtocolID> {

    private static final long serialVersionUID = 1L;
    private static final int MAX_NUMBER_ID = 65535;
    private static final int MIN_NUMBER_ID = 1;

    @Column(nullable = false)
    private final Integer communicationProtocolID;

    /**
     * SerialNumber constructor.
     *
     * @param communicationProtocolID Mandatory
     */
    protected CommunicationProtocolID(final int communicationProtocolID) {
        Preconditions.noneNull(communicationProtocolID);
        if (communicationProtocolID < MIN_NUMBER_ID || communicationProtocolID > MAX_NUMBER_ID) {
            throw new IllegalArgumentException("Invalid Communication Protocol ID");
        }
        this.communicationProtocolID = communicationProtocolID;
    }

    protected CommunicationProtocolID() {
        // for ORM
        communicationProtocolID = 0;
    }

    /**
     * Factory method for obtaining SerialNumber value objects.
     *
     * @param communicationProtocolID
     * @return a new object corresponding to the value
     */
    public static CommunicationProtocolID valueOf(final Integer communicationProtocolID) {
        return new CommunicationProtocolID(communicationProtocolID);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CommunicationProtocolID)) {
            return false;
        }

        final CommunicationProtocolID that = (CommunicationProtocolID) o;
        return communicationProtocolID.equals(that.communicationProtocolID());
    }

    @Override
    public int hashCode() {
        return new HashCoder().with(communicationProtocolID).code();
    }

    public Integer communicationProtocolID() {
        return this.communicationProtocolID;
    }

    @Override
    public int compareTo(final CommunicationProtocolID o) {
        return communicationProtocolID.compareTo(o.communicationProtocolID);
    }

    @Override
    public String toString() {
        return String.valueOf(communicationProtocolID);
    }
    
    

}

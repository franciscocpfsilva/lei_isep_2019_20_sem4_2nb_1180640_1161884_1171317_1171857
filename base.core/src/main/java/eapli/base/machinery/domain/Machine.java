/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.machinery.domain;

import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.DomainEntities;
import javax.persistence.Entity;
import eapli.framework.general.domain.model.Description;
import eapli.framework.validations.Preconditions;
import java.time.LocalDate;
import java.util.LinkedList;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Lob;
import javax.persistence.Version;

@Entity
public class Machine implements AggregateRoot<MachineID> {

    @Version
    private Long version;

    private static final long serialVersionUID = 1L;
    private static final int MAX_NUMBER_OF_CHARACTERS_FOR_DESCRIPTION = 40;
    private static final int MAX_NUMBER_OF_CHARACTERS_FOR_SERIAL_NUMBER = 20;

    @Column(unique = true, nullable = false)
    @EmbeddedId
    private MachineID machineID;

    @Column(unique = true, nullable = false)
    @AttributeOverride(name = "value", column = @Column(name = "SerialNumber", unique = true))
    private Description serialNumber;

    @AttributeOverride(name = "value", column = @Column(name = "machineDescription"))
    private Description machineDescription;

    private LocalDate installationDate;

    @AttributeOverride(name = "value", column = @Column(name = "brand"))
    private Description brand;

    @AttributeOverride(name = "value", column = @Column(name = "model"))
    private Description model;

    private CommunicationProtocolID communicationProtocolID;

    @Lob
    @Column(name = "Configuration_Files", columnDefinition = "BLOB")
    private LinkedList<ConfigurationFile> ConfigurationFiles = new LinkedList<>();

    /**
     * Machine constructor.
     *
     * @param machineID Mandatory
     * @param serialNumber Mandatory
     * @param machineDescription Mandatory
     * @param installationDate
     * @param brand Mandatory
     * @param model Mandatory
     * @param communicationProtocolID Mandatory
     */
    public Machine(final MachineID machineID, final Description serialNumber,
            final Description machineDescription, final LocalDate installationDate, final Description brand,
            final Description model, final CommunicationProtocolID communicationProtocolID) {
        Preconditions.noneNull(machineID, serialNumber, machineDescription, installationDate, brand, model, communicationProtocolID);
        Preconditions.ensure(machineDescription.length() <= MAX_NUMBER_OF_CHARACTERS_FOR_DESCRIPTION,
                "Description can't have more than 40 characters");
        Preconditions.ensure(serialNumber.length() <= MAX_NUMBER_OF_CHARACTERS_FOR_SERIAL_NUMBER,
                "Serial Number can't have more than 20 characters");
        Preconditions.ensure(installationDate.isBefore(LocalDate.now()),
                "Installation Date can't be in the future");

        this.machineID = machineID;
        this.serialNumber = serialNumber;
        this.machineDescription = machineDescription;
        this.installationDate = installationDate;
        this.brand = brand;
        this.model = model;
        this.communicationProtocolID = communicationProtocolID;
    }

    /**
     * Machine protected constructor for ORM.
     *
     */
    protected Machine() {

    }
    
    /**
     * Method that adds a ConfigurationFile to a list of Machine instance
     *
     * @param configurationFile instance of ConfigurationFile
     */
    public void addConfigurationFile(ConfigurationFile configurationFile) {
        this.ConfigurationFiles.add(configurationFile);
    }

    /**
     * Method that verifies if one machine is equal to another.
     *
     * @param o instance of other object
     */
    @Override
    public boolean equals(final Object o) {
        return DomainEntities.areEqual(this, o);
    }

    /**
     * Method that returns a hashCode for the machine. Uses EAPLI FrameWork
     */
    @Override
    public int hashCode() {
        return DomainEntities.hashCode(this);
    }

    /**
     * Method that compres one Machine to other using all attributes
     *
     * @param other instance of Machine
     * @return Boolean True if equals
     */
    @Override
    public boolean sameAs(final Object other) {
        if (!(other instanceof Machine)) {
            return false;
        }

        final Machine that = (Machine) other;
        if (this == that) {
            return true;
        }

        return identity().equals(that.identity()) && serialNumber.equals(that.serialNumber)
                && machineDescription.equals(that.machineDescription) && brand.equals(that.brand)
                && model.equals(that.model) && installationDate.equals(that.installationDate)
                && communicationProtocolID.equals(that.communicationProtocolID);
    }

    /**
     * Method that returns the machine ID.
     *
     * @return machineID
     */
    @Override
    public MachineID identity() {
        return this.machineID;
    }

    /**
     * Method that returns the serialNumber
     *
     * @return serialNumber
     */
    public Description serialNumber() {
        return Description.valueOf(serialNumber.toString());
    }

    /**
     * Method that returns the machineDescription
     *
     * @return machineDescription
     */
    public Description machineDescription() {
        return Description.valueOf(machineDescription.toString());
    }

    /**
     * Method that returns the brand
     *
     * @return brand
     */
    public Description brand() {
        return Description.valueOf(brand.toString());
    }

    /**
     * Method that returns the model
     *
     * @return model
     */
    public Description model() {
        return Description.valueOf(model.toString());
    }

    /**
     * Method that returns the model
     *
     * @return model
     */
    public String installationDate() {
        return installationDate.toString();
    }

    /**
     * Method that returns the communicationProtocolID
     *
     * @return communicationProtocolID
     */
    public Description communicationProtocolID() {
        return Description.valueOf(communicationProtocolID.toString());
    }

    /**
     * Returns a list with all of the configuration files associated with this Machine instance.
     * 
     * @return list of files
     */
    public Iterable<ConfigurationFile> configurationFiles(){
        return this.ConfigurationFiles;
    }
    /**
     * Method to print Machines
     *
     * @return string
     */
    @Override
    public String toString() {
        return "Machine \n" + "Machine ID: " + machineID + "Serial Number: " + serialNumber + "\nDescription: " + machineDescription + "\nBrand: " + brand + "\nModel: " + model + "\nCommunication Protocol ID: " + communicationProtocolID + '.';
    }
}

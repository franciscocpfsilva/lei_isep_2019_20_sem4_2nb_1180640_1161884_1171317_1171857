/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.machinery.domain;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.general.domain.model.Description;
import eapli.framework.validations.Preconditions;
import java.util.Arrays;
import java.util.Objects;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Lob;

/**
 *
 * @author joaomachado
 */
@Embeddable
public class ConfigurationFile implements ValueObject, Comparable<ConfigurationFile> {

    private static final long serialVersionUID = 1L;
    private static final int MAX_DESCRIPTION_LENGTH = 40;

    @AttributeOverride(name = "value", column = @Column(name = "Configuration_File_Description"))
    private Description configurationFileDescription;

    @Lob
    @Column(name = "Configuration_File", columnDefinition = "BLOB")
    private byte[] configurationFile;

    /**
     * ConfigurationFile constructor.
     *
     * @param configurationFileDescription Description for the configuration
     * file
     * @param configurationFile byte[] with the text of the configuration file
     */
    protected ConfigurationFile(final Description configurationFileDescription, final byte[] configurationFile) {
        Preconditions.noneNull(configurationFileDescription, configurationFile);
        Preconditions.ensure(configurationFileDescription.length() <= MAX_DESCRIPTION_LENGTH,
                "Description can't have more than 40 characters");

        this.configurationFileDescription = configurationFileDescription;
        this.configurationFile = configurationFile;
    }

    /**
     * ConfigurationFile protected constructor for ORM.
     *
     */
    protected ConfigurationFile() {

    }

    /**
     * Factory method for obtaining ConfigurationFile value objects.
     *
     * @param configurationFileDescription Description for the configuration
     * file
     * @param configurationFile byte[] with the text of the configuration file
     * @return a new object corresponding to the value
     */
    public static ConfigurationFile valueOf(final Description configurationFileDescription, final byte[] configurationFile) {
        return new ConfigurationFile(configurationFileDescription, configurationFile);
    }

    /**
     * Method that compares one ConfigurationFile to other using all attributes
     *
     * @param other instance of Machine
     * @return Boolean True if equals
     */
    @Override
    public boolean equals(final Object other) {
        if (!(other instanceof ConfigurationFile)) {
            return false;
        }

        final ConfigurationFile that = (ConfigurationFile) other;
        if (this == that) {
            return true;
        }

        return configurationFileDescription().equals(that.configurationFileDescription()) && Arrays.equals(configurationFile(), that.configurationFile());
    }

    /**
     * Method that returns hashcode of ConfigurationFile
     *
     */
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.configurationFileDescription);
        hash = 53 * hash + Arrays.hashCode(this.configurationFile);
        return hash;
    }

    /**
     * Method that returns ConfigurationFile Description
     *
     * @return configurationFileDescription Description for the configuration
     * file
     */
    public Description configurationFileDescription() {
        return Description.valueOf(configurationFileDescription.toString());
    }

    /**
     * Method that returns Configuration File
     *
     * @return configurationFile byte[] with the text of the configuration file
     */
    public byte[] configurationFile() {
        return configurationFile;
    }

    /**
     * Method that verifies if one ConfigurationFile is equal to another.
     *
     * @param o instance of other ConfigurationFile
     */
    @Override
    public int compareTo(final ConfigurationFile o) {
        if (this.configurationFileDescription().equals(o.configurationFileDescription())) {
            return 1;
        } else {
            return 0;
        }
    }
}

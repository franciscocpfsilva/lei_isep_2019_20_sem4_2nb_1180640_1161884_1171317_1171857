/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.machinery.domain;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.util.HashCoder;
import eapli.framework.validations.Preconditions;
import eapli.framework.strings.util.StringPredicates;
import java.util.regex.Pattern;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Represents an identifier for a Machine.
 *
 * Business rules: - can't be null - can't be empty - has a maximum of 10
 * characters - is aplhanumeric (therefore can't have characters other than
 * digits and capital or small letters)
 *
 * @author
 */
@Embeddable
public class MachineID implements ValueObject, Comparable<MachineID> {

    private static final long serialVersionUID = 1L;
    private static final int MAX_NUMBER_OF_CHARACTERS = 10;
    private static final Pattern IS_ALPHANUMERIC_REGEX = Pattern.compile("^[0-9a-zA-Z]+$");

    @Column(nullable = false)
    private final String machineID;

    /**
     * MachineID constructor.
     *
     * @param machineID Mandatory
     */
    public MachineID(final String machineID) {
        Preconditions.nonEmpty(machineID, "machineID can't be null nor empty");
        Preconditions.ensure(machineID.length() <= MAX_NUMBER_OF_CHARACTERS,
                "machineID can't have more than 10 characters");
        Preconditions.ensure(StringPredicates.isSingleWord(machineID),
                "machineID can't have more than 1 word");
        Preconditions.matches(IS_ALPHANUMERIC_REGEX, machineID,
                "machineID can only have digits and letters");

        this.machineID = machineID;
    }

    /**
     * Machine protected constructor for ORM.
     *
     */
    protected MachineID() {

        machineID = null;
    }

    /**
     * Factory method for obtaining MachineID value objects.
     *
     * @param machineID
     * @return a new object corresponding to the value
     */
    public static MachineID valueOf(final String machineID) {
        return new MachineID(machineID);
    }

    /**
     * Method that verifies if one MachineID is equal to another.
     *
     * @param o instance of other object
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MachineID)) {
            return false;
        }

        final MachineID that = (MachineID) o;
        return machineID.equalsIgnoreCase(that.machineID());
    }

    /**
     * Method that verifies returns hashcode of machineID
     *
     */
    @Override
    public int hashCode() {
        return new HashCoder().with(machineID).code();
    }

    /**
     * Method that returns machineID Overrides toString
     */
    @Override
    public String toString() {
        return this.machineID;
    }

    /**
     * Method that returns machineID
     *
     * @return machineID
     */
    public String machineID() {
        return this.machineID;
    }

    /**
     * Method that verifies if one MachineID is equal to another.
     *
     * @param o instance of other MachineID
     */
    @Override
    public int compareTo(final MachineID o) {
        return machineID.compareTo(o.machineID);
    }

}

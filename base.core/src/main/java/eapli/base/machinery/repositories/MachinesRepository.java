/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.machinery.repositories;

import eapli.base.machinery.domain.Machine;
import eapli.base.machinery.domain.MachineID;
import eapli.framework.domain.repositories.DomainRepository;
import java.util.Optional;

public interface MachinesRepository extends DomainRepository<MachineID, Machine> {

    /**
     * returns the Machine with the given machine id
     *
     * @param machineID the Machine id to search
     * @return Machine object
     */
    default Optional<Machine> findByID(
            final MachineID machineID) {
        return ofIdentity(machineID);
    }
}

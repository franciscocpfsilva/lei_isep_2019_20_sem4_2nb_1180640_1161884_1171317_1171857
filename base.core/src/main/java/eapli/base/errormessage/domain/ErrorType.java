/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.errormessage.domain;

/**
 *
 * @author
 */
public enum ErrorType {
    NO_PRODUCTION_ORDER,
    NO_PROCT_OR_RAW_MATERIAL,
    NO_STORAGE_UNIT,
    NO_MACHINE_ACTIVITY;
}

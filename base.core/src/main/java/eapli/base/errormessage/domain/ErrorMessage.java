/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.errormessage.domain;

import eapli.base.rawmessage.domain.RawMessage;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.DomainEntities;
import eapli.framework.validations.Preconditions;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Version;

@Entity
public class ErrorMessage implements AggregateRoot<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long pk;
    @Version
    private Long version;

    @Lob
    private RawMessage message;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private ErrorType errorType;

    /**
     * Contructor to build an ErrorMessage
     *
     * @param message raw message that contains the error
     * @param errorType error that occurred
     */
    public ErrorMessage(RawMessage message, ErrorType errorType) {
        Preconditions.noneNull(message, errorType);

        this.message = message;
        this.errorType = errorType;
    }

    /**
     * Empty Constructor
     */
    protected ErrorMessage() {
        // for ORM
    }

    /**
     * Identify the ErrorMessage
     * @return primary key
     */
    @Override
    public Long identity() {
        return this.pk;
    }

    /**
     * Compares to objects and return if they are equal
     * @param other other objct to compare
     * @return true or false
     */
    @Override
    public boolean sameAs(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof ErrorMessage)) {
            return false;
        }
        final ErrorMessage otherMessage = (ErrorMessage) other;
        return this.equals(otherMessage) && this.message.equals(otherMessage.message)
                && this.errorType.equals(otherMessage.errorType);
    }

    @Override
    public int hashCode() {
        return DomainEntities.hashCode(this);
    }

    @Override
    public boolean equals(final Object o) {
        return DomainEntities.areEqual(this, o);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.errormessage.repositories;

import eapli.base.errormessage.domain.ErrorMessage;
import eapli.framework.domain.repositories.DomainRepository;

/**
 *
 * @author 
 */
public interface ErrorMessagesRepository extends DomainRepository<Long, ErrorMessage> {
    
}

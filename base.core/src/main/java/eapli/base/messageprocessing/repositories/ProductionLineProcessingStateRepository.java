/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.messageprocessing.repositories;

import eapli.base.messageprocessing.domain.ProductionLineProcessingState;
import eapli.base.productionline.domain.ProductionLineID;
import eapli.framework.domain.repositories.DomainRepository;

/**
 *
 * @author franciscoferreiradasilva
 */
public interface ProductionLineProcessingStateRepository extends DomainRepository<ProductionLineID, ProductionLineProcessingState> {
    
}


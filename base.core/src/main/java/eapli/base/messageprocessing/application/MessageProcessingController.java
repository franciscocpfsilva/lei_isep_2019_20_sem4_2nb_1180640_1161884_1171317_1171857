/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.messageprocessing.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.messageprocessing.domain.ProductionLineProcessingState;
import eapli.base.messageprocessing.repositories.ProductionLineProcessingStateRepository;
import eapli.base.productionline.domain.ProductionLineID;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.application.UseCaseController;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import java.util.Optional;

/**
 *
 * @author joaomachado
 */
@UseCaseController
public class MessageProcessingController {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final ProductionLineProcessingStateRepository prodcutionLineRepositoryState = PersistenceContext.repositories().processingState();
    private final MessageProcessingClient client = new MessageProcessingClient();
    
    /**
     * Method returns all ProductionLinesState objects from DB
     *
     * @return ProductionLineState iterable
     */
    public Iterable<ProductionLineProcessingState> viewProductionLineState() {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER,
                BaseRoles.SHOP_FLOOR_MANAGER);
        return this.prodcutionLineRepositoryState.findAll();
    }

    /**
     * Method that updates a Production Line State
     *
     * @param productionLineID id of productionLine
     * @param state boolean represeting the state file
     * @return ProductionLineState object
     */
    public ProductionLineProcessingState changeProductionLineState(final String productionLineID, final boolean state) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER,
                BaseRoles.SHOP_FLOOR_MANAGER);
        ProductionLineProcessingState me = findProductionLineByID(productionLineID);

        me.setProcessingState(state);

        return this.prodcutionLineRepositoryState.save(me);
    }

    /**
     * Method for searching ProductionLines by ID
     *
     * @param productionLineID The Id to search for
     * @return ProductionLine Object
     */
    public ProductionLineProcessingState findProductionLineByID(final String productionLineID) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER,
                BaseRoles.SHOP_FLOOR_MANAGER);

        Optional<ProductionLineProcessingState> me = this.prodcutionLineRepositoryState.ofIdentity(ProductionLineID.valueOf(productionLineID));
        return me.get();
    }

    /**
     * Method for searching and returning the state of a Production Line state
     *
     * @param productionLineID The Id to search for
     * @return boolean representative of the state
     */
    public boolean validateProductionLineState(final String productionLineID) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER,
                BaseRoles.SHOP_FLOOR_MANAGER);

        return findProductionLineByID(productionLineID).processingState();
    }
    
    /**
     * Method that tries to send the necessary arguments in order to run the SPM
     *
     * @param args array of arguments to be sent to the SPM.
     */
    public void makeRequestToSpm(String[] args) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER, BaseRoles.SHOP_FLOOR_MANAGER);
        client.makeRequest(args);
    }
}

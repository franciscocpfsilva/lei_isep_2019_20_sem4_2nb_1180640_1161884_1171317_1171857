/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.messageprocessing.application;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

/**
 *
 * @author leona
 */
public class MessageProcessingClient {
    
//    static int PORT_N=31401;
    public static int PORT_N=9998;
    public static String IP_ADDRESS="127.0.0.1";
    public static int SO_TIMEOUT = 5;
    
    static InetAddress serverIP;
    static Socket sock;
    private DataOutputStream sOut;
    private DataInputStream sIn;
    
    public void makeRequest(String[] args){
        
        try{
            try { 
                serverIP = InetAddress.getByName(IP_ADDRESS); 
            }catch(UnknownHostException ex) {
                System.out.println("Invalid client specified: " + IP_ADDRESS);
                    return;
            }

            try { 
                System.out.println("Tenta criar socket comunicat com o SPM");
                sock = new Socket(serverIP, PORT_N); 
                System.out.println("Tenta criar socket comunicat com o SPM");
//                sock.setSoTimeout(SO_TIMEOUT);
            }catch(SocketTimeoutException ste){
                System.out.println("Error: Timeout expired!");
                return;
            }catch(IOException ex) {
                System.out.println("\nFailed to establish TCP connection");
    //                    System.exit(1);
                return;
            }

            sOut = new DataOutputStream(sock.getOutputStream());
            sIn = new DataInputStream(sock.getInputStream());
            
/* Tries to send args to SPM */
            sOut.writeInt(args.length);
            for (String arg : args){
                writeRequest(sOut, arg);
            }System.out.println("- Arguments sent to SPM server -");
            
            sock.close();
            
        }catch(IOException ioe){
            System.out.println("\nError: Unable to send config request to SPM - " + ioe.getMessage()); 
        }        
       
    }
    
    private void writeRequest(DataOutputStream dataOutputStream, String request) throws IOException{
            dataOutputStream.writeInt(request.length());
            dataOutputStream.writeBytes(request);
    }
    
    private String readReply(DataInputStream dataInputStream) throws IOException{
        int length = dataInputStream.readInt();
        String reply ="";
        for (int i=0; i<length; i++)
            reply = reply + (char) dataInputStream.read();
        return reply;
    }
    
    public static void changeServerIpAddress(String newIpAddress){
        IP_ADDRESS = newIpAddress;
    }
}

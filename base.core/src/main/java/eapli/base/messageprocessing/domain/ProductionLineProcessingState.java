/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.messageprocessing.domain;

import eapli.base.productionline.domain.ProductionLineID;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.DomainEntities;
import eapli.framework.validations.Preconditions;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Version;

/**
 * Represents the association between a production line and its state (active 
 * vs inactive) for recurrent message processing.
 * 
 * @author franciscoferreiradasilva
 */
@Entity
public class ProductionLineProcessingState implements AggregateRoot<ProductionLineID> {
    
    private static final long serialVersionUID = 1L;
    
    @Version
    private Long version;
    
    @EmbeddedId
    @AttributeOverride(name = "ID", column = @Column(name = "productionLineId"))
    private ProductionLineID productionLineId;
    private Boolean processingState;
    
    /**
     * ProductionLineProcessingState constructor.
     *
     * @param productionLineId
     *            Mandatory
     */
    public ProductionLineProcessingState(ProductionLineID productionLineId) {
        Preconditions.noneNull(productionLineId);
        
        this.productionLineId = productionLineId;
        this.processingState = Boolean.FALSE;
    }
    
    protected ProductionLineProcessingState() {
        // for ORM
    }
    
    @Override
    public boolean equals(final Object o) {
        return DomainEntities.areEqual(this, o);
    }

    @Override
    public int hashCode() {
        return DomainEntities.hashCode(this);
    }

    @Override
    public boolean sameAs(final Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof ProductionLineProcessingState)) {
            return false;
        }

        final ProductionLineProcessingState that = (ProductionLineProcessingState) other;
        return this.productionLineId.equals(that.productionLineId) 
                && this.processingState.equals(that.processingState);
    }

    @Override
    public ProductionLineID identity() {
        return productionLineId;
    }
    
    public Boolean processingState() {
        return processingState;
    }

    public void setProcessingState(Boolean processingState) {
        this.processingState = processingState;
    }
    
}

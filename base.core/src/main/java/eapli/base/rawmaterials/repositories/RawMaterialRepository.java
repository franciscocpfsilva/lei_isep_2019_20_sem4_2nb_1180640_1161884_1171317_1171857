/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.rawmaterials.repositories;

import eapli.base.rawmaterials.domain.RawMaterial;
import eapli.base.rawmaterials.domain.RawMaterialID;
import eapli.framework.domain.repositories.DomainRepository;
import java.util.Optional;

/**
 *
 * @author
 */
public interface RawMaterialRepository extends DomainRepository<RawMaterialID, RawMaterial> {
    
    
     /**
     * returns the rawMaterial with the given rawMaterial id
     *
     * @param rawMaterialID the rawmaterial id to search
     * @return
     */
    default Optional<RawMaterial> findByID(
            final RawMaterialID rawMaterialID) {
        return ofIdentity(rawMaterialID);
    }
    
}

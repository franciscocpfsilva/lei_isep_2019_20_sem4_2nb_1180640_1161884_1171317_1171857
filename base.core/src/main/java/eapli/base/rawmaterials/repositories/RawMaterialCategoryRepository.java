/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.rawmaterials.repositories;

import eapli.base.rawmaterials.domain.RawMaterialCategory;
import eapli.base.rawmaterials.domain.RawMaterialCategoryID;
import eapli.framework.domain.repositories.DomainRepository;

/**
 *
 * @author 
 */
public interface RawMaterialCategoryRepository extends DomainRepository<RawMaterialCategoryID, RawMaterialCategory>{
  
}

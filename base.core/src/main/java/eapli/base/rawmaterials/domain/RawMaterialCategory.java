/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.rawmaterials.domain;

import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.DomainEntities;
import eapli.framework.general.domain.model.Description;
import eapli.framework.validations.Preconditions;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;

/**
 * Represents the concept Raw Material Category
 * 
 * Business rules:
 * - identifier and description can't be null or empty
 * 
 * @author
 */
@Entity
public class RawMaterialCategory implements AggregateRoot<RawMaterialCategoryID>{

    private static final long serialVersionUID = 1L;
    private static final int MAX_DESCRIPTION_LENGTH = 40;
    // ORM primary key
    @Id
    @GeneratedValue // (strategy = GenerationType.IDENTITY)
    private Long pk;
    @Version
    private Long version;
    
    // business ID
    @AttributeOverride(name = "identifier", column = @Column(unique = true, nullable = false))
    private RawMaterialCategoryID identifier;
    private Description description;

    /**
     * RawMaterialCategory constructor.
     *
     * @param identifier
     *            Mandatory
     * @param description
     *            Mandatory
     */
    public RawMaterialCategory(final RawMaterialCategoryID identifier, final Description description) {
        Preconditions.noneNull(identifier, description);  
        Preconditions.ensure(description.length() <= MAX_DESCRIPTION_LENGTH, 
                "Description can only have up to 40 characters");
        
        this.identifier = identifier;
        this.description = description;
    }

    protected RawMaterialCategory() {
        // for ORM
    }
    
    @Override
    public RawMaterialCategoryID identity() {
        return this.identifier;
    }
    
    public Description description(){
        return Description.valueOf(description.toString());
    }
    
    @Override
    public boolean sameAs(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof RawMaterialCategory)) {
            return false;
        }
        final RawMaterialCategory otherRMC = (RawMaterialCategory) other;
        return this.equals(otherRMC) && description.equals(otherRMC.description); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int hashCode() {
        return DomainEntities.hashCode(this);
    }

    @Override
    public boolean equals(final Object o) {
        return DomainEntities.areEqual(this, o);
    }

    

    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.rawmaterials.domain;

import eapli.base.domain.model.general.UnitOfMeasurement;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.DomainEntities;
import eapli.framework.general.domain.model.Description;
import eapli.framework.validations.Preconditions;
import java.util.Arrays;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

@Entity
public class RawMaterial implements AggregateRoot<RawMaterialID> {

    @Version
    private Long version;

    private static final long serialVersionUID = 1L;
    private static final int MAX_NUMBER_OF_CHARACTERS_FOR_DESCRIPTION = 40;

    @Column(unique = true, nullable = false)
    @EmbeddedId
    private RawMaterialID rawMaterialID;

    @AttributeOverride(name = "theDescription", column = @Column(name = "rawMaterialDescription"))
    private Description rawMaterialDescription;

    @Enumerated(EnumType.STRING)
    private UnitOfMeasurement unit_of_measurement;

    @ManyToOne(optional = false)
    private RawMaterialCategory rawMaterialCategory;

    @Lob
    @Column(name = "technicalfile", columnDefinition = "BLOB")
    private byte[] pdf;

    /**
     * RawMaterial constructor.
     *
     * @param rawMaterialCategory Mandatory
     * @param rawMaterialID Mandatory
     * @param rawMaterialDescription Mandatory
     * @param unit_of_measurement Mandatory
     * @param pdf Mandatory
     */
    public RawMaterial(final RawMaterialCategory rawMaterialCategory, final RawMaterialID rawMaterialID,
            final Description rawMaterialDescription, final UnitOfMeasurement unit_of_measurement, final byte[] pdf) {
        Preconditions.noneNull(rawMaterialCategory, rawMaterialID, rawMaterialDescription, unit_of_measurement, pdf);
        Preconditions.ensure(rawMaterialDescription.length() <= MAX_NUMBER_OF_CHARACTERS_FOR_DESCRIPTION,
                "Description can't have more than 40 characters");

        this.rawMaterialCategory = rawMaterialCategory;
        this.rawMaterialID = rawMaterialID;
        this.rawMaterialDescription = rawMaterialDescription;
        this.unit_of_measurement = unit_of_measurement;
        this.pdf = pdf;
    }

    /**
     * RawMaterial protected constructor for ORM.
     *
     */
    protected RawMaterial() {

    }

    /**
     * Method that verifies if one RawMaterial is equal to another.
     *
     * @param o instance of other object
     */
    @Override
    public boolean equals(final Object o) {
        return DomainEntities.areEqual(this, o);
    }

    /**
     * Method that returns a hashCode for the RawMaterial. Uses EAPLI FrameWork
     */
    @Override
    public int hashCode() {
        return DomainEntities.hashCode(this);
    }

    /**
     * Method that compres one RawMaterial to other using all atributes
     *
     * @param other other instace of RawaMaterial
     * @return Boolean True if equals
     */
    @Override
    public boolean sameAs(final Object other) {
        if (!(other instanceof RawMaterial)) {
            return false;
        }

        final RawMaterial that = (RawMaterial) other;
        if (this == that) {
            return true;
        }

        return identity().equals(that.identity()) && rawMaterialCategory.equals(that.rawMaterialCategory)
                && rawMaterialDescription.equals(that.rawMaterialDescription) && unit_of_measurement.equals(that.unit_of_measurement)
                && Arrays.equals(pdf, that.pdf);
    }

    /**
     * Method that returns the RawMaterialID.
     *
     * @return machineID
     */
    @Override
    public RawMaterialID identity() {
        return RawMaterialID.valueOf(rawMaterialID.rawMaterialID());
    }

    /**
     * Method that returns the RawMaterial Category
     *
     * @return rawMaterialCategory
     */
    public RawMaterialCategoryID rawMaterialCategory() {
        return rawMaterialCategory.identity();
    }

    /**
     * Method that returns the description
     *
     * @return Description rawMaterialDescription
     */
    public Description rawMaterialDescription() {
        return Description.valueOf(rawMaterialDescription.toString());
    }

    /**
     * Method that returns unit_of_measurement
     *
     * @return unit_of_measurement
     */
    public UnitOfMeasurement unit_of_measurement() {
        return this.unit_of_measurement;
    }

    /**
     * Method to print RawMaterials
     *
     * @return string
     */
    @Override
    public String toString() {
        return "Raw-Material \n" + "Raw-Material Category: " + rawMaterialCategory + "\nRaw Material ID: " + rawMaterialID + "\nDescription: " + rawMaterialDescription + "\nUnit of Measurement: " + unit_of_measurement + "\n Technical Description: " + pdf + '.';
    }

}

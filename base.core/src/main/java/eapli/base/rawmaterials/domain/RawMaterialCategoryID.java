/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.rawmaterials.domain;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.util.HashCoder;
import eapli.framework.validations.Preconditions;
import eapli.framework.strings.util.StringPredicates;
import java.util.regex.Pattern;
import javax.persistence.Embeddable;

/**
 * Represents the identifier for a Raw Material Category
 * 
 * Business rules:
 * - has to be alphanumeric (excluding special characters)
 * - max of 10 characters in length
 * - not null nor empty
 * 
 * @author
 */
@Embeddable
public class RawMaterialCategoryID implements ValueObject, Comparable<RawMaterialCategoryID>{

    private static final long serialVersionUID = 1L;
    private static final int MAX_LENGTH = 10;
    private static final Pattern IS_ALPHANUMERIC_REGEX = Pattern.compile("^[0-9a-zA-Z]+$");
    
    private final String identifier;

    /**
     * RawMaterialCategoryID constructor.
     *
     * @param identifier
     *            Mandatory
     */
    public RawMaterialCategoryID(final String identifier) {
        Preconditions.ensure(!StringPredicates.isNullOrEmpty(identifier), "Identifier can not be null nor empty");
        Preconditions.ensure(identifier.length() <= MAX_LENGTH, 
                "Identifier can only have up to 10 characters");
        Preconditions.ensure(StringPredicates.isSingleWord(identifier), 
                "Identifier must be a single word");
        Preconditions.matches(IS_ALPHANUMERIC_REGEX, identifier, 
                "Identifier must be alphanumeric");
        
        this.identifier = identifier;
    }

    protected RawMaterialCategoryID() {
        // for ORM
        this.identifier = null;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RawMaterialCategoryID)) {
            return false;
        }

        final RawMaterialCategoryID other = (RawMaterialCategoryID) o;
        return identifier.equals(other.identifier());
    }

    @Override
    public int hashCode() {
        return new HashCoder().with(identifier).code();
    }
    
    @Override
    public String toString() {
        return this.identifier;
    }

    public String identifier() {
        return this.identifier;
    }
    
    @Override
    public int compareTo(RawMaterialCategoryID o) {
        return identifier.compareTo(o.identifier);
    }
    
}

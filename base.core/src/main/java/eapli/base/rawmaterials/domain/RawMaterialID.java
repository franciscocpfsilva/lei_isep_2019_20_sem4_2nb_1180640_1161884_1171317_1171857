/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.rawmaterials.domain;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.util.HashCoder;
import eapli.framework.validations.Preconditions;
import eapli.framework.strings.util.StringPredicates;
import java.util.regex.Pattern;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author joaomachado
 */
@Embeddable
public class RawMaterialID implements ValueObject, Comparable<RawMaterialID> {

    private static final long serialVersionUID = 1L;
    private static final int MAX_NUMBER_OF_CHARACTERS = 10;
    private static final Pattern IS_ALPHANUMERIC_REGEX = Pattern.compile("^[0-9a-zA-Z]+$");

    @Column(unique = true, nullable = false)
    private final String rawMaterialID;

    /**
     * RawMaterialID constructor.
     *
     * @param rawMaterialID Mandatory
     */
    protected RawMaterialID(final String rawMaterialID) {
        Preconditions.nonEmpty(rawMaterialID, "raw Material ID can't be null nor empty");
        Preconditions.ensure(rawMaterialID.length() <= MAX_NUMBER_OF_CHARACTERS,
                "raw Material ID can't have more than 10 characters");
        Preconditions.ensure(StringPredicates.isSingleWord(rawMaterialID),
                "raw Material ID can't have more than 1 word");
        Preconditions.matches(IS_ALPHANUMERIC_REGEX, rawMaterialID,
                "raw Material ID can only have digits and letters");

        this.rawMaterialID = rawMaterialID;
    }

    /**
     * RawMaterialID protected constructor for ORM.
     *
     */
    protected RawMaterialID() {

        rawMaterialID = null;
    }

    /**
     * Factory method for obtaining RawMaterialID value objects.
     *
     * @param rawMaterialID
     * @return a new object corresponding to the value
     */
    public static RawMaterialID valueOf(final String rawMaterialID) {
        return new RawMaterialID(rawMaterialID);
    }

    /**
     * Method that verifies if one RawMaterialID is equal to another.
     *
     * @param o instance of other object
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RawMaterialID)) {
            return false;
        }

        final RawMaterialID that = (RawMaterialID) o;
        return rawMaterialID.equalsIgnoreCase(that.rawMaterialID());
    }

    /**
     * Method that verifies returns hashcode of rawMaterialID
     *
     */
    @Override
    public int hashCode() {
        return new HashCoder().with(rawMaterialID).code();
    }

    /**
     * Method that returns rawMaterialID Overrides toString
     */
    @Override
    public String toString() {
        return this.rawMaterialID;
    }

    /**
     * Method that returns rawMaterialID
     *
     * @return rawMaterialID
     */
    public String rawMaterialID() {
        return this.rawMaterialID;
    }

    /**
     * Method that verifies if one RawMaterialID is equal to another.
     *
     * @param o instance of other RawMaterialID
     */
    @Override
    public int compareTo(final RawMaterialID o) {
        return rawMaterialID.compareTo(o.rawMaterialID);
    }

}
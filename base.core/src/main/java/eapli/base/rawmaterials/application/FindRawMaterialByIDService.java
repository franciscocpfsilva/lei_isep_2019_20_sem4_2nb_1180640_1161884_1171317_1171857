/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.rawmaterials.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.rawmaterials.domain.RawMaterial;
import eapli.base.rawmaterials.domain.RawMaterialID;
import eapli.base.rawmaterials.repositories.RawMaterialRepository;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import java.util.Optional;

/**
 *
 * @author joaomachado
 */
public class FindRawMaterialByIDService {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final RawMaterialRepository rawMaterialRepository = PersistenceContext.repositories().rawMaterial();

    /**
     * Method that querys the DB with a RawMaterialId and returns a RawMaterial Object with that ID
     *
     * @param rawMaterialID The RawMaterial ID to search for
     * @return RawMaterial Object
     */
    public Optional<RawMaterial> findByID(final String rawMaterialID) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER, BaseRoles.PRODUCTION_MANAGER);

        final Optional<RawMaterial> me = rawMaterialRepository.findByID(RawMaterialID.valueOf(rawMaterialID));
        return me;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.rawmaterials.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.rawmaterials.domain.RawMaterialCategory;
import eapli.base.rawmaterials.domain.RawMaterialCategoryID;
import eapli.base.rawmaterials.repositories.RawMaterialCategoryRepository;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.application.UseCaseController;
import eapli.framework.general.domain.model.Description;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

/**
 * Controller for US2002 - Add Raw Material Category.
 * @author
 */
@UseCaseController
public class AddRawMaterialCategoryController {
    
    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final RawMaterialCategoryRepository rawMaterialCategoryRepository = PersistenceContext.repositories().rawMaterialCategories();
    
    public RawMaterialCategory addAddRawMaterialCategory(final String identifier, final String description) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER, BaseRoles.PRODUCTION_MANAGER);

        final RawMaterialCategory newCategory = new RawMaterialCategory(
                new RawMaterialCategoryID(identifier), 
                Description.valueOf(description));
        
        return rawMaterialCategoryRepository.save(newCategory);
    }
}

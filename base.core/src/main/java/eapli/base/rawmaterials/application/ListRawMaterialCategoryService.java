/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.rawmaterials.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.rawmaterials.domain.RawMaterialCategory;
import eapli.base.rawmaterials.repositories.RawMaterialCategoryRepository;
import eapli.base.usermanagement.domain.BaseRoles;

import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

/**
 *
 * @author joaomachado
 */
public class ListRawMaterialCategoryService {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final RawMaterialCategoryRepository rawMaterialCategoryRepository = PersistenceContext.repositories()
            .rawMaterialCategories();

    /**
     * Method that querys the DB with all RawMaterialCategory's
     *
     * @return A iterable with all RawMaterialCategory's
     */
    public Iterable<RawMaterialCategory> allRawMaterialCategorys() {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER,
                BaseRoles.PRODUCTION_MANAGER);

        return this.rawMaterialCategoryRepository.findAll();
    }

}

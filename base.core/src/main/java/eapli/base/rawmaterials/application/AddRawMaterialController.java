/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.rawmaterials.application;

import eapli.base.domain.model.general.UnitOfMeasurement;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.rawmaterials.domain.RawMaterial;
import eapli.base.rawmaterials.domain.RawMaterialCategory;
import eapli.base.rawmaterials.domain.RawMaterialID;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.application.UseCaseController;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import eapli.base.rawmaterials.repositories.RawMaterialRepository;
import eapli.framework.general.domain.model.Description;

/**
 *
 * @author joaomachado
 */
@UseCaseController
public class AddRawMaterialController {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final RawMaterialRepository rawMaterialRepository = PersistenceContext.repositories().rawMaterial();
    private final ListRawMaterialCategoryService svc = new ListRawMaterialCategoryService();
    /**
     * Method that creates a new instance of RawMaterial and saves to
     * persistence.
     *
     * @param rawMaterialCategory raw Material Category of the rawMaterial
     * @param rawMaterialID Eaw Material ID of the rawMaterial
     * @param rawMaterialDescription Eaw Material Description of the rawMaterial
     * @param unit_of_measurement uni _of measurement of the rawMaterial
     * @param pdf pdf Tecnhical file of the rawMaterial
     * @return
     */
    public RawMaterial addRawMaterial(final RawMaterialCategory rawMaterialCategory, final String rawMaterialID,
            final String rawMaterialDescription, final UnitOfMeasurement unit_of_measurement, final byte[] pdf) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER, BaseRoles.PRODUCTION_MANAGER);

        final RawMaterial newRawMaterial = new RawMaterial(rawMaterialCategory, RawMaterialID.valueOf(rawMaterialID),
                Description.valueOf(rawMaterialDescription),
                unit_of_measurement, pdf);
        return rawMaterialRepository.save(newRawMaterial);
    }
    
     public Iterable<RawMaterialCategory> rawMaterialCategorys() {
        return this.svc.allRawMaterialCategorys();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.productionorderexecution.repositories;

import eapli.base.productionline.domain.ProductionLineID;
import eapli.base.productionorder.domain.ProductionOrderID;
import eapli.framework.domain.repositories.DomainRepository;
import eapli.base.productionorderexecution.domain.ProductionOrderExecution;
import java.time.LocalDateTime;

/**
 *
 * @author João Cunha
 */
public interface ProductionOrderExecutionRepository extends DomainRepository<ProductionOrderID, ProductionOrderExecution> {

    ProductionOrderExecution findProductionOrderExecutionBetweenDate(LocalDateTime date, ProductionLineID id);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.productionorderexecution.domain;

/**
 *
 * @author João Cunha
 */
public enum DateTypes {

    START_ACTIVITY,
    RESUME_ACTIVITY,
    STOP_ACTIVITY,
    END_ACTIVITY
}

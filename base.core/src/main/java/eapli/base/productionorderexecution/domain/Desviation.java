/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.productionorderexecution.domain;

import eapli.base.domain.model.general.Item;
import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;
import javax.persistence.Embeddable;

/**
 *
 * @author João Cunha
 */
@Embeddable
public class Desviation implements ValueObject {

    private Item item;

    private DesviationType type;


    /**
     * Full constructor for Desviation classe
     *
     * @param item item
     * @param type type of desviation
     */
    private Desviation(Item item, DesviationType type) {
        Preconditions.noneNull(type, item);

        this.item = item;
        this.type = type;
    }

    /**
     * Empty constuctor
     */
    protected Desviation() {
        // for ORM
    }
    
    /**
     * Factory method for obtaining Desviation value objects.
     * @param item item
     * @param desviationType type of desviation
     * @return value object Desviation
     */
    public static Desviation valueOf(final Item item, final DesviationType desviationType) {
        return new Desviation(item, desviationType);
    }
}

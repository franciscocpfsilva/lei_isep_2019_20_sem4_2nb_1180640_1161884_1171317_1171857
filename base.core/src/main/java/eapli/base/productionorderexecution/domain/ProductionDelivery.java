/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.productionorderexecution.domain;

import eapli.base.storage.domain.StorageUnitID;
import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;
import javax.persistence.Embeddable;

/**
 *
 * @author João Cunha
 */
@Embeddable
public class ProductionDelivery implements ValueObject{

    private Production production;
    private StorageUnitID storageID;

    private ProductionDelivery(Production production, StorageUnitID storageID) {
        Preconditions.noneNull(production, storageID);
        this.production = production;
        this.storageID = storageID;
    }

    protected ProductionDelivery() {
        //for ORM
    }

    /**
     * Factory method for obtaining ProductionDelivery value objects.
     *
     * @param production production
     * @param storageID storage id
     * @return value object Production
     */
    public static ProductionDelivery valueOf(final Production production,
            final StorageUnitID storageID) {
        return new ProductionDelivery(production, storageID);
    }
}

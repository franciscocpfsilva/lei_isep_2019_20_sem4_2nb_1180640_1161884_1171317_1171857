/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.productionorderexecution.domain;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedHashMap;
import javax.persistence.Embeddable;

/**
 *
 * @author João Cunha
 */
@Embeddable
public class EfectiveExecutionTime implements ValueObject {
    
    private final int START_POSITION = 0;
    
    private HashMap<LocalDateTime, DateTypes> map;

    /**
     * Initializes the list and adds the start date
     *
     * @param date date
     * @param mType message type
     */
    private EfectiveExecutionTime(LocalDateTime date, DateTypes type) {
        Preconditions.noneNull(date, type);
        this.map = new LinkedHashMap<>();
        this.map.put(date, type);
    }

    /**
     * Empty constructor
     */
    protected EfectiveExecutionTime() {
        // for ORM
    }

    /**
     * Factory method for obtaining EfectiveExecutionTime value objects.
     *
     * @param date date
     * @param type type of date
     * @return value object EfectiveExecutionTime
     */
    public static EfectiveExecutionTime valueOf(LocalDateTime date, DateTypes type) {
        return new EfectiveExecutionTime(date, type);
    }

    /**
     * Method that adds a date to the list of dates
     *
     * @param date Date
     * @param mType Date type
     * @return DateType if sucessful or null if failure
     */
    public DateTypes addDate(final LocalDateTime date, final DateTypes mType) {
        Preconditions.noneNull(date, mType);
        
        return map.put(date, mType);
    }

    /**
     * Returns the machine Stop time
     *
     * @return execution time
     */
    public LocalDateTime calculateStopTime() {
        boolean flag = true;
        LocalDateTime stopTimes = null, result;
        
        LocalDateTime array[] = new LocalDateTime[map.size()];
        map.keySet().toArray(array);
        
        for (int i = 0; i < map.size(); i++) {
            if (map.get(array[i]) == DateTypes.STOP_ACTIVITY) {
                if (map.get(array[i + 1]) == DateTypes.RESUME_ACTIVITY) {
                    if (flag) {
                        stopTimes = calculateTimeBetweenTwoDates(array[i + 1], array[i]);
                        flag = false;
                    } else {
                        result = calculateTimeBetweenTwoDates(array[i + 1], array[i]);
                        stopTimes = addTimes(stopTimes, result);
                    }
                }
            }
        }
        
        return stopTimes;
        
    }

    /**
     * Calculates the addition of two times
     *
     * @param finalTime time resulting from the addition
     * @param otherTime time to add
     * @return result of addition
     */
    private LocalDateTime addTimes(LocalDateTime finalTime, LocalDateTime otherTime) {
        finalTime = finalTime.plusSeconds(otherTime.getSecond());
        finalTime = finalTime.plusSeconds(otherTime.getMinute());
        finalTime = finalTime.plusSeconds(otherTime.getHour());
        return finalTime;
    }

    /**
     * Calculate the time between two dates
     *
     * @param end_date end date
     * @param start_date start date
     * @return execution time
     */
    private LocalDateTime calculateTimeBetweenTwoDates(LocalDateTime end_date,
            LocalDateTime start_date) {
        
        end_date = end_date.minusSeconds(start_date.getSecond());
        end_date = end_date.minusMinutes(start_date.getMinute());
        end_date = end_date.minusHours(start_date.getHour());
        
        return end_date;
        
    }

    /**
     * Calculates the effective time.
     *
     * EffectiveTime = GrossTime - Stops
     *
     * If it is not possible to calculate the gross time it returns null,
     * otherwise it returns the effective time
     *
     * @return efective time
     */
    public LocalDateTime calculateEfectiveTime() {
        LocalDateTime grossTime = calculateGrossTime();
        LocalDateTime stopTime = calculateStopTime();
        if (stopTime == null) {
            return null;
        }
        return calculateTimeBetweenTwoDates(grossTime, stopTime);
        
    }

    /**
     * Calculates the gross time! Checks if there is already an activity start
     * time and an activity end time. If both exist calculate, otherwise it
     * returns null
     *
     * @return GrossTime or Null
     */
    private LocalDateTime calculateGrossTime() {
        LocalDateTime start, end;
        int size = map.size();
        LocalDateTime array[] = new LocalDateTime[size];
        map.keySet().toArray(array);
        
        if (map.containsValue(DateTypes.START_ACTIVITY) && map.containsValue(DateTypes.END_ACTIVITY)) {
            start = array[START_POSITION];
            end = array[size - 1];
            return calculateTimeBetweenTwoDates(end, start);
        }
        return null;
        
    }
}

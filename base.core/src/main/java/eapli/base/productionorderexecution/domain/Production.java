/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.productionorderexecution.domain;

import eapli.base.domain.model.general.Item;
import eapli.base.domain.model.general.Quantity;
import eapli.base.storage.domain.StorageUnitID;
import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;

/**
 *
 * This class encompasses product production, chargeback production and
 * production delivery
 *
 * @author João Cunha
 */
public class Production implements ValueObject {

    private Batch lot;
    private Item item;
    private StorageUnitID storageID;

    /**
     * Full constructor. Note: Lot can be null!!
     *
     * @param lot lot
     * @param storageID storage unit ID
     * @param item item
     */
    private Production(Batch lot, Item item, StorageUnitID storageID) {
        Preconditions.noneNull(item);

        this.lot = lot;
        this.item = item;
        this.storageID = storageID;
    }

    /**
     * Empty constructor
     */
    protected Production() {
        // for ORM
    }

    /**
     * Factory method for obtaining Production value objects.
     *
     * @param lot lot
     * @param item item
     * @param storageID storage unit ID
     * @return value object Production
     */
    public static Production valueOf(final Batch lot, final Item item,
            final StorageUnitID storageID) {
        return new Production(lot, item, storageID);
    }

    /**
     * Return the item
     *
     * @return item
     */
    public Item item() {
        return item;
    }

    /**
     * Return the storage unit ID
     *
     * @return storage unit ID
     */
    public StorageUnitID storageID() {
        return storageID;
    }

    /**
     * Return the quantity produced
     *
     * @return quantity produced
     */
    public Quantity quantity() {
        return this.item.quantity();
    }
}

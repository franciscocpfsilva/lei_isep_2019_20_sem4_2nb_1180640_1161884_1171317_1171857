/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.productionorderexecution.domain;

import eapli.base.domain.model.general.Item;
import eapli.base.storage.domain.StorageUnitID;
import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;
import javax.persistence.Embeddable;

/**
 *
 * @author João Cunha
 */
@Embeddable
public class Chargeback implements ValueObject {

    private Item item;
    private StorageUnitID storageUnitID;

    private Chargeback(Item item, StorageUnitID storageUnitID) {
        Preconditions.noneNull(item, storageUnitID);

        this.item = item;
        this.storageUnitID = storageUnitID;
    }

    protected Chargeback() {
        //for ORM
    }

    /**
     * Factory method for obtaining Consumption value objects.
     *
     * @param storageID storageID
     * @param item item
     * @return value object Consumption
     */
    public static Chargeback valueOf(final Item item, final StorageUnitID storageID) {
        return new Chargeback(item, storageID);
    }
}

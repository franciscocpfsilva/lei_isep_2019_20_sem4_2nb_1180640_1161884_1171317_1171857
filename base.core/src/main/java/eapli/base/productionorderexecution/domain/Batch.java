/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.productionorderexecution.domain;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;
import javax.persistence.Embeddable;

/**
 *
 * @author João Cunha
 */
@Embeddable
public class Batch implements ValueObject {

    private String id;

    private Batch(String id) {
        Preconditions.noneNull(id);

        this.id = id;
    }

    protected Batch() {
    }

    /**
     * Factory method for obtaining Lot value objects.
     *
     * @param id identifier
     * @return value object Desviation
     */
    public static Batch valueOf(final String id) {
        return new Batch(id);
    }

}

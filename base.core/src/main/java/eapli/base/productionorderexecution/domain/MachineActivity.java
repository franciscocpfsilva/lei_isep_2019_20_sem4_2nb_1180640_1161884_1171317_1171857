/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.productionorderexecution.domain;

import eapli.base.machinery.domain.MachineID;
import eapli.base.rawmessage.domain.MessageType;
import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;
import java.time.LocalDateTime;
import javax.persistence.Embeddable;

/**
 *
 * @author João Cunha
 */
@Embeddable
public class MachineActivity implements ValueObject {

    private MachineID machineID;
    private EfectiveExecutionTime efectiveTime;
    private GrossExecutionTime grossTime;

    /**
     * Constuctor with machine id, startTime and date type
     *
     * @param machineID machine identifier
     */
    private MachineActivity(final MachineID machineID, final LocalDateTime startTime,
            final DateTypes type) {
        Preconditions.noneNull(machineID, startTime, type);

        this.machineID = machineID;
        this.efectiveTime = EfectiveExecutionTime.valueOf(startTime, type);
        this.grossTime = GrossExecutionTime.valueOf(startTime);
    }

    /**
     * Empty constructor
     */
    protected MachineActivity() {
        // for ORM
    }

    /**
     * Factory method for obtaining MachineActivity value objects.
     *
     * @param machineID machine Id
     * @param startTime Time
     * @param type Date type
     * @return value object MachineActivity
     */
    public static MachineActivity valueOf(final MachineID machineID,
            final LocalDateTime startTime, final DateTypes type) {
        return new MachineActivity(machineID, startTime, type);
    }

    /**
     * Return the machine id
     *
     * @return machine id
     */
    public MachineID machineID() {
        return machineID;
    }

    /**
     * Return the efective time
     *
     * @return efectiveTime
     */
    public EfectiveExecutionTime efectiveTime() {
        return efectiveTime;
    }

    /**
     * Return the gross time
     *
     * @return grossTime
     */
    public GrossExecutionTime grossTime() {
        return grossTime;
    }

}

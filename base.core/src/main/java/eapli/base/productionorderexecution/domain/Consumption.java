/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.productionorderexecution.domain;

import eapli.base.domain.model.general.Item;
import eapli.base.domain.model.general.Quantity;
import eapli.base.storage.domain.StorageUnitID;
import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;
import javax.persistence.Embeddable;

/**
 *
 * @author João Cunha
 */
@Embeddable
public class Consumption implements ValueObject {

    private StorageUnitID storageID;

    private Item item;

    /**
     * Full constructor for consumption classe
     *
     * @param storageID storage Id
     * @param item Item
     */
    private Consumption(StorageUnitID storageID, Item item) {
        Preconditions.noneNull(storageID, item);

        this.storageID = storageID;
        this.item = item;
    }

    /**
     * Empty constuctor
     */
    protected Consumption() {
        // for ORM
    }

    /**
     * Factory method for obtaining Consumption value objects.
     *
     * @param storageID storageID
     * @param item item
     * @return value object Consumption
     */
    public static Consumption valueOf(final StorageUnitID storageID, Item item) {
        return new Consumption(storageID, item);
    }

    public Item item() {
        return item;
    }

    public Quantity quantity() {
        return item.quantity();
    }
}

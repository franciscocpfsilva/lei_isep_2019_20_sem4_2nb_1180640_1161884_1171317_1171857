/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.productionorderexecution.domain;

import eapli.base.domain.model.general.Item;
import eapli.base.machinery.domain.MachineID;
import eapli.base.productionline.domain.ProductionLineID;
import eapli.base.productionorder.domain.ProductionOrderID;
import eapli.base.products.domain.ProductionSheet;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.validations.Preconditions;
import java.time.LocalDateTime;
import java.util.LinkedList;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Version;

/**
 *
 * @author
 */
@Entity
public class ProductionOrderExecution implements AggregateRoot<ProductionOrderID> {

    @Id
    @GeneratedValue
    private Long pk;
    @Version
    private Long version;

    @Column(nullable = false, unique = true)
    private ProductionOrderID productionOrderID;

    @Column(nullable = false, unique = true)
    private ProductionLineID productionLineID;

    @Column(name = "Production_Sheet")
    private ProductionSheet productionSheet;

    @Column
    private LocalDateTime startDate;

    @Column
    private LocalDateTime endDate;

    @Column
    private LocalDateTime totalGrossTime;

    @Column
    private LocalDateTime totalEfectiveTime;

    @Lob
    private LinkedList<MachineActivity> machineActivity;

    @Lob
    private LinkedList<Desviation> deviations;

    @Lob
    private LinkedList<Consumption> consumption;

    @Lob
    private LinkedList<Production> production;

    public ProductionOrderExecution(ProductionOrderID productionOrderID, ProductionLineID productionLineID,
            ProductionSheet productionSheet) {
        Preconditions.noneNull(productionOrderID, productionLineID, productionSheet);

        this.productionOrderID = productionOrderID;
        this.productionLineID = productionLineID;
        this.productionSheet = productionSheet;
        this.startDate = null;
        this.totalEfectiveTime = null;
        this.totalGrossTime = null;
        this.endDate = null;
        machineActivity = new LinkedList();
        deviations = new LinkedList();
        consumption = new LinkedList();
        production = new LinkedList();

    }

    protected ProductionOrderExecution() {
        // for ORM
    }

    @Override
    public boolean sameAs(Object other) {
        if (!(other instanceof ProductionOrderExecution)) {
            return false;
        }

        final ProductionOrderExecution that = (ProductionOrderExecution) other;
        if (this == that) {
            return true;
        }

        return identity().equals(that.identity());
    }

    @Override
    public ProductionOrderID identity() {
        return productionOrderID;
    }

    /**
     * Return the consumption
     *
     * @return list of consumptions
     */
    public LinkedList<Consumption> consumption() {
        return consumption;
    }

    /**
     * Return the production
     *
     * @return list of productions
     */
    public LinkedList<Production> production() {
        return production;
    }

    /**
     * Return the production Sheet
     *
     * @return production sheet
     */
    public ProductionSheet productionSheet() {
        return productionSheet;
    }

    /**
     * Add an element to a list Machine Activity
     *
     * @param m machine activity
     * @return sucess or failure
     */
    public boolean addToMachineActivity(MachineActivity m) {
        Preconditions.nonNull(m);
        return machineActivity.add(m);
    }

    /**
     * Add an element to a list Desviations
     *
     * @param d desviation
     * @return sucess or failure
     */
    public boolean addToDesviations(Desviation d) {
        Preconditions.nonNull(d);
        return deviations.add(d);
    }

    /**
     * Add an element to a list Consumption
     *
     * @param c Consumption
     * @return sucess or failure
     */
    public boolean addToConsumption(Consumption c) {
        Preconditions.nonNull(c);
        return consumption.add(c);
    }

    /**
     * Add an element to a list Production
     *
     * @param p Production
     * @return sucess or failure
     */
    public boolean addToProduction(Production p) {
        Preconditions.nonNull(p);
        return production.add(p);
    }

    /**
     * Checks whether there is a production with the item
     *
     * @param item item
     * @return production
     */
    public Production containsProduction(Item item) {
        for (Production production1 : production) {
            if (production1.item().equals(item) && production1.storageID() == null) {
                return production1;
            }
        }
        return null;
    }

    /**
     * Defines the production line
     *
     * @param productionLineID production line ID
     */
    public void defineProductionLineID(ProductionLineID productionLineID) {
        Preconditions.nonNull(productionLineID);
        this.productionLineID = productionLineID;
    }

    /**
     * Returns machine activity
     *
     * @param machineID machine id
     * @return MachineActivity
     */
    public MachineActivity fromMachineActivity(MachineID machineID) {
        for (MachineActivity machineActivity1 : machineActivity) {
            if (machineActivity1.machineID().equals(machineID)) {
                return machineActivity1;
            }
        }
        return null;
    }

    /**
     * Defines the end date
     *
     * @param endDate end date
     */
    public void defineEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    /**
     * Defines the start date
     *
     * @param startDate start date
     */
    public void defineStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    /**
     * Defines the production sheet. This production sheet is a copy of the
     * product sheet to be produced when the execution order started. Because
     * this can be changed over time
     *
     * @param productionSheet production sheet
     */
    public void defineProductionSheet(ProductionSheet productionSheet) {
        this.productionSheet = productionSheet;
    }

    /**
     * Calculate the total gross Time
     *
     * @return this Production Order Execution
     */
    public ProductionOrderExecution defineTotalGrossTime() {
        if (startDate != null && endDate != null) {
            this.totalGrossTime = calcDifBetweenDates(startDate, endDate);
        }
        return this;
    }

    /**
     * Calculate the total efective time
     *
     * @return Production Order Execution
     */
    public ProductionOrderExecution defineTotalEfectiveTime() {
        if (totalGrossTime != null) {
            boolean flag = true;
            LocalDateTime stopTime = null, result;
            for (MachineActivity machineActivity1 : machineActivity) {
                result = machineActivity1.efectiveTime().calculateStopTime();
                if (result != null) {
                    if (flag) {
                        stopTime = result;
                        result = null;
                        flag = false;
                    } else {
                        stopTime = stopTime.plusSeconds(result.getSecond());
                        stopTime = stopTime.plusMinutes(result.getMinute());
                        stopTime = stopTime.plusHours(result.getHour());
                    }
                }
            }
            this.totalEfectiveTime = this.totalGrossTime;
            if (stopTime != null) {
                this.totalEfectiveTime = calcDifBetweenDates(stopTime, this.totalEfectiveTime);
            }
        }
        return this;
    }

    /**
     * Calculates date subtraction
     *
     * @param startTime start date
     * @param endTime end date
     * @return date
     */
    private LocalDateTime calcDifBetweenDates(LocalDateTime startTime, LocalDateTime endTime) {
        endTime = endTime.minusSeconds(startTime.getSecond());
        endTime = endTime.minusMinutes(startTime.getMinute());
        endTime = endTime.minusHours(startTime.getHour());
        return endTime;
    }
}

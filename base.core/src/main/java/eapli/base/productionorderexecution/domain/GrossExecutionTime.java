/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.productionorderexecution.domain;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;
import java.time.LocalDateTime;
import javax.persistence.Embeddable;

/**
 *
 * @author João Cunha
 */
@Embeddable
public class GrossExecutionTime implements ValueObject {

    private LocalDateTime startDate;
    private LocalDateTime endDate;

    private GrossExecutionTime(final LocalDateTime startDate) {
        Preconditions.noneNull(startDate);
        this.startDate = startDate;

    }

    /**
     * Empty constructor
     */
    protected GrossExecutionTime() {
        //for ORM
    }

    /**
     * Factory method for obtaining GrossExecutionTime value objects.
     *
     * @param startDate when the machine started
     * @return value object Desviation
     */
    public static GrossExecutionTime valueOf(final LocalDateTime startDate) {
        return new GrossExecutionTime(startDate);
    }

    /**
     * Define the end date
     *
     * @param endDate end date
     */
    public void defineEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    /**
     * Returns the machine execution time
     *
     * @return execution time
     */
    public LocalDateTime calculateGrossTime() {
        LocalDateTime result = endDate;

        result = result.minusSeconds(startDate.getSecond());
        result = result.minusMinutes(startDate.getMinute());
        result = result.minusHours(startDate.getHour());

        return result;
    }

}

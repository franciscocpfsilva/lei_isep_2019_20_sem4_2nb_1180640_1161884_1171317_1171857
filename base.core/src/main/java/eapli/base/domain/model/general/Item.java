/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.domain.model.general;

import eapli.base.products.domain.ProductID;
import eapli.base.rawmaterials.domain.RawMaterialID;
import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;
import java.util.Objects;
import javax.persistence.Lob;

public class Item implements ValueObject, Comparable<Item> {

    private final RawMaterialID rawMaterialID;
    
    private final ProductID productID;
    
    @Lob
    private final Quantity quantity;

    /**
     * Private constructor to create both implementations of Item (with
     * rawMaterial or Product)
     *
     * @param rawMaterialID RawMaterialID for the Item
     * @param quantity quantity of the Item
     */
    public Item(final RawMaterialID rawMaterialID, final Quantity quantity) {
        Preconditions.ensure(rawMaterialID != null,
                "There must be a RawMaterial ID o");
        this.productID = null;
        this.rawMaterialID = rawMaterialID;
        this.quantity = quantity;

    }

    /**
     * Private constructor to create both implementations of Item (with
     * rawMaterial or Product)
     *
     * @param productID productID for the Item
     *
     * @param quantity quantity of the Item
     */
    public Item(final ProductID productID, final Quantity quantity) {
        Preconditions.ensure(productID != null,
                "There must be a  Product ID");
        this.rawMaterialID = null;
        this.productID = productID;
        this.quantity = quantity;

    }

    protected Item() {
        this.rawMaterialID = null;
        this.productID = null;
        this.quantity = null;
    }

    /**
     * Factory method for obtaining a item with a RawMaterial Object,
     *
     * @param rawMaterialID RawMaterialID for the Item
     * @param quantity quantity of the Item
     *
     * @return a new object corresponding to the value
     */
    public static Item valueOf(final RawMaterialID rawMaterialID, final Quantity quantity) {
        return new Item(rawMaterialID, quantity);
    }

    /**
     * Factory method for obtaining Item with a Prodcut Object.
     *
     * @param productID ProductId for the Item
     * @param quantity quantity of the Item
     *
     * @return a new object corresponding to the value
     */
    public static Item valueOf(final ProductID productID, final Quantity quantity) {
        return new Item(productID, quantity);
    }

    /**
     * Compare objects
     *
     * @param o Item type object
     * @return True or False
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Item)) {
            return false;
        }

        final Item that = (Item) o;
        return (rawMaterialID.compareTo(that.rawMaterialID) == 0 || productID.compareTo(that.productID) == 0)
                && quantity == quantity;
    }

    /**
     * Method to generate hashCode with all parameters
     *
     * @return hash int with hash code
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + Objects.hashCode(this.rawMaterialID);
        hash = 71 * hash + Objects.hashCode(this.productID);
        hash = 71 * hash + Objects.hashCode(this.quantity);
        return hash;
    }

    /**
     * Method that returns rawMaterialID
     *
     * @return rawMaterialID
     */
    public RawMaterialID rawMaterialID() {
        return this.rawMaterialID;
    }

    /**
     * Method that returns ProductID
     *
     * @return productID
     */
    public ProductID productID() {
        return this.productID;
    }

    /**
     * Method that returns quantity
     *
     * @return quantity
     */
    public Quantity quantity() {
        return this.quantity;
    }

    /**
     * Compare two objects Item
     *
     * @param o Item Object
     * @return equal, greater or smaller
     */
    @Override
    public int compareTo(Item o) {
        if (this.quantity().equals(o.quantity())) {
            return 1;
        } else {
            return 0;
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.domain.model.general;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 *
 * @author joaomachado
 */
public class Quantity implements ValueObject, Comparable<Quantity> {

    @Column(nullable = false)
    private final Double quantity;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private UnitOfMeasurement unit_of_measurement;

    /**
     * Quantity constructor.
     *
     * @param quantity Mandatory Double quantity
     * @param unit_of_measurement unit quantity refers too
     */
    protected Quantity(final Double quantity, final UnitOfMeasurement unit_of_measurement) {
        Preconditions.ensure(quantity >= 0,
                "Quantity must be a positive number!");
        Preconditions.noneNull(unit_of_measurement, "Standard Quantity cannot be null or empty");

        this.quantity = quantity;
        this.unit_of_measurement = unit_of_measurement;
    }

    /**
     * Quantity protected constructor for ORM.
     *
     */
    protected Quantity() {

        quantity = null;
    }

    /**
     * Factory method for obtaining Quantity value objects.
     *
     * @param quantity quantity Double
     * @param unit_of_measurement the unit quantity refers too
     * @return a new object corresponding to the value
     */
    public static Quantity valueOf(final Double quantity, final UnitOfMeasurement unit_of_measurement) {
        return new Quantity(quantity, unit_of_measurement);
    }

    /**
     * Method that verifies if one Quantity is equal to another.
     *
     * @param o instance of other object
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Quantity)) {
            return false;
        }

        final Quantity that = (Quantity) o;
        return quantity.equals(that.quantity()) && unit_of_measurement.equals(that.unit_of_measurement);
    }

    /**
     * Method that verifies returns hashcode of Quantity
     *
     */
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + Objects.hashCode(this.quantity);
        hash = 41 * hash + Objects.hashCode(this.unit_of_measurement);
        return hash;
    }

    /**
     * Method that returns Double quantity of something
     *
     * @return quantity in Double
     */
    public Double quantity() {
        return this.quantity;
    }

    /**
     * Method that returns UnitOfMeasurement of quantity
     *
     * @return unit_of_measurement in UnitOfMeasurement
     */
    public UnitOfMeasurement unitOfMeasurement() {
        return this.unit_of_measurement;
    }

    /**
     * Method that verifies if one Quantity is equal to another.
     *
     * @param o instance of other Order
     */
    @Override
    public int compareTo(final Quantity o) {
        return quantity.compareTo(o.quantity);
    }

    /**
     * Returns a new value object quantity if the units of measure are equal
     * otherwise it returns null
     *
     * @param firstQuantity first quantity
     * @param secondQuantity second quantity
     * @return new Quantity or null
     */
    public Quantity sumOfQuantities(Quantity firstQuantity, Quantity secondQuantity) {
        if (firstQuantity.unitOfMeasurement().equals(secondQuantity.unitOfMeasurement())) {
            return Quantity.valueOf(firstQuantity.quantity() + secondQuantity.quantity(), firstQuantity.unitOfMeasurement());
        }
        return null;
    }

    /**
     * Returns a new value object quantity if the units of measure are equal
     * otherwise it returns null
     *
     * @param firstQuantity first quantity
     * @param secondQuantity second quantity
     * @return new Quantity or null
     */
    public Quantity subtractQuantities(Quantity firstQuantity, Quantity secondQuantity) {
        if (firstQuantity.unitOfMeasurement().equals(secondQuantity.unitOfMeasurement())) {
            return Quantity.valueOf(firstQuantity.quantity() - secondQuantity.quantity(), firstQuantity.unitOfMeasurement());
        }
        return null;
    }
}

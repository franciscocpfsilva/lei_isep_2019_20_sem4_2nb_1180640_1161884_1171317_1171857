/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.rawmessage.repositories;

import eapli.base.machinery.domain.MachineID;
import eapli.base.rawmessage.domain.MessageType;
import eapli.base.rawmessage.domain.RawMessage;
import eapli.base.rawmessage.domain.RawMessageState;
import eapli.framework.domain.repositories.DomainRepository;
import java.time.LocalDateTime;
import java.util.List;

/**
 *
 * @author
 */
public interface RawMessagesRepository extends DomainRepository<Long, RawMessage> {

    /**
     * Returns the RawMEssage with the attributes passed as arguments
     *
     * @param id
     * @param type
     * @param dateTime
     * @return a RawMessage
     */
    RawMessage findRawMessageBy(MachineID id, MessageType type, LocalDateTime dateTime);

    Iterable<RawMessage> findMessagesBetween(LocalDateTime start, LocalDateTime end,
            RawMessageState state, List<MachineID> lst);
    
    Iterable<RawMessage> findMessagesFromMachines(RawMessageState state, List<MachineID> lst);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.rawmessage.domain;

/**
 * 
 * @author 
 */
public enum MessageType {
    C0("Consumption", 6),
    C9("Production Delivery", 6),
    P1("Production", 6),
    P2("Chargeback", 6),
    S0("Start of Activity", 4),
    S1("Resume Activity", 3),
    S8("Forced Stop", 4),
    S9("End of Activity", 4);
    
    
    public final String label;
    public final int nAttributes;
    
    private MessageType(String label, int nAttributes){
        this.label=label;
        this.nAttributes=nAttributes;
    }
}

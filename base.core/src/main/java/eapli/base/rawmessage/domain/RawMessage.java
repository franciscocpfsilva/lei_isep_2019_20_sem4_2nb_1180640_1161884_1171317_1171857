/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.rawmessage.domain;

import eapli.base.domain.model.general.UnitOfMeasurement;
import eapli.base.machinery.domain.MachineID;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.DomainEntities;
import eapli.framework.validations.Preconditions;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;

/**
 * Procura representar o conceito mensagem bruta, RawMessage
 *
 * @author
 */
@Entity
public class RawMessage implements AggregateRoot<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue // (strategy = GenerationType.IDENTITY)
    private Long pk;
    @Version
    private Long version;

    @Column(nullable = false)
    private MachineID machineID;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private MessageType messageType;

    @Column(nullable = false)
    private LocalDateTime generationDateTime;

    /*@Lob
    @Column(name = "Raw_Message") //< alterar
    private byte[] rawData;*/
    @Column(name = "Raw_Data")
    private String rawData;

    @Enumerated(EnumType.STRING)
    private RawMessageState state;

    /**
     * RawMaterialCategory constructor.
     *
     * @param machineID Mandatory
     * @param messageType - Message type (e.g. C0, C9, S1..) Mandatory
     * @param generationDateTime - Data/Time in which the message was generated
     * Mandatory
     * @param rawData - Raw message in CSV format Mandatory
     */
    public RawMessage(final MachineID machineID, final MessageType messageType, final LocalDateTime generationDateTime, final String rawData) {
        Preconditions.noneNull(machineID, messageType, generationDateTime);
        Preconditions.ensure(generationDateTime.isBefore(LocalDateTime.now().plusDays(1)));

        this.machineID = machineID;
        this.messageType = messageType;
        this.generationDateTime = generationDateTime;
        this.rawData = rawData;
        this.state = RawMessageState.UNPROCESSED;
    }

    protected RawMessage() {
        // for ORM
    }

    @Override
    public Long identity() {
        return this.pk;
    }

    public MachineID fromMachine() {
        return this.machineID;
    }

    public LocalDateTime generatedAtDateTime() {
        return this.generationDateTime;
    }

    public MessageType messageType() {
        return this.messageType;
    }

    public String rawData() {
        return this.rawData;
    }

    public void changeRawMessageState(RawMessageState state) {
        Preconditions.noneNull(state);
        this.state = state;
    }

    @Override
    public boolean sameAs(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof RawMessage)) {
            return false;
        }
        final RawMessage otherMessage = (RawMessage) other;
        return this.equals(otherMessage) && this.machineID.equals(otherMessage.machineID)
                && this.messageType.equals(otherMessage.messageType)
                && this.generationDateTime.equals(otherMessage.generationDateTime);
    }

    @Override
    public int hashCode() {
        return DomainEntities.hashCode(this);
    }

    @Override
    public boolean equals(final Object o) {
        return DomainEntities.areEqual(this, o);
    }
}

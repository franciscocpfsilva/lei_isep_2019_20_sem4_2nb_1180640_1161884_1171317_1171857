/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.products.repositories;

import eapli.base.products.domain.ProductID;
import eapli.base.products.domain.Product;
import eapli.framework.domain.repositories.DomainRepository;
import java.util.Optional;

/**
 *
 * @author
 */
public interface ProductsRepository extends DomainRepository<ProductID, Product> {

    /**
     * Returns the Products with no production sheet.
     *
     * @return An iterable for Products.
     */
    Iterable<Product> findWithNoProductionSheet();

    /**
     * returns the Product with the given factory id
     *
     * @param factory_id the factory id to search
     * @return Product object
     */
    default Optional<Product> findByID(
            final ProductID factory_id) {
        return ofIdentity(factory_id);
    }

}

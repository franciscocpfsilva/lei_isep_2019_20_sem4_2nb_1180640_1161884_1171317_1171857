/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.products.importing.application;

/**
 * Enumerated class to represent the available file types for importing products.
 * 
 * @author Francisco Silva on 09/05/2020
 */
public enum FileType {
    CSV
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.products.importing.application;

import eapli.base.domain.model.general.UnitOfMeasurement;
import eapli.base.products.domain.Product;
import eapli.base.products.domain.ProductID;
import eapli.framework.general.domain.model.Description;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * The CSV import Strategy implementation.
 * 
 * @author Francisco Silva on 09/05/2020
 */
public class ProductImporterCsv implements ProductImporter {
    
    private final static String ERROR_FILE_NAME = "/error.csv";
    private final static String TEMPLATE_HEADER = "CódigoFabrico;CódigoComercial;Descrição Breve;Descrição Completa;Unidade;Categoria";
    private final static String FILE_SEPARATOR = ";";
    private final static int EXPECTED_NUMBER_OF_PRODUCT_ATTRIBUTES = 6;
    private final static int POSITION_FACT_ID = 0;
    private final static int POSITION_COMM_ID = 1;
    private final static int POSITION_BRIEF_DESC = 2;
    private final static int POSITION_FULL_DESC = 3;
    private final static int POSITION_UNIT_MEASURE = 4;
    private final static int POSITION_CATEGORY = 5;
    
    private Scanner scanner; // for the file to be read
    private PrintWriter stream; // for the file where the errors will be printed
    
    @Override
    public void begin(String filename) throws IOException {
        File fileToImport = new File(filename);
        String errorFilePath = 
                ( (fileToImport.getParent() == null) ? "" : fileToImport.getParent()) 
                + ERROR_FILE_NAME;
        
        scanner = new Scanner(fileToImport);
        stream = new PrintWriter(new FileWriter(errorFilePath));
        String header = scanner.nextLine();
        if (!header.equals(TEMPLATE_HEADER)) {
            stream.println(header);
            throw new IOException("Header not according to standards.");
        }
    }

    @Override
    public Product readElement() {
        String line = scanner.nextLine();
        String[] attributes = line.split(FILE_SEPARATOR);
        
        if (attributes.length != EXPECTED_NUMBER_OF_PRODUCT_ATTRIBUTES) {
            stream.println(line);
            return null;
        }
        
        Product product;
        try {
            product = new Product(
                    new ProductID(attributes[POSITION_FACT_ID]),
                    new ProductID(attributes[POSITION_COMM_ID]), 
                    Description.valueOf(attributes[POSITION_BRIEF_DESC]),
                    Description.valueOf(attributes[POSITION_FULL_DESC]), 
                    attributes[POSITION_CATEGORY], 
                    UnitOfMeasurement.valueOf(attributes[POSITION_UNIT_MEASURE]));
            
            return product;
            
        } catch (IllegalArgumentException e) {
            stream.println(line);
            return null;
        }
    }

    @Override
    public boolean hasNextElement() {
        return scanner.hasNextLine();
    }

    @Override
    public void end() {
        scanner.close();
        stream.close();
    }

    @Override
    public void cleanup() {
        if (scanner != null) {
            scanner.close();
        }
        if (stream != null) {
            stream.close();
        }
    }
    
}

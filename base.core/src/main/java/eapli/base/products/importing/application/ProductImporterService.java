/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.products.importing.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.products.domain.Product;
import eapli.base.products.repositories.ProductsRepository;
import eapli.framework.domain.repositories.IntegrityViolationException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.PersistenceException;
import javax.persistence.RollbackException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.exception.ConstraintViolationException;

/**
 * Service class that has the template method to import products from a resource (e.g: a file).
 * 
 * @author Francisco Silva on 09/05/2020
 */
public class ProductImporterService {
    
    private static final Logger logger = LogManager.getLogger(ProductImporterService.class);

    private final ProductsRepository productsRepository = PersistenceContext.repositories().products();
    
    /**
     * Import products. This a "template method" working in conjunction with a Strategy.
     * If the {@link ProductImporter} interface had just the importProducts method we would be 
     * repeating the logic of traversing the product list in every implementation!
     *
     * @param filename
     * @param importer implementation of ProductImporter
     * @return iterable of imported products
     * @throws IOException
     */
    public Iterable<Product> importProducts(String filename, ProductImporter importer)
            throws IOException {
        
        List<Product> importedProducts = new LinkedList<>();
        
        try {
            importer.begin(filename);

            while(importer.hasNextElement()) {
                Product product = importer.readElement();
                if (product != null && saveElement(product)) {
                    importedProducts.add(product);
                }
            }

            importer.end();
            return importedProducts;
        } catch (final IOException e) {
            logger.error("Problem importing products", e);
            throw e;
        } finally {
            importer.cleanup();
        }
    }
    
    private boolean saveElement(Product product) {
        try {
            if (!this.productsRepository.containsOfIdentity(product.identity())) {
                this.productsRepository.save(product);
                return true;           
            }
            return false;
        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            return false;
        } catch (final RollbackException e) {
            if (!( e.getCause() instanceof PersistenceException 
                    && e.getCause().getCause() instanceof ConstraintViolationException ) ) {
                
                logger.error("Problem importing product with factory id " + product.identity(), e);
            }
            return false;
        }
    }
    
}

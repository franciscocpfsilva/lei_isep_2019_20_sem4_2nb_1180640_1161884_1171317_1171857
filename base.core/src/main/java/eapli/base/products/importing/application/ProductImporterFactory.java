/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.products.importing.application;

/**
 * Factory to build the correct implementation of ProductImporter interface 
 * according to a file type.
 * 
 * @author Francisco Silva on 09/05/2020
 */
public class ProductImporterFactory {
    
    public ProductImporter build(FileType format) {
        if (format == null) {
            throw new IllegalStateException("Unknown format");
        }
        
        switch (format) {
        case CSV:
            return new ProductImporterCsv();
        }
        throw new IllegalStateException("Unknown format");
    }
    
}

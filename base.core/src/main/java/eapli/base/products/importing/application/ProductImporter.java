/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.products.importing.application;

import eapli.base.products.domain.Product;
import java.io.IOException;

/**
 * The Strategy for importing products from external formats.
 * 
 * @author Francisco Silva on 09/05/2020
 */
public interface ProductImporter {
    
    /**
     * Initiate the import process. The implementation should open the underlying resource (e.g., file) and read the
     * "document start"/"header" for the respective format. It should also create 
     * a resource from the same type for error notification.
     *
     * @param filename
     * @throws java.io.IOException
     */
    void begin(String filename) throws IOException;

    /**
     * Read one single element from the resource. Returns a Product created with 
     * the information read. If not able to create a Product then returns null and 
     * prints that line into the error file.
     * 
     * @return the product read or null if can't create a product
     */
    Product readElement();
    
    /**
     * Indicates if there is another element to reade or if we have reached the 
     * end of the resource.
     * 
     * @return true if there is next element
     */
    boolean hasNextElement();

    /**
     * Indicates that there are no more elements to import. The implementation should create any "document closing"
     * element it might need and close the underlying resource.
     */
    void end();

    /**
     * Gives the importer implementation a chance to cleanup in case some exception has occurred.
     */
    void cleanup();
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.products.importing.application;

import eapli.base.products.domain.Product;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.application.UseCaseController;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import java.io.IOException;
import java.util.Arrays;

/**
 * Controller for Importing Products from resource (e.g: file).
 * 
 * @author Francisco Silva on 09/05/2020
 */
@UseCaseController
public class ImportProductsController {
    
    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final ProductImporterFactory factory = new ProductImporterFactory();
    private final ProductImporterService importSvc = new ProductImporterService();

    public Iterable<Product> importProducts(String filename, FileType format) throws IOException {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER, BaseRoles.PRODUCTION_MANAGER);
        
        final ProductImporter importer = factory.build(format);

        final Iterable<Product> importedProducts = importSvc.importProducts(filename, importer);
        return importedProducts;
    }
    
    public Iterable<FileType> allFileTypes() {
        return Arrays.asList(FileType.values());
    }
    
}

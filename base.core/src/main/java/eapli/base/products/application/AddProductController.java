/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.products.application;

import eapli.base.domain.model.general.UnitOfMeasurement;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.products.domain.ProductID;
import eapli.base.products.domain.Product;
import eapli.base.products.repositories.ProductsRepository;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.application.UseCaseController;
import eapli.framework.general.domain.model.Description;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

/**
 *
 * @author
 */
@UseCaseController
public class AddProductController {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final ProductsRepository productsRepository = PersistenceContext.repositories().products();

    public Product addProduct(final String factory_ID, final String business_ID,
            final String brief_desc, final String full_desc, final String category,
            final UnitOfMeasurement unit_of_measurement) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER,
                BaseRoles.PRODUCTION_MANAGER);

        final Product newProduct = new Product(new ProductID(factory_ID),
                new ProductID(business_ID), Description.valueOf(brief_desc),
                Description.valueOf(full_desc), category, unit_of_measurement);

        return this.productsRepository.save(newProduct);
    }

}

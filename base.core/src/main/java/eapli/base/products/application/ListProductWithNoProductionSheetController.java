/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.products.application;

import eapli.base.products.domain.Product;

/**
 *
 * @author 
 */
public class ListProductWithNoProductionSheetController {
    
    private final ListProductWithNoProductionSheetService svc = new ListProductWithNoProductionSheetService();

    public Iterable<Product> listProductWithNoProductionSheet() {
        return this.svc.allProducts();
    }
}

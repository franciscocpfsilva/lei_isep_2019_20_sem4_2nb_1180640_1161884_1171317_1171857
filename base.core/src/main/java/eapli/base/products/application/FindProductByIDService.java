/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.products.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.products.domain.Product;
import eapli.base.products.domain.ProductID;
import eapli.base.products.repositories.ProductsRepository;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import java.util.Optional;

/**
 *
 * @author joaomachado
 */
public class FindProductByIDService {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final ProductsRepository productsRepository = PersistenceContext.repositories().products();

    /**
     * Method that querys the DB with a factory_ID and returns a Product
     * Object with that ID
     *
     * @param factory_ID Id of Product to search for
     * @return Product Object
     */
    public Optional<Product> findByID(final String factory_ID) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER, BaseRoles.PRODUCTION_MANAGER);

        final Optional<Product> me = productsRepository.findByID(ProductID.valueOf(factory_ID));
        return me;
    }
}

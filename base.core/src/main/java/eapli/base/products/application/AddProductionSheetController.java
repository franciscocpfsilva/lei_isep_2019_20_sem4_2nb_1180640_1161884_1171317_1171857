/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.products.application;

import eapli.base.domain.model.general.Item;
import eapli.base.domain.model.general.Quantity;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.products.domain.Product;
import eapli.base.products.domain.ProductionSheet;
import eapli.base.products.repositories.ProductsRepository;
import eapli.base.rawmaterials.application.FindRawMaterialByIDService;
import eapli.base.rawmaterials.domain.RawMaterial;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.application.UseCaseController;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author joaomachado
 */
@UseCaseController
public class AddProductionSheetController {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final FindProductByIDService svcFindProduct = new FindProductByIDService();
    private final FindRawMaterialByIDService svcFindRawMaterial = new FindRawMaterialByIDService();
    private final ProductsRepository productsRepository = PersistenceContext.repositories().products();

    /**
     * Method that creates a ProductionSheet, adds the productionSheet to a
     * Product and then Updates the Product Object with that ID
     *
     * @param product Prodcut Object to be Updated
     * @param standardQuantity StandardQuantity of the ProductionSheet in Double
     * @param listOfItems List Of Items in a linkedlist to be added
     * @return Product Object
     */
    public Product addProductionSheet(final Product product, final Double standardQuantity, final List<Item> listOfItems) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER, BaseRoles.PRODUCTION_MANAGER);

        final ProductionSheet productionSheet = new ProductionSheet(Quantity.valueOf(standardQuantity, product.unit_of_measurement()), (LinkedList<Item>) listOfItems);

        product.addProductionSheet(productionSheet);

        return this.productsRepository.save(product);
    }

    /**
     * Mehtod that calls the service for creating a query to find a Product with
     * a specific ID
     *
     * @param factory_ID The Id to search for
     * @return Product Object
     */
    public Optional<Product> findProductByID(String factory_ID) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER,
                BaseRoles.PRODUCTION_MANAGER);

        return this.svcFindProduct.findByID(factory_ID);
    }

    /**
     * Method validates if there is in fact a Product or not
     *
     * @param factory_ID The Id to search for
     * @return Product Object
     */
    public Product validateProductByID(String factory_ID) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER,
                BaseRoles.PRODUCTION_MANAGER);

        Optional<Product> me = findProductByID(factory_ID);
        Product product = me.get();

        return product;
    }

    /**
     * Mehtod that calls the service for creating a query to find a Product with
     * a specific ID and adds to the list
     *
     * @param factory_ID The Id to search for
     * @param listOfItems the list os Item to be added
     * @param productQuantity the quantity of the product
     * @return boolean
     */
    public boolean findAndAddProductByID(String factory_ID, List<Item> listOfItems, Double productQuantity) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER,
                BaseRoles.PRODUCTION_MANAGER);

        Optional<Product> p = findProductByID(factory_ID);

        if (!p.isPresent()) {
            System.out.println("\nError: There is no Product with that id.");
            return false;
        }
        return listOfItems.add(Item.valueOf(p.get().identity(), Quantity.valueOf(productQuantity, p.get().unit_of_measurement())));
    }

    /**
     * Mehtod that calls the service for creating a query to find a RawMaterial
     * with a specific ID and adds to the list
     *
     * @param rawMaterialID The Id to search for
     * @param listOfItems the list os Item to be added
     * @param rawMaterialQuantity the quantity of the raw-amterial
     * @return boolean
     */
    public boolean findAndAddRawMaterialByID(String rawMaterialID, List<Item> listOfItems, Double rawMaterialQuantity) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER,
                BaseRoles.PRODUCTION_MANAGER);

        Optional<RawMaterial> r = this.svcFindRawMaterial.findByID(rawMaterialID);

        if (!r.isPresent()) {
            System.out.println("\nError: There is no Raw-Material with that id.");
            return false;
        }
        return listOfItems.add(Item.valueOf(r.get().identity(), Quantity.valueOf(rawMaterialQuantity, r.get().unit_of_measurement())));
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.products.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.products.domain.Product;
import eapli.base.products.repositories.ProductsRepository;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

/**
 *
 * @author 
 */
public class ListProductWithNoProductionSheetService {
    
    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final ProductsRepository productsRepository = PersistenceContext.repositories().products();

    public Iterable<Product> allProducts() {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER, BaseRoles.PRODUCTION_MANAGER);

        return this.productsRepository.findWithNoProductionSheet();
    }
}

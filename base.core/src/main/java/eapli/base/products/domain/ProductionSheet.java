/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.products.domain;

import eapli.base.domain.model.general.Item;
import eapli.base.domain.model.general.Quantity;
import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;
import java.util.LinkedList;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.persistence.Lob;

/**
 *
 * @author
 */
@Embeddable
public class ProductionSheet implements ValueObject, Comparable<ProductionSheet> {

    @Lob
    private final Quantity standardQuantity;

    @Lob
    private LinkedList<Item> items = new LinkedList<>();

    public ProductionSheet(final Quantity standardQuantity, final LinkedList<Item> listOfItems) {
        Preconditions.noneNull(listOfItems, "list Of Items cannot be null or empty");
        Preconditions.noneNull(standardQuantity, "Standard Quantity cannot be null or empty");

        this.standardQuantity = standardQuantity;
        this.items = (LinkedList<Item>) listOfItems.clone();
    }

    /**
     * Empty protected Constructor For ORM
     *
     */
    protected ProductionSheet() {
        standardQuantity = null;
    }

    /**
     * Method that returns the Standart Quantity
     *
     * @return quantity
     */
    public Quantity standardQuantity() {
        return this.standardQuantity;
    }

    /**
     * Method that returns the list of Items in the ProductionSheet
     *
     * @return items LinkedList of items
     */
    public LinkedList<Item> Items() {
        return this.items;
    }

    /**
     * Compare objects
     *
     * @param o ProductionSheet type object
     * @return True or False
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProductionSheet)) {
            return false;
        }

        final ProductionSheet that = (ProductionSheet) o;
        return standardQuantity.equals(that.standardQuantity)
                && items.equals(that.items);
    }

    /**
     * generates HashCode using all parameters
     *
     * @return hash int with the hash code
     */
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Objects.hashCode(this.standardQuantity);
        hash = 59 * hash + Objects.hashCode(this.items);
        return hash;
    }

    /**
     * Compare two objects Item
     *
     * @param o Item Object
     * @return equal, greater or smaller
     */
    @Override
    public int compareTo(ProductionSheet o) {
        if (this.standardQuantity().equals(o.standardQuantity())) {
            return 1;
        } else {
            return 0;
        }
    }
}

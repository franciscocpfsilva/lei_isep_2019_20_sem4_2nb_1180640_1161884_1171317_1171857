/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.products.domain;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.util.HashCoder;
import eapli.framework.validations.Preconditions;
import eapli.framework.strings.util.StringPredicates;
import java.util.regex.Pattern;
import javax.persistence.Embeddable;

/**
 *
 * @author
 */
@Embeddable
public class ProductID implements ValueObject, Comparable<ProductID> {

    private final String ID;

    private static final int MAX_LENGTH_ID = 15;

    /**
     * Full constructor for ProductID
     *
     * @param factoryID Factory ID Mandatory
     */
    public ProductID(final String factoryID) {
        Preconditions.nonNull(factoryID, "The ID cannot be null or empty");
        Preconditions.ensure(!StringPredicates.isNullOrWhiteSpace(factoryID),
                "The ID cannot be just spaces");
        Preconditions.matches(Pattern.compile("^[a-zA-Z0-9]+$"), factoryID,
                "The ID can only have digits and letters");
        Preconditions.ensure(factoryID.length() < MAX_LENGTH_ID,
                "The ID cannot be longer than 15 characters");

        this.ID = factoryID;
    }

    /**
     * Empty constructor for ORM
     */
    protected ProductID() {
        ID = "";
    }

    /**
     * Factory method for obtaining ProductID value objects.
     *
     * @param productID
     * @return a new object corresponding to the value
     */
    public static ProductID valueOf(final String productID) {
        return new ProductID(productID);
    }

    /**
     * Compare objects
     *
     * @param o ProductID type object
     * @return True or False
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProductID)) {
            return false;
        }

        final ProductID that = (ProductID) o;
        return ID.compareTo(that.ID) == 0;
    }

    /**
     * Generate the hashcode
     *
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return new HashCoder().with(ID).code();
    }

    /**
     * Transform ProductID into a String
     *
     * @return string representing the object
     */
    @Override
    public String toString() {
        return this.ID;
    }

    /**
     * Compare two objects ProductID
     *
     * @param o ProductID Object
     * @return equal, greater or smaller
     */
    @Override
    public int compareTo(ProductID o) {
        return this.ID.compareTo(o.ID);
    }

}

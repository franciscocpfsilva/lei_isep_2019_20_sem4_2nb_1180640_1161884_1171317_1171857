/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.products.domain;

import eapli.base.domain.model.general.UnitOfMeasurement;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Version;

import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.general.domain.model.Description;
import eapli.framework.domain.model.DomainEntities;
import eapli.framework.validations.Preconditions;
import eapli.framework.strings.util.StringPredicates;
import javax.persistence.AttributeOverride;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

/**
 * Represents a Product
 *
 * Business rules: - Full description and Brief description can't be null or
 * empty - Brief description can't have more than 30 characters
 *
 * @author
 */
@Entity
public class Product implements AggregateRoot<ProductID> {

    private static final long serialVersionUID = 1L;

    @Version
    private Long version;

    // ORM primary key
    // business ID
    @Id
    @AttributeOverride(name = "ID", column = @Column(name = "Factory_ID"))
    private ProductID factory_ID;

    @AttributeOverride(name = "ID", column = @Column(name = "Business_ID", unique = true, nullable = false))
    private ProductID commercial_ID;
    @AttributeOverride(name = "value", column = @Column(name = "Brief_Description"))
    private Description brief_desc;
    @AttributeOverride(name = "value", column = @Column(name = "Full_Description"))
    private Description full_desc;
    @Column
    private String category;
    @Enumerated(EnumType.STRING)
    private UnitOfMeasurement unit_of_measurement;

    @Column(name = "Production_Sheet")
    private ProductionSheet productionSheet;

    private static final int MAX_LENGTH_BRIEF_DESC = 30;

    /**
     *
     * @param factory_ID - Factory ID Mandatory
     * @param commercial_ID - Commercial ID Mandatory
     * @param brief_desc - Brief description Mandatory
     * @param full_desc - Full description Mandatory
     * @param category - Category Mandatory
     * @param unit_of_measurement - Unit of measurement type
     */
    public Product(final ProductID factory_ID, final ProductID commercial_ID,
            final Description brief_desc, final Description full_desc, final String category,
            final UnitOfMeasurement unit_of_measurement) {

        Preconditions.nonNull(factory_ID, "The Factory ID cannot be null.");
        Preconditions.nonNull(commercial_ID, "The Commercial ID cannot be null.");
        Preconditions.noneNull(category, "Category cannot be null or empty");
        Preconditions.ensure(!StringPredicates.isNullOrWhiteSpace(category),
                "The Category cannot be just spaces");
        Preconditions.noneNull(unit_of_measurement, "You have not selected a unit of measurement");
        Preconditions.noneNull(brief_desc, "Brief Description cannot be null or empty");
        Preconditions.noneNull(full_desc, "Full Description cannot be null or empty");

        this.factory_ID = factory_ID;
        this.commercial_ID = commercial_ID;
        this.setBrief_desc(brief_desc);
        this.full_desc = full_desc;
        this.category = category;
        this.unit_of_measurement = unit_of_measurement;
        this.productionSheet = null;
    }

    /**
     * Empty constructor for ORM
     */
    protected Product() {

    }

    /**
     * Method for adding production sheet to product
     *
     * @param productionSheet ProductionSheet type object
     * @return ProductionSheet object
     */
    public ProductionSheet addProductionSheet(ProductionSheet productionSheet) {
        return this.productionSheet = productionSheet;
    }

    /**
     * Compare objects
     *
     * @param o Product type object
     * @return True or False
     */
    @Override
    public boolean equals(final Object o) {
        return DomainEntities.areEqual(this, o);
    }

    /**
     * Generate the hashcode
     *
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return DomainEntities.hashCode(this);
    }

    /**
     * Compare if the object is the same
     *
     * @param other Product type object
     * @return True of False
     */
    @Override
    public boolean sameAs(final Object other) {
        if (!(other instanceof Product)) {
            return false;
        }

        final Product that = (Product) other;
        if (this == that) {
            return true;
        }

        return identity().equals(that.identity()) && commercial_ID.equals(that.commercial_ID)
                && brief_desc.equals(that.brief_desc) && full_desc.equals(that.full_desc)
                && category.equals(that.category) && unit_of_measurement.equals(that.unit_of_measurement);
    }

    /**
     * Definy the Product identity
     *
     * @return
     */
    @Override
    public ProductID identity() {
        return this.factory_ID;
    }

    public Description shortlyDescribedBy() {
        return this.brief_desc;
    }

    /**
     * Returns the Production Sheet associated with the Product instance.
     *
     * @return Production Sheet
     */
    public ProductionSheet productionSheet() {
        return this.productionSheet;
    }

    /**
     * Method that returns unit_of_measurement
     *
     * @return unit_of_measurement
     */
    public UnitOfMeasurement unit_of_measurement() {
        return this.unit_of_measurement;
    }

    /**
     * State of the Production Sheet
     *
     * @return true, if instance of Product has a production shhet false,
     * otherwise;
     */
    public boolean hasProductionSheet() {
        return this.productionSheet != null;
    }

    /**
     * Valid if the brief description does not exceed 30 characters
     *
     * @param brief_desc brief description
     */
    private void setBrief_desc(final Description brief_desc) {

        Preconditions.ensure(brief_desc.length() < MAX_LENGTH_BRIEF_DESC,
                "Brief description cannot exceed 30 characters");

        this.brief_desc = brief_desc;
    }

}

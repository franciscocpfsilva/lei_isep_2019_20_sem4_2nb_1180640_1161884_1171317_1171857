/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.communication.scm.repositories;

import eapli.base.communication.scm.domain.CommunicationProtocol;
import eapli.base.machinery.domain.CommunicationProtocolID;
import eapli.framework.domain.repositories.DomainRepository;
import java.util.Optional;

/**
 *
 * @author leona
 */
public interface CommunicationProtocolRepository extends DomainRepository<CommunicationProtocolID, CommunicationProtocol>{
    
    /**
     * returns the CommunicationProtocol with the given id
     *
     * @param cpID Communication Protocol ID to search for
     * @return CommunicationProtocol object
     */
    default Optional<CommunicationProtocol> findByID(
            final CommunicationProtocolID cpID) {
        return ofIdentity(cpID);
    }
}
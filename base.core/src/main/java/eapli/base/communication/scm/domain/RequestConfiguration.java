/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.communication.scm.domain;

import eapli.base.machinery.domain.CommunicationProtocolID;
import eapli.base.machinery.domain.ConfigurationFile;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.DomainEntities;
import eapli.framework.validations.Preconditions;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;

/**
 * Class that tries to represent the concept of a request for a configuration file
 * to be sent to a machine.
 * @author leona
 */
@Entity
public class RequestConfiguration implements AggregateRoot<Long>{

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue // (strategy = GenerationType.IDENTITY)
    private Long pk;
    @Version
    private Long version;
    
    @Column(nullable = false)
    private CommunicationProtocolID communicationProtocolID;
    
    private ConfigurationFile configurationFile;
    
    /**
     * RequestConfiguration constructor.
     *
     * @param cpID
     * @param configFile
     */
    public RequestConfiguration(final CommunicationProtocolID cpID, ConfigurationFile configFile){
        Preconditions.noneNull(cpID, configFile);
        this.communicationProtocolID=cpID;
        this.configurationFile=configFile;
    }
    protected RequestConfiguration(){
        // for ORM
    }
    
    public CommunicationProtocolID communicationProtocolID(){
        return this.communicationProtocolID;
    }
    public ConfigurationFile configurationFile(){
        return this.configurationFile;
    }
    
    @Override
    public boolean sameAs(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof RequestConfiguration)) {
            return false;
        }
        final RequestConfiguration otherRequest = (RequestConfiguration) other;
        return this.equals(otherRequest) 
                && this.communicationProtocolID.equals(otherRequest.communicationProtocolID) 
                && this.configurationFile.equals(otherRequest.configurationFile);
    }

    @Override
    public int hashCode() {
        return DomainEntities.hashCode(this);
    }

    @Override
    public boolean equals(final Object o) {
        return DomainEntities.areEqual(this, o);
    }

    @Override
    public Long identity() {
        return this.pk;

    }
    
}

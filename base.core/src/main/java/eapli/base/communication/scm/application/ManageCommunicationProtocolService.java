/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.communication.scm.application;

import eapli.base.communication.scm.domain.CommunicationProtocol;
import eapli.base.communication.scm.repositories.CommunicationProtocolRepository;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.machinery.domain.CommunicationProtocolID;
import eapli.base.machinery.domain.Machine;
import eapli.base.machinery.repositories.MachinesRepository;
import eapli.framework.domain.repositories.IntegrityViolationException;
import java.util.Optional;
import javax.persistence.PersistenceException;
import javax.persistence.RollbackException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.exception.ConstraintViolationException;

/**
 *
 * @author leona
 */
public class ManageCommunicationProtocolService {
    
    private static final Logger LOGGER = LogManager.getLogger(ManageCommunicationProtocolService.class);
    private final CommunicationProtocolRepository protocolsRepo = PersistenceContext.repositories().communicationProtocols();
    private final MachinesRepository machinesRepo = PersistenceContext.repositories().machines();
    
    
    public CommunicationProtocol findByID(final int id){
        Optional<CommunicationProtocol> protocol = protocolsRepo.findByID(CommunicationProtocolID.valueOf(id));
        return protocol.isPresent() ? protocol.get() : null;
    }
//    
    public void importAllCommunicationProtocolsFromMachineRepo(){
        
        for( Machine machine: machinesRepo.findAll() ){
            CommunicationProtocolID tmp = CommunicationProtocolID.valueOf(Integer.parseInt(machine.communicationProtocolID().toString()));
            if (!protocolsRepo.containsOfIdentity(tmp)){
                CommunicationProtocol cp = new CommunicationProtocol(tmp);
                saveElement(cp);
            }//else
//                System.out.println("Error: Protocol Id already registered!");
        }
    }
    public void importCommunicationProtocol(CommunicationProtocol protocol){
        saveElement(protocol);
    }
    
    private boolean saveElement(CommunicationProtocol protocol) {
        
        try {
//            if (!this.protocolsRepo.contains(protocol)){
                this.protocolsRepo.save(protocol);
                return true;           
//            }
//            LOGGER.error("Error: Protocol already existes in the DB");
//            return false;
        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            return false;
        } catch (final RollbackException e) {
            if (!( e.getCause() instanceof PersistenceException 
                    && e.getCause().getCause() instanceof ConstraintViolationException ) ) {
                
                LOGGER.error("Problem importing communication protocol with id: " + protocol.communicationProtocolID().communicationProtocolID(), e);
            }
            return false;
        }
    }
    
}

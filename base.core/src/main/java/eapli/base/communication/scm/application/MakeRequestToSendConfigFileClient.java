/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.communication.scm.application;

import eapli.base.machinery.domain.ConfigurationFile;
import eapli.base.machinery.domain.MachineID;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

/**
 *
 * @author leona
 */
public class MakeRequestToSendConfigFileClient {
    
//    static int PORT_N=31401;
    public static int PORT_N=9999;
    public static String IP_ADDRESS="127.0.0.1";
    public static int SO_TIMEOUT = 10;
    
    static InetAddress serverIP;
    static Socket sock;
    private DataOutputStream sOut;
    private DataInputStream sIn;
    
    public void makeRequest(MachineID machineID, ConfigurationFile configFile){
        
        try{
            try { 
                serverIP = InetAddress.getByName(IP_ADDRESS); 
            }catch(UnknownHostException ex) {
                System.out.println("Invalid client specified: " + IP_ADDRESS);
                    return;
            }

            try { 
                sock = new Socket(serverIP, PORT_N); 
                sock.setSoTimeout(SO_TIMEOUT);
            }catch(SocketTimeoutException ste){
                System.out.println("Error: Timeout expired!");
                return;
            }catch(IOException ex) {
                System.out.println("\nFailed to establish TCP connection");
    //                    System.exit(1);
                return;
            }

            sOut = new DataOutputStream(sock.getOutputStream());
            sIn = new DataInputStream(sock.getInputStream());
            
            String request = machineID.machineID() + ";" + configFile.configurationFileDescription().toString();
            writeRequest(sOut, request);
            System.out.println("\nSuccessfully sent config request to the SCM");
            
            String reply = readReply(sIn);
            System.out.println("Reply from server: " + reply);
            
            sock.close();
            
        }catch(IOException ioe){
            System.out.println("\nError: Unable to send config request to SCM - " + ioe.getMessage()); 
        }        
       
    }
    
    private void writeRequest(DataOutputStream dataOutputStream, String request) throws IOException{
            dataOutputStream.writeInt(request.length());
            dataOutputStream.writeBytes(request);
    }
    
    private String readReply(DataInputStream dataInputStream) throws IOException{
        int length = dataInputStream.readInt();
        String reply ="";
        for (int i=0; i<length; i++)
            reply = reply + (char) dataInputStream.read();
        return reply;
    }
    
    public static void changeServerIpAddress(String newIpAddress){
        IP_ADDRESS = newIpAddress;
    }
}

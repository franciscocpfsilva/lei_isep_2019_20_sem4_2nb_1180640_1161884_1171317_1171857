/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.communication.scm.domain;

import eapli.base.machinery.domain.CommunicationProtocolID;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.DomainEntities;
import eapli.framework.validations.Preconditions;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Version;

/**
 * Class that tries to represent the concept of communication protocol,
 * in the context of this application
 * @author leona
 */
@Entity
public class CommunicationProtocol implements AggregateRoot<CommunicationProtocolID>{

    private static final long serialVersionUID = 1L;
    
    @Version
    private Long version;
    
    @EmbeddedId
    @Column(nullable = false)
    private CommunicationProtocolID communicationProtocolID;
    
    private String ipAddress;
    
    /**
     * RawMaterialCategory constructor.
     *
     * @param cpID
     */
    public CommunicationProtocol(final CommunicationProtocolID cpID){
        Preconditions.noneNull(cpID);
        this.communicationProtocolID = cpID;
        this.ipAddress="";
    }
    protected CommunicationProtocol(){
        // for ORM
    }
    
    public CommunicationProtocolID communicationProtocolID(){
        return this.communicationProtocolID;
    }
    public String ipAddress(){
        return this.ipAddress;
    }
    public void addIpAddress(String newIpAddress){
        this.ipAddress = newIpAddress;
    }
    
    @Override
    public boolean sameAs(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof CommunicationProtocol)) {
            return false;
        }
        final CommunicationProtocol otherCP = (CommunicationProtocol) other;
        return this.equals(otherCP) && this.communicationProtocolID.equals(otherCP.communicationProtocolID) && this.ipAddress.equals(otherCP.ipAddress);
    }

    @Override
    public int hashCode() {
        return DomainEntities.hashCode(this);
    }

    @Override
    public boolean equals(final Object o) {
        return DomainEntities.areEqual(this, o);
    }

    @Override
    public CommunicationProtocolID identity() {
        return this.communicationProtocolID;

    }
    
}

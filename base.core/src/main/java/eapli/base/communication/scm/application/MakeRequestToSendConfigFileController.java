/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.communication.scm.application;

import eapli.base.communication.scm.domain.RequestConfiguration;
import eapli.base.communication.scm.repositories.RequestConfigurationRepository;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.machinery.application.FindMachineAvailableService;
import eapli.base.machinery.domain.CommunicationProtocolID;
import eapli.base.machinery.domain.ConfigurationFile;
import eapli.base.machinery.domain.Machine;
import eapli.base.machinery.domain.MachineID;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.application.UseCaseController;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

/**
 *
 * @author leona
 */
@UseCaseController
public class MakeRequestToSendConfigFileController {
    
    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final RequestConfigurationRepository requestConfigRepository = PersistenceContext.repositories().requestsConfiguration();
    private final FindMachineAvailableService service = new FindMachineAvailableService();
    private final MakeRequestToSendConfigFileClient makeRequestClient = new MakeRequestToSendConfigFileClient();
    
    /**
     * Method which creates a new instance of RequestConfiguration and saves it in the database.
     *
     * @param protocolID communication protocol ID of the machine to which to send the config. file
     * @param configFile configuration file to send
     * @return
     */
    public RequestConfiguration addRequestToSendConfigFile(CommunicationProtocolID protocolID, ConfigurationFile configFile) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER, BaseRoles.SHOP_FLOOR_MANAGER);

        final RequestConfiguration newRequest = new RequestConfiguration(protocolID, configFile);
        return requestConfigRepository.save(newRequest);
    }
    
    /**
     * Method which creates a new instance of RequestConfiguration and saves it in the database.
     *
     * @param machineID machine ID to which to send the configuration file
     * @param configFile configuration file to send
     */
    public void makeRequestToSendConfigFile(MachineID machineID, ConfigurationFile configFile) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER, BaseRoles.SHOP_FLOOR_MANAGER);
        makeRequestClient.makeRequest(machineID, configFile);
    }
    
    public Iterable<Machine> allMachines() {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER, BaseRoles.SHOP_FLOOR_MANAGER);
        return this.service.allAvailableMachines();
    }
}

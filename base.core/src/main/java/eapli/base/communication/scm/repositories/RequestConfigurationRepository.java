/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.communication.scm.repositories;

import eapli.base.communication.scm.domain.RequestConfiguration;
import eapli.framework.domain.repositories.DomainRepository;

/**
 *
 * @author leona
 */
public interface RequestConfigurationRepository extends DomainRepository<Long, RequestConfiguration>{
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.productionline.domain;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.util.HashCoder;
import eapli.framework.validations.Preconditions;
import eapli.framework.strings.util.StringPredicates;
import java.util.regex.Pattern;
import javax.persistence.Embeddable;

/**
 *
 * @author
 */
@Embeddable
public class ProductionLineID implements ValueObject, Comparable<ProductionLineID> {

    private final String ID;

    private static final int MAX_LENGTH_ID = 15;

    /**
     * Full constructor for ProductionLineID
     *
     * @param productionLineID Production Line ID Mandatory
     */
    public ProductionLineID(final String productionLineID) {
        Preconditions.nonNull(productionLineID, "The ID cannot be null or empty");
        Preconditions.ensure(!StringPredicates.isNullOrWhiteSpace(productionLineID),
                "The ID cannot be just spaces");
        Preconditions.matches(Pattern.compile("^[a-zA-Z0-9]+$"), productionLineID,
                "The ID can only have digits and letters");
        Preconditions.ensure(productionLineID.length() < MAX_LENGTH_ID,
                "The ID cannot be longer than 15 characters");

        this.ID = productionLineID;
    }

    /**
     * Empty constructor for ORM
     */
    protected ProductionLineID() {
        ID = "";
    }

    /**
     * Compare objects
     *
     * @param o ProductionLineID type object
     * @return True or False
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProductionLineID)) {
            return false;
        }

        final ProductionLineID that = (ProductionLineID) o;
        return ID.compareTo(that.ID) == 0;
    }

    /**
     * Factory method for obtaining ProductionLineID value objects.
     *
     * @param productionLineID
     * @return a new object corresponding to the value
     */
    public static ProductionLineID valueOf(final String productionLineID) {
        return new ProductionLineID(productionLineID);
    }

    /**
     * Generate the hashcode
     *
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return new HashCoder().with(ID).code();
    }

    /**
     * Transform ProductionLineID into a String
     *
     * @return string representing the object
     */
    @Override
    public String toString() {
        return this.ID;
    }

    /**
     * Compare two objects ProductionLineID
     *
     * @param o ProductionLineID Object
     * @return equal, greater or smaller
     */
    @Override
    public int compareTo(ProductionLineID o) {
        return this.ID.compareTo(o.ID);
    }
}

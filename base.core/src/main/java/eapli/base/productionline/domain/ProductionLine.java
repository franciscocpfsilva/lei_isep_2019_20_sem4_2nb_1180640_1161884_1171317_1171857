/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.productionline.domain;

import eapli.base.machinery.domain.Machine;
import eapli.base.machinery.domain.MachineID;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.DomainEntities;
import eapli.framework.validations.Preconditions;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;

/**
 *
 * @author
 */
@Entity
public class ProductionLine implements AggregateRoot<ProductionLineID> {

    private static final long serialVersionUID = 1L;

    @Version
    private Long version;

    // ORM primary key
    // business ID
    @Id
    @AttributeOverride(name = "ID", column = @Column(name = "ProductionLineID"))
    private ProductionLineID productionLine_ID;

    @OneToMany(cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<Machine> sequencia;

    /**
     *
     * @param productionLineID - Production Line ID Mandatory
     */
    public ProductionLine(final ProductionLineID productionLineID) {

        Preconditions.nonNull(productionLineID, "The Production Line ID cannot be null.");

        this.productionLine_ID = productionLineID;
        sequencia = new LinkedList<>();

    }

    /**
     * Empty constructor for ORM
     */
    protected ProductionLine() {

    }

    /**
     * Try add a machine into a given position, if that position doesn't exit
     * adds in the end of the list
     *
     * @param position chosen position
     * @param machine Machine
     * @return the sucess of operation
     */
    public boolean addMachineToSequence(int position, Machine machine) {
        if (!sequencia.contains(machine)) {
            try {
                if (machine == null) {
                    throw new IllegalArgumentException();
                }
                if (position < 0) {
                    throw new NumberFormatException();
                }
                sequencia.add(position, machine);
            } catch (IndexOutOfBoundsException e) {
                sequencia.add(machine);
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Return the Production Line ID
     *
     * @return production line ID
     */
    public ProductionLineID getProductionLine_ID() {
        return productionLine_ID;
    }

    /**
     * Checks if you have the machine instance in its sequence
     *
     * @param machineID Machine id
     * @return true or false
     */
    public boolean constainsMachine(MachineID machineID) {
        for (Machine machine : sequencia) {
            if (machine.identity().equals(machineID)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the list of machines that belong to the sequence
     *
     * @return list of machines id
     */
    public List<MachineID> getSequencia() {
        List<MachineID> lst = new LinkedList<>();
        for (Machine machine : sequencia) {
            lst.add(machine.identity());
        }
        return lst;
    }

    /**
     * Compare objects
     *
     * @param o ProductionLine type object
     * @return True or False
     */
    @Override
    public boolean equals(final Object o) {
        return DomainEntities.areEqual(this, o);
    }

    /**
     * Generate the hashcode
     *
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return DomainEntities.hashCode(this);
    }

    /**
     * Compare if the object is the same
     *
     * @param other ProductionLine type object
     * @return True of False
     */
    @Override
    public boolean sameAs(final Object other) {
        if (!(other instanceof ProductionLine)) {
            return false;
        }

        final ProductionLine that = (ProductionLine) other;
        if (this == that) {
            return true;
        }

        return identity().equals(that.identity());
    }

    /**
     * Definy the ProductionLine identity
     *
     * @return
     */
    @Override
    public ProductionLineID identity() {
        return this.productionLine_ID;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.productionline.repositories;

import eapli.base.machinery.domain.Machine;
import eapli.base.machinery.domain.MachineID;
import eapli.base.productionline.domain.ProductionLine;
import eapli.base.productionline.domain.ProductionLineID;
import eapli.framework.domain.repositories.DomainRepository;
import java.util.Optional;

/**
 *
 * @author
 */
public interface ProductionLinesRepository extends DomainRepository<ProductionLineID, ProductionLine> {

    /**
     * returns the Production Line with the given machine id
     *
     * @param machineID the Machine id to search
     * @return Production Line
     */
    ProductionLine findByMachineID(final MachineID machineID);

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.productionline.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.machinery.application.FindMachineAvailableService;
import eapli.base.machinery.domain.Machine;
import eapli.base.productionline.domain.ProductionLine;
import eapli.base.productionline.domain.ProductionLineID;
import eapli.base.productionline.repositories.ProductionLinesRepository;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.application.UseCaseController;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import java.util.Optional;

/**
 *
 * @author
 */
@UseCaseController
public class AddProductionLineController {
    
    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final ProductionLinesRepository productionLinesRepository
            = PersistenceContext.repositories().productionLines();
    private final FindMachineAvailableService serv = new FindMachineAvailableService();

    /**
     * Saves the new or the changes to a Production Line
     *
     * @param productionLine_ID Production Line ID
     * @return the object saver
     */
    public ProductionLine addProductionLine(final String productionLine_ID) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER,
                BaseRoles.SHOP_FLOOR_MANAGER);
        
        final ProductionLine newProductionLine = new ProductionLine(createProductionLineID(productionLine_ID));
        
        return this.productionLinesRepository.save(newProductionLine);
    }

    /**
     * Create a value object Production Line ID
     *
     * @param productionLine_ID production Line ID
     * @return the Production Line ID
     */
    private ProductionLineID createProductionLineID(final String productionLine_ID) {
        return new ProductionLineID(productionLine_ID);
    }

    /**
     * Return the list of available machines
     *
     * @return List of available machines
     */
    public Iterable<Machine> findMachinesAvailable() {
        
        return serv.allAvailableMachines();
    }

    /**
     * Return the Production Line given an ID
     *
     * @param productionLineID Production Line ID
     * @return
     */
    public Optional<ProductionLine> findProductionLineByID(ProductionLineID productionLineID) {
        
        return this.productionLinesRepository.ofIdentity(productionLineID);
    }

    /**
     * Save changes to the production line
     *
     * @param productionLine Production Line Object
     * @return Porduction Line
     */
    public ProductionLine saveProductionLine(ProductionLine productionLine) {
        
        return this.productionLinesRepository.save(productionLine);
    }

    /**
     * Add a Machine to the list of machines
     *
     * @param position position to insert the machine
     * @param machine machine
     * @param productionLine production Line
     * @return sucess (true) failure(false)
     */
    public boolean addMachineToSequence(int position, Machine machine, ProductionLine productionLine) {
        return productionLine.addMachineToSequence(position, machine);
    }
    
}

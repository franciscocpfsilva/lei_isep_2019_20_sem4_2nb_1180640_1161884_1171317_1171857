/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.communication.scm.domain;

import eapli.base.machinery.domain.CommunicationProtocolID;
import org.junit.Test;

/**
 *
 * @author leona
 */
public class CommunicationProtocolTest {
    
    private static final CommunicationProtocolID COMMUNICATION_PROTOCOL_ID = CommunicationProtocolID.valueOf(1);
    private static final String IP_ADDRESS = "10.8.207.33";
    
    @Test
    public void ensureCommunicationProtocolWithAllAttributes() {
        System.out.println("Communicatioin Protocol with all valid attributes");
        CommunicationProtocol cp = new CommunicationProtocol(COMMUNICATION_PROTOCOL_ID);
    }
    
    @Test
    public void ensureIsAbleToAddIpAddress() {
        System.out.println("Adds ip address");
        CommunicationProtocol cp = new CommunicationProtocol(COMMUNICATION_PROTOCOL_ID);
        cp.addIpAddress(IP_ADDRESS);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureCommunicationProtocolIDCanNotBeNull() {
        System.out.println("Communication Protocol must have a Communication Protocol ID");
        CommunicationProtocol cp = new CommunicationProtocol(null);
    }
}

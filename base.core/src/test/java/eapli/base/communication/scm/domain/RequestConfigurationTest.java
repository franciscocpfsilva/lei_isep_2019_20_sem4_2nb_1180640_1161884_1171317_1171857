/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.communication.scm.domain;

import eapli.base.machinery.domain.CommunicationProtocolID;
import eapli.base.machinery.domain.ConfigurationFile;
import eapli.framework.general.domain.model.Description;
import org.junit.Test;


/**
 *
 * @author leona
 */
public class RequestConfigurationTest {
    
    private static final CommunicationProtocolID COMMUNICATION_PROTOCOL_ID = CommunicationProtocolID.valueOf(1);
    private static final Description CONFIGURATION_FILE_DESCRIPTION = Description.valueOf("Ficheiro com broca 3mm");
    private static final byte M_FILE[] = new byte[]{1, 6, 3};
    private static final ConfigurationFile CONFIG_FILE = ConfigurationFile.valueOf(CONFIGURATION_FILE_DESCRIPTION, M_FILE);
    
    
    @Test
    public void ensureRequestConfigurationWithAllAttributes() {
        System.out.println("Request Configuratioin with all valid attributes");
        RequestConfiguration request = new RequestConfiguration(COMMUNICATION_PROTOCOL_ID, CONFIG_FILE);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureCommunicationProtocolIDCanNotBeNull() {
        System.out.println("Request Configuratioin must have a Communication Protocol ID");
        RequestConfiguration request = new RequestConfiguration(null, CONFIG_FILE);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureConfigurationFileCanNotBeNull() {
        System.out.println("Request Configuratioin must have a Configuration File");
        RequestConfiguration request = new RequestConfiguration(COMMUNICATION_PROTOCOL_ID, null);
    }
}

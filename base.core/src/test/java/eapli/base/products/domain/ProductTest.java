/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.products.domain;

import eapli.base.domain.model.general.UnitOfMeasurement;
import eapli.framework.general.domain.model.Description;
import org.junit.Test;

/**
 *
 * @author
 */
public class ProductTest {

    private static final ProductID FACTORY_ID = new ProductID("0123456789");
    private static final ProductID BUSINESS_ID = new ProductID("123456");
    private static final Description BRIEF_DESC = Description.valueOf("RolhaA");
    private static final Description FULL_DESC = Description.valueOf("Rolha de qualidade A");
    private static final String CATEGORY = "AS-MS";
    private static final UnitOfMeasurement UNIT = UnitOfMeasurement.UN;

    @Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowedFactoryID() {
        Product instance = new Product(null, BUSINESS_ID, BRIEF_DESC, FULL_DESC,
                CATEGORY, UNIT);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowedBusinessId() {
        Product instance = new Product(FACTORY_ID, null, BRIEF_DESC, FULL_DESC,
                CATEGORY, UNIT);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowedBriefDescription() {
        Product instance = new Product(FACTORY_ID, BUSINESS_ID, null, FULL_DESC,
                CATEGORY, UNIT);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowedFullDescription() {
        Product instance = new Product(FACTORY_ID, BUSINESS_ID, BRIEF_DESC, null,
                CATEGORY, UNIT);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowedCategory() {
        Product instance = new Product(FACTORY_ID, BUSINESS_ID, BRIEF_DESC, FULL_DESC,
                null, UNIT);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureEmptyIsNotAllowedCategory() {
        Product instance = new Product(FACTORY_ID, BUSINESS_ID, BRIEF_DESC, FULL_DESC,
                "", UNIT);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowedUnitOfMeasurement() {
        Product instance = new Product(FACTORY_ID, BUSINESS_ID, BRIEF_DESC, FULL_DESC,
                CATEGORY, null);
    }

    /* 
     */
    @Test(expected = IllegalArgumentException.class)
    public void briefDescrNotExceedThirtyInSize() {
        final Description brief_descrA = Description.valueOf("RolhaA de qualidade superior a RolhaB");

        Product instance = new Product(FACTORY_ID, FACTORY_ID, brief_descrA, FULL_DESC,
                CATEGORY, UNIT);
    }

}

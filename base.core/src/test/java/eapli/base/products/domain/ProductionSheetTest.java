/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.products.domain;

import eapli.base.domain.model.general.Item;
import eapli.base.domain.model.general.Quantity;
import eapli.base.domain.model.general.UnitOfMeasurement;
import eapli.base.rawmaterials.domain.RawMaterialID;
import java.util.LinkedList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author joaomachado
 */
public class ProductionSheetTest {

    private static final LinkedList<Item> items = new LinkedList<>();
    private static final UnitOfMeasurement UNIT = UnitOfMeasurement.UN;
    private static final Quantity STANDARD_QUANTITY = Quantity.valueOf(10.0, UNIT);

    @Test
    public void ensureProductionSheetWithIdNameAndQuantity() {

        items.add(Item.valueOf(RawMaterialID.valueOf("T001"), STANDARD_QUANTITY));
        ProductionSheet instance = new ProductionSheet(STANDARD_QUANTITY, items);
        assertTrue(true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowedListOfItems() {
        ProductionSheet instance = new ProductionSheet(STANDARD_QUANTITY, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowedQuantity() {
        ProductionSheet instance = new ProductionSheet(null, items);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureQuantityIsPositive() {
        Quantity quantity = Quantity.valueOf(-10.0, UNIT);
        ProductionSheet instance = new ProductionSheet(quantity, items);
    }

    @Test
    public void addItemToSheet() {
        assertTrue(items.isEmpty());
        assertEquals(0, items.size());

        items.add(Item.valueOf(RawMaterialID.valueOf("T001"), STANDARD_QUANTITY));

        assertFalse(items.isEmpty());
        assertEquals(1, items.size());

    }

}

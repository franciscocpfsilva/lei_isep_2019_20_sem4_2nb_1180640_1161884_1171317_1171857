/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.products.domain;

import org.junit.Test;

/**
 *
 * @author
 */
public class ProductIDTest {

    @Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowed() {
        ProductID instance = new ProductID(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureEmptyIsNotAllowed() {
        ProductID instance = new ProductID("");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void factoryIDDoesNotExceedFifteenInSize() {
        ProductID instance = new ProductID("01239120ASJDA019312");
    }

    @Test(expected = IllegalArgumentException.class)
    public void factoryIDIsNotASingleWord() {
        ProductID instance = new ProductID("A123 0123D ");
    }

    @Test(expected = IllegalArgumentException.class)
    public void factoryIDDoesNotContainSpecialCharacters() {
        ProductID instance = new ProductID("A123^0123D ");
    }
}

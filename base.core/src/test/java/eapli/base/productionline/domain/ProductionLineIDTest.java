/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.productionline.domain;


import org.junit.Test;

/**
 *
 * @author 
 */
public class ProductionLineIDTest {
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowed() {
        ProductionLineID instance = new ProductionLineID(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureEmptyIsNotAllowed() {
        ProductionLineID instance = new ProductionLineID("");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void productionLineIDDoesNotExceedFifteenInSize() {
        ProductionLineID instance = new ProductionLineID("ASDDSASAD0ASJDA019312");
    }

    @Test(expected = IllegalArgumentException.class)
    public void productionLineIDIsNotASingleWord() {
        ProductionLineID instance = new ProductionLineID("A123 0123D ");
    }

    @Test(expected = IllegalArgumentException.class)
    public void productionLineIDDoesNotContainSpecialCharacters() {
        ProductionLineID instance = new ProductionLineID("A123^0123D ");
    }
    
}

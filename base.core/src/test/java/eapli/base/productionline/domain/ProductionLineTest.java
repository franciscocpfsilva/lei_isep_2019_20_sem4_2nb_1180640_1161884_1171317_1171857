/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.productionline.domain;

import eapli.base.machinery.domain.CommunicationProtocolID;
import eapli.base.machinery.domain.Machine;
import eapli.base.machinery.domain.MachineID;
import eapli.framework.general.domain.model.Description;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author 
 */
public class ProductionLineTest {
    
    private static final MachineID MACHINE_ID = MachineID.valueOf("M001");
    private static final Description SERIAL_NUMBER = Description.valueOf("AYX123");
    private static final Description MACHINE_DESCRIPTION = Description.valueOf("Maquina de Recolha");
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    private LocalDate INSTALLATION_DATE = LocalDate.parse("05-05-2020", formatter);
    private static final Description BRAND = Description.valueOf("YAMAHA");
    private static final Description MODEL = Description.valueOf("Cortadora3000");
    private static final CommunicationProtocolID COMMUNICATION_PROTOCOL_ID = CommunicationProtocolID.valueOf(1);
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowedProductionLineID() {
        ProductionLine instance = new ProductionLine(null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void addNullMachineNotAllowed() {
        ProductionLine instance = new ProductionLine(new ProductionLineID("555"));
        instance.addMachineToSequence(0, null);
    }
    
    @Test(expected = NumberFormatException.class)
    public void addNegativePositionNotAllowed() {
        ProductionLine instance = new ProductionLine(new ProductionLineID("555"));
        instance.addMachineToSequence(-12, new Machine(MACHINE_ID, SERIAL_NUMBER, 
                MACHINE_DESCRIPTION, INSTALLATION_DATE, BRAND, MODEL, COMMUNICATION_PROTOCOL_ID));
    }
    
    @Test
    public void addMachineAlreadyExistsInTheListNotAllowed() {
        ProductionLine instance = new ProductionLine(new ProductionLineID("555"));
        assertTrue(instance.addMachineToSequence(1, new Machine(MACHINE_ID, SERIAL_NUMBER, 
                MACHINE_DESCRIPTION, INSTALLATION_DATE, BRAND, MODEL, COMMUNICATION_PROTOCOL_ID)));
        assertFalse(instance.addMachineToSequence(0, new Machine(MACHINE_ID, SERIAL_NUMBER, 
                MACHINE_DESCRIPTION, INSTALLATION_DATE, BRAND, MODEL, COMMUNICATION_PROTOCOL_ID)));
    }
    
    @Test
    public void addMachineSucessfully() {
        ProductionLine instance = new ProductionLine(new ProductionLineID("555"));
        assertTrue(instance.addMachineToSequence(1, new Machine(MACHINE_ID, SERIAL_NUMBER, 
                MACHINE_DESCRIPTION, INSTALLATION_DATE, BRAND, MODEL, COMMUNICATION_PROTOCOL_ID)));
    }
}

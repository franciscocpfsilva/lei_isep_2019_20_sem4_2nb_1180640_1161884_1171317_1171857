/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.productionorderexecution.domain;

import org.junit.Test;

/**
 *
 * @author João Cunha
 */
public class BatchTest {

    @Test(expected = IllegalArgumentException.class)
    public void ensureIDNullIsNotAllowed() {
        Batch instance = Batch.valueOf(null);
    }
}

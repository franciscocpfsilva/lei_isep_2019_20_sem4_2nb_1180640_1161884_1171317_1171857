/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.productionorderexecution.domain;

import eapli.base.domain.model.general.Item;
import eapli.base.domain.model.general.Quantity;
import static eapli.base.domain.model.general.UnitOfMeasurement.UN;
import eapli.base.products.domain.ProductID;
import eapli.base.storage.domain.StorageUnitID;
import org.junit.Test;

/**
 *
 * @author João Cunha
 */
public class ProductionDeliveryTest {

    StorageUnitID storageID = StorageUnitID.valueOf("S340");
    Item item = Item.valueOf(ProductID.valueOf("A123"), Quantity.valueOf(32.3, UN));
    Batch lot = Batch.valueOf("L123");

    @Test(expected = IllegalArgumentException.class)
    public void ensureProductionNullIsNotAllowed() {
        ProductionDelivery instance = ProductionDelivery.valueOf(null, storageID);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.productionorderexecution.domain;

import eapli.base.domain.model.general.Item;
import eapli.base.domain.model.general.Quantity;
import static eapli.base.domain.model.general.UnitOfMeasurement.UN;
import static eapli.base.productionorderexecution.domain.DesviationType.PRODUCTION;
import eapli.base.products.domain.ProductID;
import org.junit.Test;

/**
 *
 * @author João Cunha
 */
public class DesviationTest {

    Item item = Item.valueOf(ProductID.valueOf("A123"), Quantity.valueOf(32.3, UN));
        
    @Test(expected = IllegalArgumentException.class)
    public void ensureItemNullIsNotAllowed() {
        Desviation instance = Desviation.valueOf(null, PRODUCTION);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureDesviationTypeNullIsNotAllowed() {
        Desviation instance = Desviation.valueOf(item, null);
    }

}

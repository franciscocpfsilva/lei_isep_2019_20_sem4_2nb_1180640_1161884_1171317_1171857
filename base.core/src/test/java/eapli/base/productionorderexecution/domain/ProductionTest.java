/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.productionorderexecution.domain;

import eapli.base.domain.model.general.Item;
import eapli.base.domain.model.general.Quantity;
import eapli.base.domain.model.general.UnitOfMeasurement;
import eapli.base.products.domain.ProductID;
import eapli.base.storage.domain.StorageUnitID;
import org.junit.Test;

/**
 *
 * @author João Cunha
 */
public class ProductionTest {

    Batch lot = Batch.valueOf("L123");
    ProductID productID = ProductID.valueOf("P1001");
    Quantity quantity = Quantity.valueOf(26.0, UnitOfMeasurement.UN);
    Item item = Item.valueOf(productID, quantity);
    StorageUnitID storageID = StorageUnitID.valueOf("ST02");

    @Test(expected = IllegalArgumentException.class)
    public void ensureItemNullIsNotAllowed() {
        Production instance = Production.valueOf(lot, null, storageID);
    }

}

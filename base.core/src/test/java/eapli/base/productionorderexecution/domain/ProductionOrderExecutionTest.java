/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.productionorderexecution.domain;

import eapli.base.domain.model.general.Item;
import eapli.base.domain.model.general.Quantity;
import static eapli.base.domain.model.general.UnitOfMeasurement.UN;
import eapli.base.productionline.domain.ProductionLineID;
import eapli.base.productionorder.domain.ProductionOrderID;
import eapli.base.products.domain.ProductID;
import eapli.base.products.domain.ProductionSheet;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.junit.Test;

/**
 *
 * @author João Cunha
 */
public class ProductionOrderExecutionTest {

    ProductionOrderID productionOrderID = ProductionOrderID.valueOf("PO100");
    ProductionLineID productionLineID = new ProductionLineID("P003");
    LinkedList<Item> lst_itens = new LinkedList();
    ProductionSheet prodSheet = new ProductionSheet(Quantity.valueOf(50.0, UN), lst_itens);

    @Test(expected = IllegalArgumentException.class)
    public void ensureProductionOrderIdNullIsNotAllowed() {
        ProductionOrderExecution instance = new ProductionOrderExecution(null,
                productionLineID, prodSheet);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureProductionLineIDNullIsNotAllowed() {
        ProductionOrderExecution instance = new ProductionOrderExecution(productionOrderID,
                null, prodSheet);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureAddProductionNullIsNotAllowed() {
        ProductionOrderExecution instance = new ProductionOrderExecution(productionOrderID,
                productionLineID, prodSheet);
        instance.addToProduction(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureAddConsumptionNullIsNotAllowed() {
        ProductionOrderExecution instance = new ProductionOrderExecution(productionOrderID,
                productionLineID, prodSheet);
        instance.addToConsumption(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureAddDesviationNullIsNotAllowed() {
        ProductionOrderExecution instance = new ProductionOrderExecution(productionOrderID,
                productionLineID, prodSheet);
        instance.addToDesviations(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureAddMachineActivityNullIsNotAllowed() {
        ProductionOrderExecution instance = new ProductionOrderExecution(productionOrderID,
                productionLineID, prodSheet);
        instance.addToMachineActivity(null);
    }
}

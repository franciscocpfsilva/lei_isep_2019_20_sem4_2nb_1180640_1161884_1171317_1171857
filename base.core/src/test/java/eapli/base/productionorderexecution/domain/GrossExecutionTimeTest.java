/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.productionorderexecution.domain;

import eapli.base.machinery.domain.MachineID;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;

/**
 *
 * @author João Cunha
 */
public class GrossExecutionTimeTest {

    @Test(expected = IllegalArgumentException.class)
    public void ensureDateNullIsNotAllowed() {
        GrossExecutionTime instance = GrossExecutionTime.valueOf(null);
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.productionorderexecution.domain;

import eapli.base.machinery.domain.MachineID;
import eapli.base.rawmessage.domain.MessageType;
import java.time.LocalDateTime;
import org.junit.Test;

/**
 *
 * @author João Cunha
 */
public class MachineActivityTest {

    MachineID machineID = MachineID.valueOf("M750");
    LocalDateTime date = LocalDateTime.now();
    DateTypes type = DateTypes.START_ACTIVITY;

    @Test(expected = IllegalArgumentException.class)
    public void ensureMachineIdNullIsNotAllowed() {
        MachineActivity instance = MachineActivity.valueOf(null, date, type);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureStartTimeNullIsNotAllowed() {
        MachineActivity instance = MachineActivity.valueOf(machineID, null, type);

    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureDateTypesNullIsNotAllowed() {
        MachineActivity instance = MachineActivity.valueOf(machineID, date, null);

    }
}

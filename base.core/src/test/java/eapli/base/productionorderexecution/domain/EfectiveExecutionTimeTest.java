/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.productionorderexecution.domain;

import eapli.base.machinery.domain.MachineID;
import eapli.base.rawmessage.domain.MessageType;
import java.time.LocalDateTime;
import java.util.HashMap;
import org.junit.Test;

/**
 *
 * @author João Cunha
 */
public class EfectiveExecutionTimeTest {

    LocalDateTime date = LocalDateTime.now();
    DateTypes type = DateTypes.START_ACTIVITY;

    @Test(expected = IllegalArgumentException.class)
    public void ensureDateNullIsNotAllowed() {
        EfectiveExecutionTime instance = EfectiveExecutionTime.valueOf(null, type);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureDateTypesNullIsNotAllowed() {
        EfectiveExecutionTime instance = EfectiveExecutionTime.valueOf(date, null);
    }
}

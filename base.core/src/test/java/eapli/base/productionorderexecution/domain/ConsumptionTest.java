/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.productionorderexecution.domain;

import eapli.base.domain.model.general.Item;
import eapli.base.domain.model.general.Quantity;
import static eapli.base.domain.model.general.UnitOfMeasurement.UN;
import eapli.base.products.domain.ProductID;
import eapli.base.storage.domain.StorageUnitID;
import org.junit.Test;

/**
 *
 * @author João Cunha
 */
public class ConsumptionTest {

    Item item = Item.valueOf(ProductID.valueOf("A123"), Quantity.valueOf(32.3, UN));
    StorageUnitID storageID = StorageUnitID.valueOf("S340");
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureStorageUnitIDNullIsNotAllowed() {
        Consumption instance = Consumption.valueOf(null, item);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureItemNullIsNotAllowed() {
        Consumption instance = Consumption.valueOf(storageID, null);
    }
}

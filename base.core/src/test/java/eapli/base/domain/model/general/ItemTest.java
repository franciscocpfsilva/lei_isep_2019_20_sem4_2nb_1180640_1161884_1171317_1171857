/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.domain.model.general;

import eapli.base.products.domain.ProductID;
import eapli.base.rawmaterials.domain.RawMaterialID;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author joaomachado
 */
public class ItemTest {

    private static final RawMaterialID RAWMATERIAL_ID = RawMaterialID.valueOf("R001");
    private static final ProductID PRODUCT_ID = ProductID.valueOf("P001");
    private static final UnitOfMeasurement UNIT = UnitOfMeasurement.UN;
    private static final Quantity QUANTITY = Quantity.valueOf(10.0, UNIT);

    @Test
    public void ensureItemWithRawMaterialIdQuantityAndUnitOfMeasurement() {
        new Item(RAWMATERIAL_ID, QUANTITY);
        assertTrue(true);
    }

    @Test
    public void ensureItemWithProductIdQuantityAndUnitOfMeasurement() {
        new Item(PRODUCT_ID, QUANTITY);
        assertTrue(true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureItemMustHaveRawMaterialId() {
        final RawMaterialID RAWMATERIAL_ID = null;
        Item instance = new Item(RAWMATERIAL_ID, QUANTITY);

    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureItemMustHaveProductId() {
        final ProductID PRODUCT_ID = null;
        Item instance = new Item(PRODUCT_ID, QUANTITY);

    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.domain.model.general;

import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author joaomachado
 */
public class QuantityTest {

    private static final double quantity = 10.0;
    private static final UnitOfMeasurement UNIT = UnitOfMeasurement.UN;

    @Test
    public void ensureQuantityDWithValue() {
        new Quantity(quantity, UNIT);
        assertTrue(true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureMustBePositiveNumber() {
        System.out.println("Quantity Must be Positive");
        new Quantity(-10.0, UNIT);
    }

    @Test(expected = NullPointerException.class)
    public void ensureQuantityMustNotBeNull() {
        System.out.println("Quantity can't be null");
        new Quantity(null, UNIT);
    }
    
      @Test(expected = IllegalArgumentException.class)
    public void ensureUnitMustNotBeNull() {
        System.out.println("Unit can't be null");
        new Quantity(quantity, null);
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.productionorder.domain;

import eapli.base.domain.model.general.Quantity;
import eapli.base.domain.model.general.UnitOfMeasurement;
import eapli.base.products.domain.ProductID;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author joaomachado
 */
public class ProductionOrderTest {

    private static final ProductionOrderID PRODUCTION_ORDER_ID = ProductionOrderID.valueOf("EXO001");
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    private final LocalDate DATE_OF_ISSUE = LocalDate.parse("05-05-2020", formatter);
    private final LocalDate EXPECTED_EXECUTION_DATE = LocalDate.parse("05-05-2020", formatter);
    private static final ProductID PRODUCT_ID = ProductID.valueOf("0123456789");
    private static final ProductionOrderState STATE = ProductionOrderState.PENDENTE;
    private static final LinkedList<OrderID> orderId = new LinkedList<>();
    private static final UnitOfMeasurement UNIT = UnitOfMeasurement.UN;
    private static final Quantity QUANTITY = Quantity.valueOf(10.0, UNIT);

    @Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowedOrderId() {
        orderId.add(OrderID.valueOf("Order1"));

        ProductionOrder instance = new ProductionOrder(null, DATE_OF_ISSUE, EXPECTED_EXECUTION_DATE, PRODUCT_ID,
                orderId, QUANTITY);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowedDateOfIssue() {
        ProductionOrder instance = new ProductionOrder(PRODUCTION_ORDER_ID, null, EXPECTED_EXECUTION_DATE, PRODUCT_ID,
                orderId, QUANTITY);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowedExpecetedExecutionDate() {
        ProductionOrder instance = new ProductionOrder(PRODUCTION_ORDER_ID, DATE_OF_ISSUE, null, PRODUCT_ID,
                orderId, QUANTITY);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowedProductID() {
        ProductionOrder instance = new ProductionOrder(PRODUCTION_ORDER_ID, DATE_OF_ISSUE, EXPECTED_EXECUTION_DATE, null,
                orderId, QUANTITY);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowedOrderID() {
        orderId.add(OrderID.valueOf("Order1"));
        ProductionOrder instance = new ProductionOrder(PRODUCTION_ORDER_ID, DATE_OF_ISSUE, EXPECTED_EXECUTION_DATE, PRODUCT_ID,
                null, QUANTITY);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowedQuantity() {
        ProductionOrder instance = new ProductionOrder(PRODUCTION_ORDER_ID, DATE_OF_ISSUE, EXPECTED_EXECUTION_DATE, null,
                orderId, null);
    }

    @Test
    public void ensureProductionOrderStateIsPendente() {
        orderId.add(OrderID.valueOf("Order1"));
        ProductionOrder instance = new ProductionOrder(PRODUCTION_ORDER_ID, DATE_OF_ISSUE, EXPECTED_EXECUTION_DATE, PRODUCT_ID,
                orderId, QUANTITY);
        assertEquals(instance.productionOrderState(), STATE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureExpecetedExecutionDateIsAfterDateOfIssue() {
        orderId.add(OrderID.valueOf("Order1"));

        LocalDate dateOfIssue = LocalDate.parse("05-05-2020", formatter);
        LocalDate expectedExecutionDate = LocalDate.parse("04-05-2020", formatter);

        ProductionOrder instance = new ProductionOrder(PRODUCTION_ORDER_ID, dateOfIssue, expectedExecutionDate, PRODUCT_ID,
                orderId, QUANTITY);
    }

}

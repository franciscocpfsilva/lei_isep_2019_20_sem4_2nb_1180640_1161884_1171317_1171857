/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.machinery.domain;

import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author joaomachado
 */
public class CommunicationProtocolIDTest {
     private static final Integer communicationProtocolID = 1;

    @Test
    public void ensureCommunicationProtocolIDWithID() {
        CommunicationProtocolID instance  = CommunicationProtocolID.valueOf(communicationProtocolID);
        assertTrue(true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureCommunicationProtocolIDNotExceedes65535() {
        System.out.println("Communication Protocol ID must not exceed 65355");
         CommunicationProtocolID instance  = CommunicationProtocolID.valueOf(65536);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureCommunicationProtocolIDNotLessThanOne() {
        System.out.println("Communication Protocol ID must not be less than 1");
         CommunicationProtocolID instance  = CommunicationProtocolID.valueOf(0);
    }

}

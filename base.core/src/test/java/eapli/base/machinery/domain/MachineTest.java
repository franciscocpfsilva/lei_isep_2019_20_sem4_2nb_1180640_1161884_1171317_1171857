/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.machinery.domain;

import eapli.framework.general.domain.model.Description;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author joaomachado
 */
public class MachineTest {

    private static final MachineID MACHINE_ID = MachineID.valueOf("M001");
    private static final Description SERIAL_NUMBER = Description.valueOf("AYX123");
    private static final Description MACHINE_DESCRIPTION = Description.valueOf("Maquina de Recolha");
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    private LocalDate INSTALLATION_DATE = LocalDate.parse("05-05-2020", formatter);
    private static final Description BRAND = Description.valueOf("YAMAHA");
    private static final Description MODEL = Description.valueOf("Cortadora3000");
    private static final CommunicationProtocolID COMMUNICATION_PROTOCOL_ID = CommunicationProtocolID.valueOf(1);
    private static final LinkedList<ConfigurationFile> configFiles = new LinkedList<>();

    @Test
    public void ensureMachineWithIdSerialNumberDescriptionBrandModelAndCommunicationProtocolID() {
        new Machine(MACHINE_ID, SERIAL_NUMBER, MACHINE_DESCRIPTION, INSTALLATION_DATE, BRAND, MODEL, COMMUNICATION_PROTOCOL_ID);
        assertTrue(true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureMachineIDMustNotBeNull() {
        System.out.println("must have a Machine ID");
        new Machine(null, SERIAL_NUMBER, MACHINE_DESCRIPTION, INSTALLATION_DATE, BRAND, MODEL, COMMUNICATION_PROTOCOL_ID);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureSerialNumberMustNotBeNull() {
        System.out.println("must have a Serial Number");
        new Machine(MACHINE_ID, null, MACHINE_DESCRIPTION, INSTALLATION_DATE, BRAND, MODEL, COMMUNICATION_PROTOCOL_ID);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureMachineDescriptionMustNotBeNull() {
        System.out.println("must have a Description");
        new Machine(MACHINE_ID, SERIAL_NUMBER, null, INSTALLATION_DATE, BRAND, MODEL, COMMUNICATION_PROTOCOL_ID);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureMachineInstallationDateMustNotBeNull() {
        System.out.println("must have a Description");
        new Machine(MACHINE_ID, SERIAL_NUMBER, MACHINE_DESCRIPTION, null, BRAND, MODEL, COMMUNICATION_PROTOCOL_ID);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureBrandMustNotBeNull() {
        System.out.println("must have a Brand");
        new Machine(MACHINE_ID, SERIAL_NUMBER, MACHINE_DESCRIPTION, INSTALLATION_DATE, null, MODEL, COMMUNICATION_PROTOCOL_ID);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureModelMustNotBeNull() {
        System.out.println("must have a Model");
        new Machine(MACHINE_ID, SERIAL_NUMBER, MACHINE_DESCRIPTION, INSTALLATION_DATE, BRAND, null, COMMUNICATION_PROTOCOL_ID);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureCommunicationProtocolIDNotBeNull() {
        System.out.println("must have a Communication Protocol ID");
        new Machine(MACHINE_ID, SERIAL_NUMBER, MACHINE_DESCRIPTION, INSTALLATION_DATE, BRAND, MODEL, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureSerialNumberHasMaximumTwentyCharacters() {
        System.out.println("must not have Serial Number with more than 20 characters");
        new Machine(MACHINE_ID, Description.valueOf("123456789012345678901"), MACHINE_DESCRIPTION, INSTALLATION_DATE, BRAND, MODEL, COMMUNICATION_PROTOCOL_ID);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureMachineDescriptionHasMaximumFortyCharacters() {
        System.out.println("must not have Machine Description with more than 40 characters");
        new Machine(MACHINE_ID, SERIAL_NUMBER, Description.valueOf("1234567890123456347856348765743576q3456347856423asfsadsd78901"), INSTALLATION_DATE, BRAND, MODEL, COMMUNICATION_PROTOCOL_ID);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureMachineInstallationDateIsNotInFuture() {
        System.out.println("Installation Date must not be in Future!");
        new Machine(MACHINE_ID, SERIAL_NUMBER, MACHINE_DESCRIPTION, LocalDate.parse("05-05-2022", formatter), BRAND, MODEL, COMMUNICATION_PROTOCOL_ID);
    }

    @Test
    public void addItemToSheet() {
        byte file[] = new byte[]{1, 6, 3};
        
        assertTrue(configFiles.isEmpty());
        assertEquals(0, configFiles.size());

        configFiles.add(ConfigurationFile.valueOf(Description.valueOf("Modo broca 3mm"), file));

        assertFalse(configFiles.isEmpty());
        assertEquals(1, configFiles.size());

    }
}

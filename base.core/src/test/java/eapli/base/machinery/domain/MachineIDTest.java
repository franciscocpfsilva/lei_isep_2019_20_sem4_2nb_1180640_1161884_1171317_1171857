/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.machinery.domain;

import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author joaomachado
 */
public class MachineIDTest {

    private static final String MachineID = "M001";

    @Test
    public void ensureMachineIDWithID() {
        new MachineID(MachineID);
        assertTrue(true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureMachineIDMustNotBeNull() {
        System.out.println("must have a Machine ID");
        new MachineID(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureMachineIDMustNotBeEmpty() {
        System.out.println("must have non-empty Machine ID");
        new MachineID("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureMachineIDHasMaximumtenCharacters() {
        System.out.println("must not have Machine ID with more than 10 characters");
        new MachineID("01234567890");
    }

    @Test
    public void ensureMachineIDCanHaveDigits() {
        System.out.println("Machine ID can have digits");
        new MachineID("12345");
        assertTrue(true);
    }

    @Test
    public void ensureMachineIDCanHaveCapitalLetters() {
        System.out.println("Machine ID can have capital letters");
        new MachineID("ABCDE");
        assertTrue(true);
    }

    @Test
    public void ensureMachineIDCanHaveSmallLetters() {
        System.out.println("Machine ID can have small letters");
        new MachineID("abcde");
        assertTrue(true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureMachineIDMustNotHaveStrangeCharacters1() {
        System.out.println("Machine ID must not have strange characters (test1)");
        new MachineID("00-00");
    }

}

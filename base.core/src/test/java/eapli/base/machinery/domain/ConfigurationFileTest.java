/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.machinery.domain;

import eapli.framework.general.domain.model.Description;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author joaomachado
 */
public class ConfigurationFileTest {

    private static final Description CONFIGURATION_FILE_DESCRIPTION = Description.valueOf("Ficheiro com broca 3mm");
    private static final byte file[] = new byte[]{1, 6, 3};

    @Test
    public void ensureConfigurationFileWithDescriptionAndFile() {
        new ConfigurationFile(CONFIGURATION_FILE_DESCRIPTION, file);
        assertTrue(true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureDescriptionMustNotBeNull() {
        System.out.println("must have a description");
        new ConfigurationFile(null, file);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureFileMustNotBeNull() {
        System.out.println("must have a file");
        new ConfigurationFile(CONFIGURATION_FILE_DESCRIPTION, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureDescriptionHasMaximumFortyCharacters() {
        System.out.println("must not have Machine Description with more than 40 characters");
        new ConfigurationFile(Description.valueOf("1234567890123456347856348765743576q3456347856423asfsadsd78901"), null);
    }

  
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.rawmaterials.domain;

import eapli.base.domain.model.general.UnitOfMeasurement;
import eapli.framework.general.domain.model.Description;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author joaomachado
 */
public class RawMaterialTest {

    private static final RawMaterialCategoryID RAW_MATERIAL_CAT_ID = new RawMaterialCategoryID("RMC2002");
    private static final Description DESCRIPTION = Description.valueOf("Plasticos");
    private static final RawMaterialCategory rawMaterialCategory = new RawMaterialCategory(RAW_MATERIAL_CAT_ID, DESCRIPTION);
    private static final RawMaterialID rawMaterialID = RawMaterialID.valueOf("0123456789");
    private static final Description description = Description.valueOf("RolhaA");

    private static final UnitOfMeasurement unit = UnitOfMeasurement.KILOGRAM;
    private static final byte pdf[] = new byte[]{1, 6, 3};

    @Test
    public void ensureRawMaterialWithIdDescriptionCategoryUnitandFile() {
        RawMaterial isntance = new RawMaterial(rawMaterialCategory, rawMaterialID, description, unit, pdf);
        assertTrue(true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowedRawMaterialCategory() {
        RawMaterial instance = new RawMaterial(null, rawMaterialID, description, unit,
                pdf);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowedRawMaterialID() {
        RawMaterial instance = new RawMaterial(rawMaterialCategory, null, description, unit,
                pdf);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowedDescription() {
        RawMaterial instance = new RawMaterial(rawMaterialCategory, rawMaterialID, null, unit,
                pdf);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowedBriefUnit() {
        RawMaterial instance = new RawMaterial(rawMaterialCategory, rawMaterialID, description, null,
                pdf);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowedFullPdf() {
        RawMaterial instance = new RawMaterial(rawMaterialCategory, rawMaterialID, description, unit,
                null);
    }


    /* 
     */
    @Test(expected = IllegalArgumentException.class)
    public void briefDescrNotExceedFortyInSize() {
        Description desc = Description.valueOf("isto é um teste á descrição de raw material para garantir que não pode passar dos 40 chars");

        RawMaterial instance = new RawMaterial(rawMaterialCategory, rawMaterialID, desc, unit,
                pdf);

    }
}

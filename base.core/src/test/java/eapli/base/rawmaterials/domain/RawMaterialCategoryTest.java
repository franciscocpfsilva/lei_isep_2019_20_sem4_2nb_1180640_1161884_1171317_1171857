/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.rawmaterials.domain;

import eapli.framework.general.domain.model.Description;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author
 */
public class RawMaterialCategoryTest {
    
    private static final RawMaterialCategoryID RAW_MATERIAL_CAT_ID = new RawMaterialCategoryID("RMC2002");
    private static final Description DESCRIPTION = Description.valueOf("Plasticos");
    
    @Test
    public void ensureRawMaterialCategoryWithIdAndDescription() {
        new RawMaterialCategory(RAW_MATERIAL_CAT_ID, DESCRIPTION);
        assertTrue(true);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureRawMaterialCategoryIDCanNotNull() {
        System.out.println("category must have an identifier");
        new RawMaterialCategory(null, DESCRIPTION);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureDescriptionCanNotBeNull() {
        System.out.println("category must have a description");
        new RawMaterialCategory(RAW_MATERIAL_CAT_ID, null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureDescriptionHasMaximumFortyCharacters() {
        System.out.println("descriptioin can not have more than 40 characters");
        new RawMaterialCategory(RAW_MATERIAL_CAT_ID, Description.valueOf("Plasticos com a perticularidade de serem do tipo xyz...."));
    }
}

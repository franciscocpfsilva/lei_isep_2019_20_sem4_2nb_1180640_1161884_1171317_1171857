/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.rawmaterials.domain;

import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author
 */
public class RawMaterialCategoryIDTest {
    
    private static final String IDENTIFIER = "RMC2002";
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureIdentifierIsNotNull() {
        System.out.println("identifier can not be null");
        new RawMaterialCategoryID(null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureIdentifierIsNotEmpty() {
        System.out.println("Identifier can not be empty");
        new RawMaterialCategoryID("");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureIdentifierHasMaximumOfTenCharacters() {
        System.out.println("Identifier can not have more than 10 characters");
        new RawMaterialCategoryID("RMC2002345543");
    }
    
    @Test
    public void ensureIdentifierIsAlphanumeric1() {
        System.out.println("identifier is alphanumeric (test1)");
        new RawMaterialCategoryID("RMC2002");
        assertTrue(true);
    }
    
    @Test
    public void ensureIdentifierIsAlphanumeric2() {
        System.out.println("identifier is alphanumeric (test2)");
        new RawMaterialCategoryID("rmc2002");
        assertTrue(true);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureIdentifierIsSingleWord() {
        System.out.println("identifier must be a single word");
        new RawMaterialCategoryID("RMC1 rmc2");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureIdentifierMustNotHaveSpecialCharacters1() {
        System.out.println("identifier must not have special characters (test1)");
        new RawMaterialCategoryID("RMC+2002");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureIdentifierMustNotHaveSpecialCharacters2() {
        System.out.println("identifier must not have special characters (test2)");
        new RawMaterialCategoryID(" RMC2002 ");
    }
    
    @Test
    public void ensureRawMaterialCategoryIDWithIdentifier() {
        new RawMaterialCategoryID(IDENTIFIER);
        assertTrue(true);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.rawmaterials.domain;

import org.junit.Test;

/**
 *
 * @author joaomachado
 */
public class RawMaterialIDTest {
      @Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowed() {
        RawMaterialID instance = new RawMaterialID(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void factoryIDDoesNotExceedFifteenInSize() {
        RawMaterialID instance = new RawMaterialID("01239120ASJDA019312");
    }

    @Test(expected = IllegalArgumentException.class)
    public void factoryIDIsNotASingleWord() {
        RawMaterialID instance = new RawMaterialID("A123 0123D ");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void factoryIDContainsSpecialCharacters() {
        RawMaterialID instance = new RawMaterialID("A123^0123D ");
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.errormessage.domain;

import org.junit.Test;

/**
 *
 * @author João Cunha
 */
public class ErrorMessageTest {
    
@Test(expected = IllegalArgumentException.class)
public void ensureRawMessageNullIsNotAllowed() {
    
    ErrorMessage instance = new ErrorMessage(null, ErrorType.NO_PROCT_OR_RAW_MATERIAL);
}
    
}

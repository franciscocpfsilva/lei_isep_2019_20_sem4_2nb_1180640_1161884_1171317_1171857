/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.storage.domain;

import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author
 */
public class StorageUnitIDTest {
    
    private static final String IDENTIFIER = "DE123";
    
    @Test
    public void ensureStorageUnitIDWithIdentifier() {
        new StorageUnitID(IDENTIFIER);
        assertTrue(true);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureIdentifierMustNotBeNull() {
        System.out.println("must have an identifier");
        new StorageUnitID(null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureIdentifierMustNotBeEmpty() {
        System.out.println("must have non-empty identifier");
        new StorageUnitID("");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureIdentifierHasMaximumTenCharacters() {
        System.out.println("must not have identifier with more than 10 characters");
        new StorageUnitID("01234567890");
    }
    
    @Test
    public void ensureIdentifierCanHaveDigits() {
        System.out.println("identifier can have digits");
        new StorageUnitID("12345");
        assertTrue(true);
    }
    
    @Test
    public void ensureIdentifierCanHaveCapitalLetters() {
        System.out.println("identifier can have capital letters");
        new StorageUnitID("ABCDE");
        assertTrue(true);
    }
    
    @Test
    public void ensureIdentifierCanHaveSmallLetters() {
        System.out.println("identifier can have small letters");
        new StorageUnitID("abcde");
        assertTrue(true);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureIdentifierMustNotHaveStrangeCharacters1() {
        System.out.println("identifier must not have strange characters (test1)");
        new StorageUnitID("00-00");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureIdentifierMustNotHaveStrangeCharacters2() {
        System.out.println("identifier must not have strange characters (test2)");
        new StorageUnitID("00?00");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureIdentifierMustNotHaveStrangeCharacters3() {
        System.out.println("identifier must not have strange characters (test3)");
        new StorageUnitID("00€00");
    }
    
}

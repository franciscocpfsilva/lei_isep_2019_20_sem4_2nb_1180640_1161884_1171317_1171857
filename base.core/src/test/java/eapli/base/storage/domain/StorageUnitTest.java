/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.storage.domain;

import eapli.framework.general.domain.model.Description;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author
 */
public class StorageUnitTest {
    
    private static final StorageUnitID STORAGE_UNIT_ID = StorageUnitID.valueOf("DE123");
    private static final Description DESCRIPTION = Description.valueOf("Deposito");
    
    @Test
    public void ensureStorageUnitWithIdAndDescription() {
        new StorageUnit(STORAGE_UNIT_ID, DESCRIPTION);
        assertTrue(true);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureStorageUnitIDMustNotBeNull() {
        System.out.println("must have a storage unit ID");
        new StorageUnit(null, DESCRIPTION);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureDescriptionMustNotBeNull() {
        System.out.println("must have a description");
        new StorageUnit(STORAGE_UNIT_ID, null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureDescriptionHasMaximumFortyCharacters() {
        System.out.println("must not have description with more than 40 characters");
        new StorageUnit(STORAGE_UNIT_ID, 
                Description.valueOf("00000000001111111111222222222233333333334"));
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.rawmessage.domain;

import eapli.base.machinery.domain.MachineID;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.junit.Test;

/**
 *
 * @author leona
 */
public class RawMessageTest {
    
    private static final MachineID MACHINE_ID = MachineID.valueOf("T3");
    private static final MessageType MESSAGE_TYPE = MessageType.C0;
    private static final LocalDateTime GENERATION_DATE = LocalDateTime.now();
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
    private static final LocalDateTime GENERATION_DATE_FUTURE = LocalDateTime.parse("15-08-2020 10:44:23", FORMATTER);
    
    private static final String RAW_DATA= "T3;P1;20190326151510;50000106;1001;DE06191071";
    
    @Test
    public void ensureRawMessageWithAllAttributes() {
        System.out.println("Message with all valid attributes");
        RawMessage rawMessage = new RawMessage(MACHINE_ID, MESSAGE_TYPE, GENERATION_DATE, RAW_DATA);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureMachineIDCanNotBeNull() {
        System.out.println("Message must have a Machine ID");
        RawMessage rawMessage = new RawMessage(null, MESSAGE_TYPE, GENERATION_DATE, RAW_DATA);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureMesageTypeCanNotBeNull() {
        System.out.println("Message must have a Message Type");
        RawMessage rawMessage = new RawMessage(MACHINE_ID, null, GENERATION_DATE, RAW_DATA);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureGenerationDateTimeCanNotBeNull() {
        System.out.println("Message must have a Date of generation");
        RawMessage rawMessage = new RawMessage(MACHINE_ID, MESSAGE_TYPE, null, RAW_DATA);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureGenerationDateTimeCanNotBeFromFarIntoTheFuture() {
        System.out.println("Date of generation can't be from far into the future (difference of days)");
        RawMessage rawMessage = new RawMessage(MACHINE_ID, MESSAGE_TYPE, GENERATION_DATE_FUTURE, RAW_DATA);
    }
}

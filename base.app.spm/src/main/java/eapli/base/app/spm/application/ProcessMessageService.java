/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.spm.application;

import eapli.base.productionorderexecution.domain.ProductionOrderExecution;
import eapli.base.rawmessage.domain.RawMessage;
import java.util.NoSuchElementException;

/**
 *
 * @author João Cunha
 */
public class ProcessMessageService {

    public ProductionOrderExecution processMessage(ProductionOrderExecution executionOrder,
            ProcessMessage type, RawMessage rawMessage)
            throws NoSuchElementException {

        type.begin(executionOrder, rawMessage);
        ProductionOrderExecution orderExecution = type.processMessage();
        return orderExecution;

    }
}

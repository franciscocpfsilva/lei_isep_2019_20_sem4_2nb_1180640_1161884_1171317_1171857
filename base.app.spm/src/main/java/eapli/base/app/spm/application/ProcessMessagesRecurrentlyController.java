/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.spm.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.productionline.domain.ProductionLine;
import eapli.base.productionline.repositories.ProductionLinesRepository;
import eapli.framework.application.UseCaseController;
import java.util.LinkedList;
import java.util.List;

/**
 * Controller for US 5002 - Process Messages Recurrently.
 * 
 * @author franciscoferreiradasilva
 */
@UseCaseController
public class ProcessMessagesRecurrentlyController {
    
    private final ProductionLinesRepository productionLinesRepo 
            = PersistenceContext.repositories().productionLines();
    
    public void start() {
        Iterable<ProductionLine> prodLines = productionLinesRepo.findAll();
        List<Thread> threadLst = new LinkedList<>();
        
        for (ProductionLine prodLine : prodLines) {
            // run recurrent message processing independently for each production line
            Thread newThread = new Thread(
                    new ProcessMessagesRecurrentlyForOneProductionLineThread(prodLine));
            newThread.start();
            threadLst.add(newThread);
        }
        
        for (Thread thread : threadLst) {
            try {
                thread.join();
            } catch (InterruptedException e) {
            }
        }
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.spm.application;

import eapli.base.rawmessage.domain.MessageType;

/**
 *
 * @author João Cunha
 */
public class ProcessMessageFactory {

    public ProcessMessage build(MessageType mType) {
        if (mType == null) {
            throw new IllegalStateException("Unknown format");
        }

        switch (mType) {
            case C0:
                return new ProcessMessageConsumption();
            case C9:
                return new ProcessMessageProductionDelivery();
            case P1:
                return new ProcessMessageProduction();
            case P2:
                return new ProcessMessageChargeback();
            case S0:
                return new ProcessMessageStartActivity();
            case S1:
                return new ProcessMessageResumeActivity();
            case S8:
                return new ProcessMessageForcedStop();
            case S9:
                return new ProcessMessageEndActivity();

        }
        throw new IllegalStateException("Unknown format");
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.spm;

import eapli.base.app.spm.application.CreateListOfProductionLines;
import eapli.base.app.spm.application.ProcessMessagesRecurrentlyForOneProductionLineThread;
import eapli.base.app.spm.application.ProductionLineThread;
import eapli.base.productionline.domain.ProductionLineID;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author João Cunha
 */
public class ProcessBlockOfTime {

    public void run(String start, String end, List<ProductionLineID> lst) {
        try {
            LocalDateTime startTime, endTime;
            startTime = LocalDateTime.parse(start);
            endTime = LocalDateTime.parse(end);

            CreateListOfProductionLines exe = new CreateListOfProductionLines();
            exe.inicializeMapProductionLines();
            exe.changeProductionLineStatus(lst);

            HashMap<ProductionLineID, Boolean> map = exe.map();
            
            for (ProductionLineID productionLineID : map.keySet()) {

                Thread newThread = new Thread(
                    new ProductionLineThread(startTime, endTime, productionLineID,
                                map.get(productionLineID)));
            newThread.start();
            }

        } catch (DateTimeParseException e) {
            System.out.println("Error: " + e.getMessage() + ".");
        }

    }
}

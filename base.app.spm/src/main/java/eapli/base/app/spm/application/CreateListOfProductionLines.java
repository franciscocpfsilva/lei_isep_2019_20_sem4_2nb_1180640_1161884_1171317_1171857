/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.spm.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.productionline.domain.ProductionLine;
import eapli.base.productionline.domain.ProductionLineID;
import eapli.base.productionline.repositories.ProductionLinesRepository;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author João Cunha
 */
public class CreateListOfProductionLines {

    ProductionLinesRepository productionLinesRepository
            = PersistenceContext.repositories().productionLines();

    private HashMap<ProductionLineID, Boolean> map;

    public CreateListOfProductionLines() {
        this.map = new HashMap<>();
    }

    /**
     * Fetch all production lines from the repository and insert it on the map
     * with default state FALSE
     */
    public void inicializeMapProductionLines() {

        for (ProductionLine productionLine : productionLinesRepository.findAll()) {
            map.putIfAbsent(productionLine.getProductionLine_ID(), Boolean.FALSE);
        }
    }

    /**
     * Change the default state of production line
     *
     * @param lst List of production lines to change
     */
    public void changeProductionLineStatus(List<ProductionLineID> lst) {
        for (ProductionLineID productionLineID : lst) {
            map.replace(productionLineID, Boolean.TRUE);
        }
    }

    /**
     *
     * @return
     */
    public HashMap<ProductionLineID, Boolean> map() {
        return map;
    }

}

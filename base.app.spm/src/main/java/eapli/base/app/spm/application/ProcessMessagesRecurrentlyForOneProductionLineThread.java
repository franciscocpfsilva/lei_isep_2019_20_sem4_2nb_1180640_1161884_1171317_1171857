/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.spm.application;

import eapli.base.app.spm.ProcessMessagesGeneralMode;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.productionline.domain.ProductionLine;
import eapli.base.productionorderexecution.domain.ProductionOrderExecution;
import eapli.base.rawmessage.domain.RawMessage;
import eapli.base.rawmessage.domain.RawMessageState;
import eapli.base.rawmessage.repositories.RawMessagesRepository;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Thread to process messages recurrently for one production line. 
 * 
 * @author franciscoferreiradasilva
 */
public class ProcessMessagesRecurrentlyForOneProductionLineThread implements Runnable {

    private static final int MILLISECONDS_BETWEEN_STATE_VERIFICATION = 1 * 60 * 1000;
    private static final int MILLISECONDS_BETWEEN_PROCESSING = 15 * 60 * 1000; 
    
    private final ProductionLine productionLine;
    private final CheckProcessingStateService checkStateSvc = new CheckProcessingStateService();
    private final RawMessagesRepository rawMessagesRepo 
            = PersistenceContext.repositories().rawMessages();
    private final ProcessMessageFactory factory = new ProcessMessageFactory();
    private final ProcessMessageService processSvc = new ProcessMessageService();
    
    private ProductionOrderExecution productionOrderExecution = null;
    
    public ProcessMessagesRecurrentlyForOneProductionLineThread(ProductionLine productionLine) {
        this.productionLine = productionLine;
    }
    
    @Override
    public void run() {
        // Runs indefinitely
        while (true) {
            // Verifies state before proceeding
            if (!checkStateSvc.checkProcessingStateForProductionLine(productionLine.getProductionLine_ID())) {
                // If inactive, sleeps and checks again after some time
                try {
                    Thread.sleep(MILLISECONDS_BETWEEN_STATE_VERIFICATION);
                } catch (InterruptedException ex) {
                    Logger.getLogger(ProcessMessagesGeneralMode.class.getName()).log(Level.SEVERE, null, ex);
                }

            } else {
                // If active, processes messages
                Iterable<RawMessage> msgs = rawMessagesRepo.findMessagesFromMachines(
                        RawMessageState.UNPROCESSED, 
                        productionLine.getSequencia()
                );
                for (RawMessage rawMessage : msgs) {
                    final ProcessMessage processorType = factory.build(rawMessage.messageType());
                    productionOrderExecution = processSvc.processMessage(productionOrderExecution, processorType, rawMessage);
                }
                
                // Sleeps and tries to process again after some time
                try {
                    Thread.sleep(MILLISECONDS_BETWEEN_PROCESSING);
                } catch (InterruptedException ex) {
                    Logger.getLogger(ProcessMessagesGeneralMode.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
}

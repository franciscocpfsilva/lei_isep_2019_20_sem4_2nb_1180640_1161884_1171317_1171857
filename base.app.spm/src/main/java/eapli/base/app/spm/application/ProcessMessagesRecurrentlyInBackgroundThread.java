/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.spm.application;

/**
 * Thread to run the message processing system recurrently in the background.
 * 
 * @author franciscoferreiradasilva
 */
public class ProcessMessagesRecurrentlyInBackgroundThread implements Runnable {

    @Override
    public void run() {
        new ProcessMessagesRecurrentlyController().start();
    }
    
}

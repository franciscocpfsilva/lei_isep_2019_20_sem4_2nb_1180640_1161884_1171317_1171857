/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.spm.application;

import eapli.base.domain.model.general.Item;
import eapli.base.domain.model.general.Quantity;
import eapli.base.errormessage.domain.ErrorMessage;
import eapli.base.errormessage.domain.ErrorType;
import eapli.base.errormessage.repositories.ErrorMessagesRepository;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.productionorderexecution.domain.Batch;
import eapli.base.productionorderexecution.domain.Production;
import eapli.base.productionorderexecution.domain.ProductionOrderExecution;
import eapli.base.products.domain.Product;
import eapli.base.products.domain.ProductID;
import eapli.base.products.repositories.ProductsRepository;
import eapli.base.rawmessage.domain.RawMessage;
import eapli.base.rawmessage.domain.RawMessageState;
import eapli.base.rawmessage.repositories.RawMessagesRepository;
import java.util.NoSuchElementException;
import java.util.Optional;

/**
 *
 * @author João Cunha
 */
public class ProcessMessageProduction implements ProcessMessage {

    private final String FILE_SEPARATOR = ";";

    private final int POS_PRODUCT_ID = 3;
    private final int POS_QUANTITY = 4;
    private final int POS_BATCH = 5;

    private final ProductsRepository productsRepo
            = PersistenceContext.repositories().products();
    private final ErrorMessagesRepository errorMessagesRepo
            = PersistenceContext.repositories().errorMessages();
    private final RawMessagesRepository rawMessagesRepo
            = PersistenceContext.repositories().rawMessages();

    private ProductionOrderExecution executionOrder;
    private RawMessage rawMessage;

    @Override
    public void begin(ProductionOrderExecution executionOrder, RawMessage rawMessage) throws NoSuchElementException {
        this.executionOrder = executionOrder;
        this.rawMessage = rawMessage;
    }

    @Override
    public ProductionOrderExecution processMessage() throws NoSuchElementException, NumberFormatException {

  //      throw new UnsupportedOperationException("Not supported yet."); 
//        String array[];
//        double number;
//        try {
//            array = rawMessage.rawData().split(FILE_SEPARATOR);
//            number = Double.parseDouble(array[POS_QUANTITY]);
//        } catch (NumberFormatException n) {
//            return executionOrder;
//        }
//
//        //Create a productID and search for the product
//        final ProductID productID = new ProductID(array[POS_PRODUCT_ID]);
//        final Optional<Product> opt_product = productsRepo.ofIdentity(productID);
//        if (!opt_product.isPresent()) {
//            rawMessage.changeRawMessageState(RawMessageState.ERROR);
//            rawMessagesRepo.save(rawMessage);
//            ErrorMessage errorMessage
//                    = new ErrorMessage(rawMessage, ErrorType.NO_PROCT_OR_RAW_MATERIAL);
//            errorMessagesRepo.save(errorMessage);
//            return executionOrder;
//        }
//        final Product product = opt_product.get();
//
//        Batch batch = null;
//        //Check if exist an Batch (can be null)
//        try {
//            if (array.length == POS_BATCH) {
//                batch = Batch.valueOf(array[POS_BATCH]);
//            }
//        } catch (ArrayIndexOutOfBoundsException e) {
//            System.out.println("Bash dont find!");
//        }
//
//        final Quantity quantity = Quantity.valueOf(number, product.unit_of_measurement());
//        final Item item = new Item(productID, quantity);
//        final Production production = Production.valueOf(batch, item, null);
//
//        if (executionOrder.addToProduction(production)) {
//            rawMessage.changeRawMessageState(RawMessageState.PROCESSED);
//            rawMessagesRepo.save(rawMessage);
//            return executionOrder;
//        }
        return executionOrder;
    }

}

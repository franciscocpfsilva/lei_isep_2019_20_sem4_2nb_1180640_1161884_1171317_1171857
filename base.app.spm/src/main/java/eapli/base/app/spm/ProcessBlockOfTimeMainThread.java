/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.spm;

import java.io.IOException;

/**
 *
 * @author leona
 */
public class ProcessBlockOfTimeMainThread implements Runnable{

    @Override
    public void run() {
        try{
            ProcessBlockOfTimeServer server = new ProcessBlockOfTimeServer();
            server.listenToRequests();
            
        }catch(IOException ioe){
            System.out.println("Error: while processing messages within time window");
        }
    }
    
}

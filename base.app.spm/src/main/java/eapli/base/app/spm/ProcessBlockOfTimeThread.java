/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.spm;

import eapli.base.productionline.domain.ProductionLineID;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author leona
 */
public class ProcessBlockOfTimeThread implements Runnable{

    private final Socket socket;
    private DataOutputStream sOut;
    private DataInputStream sIn;
    
    public ProcessBlockOfTimeThread(Socket sock){
        this.socket=sock;
    }
    @Override
    public void run() {
        
        InetAddress clientIP;

        clientIP=socket.getInetAddress();
        System.out.println(">> New request from Backoffice: " + clientIP.getHostAddress() + ", port number " + socket.getPort());
        
        try {
            sOut = new DataOutputStream(socket.getOutputStream());
            sIn = new DataInputStream(socket.getInputStream());
            
/* -- Read request from Backoffice */   
            int n_args = sIn.readInt();
            String[] args = new String[n_args];
            for (int i=0; i< n_args; i++){
                String tmp = readArgToString(sIn);
                args[i] = tmp;
            }


/* -- Reply to Backoffice */
            List<ProductionLineID> lst = new ArrayList();
            for (int i = 2; i < n_args; i++) {
                lst.add(new ProductionLineID(args[i]));
            }
            final ProcessBlockOfTime exe = new ProcessBlockOfTime();
            exe.run(args[0], args[1], lst);
                
            
/* -- Terminate connection */
//            System.out.println("<< Backoffice: " + clientIP.getHostAddress() + ", port number: " + socket.getPort() + " disconnected\n");
            socket.close();
            
        } catch(IllegalArgumentException iae){
            System.out.println("Error: " + iae.getMessage());
        } catch(IOException ex) { 
            System.out.println("IOException: " + ex.getMessage()); 
        }
    }
    
    private String readArgToString(DataInputStream dataInputStream) throws IOException{
            int size = dataInputStream.readInt();
            String tmp="";
            for(int i=0; i<size; i++)
                tmp += (char) dataInputStream.readByte();
            return tmp;
    }
}

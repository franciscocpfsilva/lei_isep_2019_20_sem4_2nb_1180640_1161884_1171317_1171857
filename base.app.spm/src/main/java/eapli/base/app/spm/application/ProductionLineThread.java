/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.spm.application;

import eapli.base.productionline.domain.ProductionLineID;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.NoSuchElementException;

/**
 *
 * @author João Cunha
 */
public class ProductionLineThread implements Runnable {

    private final ProductionLineID productionLineID;
    private final boolean state;
    private final LocalDateTime start;
    private final LocalDateTime end;

    public ProductionLineThread(LocalDateTime start, LocalDateTime end, ProductionLineID productionLineID, boolean state) {
        this.start = start;
        this.end = end;
        this.productionLineID = productionLineID;
        this.state = state;
    }

    @Override
    public void run() {
        ProcessMessagesController controller = new ProcessMessagesController();
        try {
            if (state) {
                controller.processMessages(start, end, productionLineID);
            }
        } catch (IOException e) {
            System.out.println("Error! There is no production line with this ID: " + e.getMessage());
        } catch (NoSuchElementException e) {
            System.out.println("Error. " + e.getMessage() + ".");
        } catch (NumberFormatException e) {
            System.out.println("Error. One element is missing.");
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.spm;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author leona
 */
public class ProcessBlockOfTimeServer {
    
    private ServerSocket sock;
    static int DEFAULT_PORT = 9998;
    
    public void listenToRequests()throws IOException {
        Socket cliSock;
        
        try {
            System.out.println("Running Sub Process (main) 2");
            sock = new ServerSocket(DEFAULT_PORT); 
            
        } catch(IOException ex) {
            System.out.println("Failed to open server socket");
//            System.exit(1);
        }

        while(true) {
            cliSock=sock.accept();
            Thread newThread = new Thread(new ProcessBlockOfTimeThread(cliSock));
            newThread.start();
        }    
    }
}

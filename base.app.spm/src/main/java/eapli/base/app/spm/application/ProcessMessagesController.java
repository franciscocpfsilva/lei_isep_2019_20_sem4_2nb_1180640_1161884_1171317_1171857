/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.spm.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.productionline.domain.ProductionLine;
import eapli.base.productionline.domain.ProductionLineID;
import eapli.base.productionline.repositories.ProductionLinesRepository;
import eapli.base.productionorderexecution.domain.ProductionOrderExecution;
import eapli.base.rawmessage.domain.RawMessage;
import eapli.base.rawmessage.domain.RawMessageState;
import eapli.base.rawmessage.repositories.RawMessagesRepository;
import eapli.framework.application.UseCaseController;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 *
 * @author João Cunha
 */
@UseCaseController
public class ProcessMessagesController {

    private final RawMessagesRepository rawMessageRepo = PersistenceContext.repositories().rawMessages();
    private final ProductionLinesRepository productionLineRepo = PersistenceContext.repositories().productionLines();

    private final ProcessMessageFactory factory = new ProcessMessageFactory();
    private final ProcessMessageService processSvc = new ProcessMessageService();
    private ProductionOrderExecution productionOrderExecution = null;

    public void processMessages(LocalDateTime start, LocalDateTime end, ProductionLineID productionLineID)
            throws IOException {

        // check if exists a production line
        Optional<ProductionLine> opt_productionLine = productionLineRepo.ofIdentity(productionLineID);
        ProductionLine productionLine;
        if (opt_productionLine.isPresent()) {
            productionLine = opt_productionLine.get();
        } else {
            throw new IOException(productionLineID.toString());
        }
        // Fetches all messages between the dates entered
        Iterable<RawMessage> lst = rawMessageRepo.findMessagesBetween(start, end,
                RawMessageState.UNPROCESSED, productionLine.getSequencia());

        for (RawMessage rawMessage : lst) {
            final ProcessMessage type = factory.build(rawMessage.messageType());
            productionOrderExecution = processSvc.processMessage(productionOrderExecution, type, rawMessage); 
        }

    }

}

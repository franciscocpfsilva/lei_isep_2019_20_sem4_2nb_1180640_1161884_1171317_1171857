/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.spm.application;

import eapli.base.productionorderexecution.domain.ProductionOrderExecution;
import eapli.base.rawmessage.domain.RawMessage;
import java.util.NoSuchElementException;

/**
 *
 * @author João Cunha
 */
public interface ProcessMessage {

    /**
     * Starts processing. Receive a rawMessage that contains the information to
     * be processed and a production order execution that will be updated with
     * the information contained in the rawMessage
     *
     * @param executionOrder Execution of production order that will update
     * @param rawMessage raw Message
     */
    void begin(ProductionOrderExecution executionOrder, RawMessage rawMessage)
            throws NoSuchElementException;

    /**
     * Process the message by updating the information in the execution order
     *
     * @return Production Order Execution
     */
    ProductionOrderExecution processMessage() throws NoSuchElementException, NumberFormatException;
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.spm.application;

import eapli.base.domain.model.general.Item;
import eapli.base.domain.model.general.Quantity;
import eapli.base.errormessage.domain.ErrorMessage;
import eapli.base.errormessage.domain.ErrorType;
import eapli.base.errormessage.repositories.ErrorMessagesRepository;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.productionline.domain.ProductionLine;
import eapli.base.productionline.repositories.ProductionLinesRepository;
import eapli.base.productionorderexecution.domain.Batch;
import eapli.base.productionorderexecution.domain.Production;
import eapli.base.productionorderexecution.domain.ProductionOrderExecution;
import eapli.base.productionorderexecution.repositories.ProductionOrderExecutionRepository;
import eapli.base.products.domain.Product;
import eapli.base.products.domain.ProductID;
import eapli.base.products.repositories.ProductsRepository;
import eapli.base.rawmessage.domain.RawMessage;
import eapli.base.rawmessage.domain.RawMessageState;
import eapli.base.rawmessage.repositories.RawMessagesRepository;
import eapli.base.storage.domain.StorageUnit;
import eapli.base.storage.domain.StorageUnitID;
import eapli.base.storage.repositories.StorageUnitsRepository;
import java.time.LocalDateTime;
import java.util.NoSuchElementException;
import java.util.Optional;

/**
 *
 * @author João Cunha
 */
public class ProcessMessageProductionDelivery implements ProcessMessage {

    private final String FILE_SEPARATOR = ";";

    private final int POS_PRODUCT_ID = 3;
    private final int POS_QUANTITY = 4;
    private final int POS_STORAGE = 5;
    private final int POS_BATCH = 6;
    private final int WITH_BASH = 7;

    private final ProductsRepository productsRepo
            = PersistenceContext.repositories().products();
    private final StorageUnitsRepository storageUnitsRepo
            = PersistenceContext.repositories().storageUnits();
    private final ErrorMessagesRepository errorMessagesRepo
            = PersistenceContext.repositories().errorMessages();
    private final RawMessagesRepository rawMessagesRepo
            = PersistenceContext.repositories().rawMessages();
    private final ProductionLinesRepository productionLinesRepo
            = PersistenceContext.repositories().productionLines();
    private final ProductionOrderExecutionRepository productionOrderExeRepo
            = PersistenceContext.repositories().productionOrderExecution();

    private ProductionOrderExecution executionOrder;
    private RawMessage rawMessage;

    @Override
    public void begin(ProductionOrderExecution executionOrder, RawMessage rawMessage) throws NoSuchElementException {
        this.executionOrder = executionOrder;
        this.rawMessage = rawMessage;
    }

    @Override
    public ProductionOrderExecution processMessage() throws NoSuchElementException, NumberFormatException {

        String array[];
        double number;
        try {
            array = rawMessage.rawData().split(FILE_SEPARATOR);
            number = Double.parseDouble(array[POS_QUANTITY]);
        } catch (NumberFormatException n) {
            return executionOrder;
        }

        //Create a productID and search for the product
        final ProductID productID = new ProductID(array[POS_PRODUCT_ID]);
        final Optional<Product> opt = productsRepo.ofIdentity(productID);
        if (!opt.isPresent()) {
            rawMessage.changeRawMessageState(RawMessageState.ERROR);
            rawMessagesRepo.save(rawMessage);
            ErrorMessage errorMessage = new ErrorMessage(rawMessage, ErrorType.NO_PROCT_OR_RAW_MATERIAL);
            errorMessagesRepo.save(errorMessage);
            return executionOrder;
        }

        final Product product = opt.get();

        //Create a StorageUnitID and search for the StorageUnit
        StorageUnitID storageID = StorageUnitID.valueOf(array[POS_STORAGE]);
        final Optional<StorageUnit> opt_storage = storageUnitsRepo.ofIdentity(storageID);
        if (!opt_storage.isPresent()) {
            rawMessage.changeRawMessageState(RawMessageState.ERROR);
            rawMessagesRepo.save(rawMessage);
            ErrorMessage errorMessage = new ErrorMessage(rawMessage, ErrorType.NO_STORAGE_UNIT);
            errorMessagesRepo.save(errorMessage);
            return executionOrder;
        }
        final Quantity quantity = Quantity.valueOf(number, product.unit_of_measurement());
        final Item item = new Item(productID, quantity);
        LocalDateTime date = rawMessage.generatedAtDateTime();
        Production productionDelivery;

        if (array.length == WITH_BASH) {
            Batch batch = Batch.valueOf(array[POS_BATCH]);
            productionDelivery = Production.valueOf(batch, item, storageID);
        } else {
            productionDelivery = Production.valueOf(null, item, storageID);
        }

        if (executionOrder != null) {
            if (executionOrder.addToProduction(productionDelivery)) {
                rawMessage.changeRawMessageState(RawMessageState.PROCESSED);
                rawMessagesRepo.save(rawMessage);

                executionOrder = productionOrderExeRepo.save(executionOrder);
            }
        } else {
            final ProductionLine productionline
                    = productionLinesRepo.findByMachineID(rawMessage.fromMachine());
            if (productionline != null) {
                ProductionOrderExecution productionOrderExecution
                        = productionOrderExeRepo.findProductionOrderExecutionBetweenDate(date, productionline.getProductionLine_ID());
                if (productionOrderExecution != null) {
                    if (productionOrderExecution.addToProduction(productionDelivery)) {
                        rawMessage.changeRawMessageState(RawMessageState.PROCESSED);
                        rawMessagesRepo.save(rawMessage);

                        productionOrderExeRepo.save(productionOrderExecution);
                    }
                }

            }
        }

        return executionOrder;
    }
}

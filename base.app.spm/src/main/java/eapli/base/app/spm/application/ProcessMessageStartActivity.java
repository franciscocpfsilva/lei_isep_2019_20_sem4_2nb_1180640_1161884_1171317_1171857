/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.spm.application;

import eapli.base.errormessage.domain.ErrorMessage;
import eapli.base.errormessage.domain.ErrorType;
import eapli.base.errormessage.repositories.ErrorMessagesRepository;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.machinery.domain.MachineID;
import eapli.base.productionline.domain.ProductionLine;
import eapli.base.productionline.repositories.ProductionLinesRepository;
import eapli.base.productionorder.domain.ProductionOrder;
import eapli.base.productionorder.domain.ProductionOrderID;
import eapli.base.productionorder.domain.ProductionOrderState;
import eapli.base.productionorder.repositories.ProductionOrderRepository;
import eapli.base.productionorderexecution.domain.DateTypes;
import eapli.base.productionorderexecution.domain.MachineActivity;
import eapli.base.productionorderexecution.domain.ProductionOrderExecution;
import eapli.base.productionorderexecution.repositories.ProductionOrderExecutionRepository;
import eapli.base.products.domain.Product;
import eapli.base.products.domain.ProductID;
import eapli.base.products.domain.ProductionSheet;
import eapli.base.products.repositories.ProductsRepository;
import eapli.base.rawmessage.domain.RawMessage;
import eapli.base.rawmessage.domain.RawMessageState;
import eapli.base.rawmessage.repositories.RawMessagesRepository;
import java.time.LocalDateTime;
import java.util.NoSuchElementException;
import java.util.Optional;

/**
 *
 * @author João Cunha
 */
public class ProcessMessageStartActivity implements ProcessMessage {

    private final String FILE_SEPARATOR = ";";

    private final int POS_PRODUCTION_ORDER_ID = 3;

    private final ProductionOrderExecutionRepository productionOrderExeRepo
            = PersistenceContext.repositories().productionOrderExecution();
    private final ProductionOrderRepository productionOrderRepo
            = PersistenceContext.repositories().productionOrder();
    private final ErrorMessagesRepository errorMessagesRepo
            = PersistenceContext.repositories().errorMessages();
    private final RawMessagesRepository rawMessagesRepo
            = PersistenceContext.repositories().rawMessages();
    private final ProductionLinesRepository productionLinesRepo
            = PersistenceContext.repositories().productionLines();
    private final ProductsRepository productsRepo
            = PersistenceContext.repositories().products();

    private ProductionOrderExecution executionOrder;
    private RawMessage rawMessage;

    @Override
    public void begin(ProductionOrderExecution executionOrder, RawMessage rawMessage)
            throws NoSuchElementException {
        this.executionOrder = executionOrder;
        this.rawMessage = rawMessage;
    }

    @Override
    public ProductionOrderExecution processMessage() throws NoSuchElementException, NumberFormatException {

        final String array[] = rawMessage.rawData().split(FILE_SEPARATOR);

        final ProductionLine productionline
                = productionLinesRepo.findByMachineID(rawMessage.fromMachine());

        //Checks whether the production order field exists
        if (array.length != POS_PRODUCTION_ORDER_ID + 1) {
            rawMessage.changeRawMessageState(RawMessageState.ERROR);
            rawMessagesRepo.save(rawMessage);

            ErrorMessage errorMessage = new ErrorMessage(rawMessage, ErrorType.NO_PRODUCTION_ORDER);
            errorMessagesRepo.save(errorMessage);
            throw new NoSuchElementException(rawMessage.rawData() + " There is no production order");
        }

        //Create a ProductionOrderID and search for the product
        final ProductionOrderID productionOrderID
                = ProductionOrderID.valueOf(array[POS_PRODUCTION_ORDER_ID]);

        final Optional<ProductionOrder> opt_prodOrder
                = productionOrderRepo.ofIdentity(productionOrderID);

        // Checks if there is a production order with the productionOrderID
        if (!opt_prodOrder.isPresent()) {
            rawMessage.changeRawMessageState(RawMessageState.ERROR);
            rawMessagesRepo.save(rawMessage);

            ErrorMessage errorMessage = new ErrorMessage(rawMessage, ErrorType.NO_PRODUCTION_ORDER);
            errorMessagesRepo.save(errorMessage);
            throw new NoSuchElementException(rawMessage.rawData()
                    + " There is no production order corresponding to this ID");
        }
        final ProductionOrder productionOrder = opt_prodOrder.get();
        productionOrder.changeProductionOrderState(ProductionOrderState.EM_EXECUCAO);

        final Optional<ProductionOrderExecution> opt_prodOrderExe
                = productionOrderExeRepo.ofIdentity(productionOrderID);

        //Create a productID and search for the product
        final ProductID productID = productionOrder.productID();
        final Optional<Product> opt = productsRepo.ofIdentity(productID);
        final Product product = opt.get();
        ProductionSheet productionSheet = product.productionSheet();

        LocalDateTime startDate = rawMessage.generatedAtDateTime();
        //Checks if an Execution Order already exists associated with the production order
        if (opt_prodOrderExe.isPresent()) {
            this.executionOrder = opt_prodOrderExe.get();
        } else {
            this.executionOrder
                    = new ProductionOrderExecution(productionOrderID, productionline.identity(),
                            productionSheet);
        }

        //Adds information to the execution order
        MachineID machineID = rawMessage.fromMachine();
        MachineActivity machineActivity
                = MachineActivity.valueOf(machineID, startDate, DateTypes.START_ACTIVITY);

        executionOrder.addToMachineActivity(machineActivity);
        int size = productionline.getSequencia().size();
        MachineID lst_machineID[] = new MachineID[size];
        productionline.getSequencia().toArray(lst_machineID);

        if (lst_machineID[0].equals(machineID)) {
            executionOrder.defineStartDate(startDate);
        }

        //Updates the status of RawMessage
        rawMessage.changeRawMessageState(RawMessageState.PROCESSED);
        rawMessagesRepo.save(rawMessage);

        executionOrder = productionOrderExeRepo.save(executionOrder);
        productionOrderRepo.save(productionOrder);

        return executionOrder;

    }

}

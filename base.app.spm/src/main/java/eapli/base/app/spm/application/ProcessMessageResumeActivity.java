/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.spm.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.machinery.domain.MachineID;
import eapli.base.productionline.domain.ProductionLine;
import eapli.base.productionline.repositories.ProductionLinesRepository;
import eapli.base.productionorderexecution.domain.DateTypes;
import eapli.base.productionorderexecution.domain.MachineActivity;
import eapli.base.productionorderexecution.domain.ProductionOrderExecution;
import eapli.base.productionorderexecution.repositories.ProductionOrderExecutionRepository;
import eapli.base.rawmessage.domain.RawMessage;
import eapli.base.rawmessage.domain.RawMessageState;
import eapli.base.rawmessage.repositories.RawMessagesRepository;
import java.time.LocalDateTime;
import java.util.NoSuchElementException;

/**
 *
 * @author João Cunha
 */
public class ProcessMessageResumeActivity implements ProcessMessage {

    private ProductionOrderExecution executionOrder;
    private RawMessage rawMessage;

    private final RawMessagesRepository rawMessagesRepo
            = PersistenceContext.repositories().rawMessages();
    private final ProductionLinesRepository productionLinesRepo
            = PersistenceContext.repositories().productionLines();
    private final ProductionOrderExecutionRepository productionOrderExeRepo
            = PersistenceContext.repositories().productionOrderExecution();

    @Override
    public void begin(ProductionOrderExecution executionOrder, RawMessage rawMessage) throws NoSuchElementException {
        this.executionOrder = executionOrder;
        this.rawMessage = rawMessage;
    }

    @Override
    public ProductionOrderExecution processMessage() throws NoSuchElementException, NumberFormatException {

        MachineID machineID = rawMessage.fromMachine();
        LocalDateTime date = rawMessage.generatedAtDateTime();

        if (executionOrder != null) {
            //Search for machine activity with the machine id
            MachineActivity machineActivity = executionOrder.fromMachineActivity(machineID);

            //Checks whether it exists
            if (machineActivity != null) {
                machineActivity.efectiveTime().addDate(date, DateTypes.RESUME_ACTIVITY);
                rawMessage.changeRawMessageState(RawMessageState.PROCESSED);
                rawMessagesRepo.save(rawMessage);

                executionOrder = productionOrderExeRepo.save(executionOrder);
            }
        } else {
            final ProductionLine productionline
                    = productionLinesRepo.findByMachineID(rawMessage.fromMachine());
            if (productionline != null) {
                ProductionOrderExecution productionOrderExecution
                        = productionOrderExeRepo.findProductionOrderExecutionBetweenDate(date, productionline.getProductionLine_ID());
                if (productionOrderExecution != null) {
                    //Search for machine activity with the machine id
                    MachineActivity machineActivity = productionOrderExecution.fromMachineActivity(machineID);

                    //Checks whether it exists
                    if (machineActivity != null) {
                        machineActivity.efectiveTime().addDate(date, DateTypes.RESUME_ACTIVITY);
                        rawMessage.changeRawMessageState(RawMessageState.PROCESSED);
                        rawMessagesRepo.save(rawMessage);

                        productionOrderExeRepo.save(productionOrderExecution);
                    }
                }

            }
        }

        return executionOrder;

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.spm;

import eapli.base.app.common.console.BaseApplication;
import eapli.base.app.spm.application.ProcessMessagesRecurrentlyController;
import eapli.framework.infrastructure.eventpubsub.EventDispatcher;

/**
 * Class to run the app in general mode. That is: i) recurrent message 
 * processing is done in the background; ii) a UI is available where the user 
 * can alternate between recurrent processing or time interval restricted 
 * processing, between other things.
 * 
 * @author franciscoferreiradasilva
 */
public class ProcessMessagesGeneralMode extends BaseApplication {

    @Override
    protected void doMain(String[] args) {

        Thread newThread = new Thread(new ProcessBlockOfTimeMainThread());
        newThread.start();
        
        // run recurrent message processing
        new ProcessMessagesRecurrentlyController().start();
        
        try{
            newThread.join();
        }catch(InterruptedException ie){
            System.out.println("Error: " + ie.getMessage());
        }
    }

    @Override
    protected String appTitle() {
        return "Message Processing System - General Mode";
    }

    @Override
    protected String appGoodbye() {
        return "Message Processing System - General Mode [End]";
    }

    @Override
    protected void doSetupEventHandlers(final EventDispatcher dispatcher) {
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.spm.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.messageprocessing.domain.ProductionLineProcessingState;
import eapli.base.messageprocessing.repositories.ProductionLineProcessingStateRepository;
import eapli.base.productionline.domain.ProductionLineID;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Service to get the processing state of a given production line. If it's not 
 * persisted yet, creates a new entry in the ProductionLineProcessingState 
 * table with state for the given production line defaulted to false (inactive). 
 * 
 * @author franciscoferreiradasilva
 */
public class CheckProcessingStateService {
    
    private static final Logger logger = LogManager.getLogger(CheckProcessingStateService.class);

    private ProductionLineProcessingStateRepository processingStateRepo 
            = PersistenceContext.repositories().processingState();
    
    public Boolean checkProcessingStateForProductionLine(ProductionLineID productionLineId) {
        
        // To make sure it detects new changes on the repository
        processingStateRepo = PersistenceContext.repositories().processingState();

        // Checks if there is an entry with the processing status for that production line 
        Optional<ProductionLineProcessingState> opt = processingStateRepo.ofIdentity(productionLineId);
        
        if (opt.isPresent()) {
            return opt.get().processingState();
        }
        
        // If not present, creates entry and returns the default value for the state
        ProductionLineProcessingState newEntry = new ProductionLineProcessingState(productionLineId);
        processingStateRepo.save(newEntry);
        
        return newEntry.processingState();
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.spm.application;

import eapli.base.errormessage.domain.ErrorMessage;
import eapli.base.errormessage.domain.ErrorType;
import eapli.base.errormessage.repositories.ErrorMessagesRepository;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.machinery.domain.MachineID;
import eapli.base.productionline.domain.ProductionLine;
import eapli.base.productionline.repositories.ProductionLinesRepository;
import eapli.base.productionorder.domain.ProductionOrder;
import eapli.base.productionorder.domain.ProductionOrderID;
import eapli.base.productionorder.domain.ProductionOrderState;
import eapli.base.productionorder.repositories.ProductionOrderRepository;
import eapli.base.productionorderexecution.domain.DateTypes;
import eapli.base.productionorderexecution.domain.DesviationService;
import eapli.base.productionorderexecution.domain.MachineActivity;
import eapli.base.productionorderexecution.domain.ProductionOrderExecution;
import eapli.base.productionorderexecution.repositories.ProductionOrderExecutionRepository;
import eapli.base.rawmessage.domain.RawMessage;
import eapli.base.rawmessage.domain.RawMessageState;
import eapli.base.rawmessage.repositories.RawMessagesRepository;
import java.time.LocalDateTime;
import java.util.NoSuchElementException;
import java.util.Optional;

/**
 *
 * @author João Cunha
 */
public class ProcessMessageEndActivity implements ProcessMessage {

    private final String FILE_SEPARATOR = ";";

    private final int POS_PRODUCTION_ORDER_ID = 3;

    private final ProductionOrderExecutionRepository productionOrderExecutionRepo
            = PersistenceContext.repositories().productionOrderExecution();
    private final ErrorMessagesRepository errorMessagesRepo
            = PersistenceContext.repositories().errorMessages();
    private final RawMessagesRepository rawMessagesRepo
            = PersistenceContext.repositories().rawMessages();
    private final ProductionLinesRepository productionLinesRepo
            = PersistenceContext.repositories().productionLines();
    private final ProductionOrderRepository productionOrderRepo
            = PersistenceContext.repositories().productionOrder();

    private ProductionOrderExecution executionOrder;
    private RawMessage rawMessage;

    @Override
    public void begin(ProductionOrderExecution executionOrder, RawMessage rawMessage) throws NoSuchElementException {
        this.executionOrder = executionOrder;
        this.rawMessage = rawMessage;
    }

    @Override
    public ProductionOrderExecution processMessage() throws NoSuchElementException, NumberFormatException {

        final String array[] = rawMessage.rawData().split(FILE_SEPARATOR);

        //Checks whether the production order field exists
        if (array.length != POS_PRODUCTION_ORDER_ID + 1) {
            rawMessage.changeRawMessageState(RawMessageState.ERROR);
            rawMessagesRepo.save(rawMessage);

            ErrorMessage errorMessage = new ErrorMessage(rawMessage, ErrorType.NO_PRODUCTION_ORDER);
            errorMessagesRepo.save(errorMessage);
            throw new NoSuchElementException(rawMessage.rawData() + " There is no production order");
        }

        //Create a productID and search for the product
        final ProductionOrderID productionOrderID
                = ProductionOrderID.valueOf(array[POS_PRODUCTION_ORDER_ID]);
        //Search for Production order with the production order id
        final Optional<ProductionOrder> opt_prodOrder
                = productionOrderRepo.ofIdentity(productionOrderID);

        // Checks if there is a production order with the ID
        if (!opt_prodOrder.isPresent()) {
            rawMessage.changeRawMessageState(RawMessageState.ERROR);
            rawMessagesRepo.save(rawMessage);

            ErrorMessage errorMessage = new ErrorMessage(rawMessage, ErrorType.NO_PRODUCTION_ORDER);
            errorMessagesRepo.save(errorMessage);
            throw new NoSuchElementException(rawMessage.rawData() + " There is no production order corresponding to this ID");
        }
        final ProductionOrder productionOrder = opt_prodOrder.get();

        final ProductionLine productionline
                = productionLinesRepo.findByMachineID(rawMessage.fromMachine());

        MachineID machineID = rawMessage.fromMachine();
        LocalDateTime date = rawMessage.generatedAtDateTime();
        if (executionOrder != null) {
            if (executionOrder.identity().equals(productionOrderID)) {

                //Search for machine activity with the machine id
                MachineActivity machineActivity = executionOrder.fromMachineActivity(machineID);

                //Checks whether it exists
                if (machineActivity != null) {
                    machineActivity.efectiveTime().addDate(date, DateTypes.END_ACTIVITY);
                    rawMessage.changeRawMessageState(RawMessageState.PROCESSED);
                    rawMessagesRepo.save(rawMessage);

                    int size = productionline.getSequencia().size();
                    MachineID lst_machineID[] = new MachineID[size];
                    productionline.getSequencia().toArray(lst_machineID);
                    //If it's the last machine
                    if (lst_machineID[size - 1].equals(machineID)) {
                        executionOrder.defineEndDate(date);
                        DesviationService serv = new DesviationService();
                        executionOrder = serv.calcDesviations(executionOrder, productionOrder.quantity(), productionOrder.productID());
                        executionOrder = executionOrder.defineTotalGrossTime();
                        executionOrder = executionOrder.defineTotalEfectiveTime();
                    }
                    executionOrder = productionOrderExecutionRepo.save(executionOrder);

                    productionOrder.changeProductionOrderState(ProductionOrderState.CONCLUIDA);
                    productionOrderRepo.save(productionOrder);
                } else {
                    rawMessage.changeRawMessageState(RawMessageState.ERROR);
                    rawMessagesRepo.save(rawMessage);

                    ErrorMessage errorMessage = new ErrorMessage(rawMessage, ErrorType.NO_MACHINE_ACTIVITY);
                    errorMessagesRepo.save(errorMessage);
                }

                return executionOrder;
            }
        } else {
            final Optional<ProductionOrderExecution> opt_prodOrderExecution
                    = this.productionOrderExecutionRepo.ofIdentity(productionOrderID);
            if (opt_prodOrderExecution.isPresent()) {
                ProductionOrderExecution prodOrderExecution
                        = opt_prodOrderExecution.get();

                //Search for machine activity with the machine id
                MachineActivity machineActivity = prodOrderExecution.fromMachineActivity(machineID);

                //Checks whether it exists
                if (machineActivity != null) {
                    machineActivity.efectiveTime().addDate(date, DateTypes.END_ACTIVITY);
                    rawMessage.changeRawMessageState(RawMessageState.PROCESSED);
                    rawMessagesRepo.save(rawMessage);

                    int size = productionline.getSequencia().size();
                    MachineID lst_machineID[] = new MachineID[size];
                    productionline.getSequencia().toArray(lst_machineID);
                    //If it's the last machine! 
                    if (lst_machineID[size - 1].equals(machineID)) {
                        prodOrderExecution.defineEndDate(date);
                        productionOrder.changeProductionOrderState(ProductionOrderState.CONCLUIDA);
                        DesviationService serv = new DesviationService();
                        prodOrderExecution
                                = serv.calcDesviations(prodOrderExecution,
                                        productionOrder.quantity(), productionOrder.productID());
                        prodOrderExecution = prodOrderExecution.defineTotalGrossTime();
                        prodOrderExecution = prodOrderExecution.defineTotalEfectiveTime();
                    }

                    productionOrderExecutionRepo.save(prodOrderExecution);
                    productionOrderRepo.save(productionOrder);

                }

            }
        }
        return executionOrder;
    }

}

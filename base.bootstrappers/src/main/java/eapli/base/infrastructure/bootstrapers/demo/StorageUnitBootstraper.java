/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.infrastructure.bootstrapers.demo;

import eapli.base.storage.application.AddStorageUnitController;
import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import javax.persistence.RollbackException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author 
 */
public class StorageUnitBootstraper implements Action{
    
    private static final Logger LOGGER = LogManager.getLogger(StorageUnitBootstraper.class);
    
    AddStorageUnitController controller = new AddStorageUnitController();
    
    @Override
    public boolean execute() {
        register("DE1", "Depósito de entrada");
        register("DE2", "Depósito de saída");
        return true;
    }
    
    private void register(final String identifier, final String description) {
        try {
            controller.addStorageUnit(identifier, description);
            LOGGER.info(identifier);
        } catch (final IntegrityViolationException | ConcurrencyException | RollbackException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
            LOGGER.warn("Assuming {} already exists (activate trace log for details)", identifier);
            LOGGER.trace("Assuming existing record", e);
        }
    }
}

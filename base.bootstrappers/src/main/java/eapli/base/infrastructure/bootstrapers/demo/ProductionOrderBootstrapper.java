/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.infrastructure.bootstrapers.demo;

import eapli.base.domain.model.general.UnitOfMeasurement;
import eapli.base.productionorder.application.AddProductionOrderController;
import eapli.base.productionorder.domain.OrderID;
import eapli.framework.actions.Action;
import java.util.LinkedList;

/**
 *
 * @author João Cunha
 */
public class ProductionOrderBootstrapper implements Action {

    private final AddProductionOrderController controller = new AddProductionOrderController();

    @Override
    public boolean execute() {
        LinkedList<OrderID> lst = new LinkedList<>();
        OrderID orderID = OrderID.valueOf("O120");
        lst.add(orderID);
        register("OP1000", "26-03-2019", "26-03-2019", "PR1001", lst, 1000.0, UnitOfMeasurement.UN);
        lst.removeFirst();
        orderID = OrderID.valueOf("O250");
        lst.add(orderID);
        register("OP1001", "26-03-2019", "26-03-2019", "PR1001", lst, 500.0, UnitOfMeasurement.UN);

        return true;
    }

    private void register(final String productionOrderID, final String dateIssue,
            final String ExecutionDate, final String productId, final LinkedList<OrderID> orderId,
            final double quantity, final UnitOfMeasurement unit) {
        controller.addExecutionOrder(productionOrderID, dateIssue, ExecutionDate, productId, orderId, quantity, unit);
    }
}

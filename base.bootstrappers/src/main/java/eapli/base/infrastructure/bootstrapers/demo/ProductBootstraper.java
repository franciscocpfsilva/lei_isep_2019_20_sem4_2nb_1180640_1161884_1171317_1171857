/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.infrastructure.bootstrapers.demo;

import eapli.base.domain.model.general.UnitOfMeasurement;
import eapli.base.infrastructure.bootstrapers.TestDataConstants;
import eapli.base.products.application.AddProductController;
import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import javax.persistence.RollbackException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author franciscoferreiradasilva
 */
public class ProductBootstraper implements Action {
    
    private static final Logger LOGGER = LogManager.getLogger(eapli.base.infrastructure.bootstrapers.demo.ProductBootstraper.class);

    private final AddProductController controller = new AddProductController();
    
    @Override
    public boolean execute() {
        register("PR1001", "CPR9001", TestDataConstants.PRODUCT_ROLHA_CHAMPANHE, 
                TestDataConstants.PRODUCT_ROLHA_CHAMPANHE, "Rolha", UnitOfMeasurement.UN);
        register("PR1002", "CPR9002", TestDataConstants.PRODUCT_ROLHA_NATURAL, 
                TestDataConstants.PRODUCT_ROLHA_NATURAL, "Rolha", UnitOfMeasurement.UN);
        register("PR1003", "CPR9003", TestDataConstants.PRODUCT_ROLHA_MICRO_GRANULADA, 
                TestDataConstants.PRODUCT_ROLHA_MICRO_GRANULADA, "Rolha", UnitOfMeasurement.UN);
        register("PR1004", "CPR9004", TestDataConstants.PRODUCT_ROLHA_CAPSULADA, 
                TestDataConstants.PRODUCT_ROLHA_CAPSULADA, "Rolha", UnitOfMeasurement.UN);
        register("PR1005", "CPR9005", TestDataConstants.PRODUCT_ROLHA_SINTETICA, 
                TestDataConstants.PRODUCT_ROLHA_SINTETICA, "Rolha", UnitOfMeasurement.UN);
        register("PR1006", "CPR9006", TestDataConstants.PRODUCT_SAPATILHA_GEOX, 
                TestDataConstants.PRODUCT_SAPATILHA_GEOX, "Calçado", UnitOfMeasurement.UN);
        register("PR1007", "CPR9007", TestDataConstants.PRODUCT_SAPATO_VELA, 
                TestDataConstants.PRODUCT_SAPATO_VELA, "Calçado", UnitOfMeasurement.UN);
        register("PR1008", "CPR9008", TestDataConstants.PRODUCT_CALCADO_MONTANHA, 
                TestDataConstants.PRODUCT_CALCADO_MONTANHA, "Calçado", UnitOfMeasurement.UN);
        return true;
    }
    
    private void register(final String factoryId, final String commercialId, final String briefDesc, 
            final String fullDesc, final String category, final UnitOfMeasurement unitMeasure) {
        try {
            controller.addProduct(factoryId, commercialId, briefDesc, fullDesc, category, unitMeasure);
            LOGGER.info(briefDesc);
        } catch (final IntegrityViolationException | ConcurrencyException | RollbackException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
            LOGGER.warn("Assuming {} already exists (activate trace log for details)", briefDesc);
            LOGGER.trace("Assuming existing record", e);
        }
    }
    
}

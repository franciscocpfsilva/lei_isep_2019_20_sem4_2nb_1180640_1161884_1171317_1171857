/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.infrastructure.bootstrapers.demo;

import eapli.base.infrastructure.bootstrapers.UsersBootstrapperBase;
import eapli.base.rawmaterials.application.AddRawMaterialCategoryController;
import eapli.framework.actions.Action;

public class RawMaterialCategoryBootstrapper implements Action {

    /**
     * Method for bootstrapping RawMaterialCategorys
     *
     * @return Boolean True if all categorys were added.
     */
    @Override
    public boolean execute() {
        addRawMaterialCategory("1", "Madeira");
        addRawMaterialCategory("2", "Pastico");
        addRawMaterialCategory("3", "Pedra");
        addRawMaterialCategory("4", "Metal");
        addRawMaterialCategory("5", "Ceramica");
        addRawMaterialCategory("6", "Textil");

        return true;
    }

    /**
     * Method creates an instance of RawMaterialCategory Controller and call's 
     * for it's method of adding RawMaterialCategory
     *
     */
    private void addRawMaterialCategory(final String identifier, final String description) {

        AddRawMaterialCategoryController controller = new AddRawMaterialCategoryController();
        controller.addAddRawMaterialCategory(identifier, description);
    }
}

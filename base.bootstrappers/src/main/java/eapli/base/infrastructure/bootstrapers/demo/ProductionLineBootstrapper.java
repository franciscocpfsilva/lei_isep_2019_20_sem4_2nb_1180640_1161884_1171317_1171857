/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.infrastructure.bootstrapers.demo;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.machinery.domain.Machine;
import eapli.base.machinery.domain.MachineID;
import eapli.base.machinery.repositories.MachinesRepository;
import eapli.base.productionline.application.AddProductionLineController;
import eapli.base.productionline.domain.ProductionLine;
import eapli.base.productionline.domain.ProductionLineID;
import eapli.framework.actions.Action;
/**
 *
 * @author João Cunha
 */
public class ProductionLineBootstrapper implements Action {

    private final AddProductionLineController controller = new AddProductionLineController();
    private final MachinesRepository machineRepo
            = PersistenceContext.repositories().machines();
    

    @Override
    public boolean execute() {
        ProductionLine p0003 = new ProductionLine(new ProductionLineID("P0003"));
        ProductionLine p0004 = new ProductionLine(new ProductionLineID("P0004"));

        Machine machine1 = machineRepo.findByID(MachineID.valueOf("M1000")).get();

        Machine machine2 = machineRepo.findByID(MachineID.valueOf("M970")).get();

        Machine machine3 = machineRepo.findByID(MachineID.valueOf("M750")).get();

        Machine machine4 = machineRepo.findByID(MachineID.valueOf("M2500")).get();

        addMachineToSequence(machine1, 0, p0003);
        addMachineToSequence(machine2, 1, p0003);
        register(p0003);

        addMachineToSequence(machine3, 0, p0004);
        addMachineToSequence(machine4, 1, p0004);
        register(p0004);

        return true;
    }

    private void register(final ProductionLine prod) {
        controller.saveProductionLine(prod);

    }

    private void addMachineToSequence(final Machine machine, final int position, final ProductionLine prod) {
        controller.addMachineToSequence(position, machine, prod);

    }
}

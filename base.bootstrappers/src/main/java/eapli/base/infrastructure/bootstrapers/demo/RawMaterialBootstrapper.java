/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.infrastructure.bootstrapers.demo;

import eapli.base.domain.model.general.UnitOfMeasurement;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.rawmaterials.application.AddRawMaterialController;
import eapli.base.rawmaterials.domain.RawMaterialCategory;
import eapli.base.rawmaterials.domain.RawMaterialCategoryID;
import eapli.base.rawmaterials.repositories.RawMaterialCategoryRepository;
import eapli.framework.actions.Action;

/**
 *
 * @author joaomachado
 */
public class RawMaterialBootstrapper implements Action {

    private final RawMaterialCategoryRepository rawMaterialCategoruRepo = PersistenceContext.repositories().rawMaterialCategories();
    private final AddRawMaterialController controller = new AddRawMaterialController();

    private RawMaterialCategory getRawMaterialCategory(final RawMaterialCategoryID acronym) {
        return rawMaterialCategoruRepo.ofIdentity(acronym).orElseThrow(IllegalStateException::new);
    }
    private static final RawMaterialCategoryID madeira = new RawMaterialCategoryID("1");
    private static final RawMaterialCategoryID plasticos = new RawMaterialCategoryID("2");
    private static final RawMaterialCategoryID textil = new RawMaterialCategoryID("6");
    private static final byte pdf[] = new byte[]{1};
    /**
     * Method for bootstrapping RawMaterialCategorys
     *
     * @return Boolean True if all categorys were added.
     */
    @Override
    public boolean execute() {
        
        final RawMaterialCategory Madeira = getRawMaterialCategory(madeira);
        final RawMaterialCategory Plasticos = getRawMaterialCategory(plasticos);
        final RawMaterialCategory Textil = getRawMaterialCategory(textil);

        addRawMaterial(Madeira,"T01","Base Salto Alto Tamanho32",UnitOfMeasurement.METRE,pdf);
        addRawMaterial(Plasticos,"P01","Logo Nike M",UnitOfMeasurement.UN,pdf);
        addRawMaterial(Textil,"TA0102","Cordões Pretos Basico L",UnitOfMeasurement.UN,pdf);
        
        return true;
    }

    /**
     * Method creates an instance of RawMaterialCategory Controller and call's
     * for it's method of adding RawMaterialCategory
     *
     */
    private void addRawMaterial(final RawMaterialCategory rawMaterialCategory, 
            final String rawMaterialID,final String description,final UnitOfMeasurement unitOfMeasurement,final byte[] pdf) {


        controller.addRawMaterial(rawMaterialCategory, rawMaterialID, description, unitOfMeasurement, pdf);
    }
}

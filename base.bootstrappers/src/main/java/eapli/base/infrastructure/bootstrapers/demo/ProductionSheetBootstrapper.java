/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.infrastructure.bootstrapers.demo;

import eapli.base.domain.model.general.Item;
import eapli.base.domain.model.general.Quantity;
import eapli.base.domain.model.general.UnitOfMeasurement;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.products.application.AddProductionSheetController;
import eapli.base.products.domain.Product;
import eapli.base.products.domain.ProductID;
import eapli.base.products.repositories.ProductsRepository;
import eapli.base.rawmaterials.domain.RawMaterialID;
import eapli.framework.actions.Action;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author João Cunha
 */
public class ProductionSheetBootstrapper implements Action {

    private final AddProductionSheetController controller = new AddProductionSheetController();
    private final ProductsRepository productsRepo
            = PersistenceContext.repositories().products();

    @Override
    public boolean execute() {

        ProductID productID = ProductID.valueOf("PR1001");

        Optional<Product> opt_product = productsRepo.ofIdentity(productID);
        Product product = opt_product.get();
        List<Item> lst_itens = new LinkedList();
        Quantity quantity = Quantity.valueOf(50.0, UnitOfMeasurement.UN);
        Item item = Item.valueOf(RawMaterialID.valueOf("P01"), quantity);
        lst_itens.add(item);

        register(product, lst_itens);
        return true;
    }

    private void register(final Product product, List<Item> lst_itens) {
        controller.addProductionSheet(product, 50.0, lst_itens);
    }
}

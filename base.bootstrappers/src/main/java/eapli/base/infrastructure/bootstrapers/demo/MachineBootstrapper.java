/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.infrastructure.bootstrapers.demo;

import eapli.base.machinery.application.AddMachineController;
import eapli.framework.actions.Action;

/**
 *
 * @author 
 */
public class MachineBootstrapper implements Action{

    @Override
    public boolean execute() {
        registerMachine("M1000", "10000001", "Maquina de recolha de matérias Primas",
                "08-05-2019", "YAMAHA", "Serie750", 10000);
        registerMachine("M970", "10333222", "Maquina de analise de matérias Primas",
                "05-01-2019", "SIEMENS", "Serie920", 12310);
        registerMachine("M2297", "32312331", "Maquina de cortes",
                "26-11-2019", "SIEMENS", "Serie1570", 13333);
        registerMachine("M500", "42312331", "Maquina de fazer lotes",
                "09-02-2018", "YAMAHA", "Serie430", 150);
        registerMachine("M750", "15443212", "Maquina de prensar",
                "11-11-2018", "MITSUBISHI", "A860", 750);
        registerMachine("M2500", "72632862", "Maquina de analise de qualidade",
                "27-04-2020", "SIEMENS", "Serie920", 56798);
        
         return true;
    }
    
    private void registerMachine(final String machineID, final String serialNumber, 
            final String machineDescription,final String installationDate, 
            final String brand, final String model, final int communicationProtocolID) {
        
        AddMachineController controller = new AddMachineController();
        controller.addMachine(machineID, serialNumber, machineDescription, 
                installationDate, brand, model, communicationProtocolID);     
    }
}

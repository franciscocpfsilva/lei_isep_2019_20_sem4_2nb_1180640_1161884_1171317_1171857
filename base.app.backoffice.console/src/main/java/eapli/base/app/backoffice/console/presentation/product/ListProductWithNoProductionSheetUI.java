/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.product;

import eapli.base.products.application.ListProductWithNoProductionSheetController;
import eapli.base.products.domain.Product;
import eapli.framework.infrastructure.authz.application.exceptions.UnauthorizedException;
import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;
import java.util.ArrayList;

/**
 * US2003 - List Product With No Production Sheet
 * @author
 */
public class ListProductWithNoProductionSheetUI extends AbstractListUI<Product>{

    private final ListProductWithNoProductionSheetController controller = 
            new ListProductWithNoProductionSheetController();
    
    @Override
    protected Iterable<Product> elements() {
        try{
            final Iterable<Product> listProducts = this.controller.listProductWithNoProductionSheet();
            return listProducts;
        } catch (UnauthorizedException e) {
            System.out.println("\nErro: Unauthorized User");
        }
        return  new ArrayList<>();
    }

    @Override
    protected Visitor<Product> elementPrinter() {
        return new ProductPrinter();
    }

    @Override
    protected String elementName() {
        return "Product";
    }

    @Override
    protected String listHeader() {
        return "PRODUCT";
    }
    
    @Override
    public String headline() {
        return "List products with no production sheet";
    }
    
    @Override
    protected String emptyMessage() {
        return "No data.";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.unitofmeasurement;

import eapli.base.domain.model.general.UnitOfMeasurement;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author 
 */
public class UnitOfMeasurementPrinter implements Visitor<UnitOfMeasurement> {

    @Override
    public void visit(final UnitOfMeasurement visitee) {
        System.out.printf("%-30s", visitee.name());
    }
}

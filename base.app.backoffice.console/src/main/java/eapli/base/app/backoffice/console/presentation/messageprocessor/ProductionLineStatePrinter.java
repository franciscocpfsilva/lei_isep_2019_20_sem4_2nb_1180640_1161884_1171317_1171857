/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.messageprocessor;

import eapli.base.messageprocessing.domain.ProductionLineProcessingState;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author joaomachado
 */
@SuppressWarnings("squid:S106")
public class ProductionLineStatePrinter implements Visitor<ProductionLineProcessingState> {

    @Override
    public void visit(final ProductionLineProcessingState visitee) {
        System.out.printf("%-15s%-30s", visitee.identity(), visitee.processingState());
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.machine;

import eapli.base.machinery.application.AddConfigurationFileController;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.infrastructure.authz.application.exceptions.UnauthorizedException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.NoSuchElementException;
import javax.persistence.RollbackException;

/**
 *
 * @author joaomachado
 */
public class AddConfigurationFileUI extends AbstractUI {

    private final AddConfigurationFileController theController = new AddConfigurationFileController();

    @Override
    protected boolean doShow() {

        String machineID = Console.readNonEmptyLine("Machine ID", "An ID is requiered");

        final String configurationFileDescription = Console.readLine("Configuration File Description:");

        byte[] configurationFile = null;
        final String filePath = Console.readLine("File Path:");
        Path path = Paths.get(filePath);
        try {
            configurationFile = Files.readAllBytes(path);
        } catch (IOException ex) {
            System.out.println("There was an error trying to read the Configuration File.");
            return false;
        }
        try {
            this.theController.addConfigurationFile(machineID, configurationFileDescription, configurationFile);
            System.out.println("\nConfiguration File: " + configurationFileDescription + " was added to the MachineID: " + machineID);
        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("\nError: That ID is already in use.");
        } catch (UnauthorizedException e) {
            System.out.println("\nErro: " + e.getMessage() + ".");
        } catch (final IllegalArgumentException e) {
            System.out.println("\nError: " + e.getMessage() + ".");
        } catch (final NoSuchElementException e) {
            System.out.println("\nError: Thre is no Machine with that ID");
        } catch (final RollbackException e) {
            System.out.println("\nError: " + e.getMessage() + ".");

        }
        return false;
    }

    /**
     * Method that shows the SubMenu Title
     *
     * @return The title
     */
    @Override
    public String headline() {
        return "Add Configuration File";
    }

}

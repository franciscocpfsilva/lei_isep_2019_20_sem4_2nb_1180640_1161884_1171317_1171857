/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.product;

import eapli.base.products.importing.application.FileType;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author Francisco Silva on 09/05/2020
 */
@SuppressWarnings("squid:S106")
public class FileTypePrinter implements Visitor<FileType> {

    @Override
    public void visit(final FileType visitee) {
        System.out.printf("%-30s", visitee.name());
    }
    
}

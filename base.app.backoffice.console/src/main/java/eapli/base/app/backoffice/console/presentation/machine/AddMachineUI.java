/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.machine;

import eapli.base.machinery.application.AddMachineController;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.infrastructure.authz.application.exceptions.UnauthorizedException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import java.time.format.DateTimeParseException;
import javax.persistence.PersistenceException;
import javax.persistence.RollbackException;

/**
 *
 * @author joaomachado
 */
public class AddMachineUI extends AbstractUI {

    private final AddMachineController theController = new AddMachineController();

    /**
     * Method to show the UI for adding a Machine
     *
     * @return True if Machine was added
     */
    @Override
    protected boolean doShow() {
        final String machineID = Console.readLine("Machine ID:");
        final String serialNumber = Console.readLine("Machine Serial Number:");
        final String machineDescription = Console.readLine("Machine Description:");
        final String installationDate = Console.readLine("Machine Installation Date: (dd-MM-yyyy)");
        final String brand = Console.readLine("Machine Brand:");
        final String model = Console.readLine("Machine Model:");
        final Integer communicationProtocolID = Console.readInteger("Machine Communication Protocol ID:");
        try {
            this.theController.addMachine(machineID, serialNumber, machineDescription, installationDate, brand, model, communicationProtocolID);
            System.out.println("\nMachine with the ID: " + machineID + " and Serial Number: " + serialNumber + " saved with success.");
        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("\nError: That ID is already in use.");
        } catch (@SuppressWarnings("unused") final DateTimeParseException e) {
            System.out.println("\nError: Wrong date format.");
        } catch (UnauthorizedException e) {
            System.out.println("\nErro: " + e.getMessage() + ".");
        } catch (final RollbackException e) {
            if (e.getCause() instanceof PersistenceException && e.getCause().getMessage().contains("ConstraintViolation")) {
                System.out.println("\nAt least one ID is already in use!");
            } else {
                System.out.println("\nError: " + e.getMessage() + ".");
            }
        } catch (final IllegalArgumentException e) {
            System.out.println("\nError: " + e.getMessage() + ".");
        }

        return false;
    }

    /**
     * Method that shows the SubMenu Title
     *
     * @return  The title 
     */
    @Override
    public String headline() {
        return "Add Machine";
    }

}

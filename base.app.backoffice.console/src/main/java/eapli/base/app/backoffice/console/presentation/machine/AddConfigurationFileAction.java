/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.machine;

import eapli.framework.actions.Action;

/**
 *
 * @author joaomachado
 */
public class AddConfigurationFileAction implements Action {

    /**
     * Method that calls the SubMenu of Adding AddConfigurationFile
     *
     * @return True if AddConfigurationFile was added
     */
    @Override
    public boolean execute() {
        return new AddConfigurationFileUI().show();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.rawmaterial;

import eapli.base.rawmaterials.domain.RawMaterialCategory;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author joaomachado
 */
@SuppressWarnings("squid:S106")
class RawMaterialCategoryPrinter implements Visitor<RawMaterialCategory> {

    /**
     * Method that shows a formated print with the id and description of RawMaterialCategory
     *
     */
    @Override
    public void visit(final RawMaterialCategory visitee) {
        System.out.printf("%-10s%-30s", visitee.identity(), visitee.description());
    }

}

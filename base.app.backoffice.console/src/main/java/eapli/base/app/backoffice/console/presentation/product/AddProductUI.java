/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.product;

import eapli.base.app.backoffice.console.presentation.unitofmeasurement.UnitOfMeasurementPrinter;
import eapli.base.domain.model.general.UnitOfMeasurement;
import eapli.base.products.application.AddProductController;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.infrastructure.authz.application.exceptions.UnauthorizedException;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.util.Console;
import java.util.Arrays;
import javax.persistence.PersistenceException;
import javax.persistence.RollbackException;

/**
 *
 * @author
 */
public class AddProductUI extends AbstractUI {

    private final AddProductController theController = new AddProductController();

    @Override
    protected boolean doShow() {
        final String factory_ID = Console.readLine("Factory ID:");
        final String commercial_ID = Console.readLine("Commercial ID:");
        final String brief_desc = Console.readLine("Brief Description:");
        final String full_desc = Console.readLine("Full Description:");
        final String category = Console.readLine("Category:");

        final Iterable<UnitOfMeasurement> listOfUnitOfMeasurement = Arrays.asList(UnitOfMeasurement.values());
        final SelectWidget<UnitOfMeasurement> selectorUnitOfMeasurement
                = new SelectWidget<>("Select a Unit Of Measurement", listOfUnitOfMeasurement,
                        new UnitOfMeasurementPrinter());
        selectorUnitOfMeasurement.show();
        final UnitOfMeasurement unit = selectorUnitOfMeasurement.selectedElement();

        try {
            this.theController.addProduct(factory_ID, commercial_ID, brief_desc,
                    full_desc, category, unit);
            System.out.printf("\nSuccess: Produtc created with:\n"
                    + "FactoryID %s  and  Brief Description %s.\n",
                    factory_ID, brief_desc);
        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("That ID is already in use.");
        } catch (final IllegalArgumentException e) {
            System.out.println("\nErro: " + e.getMessage() + ".");
        } catch (final RollbackException e) {
            if (e.getCause() instanceof PersistenceException) {
                if (e.getCause().getMessage().contains("ConstraintViolation")) {
                    System.out.println("\nErro: A product with that factory and/or commercial ID already exists.");
                }
            } else {
                System.out.println("\nErro: " + e.getMessage() + ".");
            }
        } catch (UnauthorizedException e) {
            System.out.println("\nErro: " + e.getMessage() + ".");
        }

        return false;
    }

    @Override
    public String headline() {
        return "Add a new Product";
    }
}

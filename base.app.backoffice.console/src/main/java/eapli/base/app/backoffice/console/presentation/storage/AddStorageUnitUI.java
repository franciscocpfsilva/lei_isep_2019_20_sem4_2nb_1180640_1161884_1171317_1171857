/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.storage;

import eapli.base.storage.application.AddStorageUnitController;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.infrastructure.authz.application.exceptions.UnauthorizedException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import javax.persistence.PersistenceException;
import javax.persistence.RollbackException;
import org.hibernate.exception.ConstraintViolationException;

/**
 *
 * @author
 */
public class AddStorageUnitUI extends AbstractUI {

    private final AddStorageUnitController theController = new AddStorageUnitController();

    @Override
    protected boolean doShow() {
        final String identifier = Console.readLine("Storage Unit Identifier:");
        final String description = Console.readLine("Storage Unit Description:");

        try {
            this.theController.addStorageUnit(identifier, description);
            System.out.println("\nSuccess: Storage unit created with identifier " + identifier +  ".");
        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("\nError: That identifier is already in use.");
        } catch (final IllegalArgumentException e) {
            System.out.println("\nError: " + e.getMessage() + ".");
        } catch (final RollbackException e) {
            if (e.getCause() instanceof PersistenceException 
                    && e.getCause().getCause() instanceof ConstraintViolationException) {
                System.out.println("\nError: That identifier is already in use.");
            } else {
                System.out.println("\nError: Some unknown error occurred in the database.");
            }
        } catch (final UnauthorizedException e) {
            System.out.println("\nError: User doesn't have permission.");
        }
        return false;
    }

    @Override
    public String headline() {
        return "Add Storage Unit";
    }
    
}

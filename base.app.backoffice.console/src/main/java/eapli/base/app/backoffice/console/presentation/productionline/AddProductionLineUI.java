/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.productionline;

import eapli.base.productionline.application.AddProductionLineController;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.infrastructure.authz.application.exceptions.UnauthorizedException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import javax.persistence.PersistenceException;
import javax.persistence.RollbackException;

/**
 *
 * @author
 */
public class AddProductionLineUI extends AbstractUI {

    private final AddProductionLineController theController = new AddProductionLineController();

    /**
     * Show the menus and requires data from da user
     *
     * @return sucess or failure
     */
    @Override
    protected boolean doShow() {
        final String productionLine_ID = Console.readLine("Production Line ID:");

        try {
            this.theController.addProductionLine(productionLine_ID);

            System.out.printf("\nSuccess: Prodution Line created with:\n"
                    + "Production Line ID %s.\n", productionLine_ID);
        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("That ID is already in use.");
        } catch (final IllegalArgumentException e) {
            System.out.println("\nErro: " + e.getMessage() + ".");
        } catch (final RollbackException e) {
            if (e.getCause() instanceof PersistenceException) {
                if (e.getCause().getMessage().contains("ConstraintViolation")) {
                    System.out.println("\nErro: A Production Line with that ID already exists.");
                }
            } else {
                System.out.println("\nErro: " + e.getMessage() + ".");
            }
        } catch (UnauthorizedException e) {
            System.out.println("\nErro: " + e.getMessage() + ".");
        }

        return false;
    }

    /**
     * Headline for adding a production Line
     *
     * @return healine
     */
    @Override
    public String headline() {
        return "Add a new Production Line";
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.productionorder;

import eapli.framework.actions.Action;

/**
 *
 * @author joaomachado
 */
public class AddProductionOrderAction implements Action {

    @Override
    public boolean execute() {
        return new AddProductionOrderUI().show();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.productionline;

import eapli.base.app.backoffice.console.presentation.machine.MachinePrinter;
import eapli.base.machinery.domain.Machine;
import eapli.base.productionline.application.AddProductionLineController;
import eapli.base.productionline.domain.ProductionLine;
import eapli.base.productionline.domain.ProductionLineID;
import eapli.framework.application.UseCaseController;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.infrastructure.authz.application.exceptions.UnauthorizedException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.util.Console;
import java.util.NoSuchElementException;
import javax.persistence.PersistenceException;
import javax.persistence.RollbackException;

/**
 *
 * @author
 */
@UseCaseController
public class AssociateMachineProductionLineUI extends AbstractUI {

    private final AddProductionLineController theController = new AddProductionLineController();

    /**
     * Show the menus and requires data from da user
     *
     * @return sucess or failure
     */
    @Override
    protected boolean doShow() {
        try {
            final ProductionLineID productionLine_ID
                    = new ProductionLineID(Console.readLine("Production Line ID:"));
            ProductionLine productionLine = this.theController.findProductionLineByID(productionLine_ID).get();
            final Iterable<Machine> listOfMachines = theController.findMachinesAvailable();
            final SelectWidget<Machine> selectorMachine
                    = new SelectWidget<>("Select a Machine", listOfMachines,
                            new MachinePrinter());
            boolean rep = true;

            while (rep) {
                selectorMachine.show();
                final Machine machine = selectorMachine.selectedElement();
                final Integer position = Console.readInteger("Type the position you want to place the machine");
                if (theController.addMachineToSequence(position, machine, productionLine)) {
                } else {
                    System.out.println("Already exist an machine in the 'sequencia'\n");
                }
                System.out.println("1-Continue to associate machine\n0-Exit");
                rep = 1 == Console.readOption(1, 1, 0);

            }
            this.theController.saveProductionLine(productionLine);
            System.out.println("Add sequence with sucesss!");
        } catch ( final NumberFormatException ex) {
            System.out.println("The position of the machine must be greater than zero");
        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("That Macine is already associated with another Production Line.");
        } catch (final IllegalArgumentException e) {
            System.out.println("\nErro: You have to choose a Machine.");
        } catch (final UnauthorizedException e) {
            System.out.println("\nErro: " + e.getMessage() + ".");
        } catch (final NoSuchElementException e) {
            System.out.println("That Machine doesn't exist!");
        }

        return false;
    }

    /**
     * Headline for associate machine to a production Line
     *
     * @return healine
     */
    @Override
    public String headline() {
        return "Associate Machine to a Production Line";
    }
}

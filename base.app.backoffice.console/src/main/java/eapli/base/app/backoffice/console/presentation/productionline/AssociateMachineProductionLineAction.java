/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.productionline;

import eapli.framework.actions.Action;

/**
 *
 * @author
 */
public class AssociateMachineProductionLineAction implements Action {

    /**
     * Method that calls the SubMenu of Associate Machines to a Production Line
     *
     * @return True if the Machines where associated to a Production Line
     */
    @Override
    public boolean execute() {
        return new AssociateMachineProductionLineUI().show();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.messageprocessor;

import eapli.base.messageprocessing.application.MessageProcessingController;
import eapli.framework.infrastructure.authz.application.exceptions.UnauthorizedException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import javax.persistence.RollbackException;
import eapli.base.app.spm.Main;
import eapli.base.messageprocessing.application.MessageProcessingClient;
import java.time.format.DateTimeParseException;

/**
 *
 * @author joaomachado
 */
public class ProcessMessagesBlockUI extends AbstractUI {

    private final MessageProcessingController controller = new MessageProcessingController();

    /**
     * Method to show the UI for Processing Messages by Block UI
     *
     * @return True if sucess
     */
    @Override
    protected boolean doShow() {
        boolean rep = true;
        List<String> productionLinesID = new ArrayList<String>();
        final String startTime = Console.readLine("Start Date: (yyyy-MM-dd HH:mm)");
        final String endTime = Console.readLine("End Date: (yyyy-MM-dd HH:mm)");

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        try {
            LocalDateTime start = LocalDateTime.parse(startTime, formatter);
            LocalDateTime end = LocalDateTime.parse(endTime, formatter);
            productionLinesID.add(start.toString());
            productionLinesID.add(end.toString());
            while (rep) {
                String id = Console.readNonEmptyLine("Production Line ID", "An ID is required");
                if (controller.validateProductionLineState(id)) {
                    System.out.println("This Production Line is being processed in a Recurrent mode!");
                    break;
                }
                productionLinesID.add(id);
                rep = Console.readBoolean("'Y' or 'S' to add more ProdutionLines ID - 'N' to terminate\n");
            }

            String[] args = new String[productionLinesID.size()];
            for (int i = 0; i < productionLinesID.size(); i++) {
                args[i] = (String) productionLinesID.get(i);
            }

 // ----- NEW 
/*-- Gives a chance to change the server IP address */            
            if (productionLinesID.size() > 2) { // In case there is at least one valid production line
                String option = Console.readLine("\nCustomize ip address? (y/n)");
                if (option.equals("y")){
                    System.out.printf("Current configuration:\n\tSCM Server Port: %d\n\tSCM Server Ip Address: %s\n", 
                            MessageProcessingClient.PORT_N,
                            MessageProcessingClient.IP_ADDRESS);

                    String newIpAddress = Console.readLine("\nEnter a new ip address : ");
                    MessageProcessingClient.changeServerIpAddress(newIpAddress);
                }
    //            Main.main(args);
                controller.makeRequestToSpm(args);
            }
 // ----- NEW 
            
        } catch (DateTimeParseException e) {
            System.out.println("\nError: That Date format is not supported.");
        } catch (UnauthorizedException e) {
            System.out.println("\nErro: " + e.getMessage() + ".");
        } catch (final IllegalArgumentException e) {
            System.out.println("\nError: " + e.getMessage() + ".");
        } catch (final NoSuchElementException e) {
            System.out.println("\nError: There is no Production Line with that ID");
        } catch (final RollbackException e) {
            System.out.println("\nError: " + e.getMessage() + ".");

        }
        return false;
    }

    @Override
    public String headline() {
        return "Process Messages in Block";
    }
}

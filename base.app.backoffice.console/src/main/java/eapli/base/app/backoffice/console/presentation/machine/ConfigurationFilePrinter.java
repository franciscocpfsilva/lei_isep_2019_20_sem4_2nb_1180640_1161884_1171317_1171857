/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.machine;

import eapli.base.machinery.domain.ConfigurationFile;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author leona
 */
@SuppressWarnings("squid:S106")
public class ConfigurationFilePrinter implements Visitor<ConfigurationFile> {
    
    @Override
    public void visit(final ConfigurationFile visitee) {
        System.out.printf("%-10s", visitee.configurationFileDescription());
    }
    
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.messageprocessor;

import eapli.base.messageprocessing.application.MessageProcessingController;
import eapli.framework.infrastructure.authz.application.exceptions.UnauthorizedException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import java.util.NoSuchElementException;
import javax.persistence.RollbackException;

/**
 *
 * @author joaomachado
 */
public class ChangeProcessMessagesRecurrentlyStateUI extends AbstractUI {

    private final MessageProcessingController controller = new MessageProcessingController();

    /**
     * Method to show the UI for Changing Processing Messages Rcurrently
     *
     * @return True if production line state was changed
     */
    @Override
    protected boolean doShow() {
        String productionLineID = Console.readNonEmptyLine("Production Line ID", "An ID is requiered");
        boolean productionLineState = Console.readBoolean("(Y) For activating processing - (N) For deactivating processing");
        try {
            this.controller.changeProductionLineState(productionLineID, productionLineState);
            System.out.println("\nProduction Line ID: " + productionLineID + " changed processing state to: " + productionLineState);
        } catch (UnauthorizedException e) {
            System.out.println("\nErro: " + e.getMessage() + ".");
        } catch (final IllegalArgumentException e) {
            System.out.println("\nError: " + e.getMessage() + ".");
        } catch (final NoSuchElementException e) {
            System.out.println("\nError: TThere is no Production Line with that ID");
        } catch (final RollbackException e) {
            System.out.println("\nError: " + e.getMessage() + ".");

        }
        return false;
    }

    @Override
    public String headline() {
        return "Process Messages Recurrently";
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.messageprocessor;

import eapli.framework.actions.Action;

/**
 *
 * @author joaomachado
 */
public class ChangeProcessMessagesRecurrentlyStateAction implements Action {

    /**
     * Method that calls the SubMenu for Changing Processing Messages Rcurrently
     *
     * @return True if sucess
     */
    @Override
    public boolean execute() {
        return new ChangeProcessMessagesRecurrentlyStateUI().show();
    }
}

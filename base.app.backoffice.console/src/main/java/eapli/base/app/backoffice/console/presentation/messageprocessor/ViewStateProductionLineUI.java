/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.messageprocessor;

import eapli.base.messageprocessing.application.MessageProcessingController;
import eapli.base.messageprocessing.domain.ProductionLineProcessingState;
import eapli.framework.infrastructure.authz.application.exceptions.UnauthorizedException;
import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;
import java.util.ArrayList;

/**
 *
 * @author joaomachado
 */
public class ViewStateProductionLineUI extends AbstractListUI<ProductionLineProcessingState> {

    private final MessageProcessingController controller = new MessageProcessingController();

    /**
     * Method to show all the production lines ID and state
     *
     * @return returns iterable with respective objects
     */
    @Override
    protected Iterable<ProductionLineProcessingState> elements() {
        try {
            final Iterable<ProductionLineProcessingState> list = this.controller.viewProductionLineState();
            return list;
        } catch (UnauthorizedException e) {
            System.out.println("\nErro: Unauthorized User");
        }
        return new ArrayList<>();
    }

    @Override
    protected Visitor<ProductionLineProcessingState> elementPrinter() {
        return new ProductionLineStatePrinter();
    }

    @Override
    protected String elementName() {
        return "Production Line";
    }

    @Override
    protected String listHeader() {
        return "Production Lines State";
    }

    @Override
    protected String emptyMessage() {
        return "No data.";
    }

    @Override
    public String headline() {
        return "View State of Production Lines";
    }
}

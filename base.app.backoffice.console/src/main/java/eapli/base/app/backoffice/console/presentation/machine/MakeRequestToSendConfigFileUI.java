/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.machine;

import eapli.base.communication.scm.application.MakeRequestToSendConfigFileController;
import eapli.base.communication.scm.application.MakeRequestToSendConfigFileClient;
import eapli.base.machinery.domain.ConfigurationFile;
import eapli.base.machinery.domain.Machine;
import eapli.framework.infrastructure.authz.application.exceptions.UnauthorizedException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.util.Console;

/**
 *
 * @author leona
 */
public class MakeRequestToSendConfigFileUI extends AbstractUI {
    
    private final MakeRequestToSendConfigFileController controller = new MakeRequestToSendConfigFileController();

    @Override
    protected boolean doShow() {
        
        Machine machine = null;
        ConfigurationFile configFile=null;
//        CommunicationProtocolID communicationProtocolID;
        
        String option = Console.readLine("Customize ip address? (y/n)");
        if (option.equals("y")){
            System.out.printf("Current configuration:\n\tSCM Server Port: %d\n\tSCM Server Ip Address: %s\n", 
                    MakeRequestToSendConfigFileClient.PORT_N,
                    MakeRequestToSendConfigFileClient.IP_ADDRESS);
           
            String newIpAddress = Console.readLine("\nEnter a new ip address : ");
            MakeRequestToSendConfigFileClient.changeServerIpAddress(newIpAddress);
        }
        
        try{
            final Iterable<Machine> machines = this.controller.allMachines();
            final SelectWidget<Machine> selector = new SelectWidget<>("Choose a Machine:", machines,
                    new MachinePrinter());
            selector.show();
            machine = selector.selectedElement();
            
        } catch (UnauthorizedException e) {
            System.out.println("\nErro: " + e.getMessage());
            return false;
        }
        if (machine == null)
            return false;
        
        final SelectWidget<ConfigurationFile> selector = new SelectWidget<>("Choose a File:", machine.configurationFiles(),
                    new ConfigurationFilePrinter());
        selector.show();
        configFile = selector.selectedElement();
        
        if (configFile == null)
            return false;
        
        try {
//            this.controller.addRequestToSendConfigFile(communicationProtocolID, configFile);
            this.controller.makeRequestToSendConfigFile(machine.identity(), configFile);
        } catch (final IllegalArgumentException e) {
            System.out.println("\nError: " + e.getMessage() + ".");
        } catch (final UnauthorizedException e) {
            System.out.println("\nError: User doesn't have permission.");
        }
        
        return false;
    }

    @Override
    public String headline() {
        return "Make a Request to Send a Configuration File to a Machine";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.machine;

import eapli.base.machinery.domain.Machine;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author 
 */
@SuppressWarnings("squid:S106")
public class MachinePrinter implements Visitor<Machine>  {
    
    /**
     * Method that shows a formated print with the id and description of RawMaterialCategory
     *
     */
    @Override
    public void visit(final Machine visitee) {
        System.out.printf("%-10s%-30s", visitee.identity(), visitee.machineDescription());
    }
}

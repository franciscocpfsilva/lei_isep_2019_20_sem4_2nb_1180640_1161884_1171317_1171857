/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.rawmaterial;

import eapli.base.rawmaterials.application.AddRawMaterialCategoryController;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.infrastructure.authz.application.exceptions.UnauthorizedException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import javax.persistence.PersistenceException;
import javax.persistence.RollbackException;
import org.hibernate.exception.ConstraintViolationException;

/**
 * US2002 - Add Raw MAterial Category UI
 * @author
 */
public class AddRawMaterialCategoryUI extends AbstractUI{

    private final AddRawMaterialCategoryController controller = new AddRawMaterialCategoryController();
    
    @Override
    protected boolean doShow() {
        final String identifier = Console.readLine("Raw Material Category Identifier:");
        final String description = Console.readLine("Raw Material Category Description:");

        try {
            this.controller.addAddRawMaterialCategory(identifier, description);
            System.out.println("\nSuccess: Raw Material Category created. Identifier; " + identifier +  "; Description: " + description);
        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("\nError: Identifier is already in use.");
        } catch (final IllegalArgumentException e) { // --------------------> who throws this exception?
            System.out.println("\nError: " + e.getMessage() + ".");
        } catch (final RollbackException e) { // --------------------> ?????
            if (e.getCause() instanceof PersistenceException 
                    && e.getCause().getCause() instanceof ConstraintViolationException) {
                System.out.println("\nError: Identifier is already in use.");
            } else {
                System.out.println("\nError: Some unknown error occurred in the database.");
            }
        } catch (final UnauthorizedException e) {
            System.out.println("\nError: Unauthorized User.");
        }
        return false;
    }

    @Override
    public String headline() {
        return "Add Raw Material Category";
    }
    
}

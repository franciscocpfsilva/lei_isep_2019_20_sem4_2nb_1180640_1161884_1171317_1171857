/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.product;

import eapli.framework.actions.Action;

/**
 *
 * @author Francisco Silva on 09/05/2020
 */
public class ImportProductsAction implements Action {
    
    @Override
    public boolean execute() {
        return new ImportProductsUI().show();
    }
    
}

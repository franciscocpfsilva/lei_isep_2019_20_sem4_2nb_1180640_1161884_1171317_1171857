/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.rawmaterial;

import eapli.base.app.backoffice.console.presentation.unitofmeasurement.UnitOfMeasurementPrinter;
import eapli.base.domain.model.general.UnitOfMeasurement;
import eapli.base.rawmaterials.application.AddRawMaterialController;
import eapli.base.rawmaterials.domain.RawMaterialCategory;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.infrastructure.authz.application.exceptions.UnauthorizedException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.util.Console;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import javax.persistence.PersistenceException;
import javax.persistence.RollbackException;

/**
 *
 * @author joaomachado
 */
public class AddRawMaterialUI extends AbstractUI {

    private final AddRawMaterialController theController = new AddRawMaterialController();
    private RawMaterialCategory theRawMaterialCategory = null;

    /**
     * Method to show the UI for adding a RawMaterial
     *
     * @return True if RawMaterial was added
     */
    @Override
    protected boolean doShow() {
        //List and Select RawMaterialCategory

        try {
            final Iterable<RawMaterialCategory> rawMaterialCategory = this.theController.rawMaterialCategorys();
            final SelectWidget<RawMaterialCategory> selector = new SelectWidget<>("Raw-Material Category's:", rawMaterialCategory,
                    new RawMaterialCategoryPrinter());
            selector.show();
            theRawMaterialCategory = selector.selectedElement();
        } catch (UnauthorizedException e) {
            System.out.println("\nErro: " + e.getMessage() + ".");
            return false;
        }

        final String rawMaterialID = Console.readLine("Raw MaterialID ID:");
        final String rawMaterialDescription = Console.readLine("Raw Material Description:");

        //List of Unit of Measurement
        final Iterable<UnitOfMeasurement> listOfUnitOfMeasurement = Arrays.asList(UnitOfMeasurement.values());
        final SelectWidget<UnitOfMeasurement> selectorUnitOfMeasurement
                = new SelectWidget<>("Select a Unit Of Measurement", listOfUnitOfMeasurement,
                        new UnitOfMeasurementPrinter());
        selectorUnitOfMeasurement.show();
        final UnitOfMeasurement unit = selectorUnitOfMeasurement.selectedElement();

        //For reading a file
        byte[] file = null;
        final String filePath = Console.readLine("File Path:");
        Path path = Paths.get(filePath);
        try {
            file = Files.readAllBytes(path);
        } catch (IOException ex) {
            System.out.println("There was an error trying to read the Technical file.");
        }
        try {
            this.theController.addRawMaterial(theRawMaterialCategory, rawMaterialID, rawMaterialDescription, unit, file);
            System.out.println("\nRaw-Material with the ID: " + rawMaterialID + " and Raw-Material Description: " + rawMaterialDescription + " saved with success.");
        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("\nError: That ID is already in use.");
        } catch (UnauthorizedException e) {
            System.out.println("\nErro: " + e.getMessage() + ".");
        } catch (final RollbackException e) {
            if (e.getCause() instanceof PersistenceException && e.getCause().getMessage().contains("ConstraintViolation")) {
                System.out.println("\nAt least one ID is already in use!");
            } else {
                System.out.println("\nError: " + e.getMessage() + ".");
            }
        } catch (final IllegalArgumentException e) {
            System.out.println("\nError: " + e.getMessage() + ".");
        }

        return false;
    }

    /**
     * Method that shows the SubMenu Title
     *
     * @return The title
     */
    @Override
    public String headline() {
        return "Add Machine";
    }
}

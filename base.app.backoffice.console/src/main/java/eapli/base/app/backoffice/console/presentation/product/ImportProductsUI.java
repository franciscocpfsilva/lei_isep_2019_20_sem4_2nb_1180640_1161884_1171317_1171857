/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.product;

import eapli.base.products.domain.Product;
import eapli.base.products.importing.application.FileType;
import eapli.base.products.importing.application.ImportProductsController;
import eapli.framework.infrastructure.authz.application.exceptions.UnauthorizedException;
import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.util.Console;
import eapli.framework.visitor.Visitor;
import java.io.IOException;
import java.util.ArrayList;

/**
 * This UI has been created as an AbstractListUI in order to show the summary of 
 * the imported products in the end of the use case. Therefore the method element()
 * has the interaction with the user (asking for data) that a common UI generally has.
 * 
 * @author Francisco Silva on 09/05/2020
 */
public class ImportProductsUI extends AbstractListUI<Product> {

    private final ImportProductsController theController = new ImportProductsController();

    @Override
    protected Iterable elements() {
        final Iterable<FileType> fileTypes = theController.allFileTypes();
        final SelectWidget<FileType> selectorFileType
                = new SelectWidget<>("Select a File Type", fileTypes, new FileTypePrinter());
        selectorFileType.show();
        FileType fileType = selectorFileType.selectedElement();
        
        final String filePath = Console.readLine("File path:");
        
        System.out.println();
        
        try {
            final Iterable<Product> importedProducts = this.theController.importProducts(filePath, fileType);
            return importedProducts;
        } catch(final IOException e) {
            System.out.println("\nError: There was a problem with the file.\n" + e.getMessage());
        } catch (final UnauthorizedException e) {
            System.out.println("\nError: User doesn't have permission.");
        } catch (final IllegalStateException e) {
            System.out.println("\nError: " + e.getMessage() + ".");
        }
        return new ArrayList<>();
    }

    @Override
    public String headline() {
        return "Import Products";
    }
    
    @Override
    protected Visitor elementPrinter() {
        return new ProductPrinter();
    }

    @Override
    protected String elementName() {
        return "Product";
    }

    @Override
    protected String listHeader() {
        return "IMPORTED PRODUCTS";
    }
    
    @Override
    protected String emptyMessage() {
        return "No imported data.";
    }
    
    
}

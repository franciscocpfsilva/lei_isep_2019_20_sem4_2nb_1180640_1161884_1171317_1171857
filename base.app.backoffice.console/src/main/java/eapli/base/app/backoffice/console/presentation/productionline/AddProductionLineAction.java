/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.productionline;

import eapli.framework.actions.Action;

/**
 *
 * @author 
 */
public class AddProductionLineAction implements Action {
   
    /**
     * Method that calls the SubMenu of Adding Production Line
     *
     * @return True if Production Line was added
     */
    @Override
    public boolean execute() {
        return new AddProductionLineUI().show();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.product;

import eapli.base.domain.model.general.Item;
import eapli.base.products.application.AddProductionSheetController;
import eapli.base.products.domain.Product;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.infrastructure.authz.application.exceptions.UnauthorizedException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import javax.persistence.RollbackException;

/**
 *
 * @author joaomachado
 */
public class AddProductionSheetUI extends AbstractUI {

    private final AddProductionSheetController theController = new AddProductionSheetController();

    /**
     * Method to show the UI for adding a ProductionSheet to a Product
     *
     * @return True if RawMaterial was added
     */
    @Override
    protected boolean doShow() {
        boolean rep = true;
        LinkedList<Item> listOfItems = new LinkedList<>();
        Product product = null;

        //Get the product to add sheet
        final String prodID = Console.readLine("Product ID:");
        try {
            product = theController.validateProductByID(prodID);
            if (product.hasProductionSheet()) {
                System.out.println("This Product already has a Production Sheet! \n");
                return false;
            }

        } catch (NoSuchElementException e) {
            System.out.println("\nError: There is no Product with that ID.");
            return false;
        } catch (UnauthorizedException e) {
            System.out.println("\nError: " + e.getMessage() + ".");
            return false;
        }

        final double standardQuantity = Console.readDouble("Standard Quantity in (" + product.unit_of_measurement() + "): ");

        //Creating a LinkedList with all the Items that make the ProductionSheet
        while (rep) {
            final String option = Console.readLine("'1' - For adding a RawMaterial \n'2' - For adding a Product\n");

            switch (option) {
                case "1": {

                    String id = Console.readLine("RawMaterial ID");
                    double rawMaterialQuantity = Console.readDouble("Quantity:");

                    if (!theController.findAndAddRawMaterialByID(id, listOfItems, rawMaterialQuantity)) {
                        System.out.println("Could not add RawMaterial to Production Sheet \n");
                        break;
                    }
                    break;
                }
                case "2": {
                    String id = Console.readLine("Product ID");
                    double productQuantity = Console.readDouble("Quantity:");

                    if (!theController.findAndAddProductByID(id, listOfItems, productQuantity)) {
                        System.out.println("Could not add Product to Production Sheet \n");
                        break;
                    }
                    break;
                }
                default:
                    System.out.println("Please choose a correct option \n");
                    break;
            }
            rep = Console.readBoolean("'Y' or 'S' to add more Items - 'N' to terminate\n");
        }

        //Creating a ProductionSheet, associate with the Product and Updating the Product.
        try {
            if (listOfItems.isEmpty()) {
                System.out.println("Could not create Production Sheet. No Items where added! \n");
                return false;
            }
            this.theController.addProductionSheet(product, standardQuantity, listOfItems);
            System.out.println("\nSuccess: Product with the ID: " + prodID + " was Updated with a new Production Sheet \n");
        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("\nError: That identifier is already in use.");
        } catch (@SuppressWarnings("unused") final NullPointerException e) {
            System.out.println("\nError: That Product already has a ProdcutionSheet.");
        } catch (UnauthorizedException e) {
            System.out.println("\nErro: " + e.getMessage() + ".");
        } catch (final IllegalArgumentException e) {
            System.out.println("\nError: " + e.getMessage() + ".");
        } catch (final RollbackException e) {
            System.out.println("\nError: " + e.getMessage() + ".");
        }
        return false;
    }

    /**
     * Method that shows the SubMenu Title
     *
     * @return The title
     */
    @Override

    public String headline() {
        return "Add Production Sheet";
    }
}

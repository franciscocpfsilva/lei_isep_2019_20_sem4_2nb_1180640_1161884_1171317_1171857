/*
 * Copyright (c) 2013-2019 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.base.app.backoffice.console.presentation;

import eapli.base.app.common.console.presentation.authz.MyUserMenu;
import eapli.base.Application;
import eapli.base.app.backoffice.console.presentation.productionline.AddProductionLineAction;
import eapli.base.app.backoffice.console.presentation.productionline.AssociateMachineProductionLineAction;
import eapli.base.app.backoffice.console.presentation.authz.AddUserUI;
import eapli.base.app.backoffice.console.presentation.authz.DeactivateUserAction;
import eapli.base.app.backoffice.console.presentation.authz.ListUsersAction;
import eapli.base.app.backoffice.console.presentation.clientuser.AcceptRefuseSignupRequestAction;
import eapli.base.app.backoffice.console.presentation.machine.AddConfigurationFileAction;
import eapli.base.app.backoffice.console.presentation.product.AddProductAction;
import eapli.base.app.backoffice.console.presentation.machine.AddMachineAction;
import eapli.base.app.backoffice.console.presentation.machine.MakeRequestToSendConfigFileAction;
import eapli.base.app.backoffice.console.presentation.messageprocessor.ProcessMessagesBlockAction;
import eapli.base.app.backoffice.console.presentation.messageprocessor.ChangeProcessMessagesRecurrentlyStateAction;
import eapli.base.app.backoffice.console.presentation.messageprocessor.ViewStateProductionLineAction;
import eapli.base.app.backoffice.console.presentation.product.AddProductionSheetAction;
import eapli.base.app.backoffice.console.presentation.product.ImportProductsAction;
import eapli.base.app.backoffice.console.presentation.product.ListProductWithNoProductionSheetAction;
import eapli.base.app.backoffice.console.presentation.rawmaterial.AddRawMaterialAction;
import eapli.base.app.backoffice.console.presentation.rawmaterial.AddRawMaterialCategoryAction;
import eapli.base.app.backoffice.console.presentation.storage.AddStorageUnitAction;
import eapli.base.app.backoffice.console.presentation.productionorder.AddProductionOrderAction;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.actions.Actions;
import eapli.framework.actions.menu.Menu;
import eapli.framework.actions.menu.MenuItem;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.ExitWithMessageAction;
import eapli.framework.presentation.console.ShowMessageAction;
import eapli.framework.presentation.console.menu.HorizontalMenuRenderer;
import eapli.framework.presentation.console.menu.MenuItemRenderer;
import eapli.framework.presentation.console.menu.MenuRenderer;
import eapli.framework.presentation.console.menu.VerticalMenuRenderer;

/**
 * TODO split this class in more specialized classes for each menu
 *
 * @author Paulo Gandra Sousa
 */
public class MainMenu extends AbstractUI {

    private static final String RETURN_LABEL = "Return ";

    private static final int EXIT_OPTION = 0;

    // USERS
    private static final int ADD_USER_OPTION = 1;
    private static final int LIST_USERS_OPTION = 2;
    private static final int DEACTIVATE_USER_OPTION = 3;
    private static final int ACCEPT_REFUSE_SIGNUP_REQUEST_OPTION = 4;

    // SETTINGS
    private static final int SET_KITCHEN_ALERT_LIMIT_OPTION = 1;

    // DISH TYPES
    private static final int DISH_TYPE_REGISTER_OPTION = 1;
    private static final int DISH_TYPE_LIST_OPTION = 2;
    private static final int DISH_TYPE_CHANGE_OPTION = 3;
    private static final int DISH_TYPE_ACTIVATE_DEACTIVATE_OPTION = 4;

    // DISHES
    private static final int DISH_REGISTER_OPTION = 5;
    private static final int DISH_LIST_OPTION = 6;
    private static final int DISH_REGISTER_DTO_OPTION = 7;
    private static final int DISH_LIST_DTO_OPTION = 8;
    private static final int DISH_ACTIVATE_DEACTIVATE_OPTION = 9;
    private static final int DISH_CHANGE_OPTION = 10;

    // DISH PROPERTIES
    private static final int CHANGE_DISH_NUTRICIONAL_INFO_OPTION = 1;
    private static final int CHANGE_DISH_PRICE_OPTION = 2;

    // MATERIALS
    private static final int MATERIAL_REGISTER_OPTION = 1;
    private static final int MATERIAL_LIST_OPTION = 2;

    // REPORTING
    private static final int REPORTING_DISHES_PER_DISHTYPE_OPTION = 1;
    private static final int REPORTING_HIGH_CALORIES_DISHES_OPTION = 2;
    private static final int REPORTING_DISHES_PER_CALORIC_CATEGORY_OPTION = 3;

    // MEALS
    private static final int LIST_MEALS_OPTION = 1;
    private static final int MEAL_REGISTER_OPTION = 2;

    // RAW MATERIALS
    private static final int ADD_RAW_MATERIAL_CATEGORY_OPTION = 1;
    private static final int ADD_RAW_MATERIAL_OPTION = 2;

    // PRODUCTS
    private static final int ADD_PRODUCT_OPTION = 1;
    private static final int ADD_PRODUCT_SHEET_TO_PRODUCT_OPTION = 2;
    private static final int LIST_PRODUCTS_WITHOUT_PRODUCT_SHEET_OPTION = 3;
    private static final int IMPORT_PRODUCTS = 4;

    // MACHINERY
    private static final int ADD_MACHINE_OPTION = 1;
    private static final int ADD_CONFIGURATION_FILE_TO_MACHINE_OPTION = 2;
    private static final int SEND_CONFIGURATION_FILE = 3;

    // PRODUCTION LINES
    private static final int ADD_PRODUCTION_LINE_OPTION = 1;
    private static final int ASSOCIATE_MACHINE_TO_PRODUCTION_LINE_OPTION = 2;

    // STORAGE
    private static final int ADD_STORAGE_UNIT_OPTION = 1;

    // EXECUTION ORDER
    private static final int ADD_PRODUCTION_ORDER_OPTION = 1;

    // PROCESS MESSAGES
    private static final int VIEW_STATE_OF_PRODUCTION_LINE = 1;
    private static final int CHANGE_STATE_PRODUCTION_LINE = 2;
    private static final int PROCESS_MESSAGES_BLOCK = 3;

    // MAIN MENU
    private static final int MY_USER_OPTION = 1;
    private static final int USERS_OPTION = 2;
    private static final int SETTINGS_OPTION = 4;
    private static final int DISH_OPTION = 5;
    private static final int TRACEABILITY_OPTION = 6;
    private static final int MEALS_OPTION = 7;
    private static final int REPORTING_DISHES_OPTION = 8;
    private static final int RAW_MATERIALS_OPTION = 9;
    private static final int PRODUCTS_OPTION = 10;
    private static final int MACHINERY_OPTION = 11;
    private static final int PRODUCTION_LINES_OPTION = 12;
    private static final int STORAGE_OPTION = 13;
    private static final int EXECUTION_ORDER_OPTION = 14;
    private static final int PROCESS_MESSAGES = 15;

    private static final String SEPARATOR_LABEL = "--------------";

    private final AuthorizationService authz = AuthzRegistry.authorizationService();

    @Override
    public boolean show() {
        drawFormTitle();
        return doShow();
    }

    /**
     * @return true if the user selected the exit option
     */
    @Override
    public boolean doShow() {
        final Menu menu = buildMainMenu();
        final MenuRenderer renderer;
        if (Application.settings().isMenuLayoutHorizontal()) {
            renderer = new HorizontalMenuRenderer(menu, MenuItemRenderer.DEFAULT);
        } else {
            renderer = new VerticalMenuRenderer(menu, MenuItemRenderer.DEFAULT);
        }
        return renderer.render();
    }

    @Override
    public String headline() {

        return authz.session().map(s -> "Base [ @" + s.authenticatedUser().identity() + " ]")
                .orElse("Base [ ==Anonymous== ]");
    }

    private Menu buildMainMenu() {
        final Menu mainMenu = new Menu();

        final Menu myUserMenu = new MyUserMenu();
        mainMenu.addSubMenu(MY_USER_OPTION, myUserMenu);

        if (!Application.settings().isMenuLayoutHorizontal()) {
            mainMenu.addItem(MenuItem.separator(SEPARATOR_LABEL));
        }

        if (authz.isAuthenticatedUserAuthorizedTo(BaseRoles.POWER_USER, BaseRoles.ADMIN,
                BaseRoles.PRODUCTION_MANAGER, BaseRoles.SHOP_FLOOR_MANAGER)) {
            final Menu usersMenu = buildUsersMenu();
            mainMenu.addSubMenu(USERS_OPTION, usersMenu);
            final Menu settingsMenu = buildAdminSettingsMenu();
            mainMenu.addSubMenu(SETTINGS_OPTION, settingsMenu);
            final Menu rawMaterialsMenu = buildRawMaterialsMenu();
            mainMenu.addSubMenu(RAW_MATERIALS_OPTION, rawMaterialsMenu);
            final Menu productsMenu = buildProductsMenu();
            mainMenu.addSubMenu(PRODUCTS_OPTION, productsMenu);
            final Menu machineryMenu = buildMachineryMenu();
            mainMenu.addSubMenu(MACHINERY_OPTION, machineryMenu);
            final Menu productionLinesMenu = buildProductionLinesMenu();
            mainMenu.addSubMenu(PRODUCTION_LINES_OPTION, productionLinesMenu);
            final Menu storageMenu = buildStorageMenu();
            mainMenu.addSubMenu(STORAGE_OPTION, storageMenu);
            final Menu executionOrderMenu = buildExecutionOrderMenu();
            mainMenu.addSubMenu(EXECUTION_ORDER_OPTION, executionOrderMenu);
            final Menu messageProcessorMenu = buildMessageProcessorMenu();
            mainMenu.addSubMenu(PROCESS_MESSAGES, messageProcessorMenu);

        }

        if (!Application.settings().isMenuLayoutHorizontal()) {
            mainMenu.addItem(MenuItem.separator(SEPARATOR_LABEL));
        }

        mainMenu.addItem(EXIT_OPTION, "Exit", new ExitWithMessageAction("Bye, Bye"));

        return mainMenu;
    }

    private Menu buildAdminSettingsMenu() {
        final Menu menu = new Menu("Settings >");

        menu.addItem(SET_KITCHEN_ALERT_LIMIT_OPTION, "Set kitchen alert limit",
                new ShowMessageAction("Not implemented yet"));
        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }

    private Menu buildUsersMenu() {
        final Menu menu = new Menu("Users >");

        menu.addItem(ADD_USER_OPTION, "Add User", new AddUserUI()::show);
        menu.addItem(LIST_USERS_OPTION, "List all Users", new ListUsersAction());
        menu.addItem(DEACTIVATE_USER_OPTION, "Deactivate User", new DeactivateUserAction());
        menu.addItem(ACCEPT_REFUSE_SIGNUP_REQUEST_OPTION, "Accept/Refuse Signup Request",
                new AcceptRefuseSignupRequestAction());
        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }

    private Menu buildRawMaterialsMenu() {
        final Menu menu = new Menu("Raw Materials >");

        menu.addItem(ADD_RAW_MATERIAL_CATEGORY_OPTION, "Add Raw Material's Category",
                new AddRawMaterialCategoryAction());
        menu.addItem(ADD_RAW_MATERIAL_OPTION, "Add Raw Material", new AddRawMaterialAction());
        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }

    private Menu buildProductsMenu() {
        final Menu menu = new Menu("Products >");

        menu.addItem(ADD_PRODUCT_OPTION, "Add Product", new AddProductAction());
        menu.addItem(ADD_PRODUCT_SHEET_TO_PRODUCT_OPTION, "Add Product Sheet to Product",
                new AddProductionSheetAction());
        menu.addItem(LIST_PRODUCTS_WITHOUT_PRODUCT_SHEET_OPTION, "List Products without Product Sheet",
                new ListProductWithNoProductionSheetAction());
        menu.addItem(IMPORT_PRODUCTS, "Import Products",
                new ImportProductsAction());
        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }

    private Menu buildMachineryMenu() {
        final Menu menu = new Menu("Machinery >");

        menu.addItem(ADD_MACHINE_OPTION, "Add Machine", new AddMachineAction());
        menu.addItem(ADD_CONFIGURATION_FILE_TO_MACHINE_OPTION,
                "Associate Configuration File to Machine", new AddConfigurationFileAction());
        menu.addItem(SEND_CONFIGURATION_FILE,
                "Send a Configuration File to a Machine", new MakeRequestToSendConfigFileAction());
        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }

    private Menu buildProductionLinesMenu() {
        final Menu menu = new Menu("Production Lines >");

        menu.addItem(ADD_PRODUCTION_LINE_OPTION, "Add Production Line", new AddProductionLineAction());
        menu.addItem(ASSOCIATE_MACHINE_TO_PRODUCTION_LINE_OPTION,
                "Associate Machine to Production Line", new AssociateMachineProductionLineAction());
        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }

    private Menu buildStorageMenu() {
        final Menu menu = new Menu("Storage >");

        menu.addItem(ADD_STORAGE_UNIT_OPTION, "Add Storage Unit", new AddStorageUnitAction());
        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }

    private Menu buildExecutionOrderMenu() {
        final Menu menu = new Menu("Production Order >");

        menu.addItem(ADD_PRODUCTION_ORDER_OPTION, "Add Production Order", new AddProductionOrderAction());
        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }

    private Menu buildMessageProcessorMenu() {
        final Menu menu = new Menu("Process Messages >");

        menu.addItem(VIEW_STATE_OF_PRODUCTION_LINE, "View State of Production Lines", new ViewStateProductionLineAction());
        menu.addItem(CHANGE_STATE_PRODUCTION_LINE,
                "Change Process Messages Recurrently State for a Production Line", new ChangeProcessMessagesRecurrentlyStateAction());
        menu.addItem(PROCESS_MESSAGES_BLOCK,
                "Process Messages in block ", new ProcessMessagesBlockAction());

        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }

}

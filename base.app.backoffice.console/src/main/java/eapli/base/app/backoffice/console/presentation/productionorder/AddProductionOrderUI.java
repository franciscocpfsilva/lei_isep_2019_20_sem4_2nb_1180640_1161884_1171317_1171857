/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.productionorder;

import eapli.base.app.backoffice.console.presentation.unitofmeasurement.UnitOfMeasurementPrinter;
import eapli.base.domain.model.general.UnitOfMeasurement;
import eapli.base.productionorder.application.AddProductionOrderController;
import eapli.base.productionorder.domain.OrderID;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.infrastructure.authz.application.exceptions.UnauthorizedException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.util.Console;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import javax.persistence.RollbackException;

/**
 *
 * @author joaomachado
 */
public class AddProductionOrderUI extends AbstractUI {

    private final AddProductionOrderController theController = new AddProductionOrderController();

    @Override
    protected boolean doShow() {
        boolean rep = true;

        LinkedList<OrderID> listOrderId = new LinkedList<>();

        final String producitonId = Console.readLine("Produciton Order ID:");
        final String dateOfIssue = Console.readLine("Produciton Order Date of Issue: (dd-MM-yyyy)");
        final String expectedExecutionDate = Console.readLine("Expected Execution Date: (dd-MM-yyyy)");

        String productId = Console.readLine("Product ID");

        try {
            if (!theController.findAndValidateProductByID(productId)) {
                System.out.println("The Product does not have a Production Sheet!");
                return false;
            }
        } catch (@SuppressWarnings("unused") final NoSuchElementException e) {
            System.out.println("\nError: There is no Product with that id.");
            return false;
        } catch (UnauthorizedException e) {
            System.out.println("\nErro: " + e.getMessage() + ".");
            return false;
        }
        try {
            while (rep) {
                String orderId = Console.readNonEmptyLine("Order ID", "An ID is requiered");
                listOrderId.add(OrderID.valueOf(orderId));
                rep = Console.readBoolean("'Y' or 'S' to add more Order Id - 'N' to terminate\n");
            }
            final double quantity = Console.readDouble("Quantity to produce:");
            final Iterable<UnitOfMeasurement> listOfUnitOfMeasurement = Arrays.asList(UnitOfMeasurement.values());
            final SelectWidget<UnitOfMeasurement> selectorUnitOfMeasurement
                    = new SelectWidget<>("Select a Unit Of Measurement", listOfUnitOfMeasurement,
                            new UnitOfMeasurementPrinter());
            selectorUnitOfMeasurement.show();
            final UnitOfMeasurement unit = selectorUnitOfMeasurement.selectedElement();

            this.theController.addExecutionOrder(producitonId, dateOfIssue, expectedExecutionDate, productId, listOrderId, quantity, unit);
            System.out.println("\nSuccess: Produciton Order with the ID: " + producitonId + " was Added \n");
        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("\nError: That identifier is already in use.");
        } catch (@SuppressWarnings("unused") final NullPointerException e) {
            System.out.println("\nError: That Product already has a ProdcutionSheet.");
        } catch (UnauthorizedException e) {
            System.out.println("\nErro: " + e.getMessage() + ".");
        } catch (final IllegalArgumentException e) {
            System.out.println("\nError: " + e.getMessage() + ".");
        } catch (final RollbackException e) {
            System.out.println("\nError: " + e.getMessage() + ".");
        } catch (final DateTimeParseException e) {
            System.out.println("\nError: " + e.getMessage() + ".");
        }

        return false;

    }

    @Override
    public String headline() {
        return "Add Execution Order";
    }

}

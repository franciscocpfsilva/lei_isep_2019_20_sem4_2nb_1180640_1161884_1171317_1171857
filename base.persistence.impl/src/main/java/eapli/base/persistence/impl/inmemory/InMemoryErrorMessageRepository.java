/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.inmemory;

import eapli.base.errormessage.domain.ErrorMessage;
import eapli.base.errormessage.repositories.ErrorMessagesRepository;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;

/**
 *
 * @author
 */
public class InMemoryErrorMessageRepository extends InMemoryDomainRepository<ErrorMessage, Long>
        implements ErrorMessagesRepository {

    static {
        InMemoryInitializer.init();
    }
}

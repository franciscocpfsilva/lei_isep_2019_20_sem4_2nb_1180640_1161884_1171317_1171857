/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.jpa;

import eapli.base.messageprocessing.domain.ProductionLineProcessingState;
import eapli.base.messageprocessing.repositories.ProductionLineProcessingStateRepository;
import eapli.base.productionline.domain.ProductionLineID;

/**
 *
 * @author franciscoferreiradasilva
 */
public class JpaProductionLineProcessingStateRepository extends BasepaRepositoryBase<ProductionLineProcessingState, ProductionLineID, ProductionLineID>
        implements ProductionLineProcessingStateRepository {
    
    JpaProductionLineProcessingStateRepository() {
        super("productionLineId");
    }
}

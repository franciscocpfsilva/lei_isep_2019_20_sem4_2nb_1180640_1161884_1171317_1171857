/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.jpa;

import eapli.base.communication.scm.domain.RequestConfiguration;
import eapli.base.communication.scm.repositories.RequestConfigurationRepository;

/**
 *
 * @author leona
 */
public class JpaRequestConfigurationRepository extends BasepaRepositoryBase<RequestConfiguration, Long, Long>
        implements RequestConfigurationRepository{
    
    JpaRequestConfigurationRepository(){
        super("pk");
    }
    
}

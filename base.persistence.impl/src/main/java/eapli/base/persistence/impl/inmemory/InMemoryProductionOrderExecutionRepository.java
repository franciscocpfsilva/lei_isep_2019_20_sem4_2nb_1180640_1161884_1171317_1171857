/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.inmemory;

import eapli.base.productionline.domain.ProductionLineID;
import eapli.base.productionorder.domain.ProductionOrderID;
import eapli.base.productionorderexecution.domain.ProductionOrderExecution;
import eapli.base.productionorderexecution.repositories.ProductionOrderExecutionRepository;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;
import java.time.LocalDateTime;

/**
 *
 * @author João Cunha
 */
public class InMemoryProductionOrderExecutionRepository extends
        InMemoryDomainRepository<ProductionOrderExecution, ProductionOrderID>
        implements ProductionOrderExecutionRepository {

    static {
        InMemoryInitializer.init();
    }

    @Override
    public ProductionOrderExecution findProductionOrderExecutionBetweenDate(LocalDateTime date, ProductionLineID id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

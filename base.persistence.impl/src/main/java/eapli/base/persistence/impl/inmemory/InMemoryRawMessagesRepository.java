/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.inmemory;

import eapli.base.machinery.domain.MachineID;
import eapli.base.rawmessage.domain.MessageType;
import eapli.base.rawmessage.domain.RawMessage;
import eapli.base.rawmessage.domain.RawMessageState;
import eapli.base.rawmessage.repositories.RawMessagesRepository;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;
import java.time.LocalDateTime;
import java.util.List;

/**
 *
 * @author
 */
public class InMemoryRawMessagesRepository extends InMemoryDomainRepository<RawMessage, Long>
        implements RawMessagesRepository {

    static {
        InMemoryInitializer.init();
    }

    @Override
    public RawMessage findRawMessageBy(MachineID id, MessageType type, LocalDateTime dateTime) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterable<RawMessage> findMessagesBetween(LocalDateTime start, 
            LocalDateTime end, RawMessageState state, List<MachineID> lst) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterable<RawMessage> findMessagesFromMachines(RawMessageState state, List<MachineID> lst) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}

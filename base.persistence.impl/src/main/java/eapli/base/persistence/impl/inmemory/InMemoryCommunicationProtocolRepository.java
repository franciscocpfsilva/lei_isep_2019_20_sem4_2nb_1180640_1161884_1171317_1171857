/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.inmemory;

import eapli.base.communication.scm.domain.CommunicationProtocol;
import eapli.base.communication.scm.repositories.CommunicationProtocolRepository;
import eapli.base.machinery.domain.CommunicationProtocolID;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;

/**
 *
 * @author leona
 */
public class InMemoryCommunicationProtocolRepository extends InMemoryDomainRepository<CommunicationProtocol, CommunicationProtocolID>
        implements CommunicationProtocolRepository {

    static {
        InMemoryInitializer.init();
    }
}

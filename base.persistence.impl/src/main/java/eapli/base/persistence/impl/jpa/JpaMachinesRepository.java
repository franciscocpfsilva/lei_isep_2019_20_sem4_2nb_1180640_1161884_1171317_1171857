/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.jpa;

import eapli.base.machinery.domain.Machine;
import eapli.base.machinery.domain.MachineID;
import eapli.base.machinery.repositories.MachinesRepository;


public class JpaMachinesRepository extends BasepaRepositoryBase<Machine, Long, MachineID>
        implements MachinesRepository {
    
    JpaMachinesRepository() {
        super("machineID"); // business identity name of MachineID
    }
}
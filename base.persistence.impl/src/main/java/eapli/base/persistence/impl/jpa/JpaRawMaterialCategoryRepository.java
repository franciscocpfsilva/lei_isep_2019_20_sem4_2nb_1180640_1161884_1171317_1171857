/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.jpa;

import eapli.base.rawmaterials.domain.RawMaterialCategory;
import eapli.base.rawmaterials.domain.RawMaterialCategoryID;
import eapli.base.rawmaterials.repositories.RawMaterialCategoryRepository;

/**
 *
 * @author 
 */
public class JpaRawMaterialCategoryRepository extends BasepaRepositoryBase<RawMaterialCategory, Long, RawMaterialCategoryID>
        implements RawMaterialCategoryRepository {
    
    JpaRawMaterialCategoryRepository() {
        super("identifier"); // "identifier" -> business identity name of RawMaterialCategory (name used in class RawMaterialCategoryID)
    }
}

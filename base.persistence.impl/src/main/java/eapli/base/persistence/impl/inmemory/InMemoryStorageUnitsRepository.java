/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.inmemory;

import eapli.base.storage.domain.StorageUnit;
import eapli.base.storage.domain.StorageUnitID;
import eapli.base.storage.repositories.StorageUnitsRepository;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;

/**
 *
 * @author
 */
public class InMemoryStorageUnitsRepository extends InMemoryDomainRepository<StorageUnit, StorageUnitID>
        implements StorageUnitsRepository {
    
    static {
        InMemoryInitializer.init();
    }   
}

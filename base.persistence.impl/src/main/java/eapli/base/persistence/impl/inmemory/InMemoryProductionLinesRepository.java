/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.inmemory;

import eapli.base.machinery.domain.MachineID;
import eapli.base.productionline.domain.ProductionLine;
import eapli.base.productionline.domain.ProductionLineID;
import eapli.base.productionline.repositories.ProductionLinesRepository;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;
import java.util.Optional;

/**
 *
 * @author 
 */
public class InMemoryProductionLinesRepository extends InMemoryDomainRepository<ProductionLine, ProductionLineID>
        implements ProductionLinesRepository {

    static {
        InMemoryInitializer.init();
    }

    @Override
    public ProductionLine findByMachineID(MachineID machineID) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

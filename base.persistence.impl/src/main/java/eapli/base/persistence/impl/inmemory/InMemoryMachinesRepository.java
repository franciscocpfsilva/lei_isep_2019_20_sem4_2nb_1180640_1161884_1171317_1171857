/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.inmemory;

import eapli.base.machinery.domain.Machine;
import eapli.base.machinery.domain.MachineID;
import eapli.base.machinery.repositories.MachinesRepository;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;

/**
 *
 * @author joaomachado
 */
public class InMemoryMachinesRepository extends InMemoryDomainRepository<Machine, MachineID>
        implements MachinesRepository {

    static {
        InMemoryInitializer.init();
    }
}

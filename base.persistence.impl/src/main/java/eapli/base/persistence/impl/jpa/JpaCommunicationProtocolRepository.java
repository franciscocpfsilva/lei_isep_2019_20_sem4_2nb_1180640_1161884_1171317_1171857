/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.jpa;

import eapli.base.communication.scm.domain.CommunicationProtocol;
import eapli.base.communication.scm.repositories.CommunicationProtocolRepository;
import eapli.base.machinery.domain.CommunicationProtocolID;

/**
 *
 * @author leona
 */
public class JpaCommunicationProtocolRepository extends BasepaRepositoryBase<CommunicationProtocol, Long, CommunicationProtocolID>
        implements CommunicationProtocolRepository{
    
    JpaCommunicationProtocolRepository() {
        super("communicationProtocolID"); // business identity name of StorageUnit (name used in class StorageUnitID)
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.inmemory;

import eapli.base.rawmaterials.domain.RawMaterialCategory;
import eapli.base.rawmaterials.domain.RawMaterialCategoryID;
import eapli.base.rawmaterials.repositories.RawMaterialCategoryRepository;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;

/**
 *
 * @author 
 */
public class InMemoryRawMaterialCategoryRepository extends InMemoryDomainRepository<RawMaterialCategory, RawMaterialCategoryID>
        implements RawMaterialCategoryRepository {
    
    static {
        InMemoryInitializer.init();
    }   
}

package eapli.base.persistence.impl.jpa;

import eapli.base.Application;
import eapli.base.clientusermanagement.repositories.SignupRequestRepository;
import eapli.base.errormessage.repositories.ErrorMessagesRepository;
import eapli.base.communication.scm.repositories.CommunicationProtocolRepository;
import eapli.base.communication.scm.repositories.RequestConfigurationRepository;
import eapli.base.infrastructure.persistence.RepositoryFactory;
import eapli.base.products.repositories.ProductsRepository;
import eapli.base.machinery.repositories.MachinesRepository;
import eapli.base.messageprocessing.repositories.ProductionLineProcessingStateRepository;
import eapli.base.productionline.repositories.ProductionLinesRepository;
import eapli.base.rawmaterials.repositories.RawMaterialCategoryRepository;
import eapli.base.storage.repositories.StorageUnitsRepository;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.authz.domain.repositories.UserRepository;
import eapli.framework.infrastructure.authz.repositories.impl.JpaAutoTxUserRepository;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaAutoTxRepository;
import eapli.base.rawmaterials.repositories.RawMaterialRepository;
import eapli.base.rawmessage.repositories.RawMessagesRepository;
import eapli.base.productionorder.repositories.ProductionOrderRepository;
import eapli.base.productionorderexecution.repositories.ProductionOrderExecutionRepository;

/**
 *
 * Created by nuno on 21/03/16.
 */
public class JpaRepositoryFactory implements RepositoryFactory {

    @Override
    public UserRepository users(final TransactionalContext autoTx) {
        return new JpaAutoTxUserRepository(autoTx);
    }

    @Override
    public UserRepository users() {
        return new JpaAutoTxUserRepository(Application.settings().getPersistenceUnitName(),
                Application.settings().getExtendedPersistenceProperties());
    }

    @Override
    public JpaClientUserRepository clientUsers(final TransactionalContext autoTx) {
        return new JpaClientUserRepository(autoTx);
    }

    @Override
    public JpaClientUserRepository clientUsers() {
        return new JpaClientUserRepository(Application.settings().getPersistenceUnitName());
    }

    @Override
    public SignupRequestRepository signupRequests(final TransactionalContext autoTx) {
        return new JpaSignupRequestRepository(autoTx);
    }

    @Override
    public SignupRequestRepository signupRequests() {
        return new JpaSignupRequestRepository(Application.settings().getPersistenceUnitName());
    }

    @Override
    public TransactionalContext newTransactionalContext() {
        return JpaAutoTxRepository.buildTransactionalContext(Application.settings().getPersistenceUnitName(),
                Application.settings().getExtendedPersistenceProperties());
    }

    @Override
    public ProductsRepository products() {
        return new JpaProductsRepository();
    }

    @Override
    public StorageUnitsRepository storageUnits() {
        return new JpaStorageUnitsRepository();
    }

    @Override
    public MachinesRepository machines() {
        return new JpaMachinesRepository();
    }

    @Override
    public RawMaterialCategoryRepository rawMaterialCategories() {
        return new JpaRawMaterialCategoryRepository();
    }

    @Override
    public RawMaterialRepository rawMaterial() {
        return new JpaRawMaterialRepository();
    }

    @Override
    public ProductionLinesRepository productionLines() {
        return new JpaProductionLineRepository();
    }

    @Override
    public ProductionOrderRepository productionOrder() {
        return new JpaProductionOrderRepository();
    }

    @Override
    public RawMessagesRepository rawMessages() {
        return new JpaRawMessagesRepository();
    }

    @Override
    public ErrorMessagesRepository errorMessages() {
        return new JpaErrorMessagesRepository();
    }

    @Override
    public ProductionOrderExecutionRepository productionOrderExecution() {
        return new JpaProductionOrderExecutionRepository();
    }

    @Override
    public CommunicationProtocolRepository communicationProtocols() {
        return new JpaCommunicationProtocolRepository();

    }

    @Override
    public RequestConfigurationRepository requestsConfiguration() {
        return new JpaRequestConfigurationRepository();
    }
    
    @Override
    public ProductionLineProcessingStateRepository processingState() {
        return new JpaProductionLineProcessingStateRepository();
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.jpa;

import eapli.base.storage.domain.StorageUnit;
import eapli.base.storage.domain.StorageUnitID;
import eapli.base.storage.repositories.StorageUnitsRepository;

/**
 *
 * @author
 */
public class JpaStorageUnitsRepository extends BasepaRepositoryBase<StorageUnit, Long, StorageUnitID>
        implements StorageUnitsRepository {
    
    JpaStorageUnitsRepository() {
        super("identifier"); // business identity name of StorageUnit (name used in class StorageUnitID)
    }
}

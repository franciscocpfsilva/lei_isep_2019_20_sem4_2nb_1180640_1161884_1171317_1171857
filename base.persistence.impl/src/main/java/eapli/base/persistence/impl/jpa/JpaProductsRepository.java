/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.jpa;

import eapli.base.products.repositories.ProductsRepository;
import eapli.base.products.domain.ProductID;
import eapli.base.products.domain.Product;

/**
 *
 * @author
 */
public class JpaProductsRepository extends BasepaRepositoryBase<Product, String, ProductID>
        implements ProductsRepository {

    public JpaProductsRepository() {
        super("factory_ID");
    }

    @Override
    public Iterable<Product> findWithNoProductionSheet() {
        return match("e.productionSheet=NULL");
    }
  
}

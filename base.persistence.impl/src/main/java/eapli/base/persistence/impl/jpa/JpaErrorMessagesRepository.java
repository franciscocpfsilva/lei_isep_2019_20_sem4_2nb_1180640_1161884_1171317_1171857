/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.jpa;

import eapli.base.errormessage.domain.ErrorMessage;
import eapli.base.errormessage.repositories.ErrorMessagesRepository;

/**
 *
 * @author 
 */
public class JpaErrorMessagesRepository extends BasepaRepositoryBase<ErrorMessage, Long, Long>
        implements ErrorMessagesRepository {

    JpaErrorMessagesRepository() {
        super("pk");
    }
}

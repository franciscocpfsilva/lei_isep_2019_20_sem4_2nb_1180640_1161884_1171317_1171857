/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.jpa;

import eapli.base.machinery.domain.MachineID;
import eapli.base.productionline.domain.ProductionLine;
import eapli.base.productionline.domain.ProductionLineID;
import eapli.base.productionline.repositories.ProductionLinesRepository;
import eapli.base.rawmessage.domain.RawMessage;
import java.util.Optional;
import javax.persistence.TypedQuery;

/**
 *
 * @author 
 */
public class JpaProductionLineRepository extends BasepaRepositoryBase<ProductionLine, String, ProductionLineID>
        implements ProductionLinesRepository {

    public JpaProductionLineRepository() {
        super("productionLine_ID");
    }

    @Override
    public ProductionLine findByMachineID(MachineID machineID) {
        final TypedQuery<ProductionLine> query = entityManager().createQuery(
                "SELECT p FROM ProductionLine p JOIN p.sequencia ps WHERE ps.machineID = :id", ProductionLine.class);
        query.setParameter("id", machineID);
        
        return query.getSingleResult();
    }
}

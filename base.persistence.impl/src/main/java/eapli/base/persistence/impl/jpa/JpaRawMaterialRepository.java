/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.jpa;

import eapli.base.rawmaterials.domain.RawMaterial;
import eapli.base.rawmaterials.domain.RawMaterialID;
import eapli.base.rawmaterials.repositories.RawMaterialRepository;

/**
 *
 * @author joaomachado
 */
public class JpaRawMaterialRepository extends BasepaRepositoryBase<RawMaterial, Long, RawMaterialID>
        implements RawMaterialRepository {

    JpaRawMaterialRepository() {
        super("rawMaterialID"); // business identity name of StorageUnit (name used in class StorageUnitID)
    }
    
  
}

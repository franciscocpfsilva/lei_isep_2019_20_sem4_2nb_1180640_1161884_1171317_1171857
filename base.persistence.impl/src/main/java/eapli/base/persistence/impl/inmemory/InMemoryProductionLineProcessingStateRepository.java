/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.inmemory;

import eapli.base.messageprocessing.domain.ProductionLineProcessingState;
import eapli.base.messageprocessing.repositories.ProductionLineProcessingStateRepository;
import eapli.base.productionline.domain.ProductionLineID;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;

/**
 *
 * @author franciscoferreiradasilva
 */
public class InMemoryProductionLineProcessingStateRepository extends InMemoryDomainRepository<ProductionLineProcessingState, ProductionLineID>
        implements ProductionLineProcessingStateRepository {
    
    static {
        InMemoryInitializer.init();
    }   
}

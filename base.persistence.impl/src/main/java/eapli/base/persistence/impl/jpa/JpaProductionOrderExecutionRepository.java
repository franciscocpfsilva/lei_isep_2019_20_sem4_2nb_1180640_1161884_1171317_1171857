/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.jpa;

import eapli.base.productionline.domain.ProductionLineID;
import eapli.base.productionorder.domain.ProductionOrderID;
import eapli.base.productionorderexecution.domain.ProductionOrderExecution;
import eapli.base.productionorderexecution.repositories.ProductionOrderExecutionRepository;
import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.TypedQuery;

/**
 *
 * @author João Cunha
 */
public class JpaProductionOrderExecutionRepository extends BasepaRepositoryBase<ProductionOrderExecution, Long, ProductionOrderID>
        implements ProductionOrderExecutionRepository {

    public JpaProductionOrderExecutionRepository() {
        super("productionOrderID"); // business identity name of ProductionOrder (name used in class ProductionOrderID)
    }

    @Override
    public ProductionOrderExecution findProductionOrderExecutionBetweenDate(LocalDateTime date, ProductionLineID id) {
        final TypedQuery<ProductionOrderExecution> query = entityManager().createQuery(
                "SELECT p FROM ProductionOrderExecution p WHERE p.startDate < :date AND p.endDate > :date AND p.productionLineID = :id", ProductionOrderExecution.class);

        query.setParameter("date", date);
        query.setParameter("id", id);
        List<ProductionOrderExecution> lst = query.getResultList();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst.get(0);
        }

    }
}

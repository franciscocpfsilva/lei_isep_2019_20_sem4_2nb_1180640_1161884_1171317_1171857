/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.inmemory;

import eapli.base.rawmaterials.domain.RawMaterial;
import eapli.base.rawmaterials.domain.RawMaterialID;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;
import eapli.base.rawmaterials.repositories.RawMaterialRepository;

/**
 *
 * @author joaomachado
 */
public class InMemoryRawMaterialRepository extends InMemoryDomainRepository<RawMaterial, RawMaterialID>
        implements RawMaterialRepository { 
    
 static {
        InMemoryInitializer.init();
    }
 
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.jpa;

import eapli.base.machinery.domain.MachineID;
import eapli.base.rawmessage.domain.MessageType;
import eapli.base.rawmessage.domain.RawMessage;
import eapli.base.rawmessage.domain.RawMessageState;
import eapli.base.rawmessage.repositories.RawMessagesRepository;
import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.TypedQuery;

/**
 *
 * @author
 */
public class JpaRawMessagesRepository extends BasepaRepositoryBase<RawMessage, Long, Long>
        implements RawMessagesRepository {

    JpaRawMessagesRepository() {
        super("pk");
    }

    @Override
    public RawMessage findRawMessageBy(MachineID id, MessageType type, LocalDateTime dateTime) {
        final TypedQuery<RawMessage> query = entityManager().createQuery(
                "SELECT m FROM RawMessage m WHERE m.machineID = :id AND m.messageType = :type AND m.generationDateTime = :date", RawMessage.class);
        query.setParameter("id", id);
        query.setParameter("type", type);
        query.setParameter("date", dateTime);
        return query.getSingleResult();
    }

    @Override
    public Iterable<RawMessage> findMessagesBetween(LocalDateTime start,
            LocalDateTime end, RawMessageState state, List<MachineID> lst) {
        MachineID array[] = new MachineID[lst.size()];
        lst.toArray(array);
        String firstPart = " AND (m.machineID = :";
        String secondPart = " OR m.machineID = :";
        String parameter = "array";
        String endOfQuery = "";
        boolean flag = true;
        for (int i = 0; i < lst.size(); i++) {
            if (flag) {
                endOfQuery += firstPart + parameter + i;
                flag = false;
            } else {
                endOfQuery += secondPart + parameter + i;
            }
        }
        endOfQuery += ")";

        final TypedQuery<RawMessage> query = entityManager().createQuery(
                "SELECT m FROM RawMessage m WHERE m.generationDateTime > :startDate AND m.generationDateTime < :endDate AND m.state = :state" + endOfQuery + " ORDER BY m.generationDateTime ASC", RawMessage.class);
        query.setParameter("startDate", start);
        query.setParameter("endDate", end);
        query.setParameter("state", state);
        String result;
        MachineID machine;
        for (int i = 0; i < lst.size(); i++) {
            result = parameter + i;
            machine = lst.get(i);
            query.setParameter(result, machine);
        }

        return query.getResultList();
    }
    
    @Override
    public Iterable<RawMessage> findMessagesFromMachines(RawMessageState state, List<MachineID> lst) {
        String machines = "";
        if (!lst.isEmpty()) {
            machines += " AND m.machineID IN (";
            for (int i = 0; i < lst.size(); i++) {
                machines += ":machineId" + i;
                if (i != lst.size() - 1) {
                    machines += ", ";
                }
            }
            machines += ")";
        }
        
        
        final TypedQuery<RawMessage> query = entityManager().createQuery(
                "SELECT m FROM RawMessage m WHERE m.state = :state" + machines, RawMessage.class);
        query.setParameter("state", state);
        
        for (int i = 0; i < lst.size(); i++) {
            String param = "machineId" + i;
            query.setParameter(param, lst.get(i));
        }
        
        return query.getResultList();
    }

}

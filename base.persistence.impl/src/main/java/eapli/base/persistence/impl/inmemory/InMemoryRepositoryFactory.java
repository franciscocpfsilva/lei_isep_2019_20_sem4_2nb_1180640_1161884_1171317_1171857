package eapli.base.persistence.impl.inmemory;

import eapli.base.clientusermanagement.repositories.ClientUserRepository;
import eapli.base.clientusermanagement.repositories.SignupRequestRepository;
import eapli.base.errormessage.repositories.ErrorMessagesRepository;
import eapli.base.communication.scm.repositories.CommunicationProtocolRepository;
import eapli.base.communication.scm.repositories.RequestConfigurationRepository;
import eapli.base.infrastructure.bootstrapers.BaseBootstrapper;
import eapli.base.infrastructure.persistence.RepositoryFactory;
import eapli.base.products.repositories.ProductsRepository;
import eapli.base.machinery.repositories.MachinesRepository;
import eapli.base.messageprocessing.repositories.ProductionLineProcessingStateRepository;
import eapli.base.productionline.repositories.ProductionLinesRepository;
import eapli.base.rawmaterials.repositories.RawMaterialCategoryRepository;
import eapli.base.storage.repositories.StorageUnitsRepository;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.authz.domain.repositories.UserRepository;
import eapli.framework.infrastructure.authz.repositories.impl.InMemoryUserRepository;
import eapli.base.rawmaterials.repositories.RawMaterialRepository;
import eapli.base.rawmessage.repositories.RawMessagesRepository;
import eapli.base.productionorder.repositories.ProductionOrderRepository;
import eapli.base.productionorderexecution.repositories.ProductionOrderExecutionRepository;

/**
 *
 * Created by nuno on 20/03/16.
 */
public class InMemoryRepositoryFactory implements RepositoryFactory {

    static {
        // only needed because of the in memory persistence
        new BaseBootstrapper().execute();
    }

    @Override
    public UserRepository users(final TransactionalContext tx) {
        return new InMemoryUserRepository();
    }

    @Override
    public UserRepository users() {
        return users(null);
    }

    @Override
    public ClientUserRepository clientUsers(final TransactionalContext tx) {

        return new InMemoryClientUserRepository();
    }

    @Override
    public ClientUserRepository clientUsers() {
        return clientUsers(null);
    }

    @Override
    public SignupRequestRepository signupRequests() {
        return signupRequests(null);
    }

    @Override
    public SignupRequestRepository signupRequests(final TransactionalContext tx) {
        return new InMemorySignupRequestRepository();
    }

    @Override
    public TransactionalContext newTransactionalContext() {
        // in memory does not support transactions...
        return null;
    }

    @Override
    public ProductsRepository products() {
        return new InMemoryProductsRepository();
    }

    public StorageUnitsRepository storageUnits() {
        return new InMemoryStorageUnitsRepository();
    }

    @Override
    public MachinesRepository machines() {
        return new InMemoryMachinesRepository();

    }

    @Override
    public RawMaterialCategoryRepository rawMaterialCategories() {
        return new InMemoryRawMaterialCategoryRepository();
    }

    @Override
    public RawMaterialRepository rawMaterial() {
        return new InMemoryRawMaterialRepository();
    }

    @Override
    public ProductionLinesRepository productionLines() {
        return new InMemoryProductionLinesRepository();
    }

    @Override
    public ProductionOrderRepository productionOrder() {
        return new InMemoryProductionOrderRepository();
    }

    @Override
    public RawMessagesRepository rawMessages() {
        return new InMemoryRawMessagesRepository();
    }

    @Override
    public ErrorMessagesRepository errorMessages() {
        return new InMemoryErrorMessageRepository();
    }

    @Override
    public ProductionOrderExecutionRepository productionOrderExecution() {
        return new InMemoryProductionOrderExecutionRepository();
    }

    @Override
    public CommunicationProtocolRepository communicationProtocols() {
        return new InMemoryCommunicationProtocolRepository();

    }

    @Override
    public RequestConfigurationRepository requestsConfiguration() {
        return new InMemoryRequestConfigurationRepository();
    }
    
    @Override
    public ProductionLineProcessingStateRepository processingState() {
        return new InMemoryProductionLineProcessingStateRepository();
    }

}

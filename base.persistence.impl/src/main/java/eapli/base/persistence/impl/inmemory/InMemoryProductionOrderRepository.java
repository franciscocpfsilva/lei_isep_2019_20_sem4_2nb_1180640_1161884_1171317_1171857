/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.inmemory;

import eapli.base.productionorder.domain.ProductionOrder;
import eapli.base.productionorder.domain.ProductionOrderID;

import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;
import eapli.base.productionorder.repositories.ProductionOrderRepository;

/**
 *
 * @author joaomachado
 */
public class InMemoryProductionOrderRepository extends InMemoryDomainRepository<ProductionOrder, ProductionOrderID>
        implements ProductionOrderRepository {

    static {
        InMemoryInitializer.init();
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.inmemory;

import eapli.base.communication.scm.domain.RequestConfiguration;
import eapli.base.communication.scm.repositories.RequestConfigurationRepository;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;

/**
 *
 * @author leona
 */
public class InMemoryRequestConfigurationRepository extends InMemoryDomainRepository<RequestConfiguration, Long>
        implements RequestConfigurationRepository{
}

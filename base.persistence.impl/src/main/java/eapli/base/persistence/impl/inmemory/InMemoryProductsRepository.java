/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.inmemory;

import eapli.base.products.domain.ProductID;
import eapli.base.products.domain.Product;
import eapli.base.products.repositories.ProductsRepository;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;
import java.util.function.Predicate;

/**
 *
 * @author
 */
public class InMemoryProductsRepository extends InMemoryDomainRepository<Product, ProductID>
        implements ProductsRepository {

    static {
        InMemoryInitializer.init();
    }

    @Override
    public Iterable<Product> findWithNoProductionSheet() {
        Predicate<Product> predicate = Product::hasProductionSheet;
        return match(predicate.negate());
    }


}

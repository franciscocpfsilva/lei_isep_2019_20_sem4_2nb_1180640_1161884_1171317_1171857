**João Cunha [1171857](../)** - UC2-3-1012 Como Gestor de Projeto, eu pretendo que o simulador de máquina suporte pedidos de monitorização do seu estado.
=======================================

# 1. Requisitos

**US1012** Como Gestor de Projeto, eu pretendo que o simulador de máquina suporte pedidos de monitorização do seu estado.

Pretende-se adicionar uma nova funcionalidade à aplicação que simule o funcionamento de uma máquina, em que a máquina suporte pedidos de monitorização do seu estado.

Esta User Story depende da 1011 e da 6001. A 1011 é a aplicação, e 6001 será a relação entre o nosso sistema de monitorização de maquinas e o simulador de máquina.

A máquina deve responder ao pedido *HELLO* com ACK ou NACK, e com a mensagem de texto que vem da ultima resposta do sistema central

Regras de negócio (identificadas no documento do protocolo de comunicação):
- As máquinas industriais tornam-se conhecidas pelo sistema de monitorização ao responder a
pedidos *HELLO* remetidos por este;
- As máquinas industriais nunca enviam pedidos ao sistema de monitorização.

# 2. Análise

Dado que é um sistema fora da nossa aplicação principal, ele está representado como *Machine* mas esta user story será sobre o tipo de comunicação com o sistema de monitorização. 

Esta comunicação não está representada no modelo de dominio.

A *Machine* irá esperar por mensagem do sistema de monitorização. Após receber a mensagem  é processada a informação, que é recebida como um conjuntos de bytes. De seguida verifica se o *CODE*, que é um atributo da mensagem, é do tipo *HELLO*. Se for, a *Machine* devolve a ultima resposta recebida pelo sistema central, caso contrário não envia nenhuma resposta.

Esta ultima resposta do sistema central vai ser recebida pela conexão TCP. Ficou defino que iriamos utiliza uma variavel **msg** do tipo Message e que iria estar na HEAP para as threads teres acesso a mesma! Controlando só o acesso a mesma para não existir problemas com a concorrencias 

# 3. Design

## 3.1. Realização da Funcionalidade

![](SSD.svg)


## 3.2. Diagrama de Classes

![](ClassDiagram.svg)

## 3.3. Padrões Aplicados

Não foram aplicados nenhuns padrões relevantes.

## 3.4. Testes 

As classes Domain já foram testadas previamente na criação do sistema de simulação de máquinas.

# 4. Implementação

Criação do UDP socket para comunicar com o Sistema de Monitorização de Máquinas. Foi criada uma struct *result* para poder retornar dois valores, o sock e o client. 

![](CommunicationWithSMMSocket.svg)

![](CommunicationWithSMMStruct.svg)

Ciclo infinito que aguarda por mensagens verificando se são do tipo *HELLO* ou do tipo *RESET*. As mensagem caso sejam do tipo *HELLO* envia uma resposta com a ultima mensagem recebida pelo sistema central ou caso sejam do tipo *RESET* o sistema envia uma mensagem do tipo *HELLO* ao sistema central e devolve a resposta do sistema central para o sistema de monitorização!

![](CommunicationWithSMMController.svg)
![](CommunicationWithSMMController_2.svg)

# 5. Integração/Demonstração

A unica integração necessária foi a parte de envio da mensagem partilha entre as duas threads, que foi discutida com o colega que estava a executar essa US Story e ficou predefinido como iriamos abordar estas User Story's.

# 6. Observações

Aspecto positivo aplicação de boas práticas de programa em C. Aquisição de informação como trabalhar com um programa em C utilizando varios packages e diferentes classes.
**Aluno [1171317](../)** - US4002 Recolher mensagens geradas nas máquinas
=======================================

# 1. Requisitos

_Nesta secção a equipa deve indicar a funcionalidade desenvolvida bem como descrever a sua interpretação sobre a mesma e sua correlação e/ou dependência de/com outros requisitos._

**US4002** Como Serviço de Comunicação com as Máquinas (SCM), pretendo proceder à recolha das mensagens geradas nas/pelas máquinas de uma determinada linha de produção.

O objetivo desta US, é o de oferecer a funcionalidade de recolha, de forma concorrente (usando threads), de mensagens geradas por máquinas e persisti-las na base de dados para posterior processamento (US5001). Este é um fluxo alternativo ao fluxo que envolve a importação de mensagens a partir de ficheiros de texto (**US4001**), e que igualmente as disponibiliza ao resto do sistema. Este programa terá a particularidade e dar resposta à **US1011**, responsável por simular o envio de mensagens por uma máquina para o SCM.

### Fórum para Esclarecimento de Requisitos (de suporte à US):

## 1.1. Critérios de aceitação

- As mensagens recolhidas devem ser disponibilizadas para posterior processamento.
- Deve ser criado um log de todas as mensagens recebidas das máquinas.
- A recolha das mensagens deve ser feita de forma concorrente (por linha e máquina).

## 1.2 Requisitos Protocolo de Comunicação

- O envio de uma mensagem implica o estabelecimento de uma conexão TCP sobre a qual é enviado o pedido e assim que a resposta seja obtida a conexão é fechada.
- Assume-se que as máquinas industriais são identificadas através de um número de identificação único (unique identification number), que corresponde a umnúmero inteiro positivo entre1e 65535.
- Criação de uma thread por mensagem.
- O sistema central conhece o número de identificação único de cada máquina industrial através do seu repositório de dados. Contudo, o endereço de rede de cada máquina industrial somente é conhecido depois de este ter sido ser contatado pela máquina através deuma mensagem HELLO.

### Estrutura de uma mensagem:

![messageStructure](messageStructure.svg)
‌
## 1.3 Dependências

# 2. Análise

_Neste secção a equipa deve relatar o estudo/análise/comparação que fez com o intuito de tomar as melhores opções de design para a funcionalidade bem como aplicar diagramas/artefactos de análise adequados._

_Recomenda-se que organize este conteúdo por subsecções._

## 2.1. Modelo de Domínio

Para além de toda as classes necessárias à realização desta US, foi necessário criar um novo conceito de dominio, MachineMessage, independente do conceito de dominio RawMessagem, que procura não só capturar a estrutura de uma mensagem gerada por uma máquina de acordo com os requisitos supracitados, como também habilita-la da capacidade de converter a instancia de Mensagem numa stream de bytes, e claro, habilita-la da capacidade de reverter este processo. 

Assinatura da classe mensagem de acordo com o formato geral:

- public class MachineMessage()

O código da mensagem pode ser um dos seguintes:

- 0 - HELLO request
- 1 - MSG request
- 2 - CONFIG request
- 3 - RESET request
- 150 - ACK response
- 151 - NACK response

#### Regras de negócio

No contexto de mensagem gerada por uma máquina, MachineMessage:

- A versão do protocolo tem de estar dentro do intervalo [0; 255]
- O código da mensagem tem de ser um dos fornecidos
- O campo tamanho dos dados deve ser >= 0

![MD_US4002](MD_US4002.svg)

# 3. Design

_Nesta secção a equipa deve descrever o design adotado para satisfazer a funcionalidade. Entre outros, a equipa deve apresentar diagrama(s) de realização da funcionalidade, diagrama(s) de classes, identificação de padrões aplicados e quais foram os principais testes especificados para validar a funcionalidade._

_Para além das secções sugeridas, podem ser incluídas outras._

## 3.1. Realização da Funcionalidade

_Nesta secção deve apresentar e descrever o fluxo/sequência que permite realizar a funcionalidade._

## 3.2. Diagrama de Classes

_Nesta secção deve apresentar e descrever as principais classes envolvidas na realização da funcionalidade._

## 3.3. Padrões Aplicados

_Nesta secção deve apresentar e explicar quais e como foram os padrões de design aplicados e as melhores práticas_

## 3.4. Testes

_Nesta secção deve sistematizar como os testes foram concebidos para permitir uma correta aferição da satisfação dos requisitos._

# 4. Implementação

_Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;_

_Recomenda-se que organize este conteúdo por subsecções._

# 5. Integração/Demonstração

_Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema._

Apesar de desenvolvida no contexto de uma nova aplicação, esta UC, integra e utiliza na sua realização o projeto **base.core** para reutilização de classes da camada de domínio pertinentes ao use case, e o projeto **base.persistence.impl** para o acesso à BD. 

# 6. Observações

_Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados._

**João Machado [1161884](../)** - UC3-3-6001 Monitorizar estado de Máquinas
=======================================

# 1. Requisitos
UC3-3-6001: Como Sistema de Monitorização das Máquinas (SMM), pretendo monitorizar o estado das máquinas por linha de produção.

A interpretação desta US foi complexa, existiram várias conversas com o cliente no fórum e em privado e originou vários debates internos no grupo.
 
Os requisitos identificados até ao momento é o desenvolvimento de uma nova aplicação em Java, Sistema de Monitorização das Máquinas (SMM), e a implementação de um protocolo UDP para comunicar diretamente com as máquinas de uma linha de produção.

### 1.1 Regras de Negócio

- A SMM ao iniciar deve enviar um pedido Hello para uma lista de IP's já conhecida em unicast ou broadcast a cada trinta segundos (ou outro valor configurado).
- A SSM deve poder receber respostas das Máquinas (AKN ou NACK).
- A SSM deve construir e manter uma lista de máquinas.
- Se não houver atualização de uma máquina industrial por mais de um minuto (valor configurável), esta deverá ser marcada como indisponível, mas não removida da lista.

# 2. Análise

Esta US não está contemplada no Modelo de Domínio desenvolvido no Sprint A, como tal, o processo de levantamento de requisitos tem de ser repetido.

Existe uma dependência direta com a US 1012 (permitir que o simulador de máquina suporte pedidos de monitorização do seu estado) o que irá obrigar a que muitas deceções sejam tomadas em conjunto com o responsável dessa US.

## 2.1 Estudo

Como a nossa aplicação deve suportar dois valores configuráveis (cadencia do envio dos pedidos Hello e o tempo de atualização do estado da máquina), irá ser necessário fazer a validação destes parâmetros passados por linha de comandos.

Outra necessidade é implementar diretamente na aplicação uma lista de endereços para onde queremos enviar os pedidos. Por ainda não estar definido em que redes serão colocados os simuladores, esta lista apenas irá conter o broadcast (255.255.255.255), permitindo assim testar com uma máquina virtual o funcionamento da mesma.

Inicialmente a aplicação irá enviar um pedido Hello. Este tem de obedecer ao formato geral das Mensagens e como tal, apenas os campos "Versão" e "Código de Pedido" estarão preenchidos.

Esta criação da Mensagem e do pacote UDP será feita dentro de um ciclo, com a cadência passada por parâmetro ou então com o valor *default* de 30 segundos.

Sempre que aplicação receber um pacote, este será transformado dados manipuláveis onde será feita a verificação do código de resposta. Este código para já poderá ser AKN ou NACK.

Se o código for AKN, a aplicação irá criar uma máquina com os dados recebidos e adicionada a uma estrutura de dados. Durante esta fase, será verificado se já existe máquinas com o mesmo código de comunicação e se esse for o caso, confirmar que contêm o mesmo IP, confirmando que se trata da mesma máquina. Ainda durante estas validações, validamos o tempo desde a última resposta desta máquina me que, se tiver excedido 1 minutos (ou outro valor configurável) esta é marcada como indisponível.

Se a resposta do pedido for NACK, é verificado se a máquina já existe na nossa estrutura. Caso exista é necessário modificar o estado dela para "Indisponível", se ainda não existir então esta informação não é recolhida.

## 2.2 UI

Tudo indica que não será necessário UI para esta US. A minha ideia para este caso é sempre que seja enviado um pedido Hello, também seja mostrada a informação das máquinas já disponíveis no sistema.

# 3. Design

## 3.1. Realização da Funcionalidade

No diagrama seguinte apresento o fluxo lógico desta US.

![SD_SMM.svg](SD_SMM.svg) 

## 3.2. Diagrama de Classes

Para o diagrama de classes optei por incluir todos os packages do projeto com o intuito de centralizar o observador para esta US e representar as diversas partes que são utlizadas.

![CD_SMM.svg](CD_SMM.svg) 

## 3.4. Testes 

### Testes unitários

**Teste 1:** Verificar que não é possível criar uma instância da classe Machine com valores nulos.

**Teste 2:** Verificar que não é possível criar uma instância da classe Message com valores nulos.

**Teste 3:** Verificar que a conversão de uma Mensagem para um byte[] é correta.

**Teste 4:** Verificar que a conversão de um byte[] para uma Mensagem é correta.

**Teste 5:** Verificar que é possivel fazer um update ao Estado de uma Máquina.

# 4. Implementação

Nesta secção apresento excerto do código desenvolvido, representativo do culminar do processo de planeamento desenvolvido para esta UC.
Inspirado nas US previamente desenvolvidas na aplicação ecafetaria, as classes implementadas foram desenvolvidas aplicando os conceitos lecionados em EAPLI, mantendo sempre em mente as boas práticas e os diversos padrões de POO.

## 4.1 Domain

Neste package é possível encontrar duas classes, Machine e Message. A classe Machine é responsável por criar instâncias com a informação recebida referente ás máquina, nomeadamente o ID do protocolo de comunicação, a *raw data* recebida na mensagem, o IP da máquina, o seu estado e a hora que recebeu o ultimo pacote. Este último campo é necessário para calcular se a máquina está num estado disponível ou indisponível. Isto é conseguido através da diferença entre essa hora guardada e a hora atual, sendo que se for maior do que o valor definido no arranque da aplicação o seu estado irá ser colocado a *Indisponível*.

Na classe Message existe um construtor para criar mensagens com os campos necessários. Também aqui existem dois métodos Serialized e Deserialized, responsáveis por criar um byte[] com os respetivos off-set para cada campo e por receber um byte[] e transformar em dados legíveis respetivamente. O Deserialized retorna uma instância de Machine criada com a informação que recebeu.

O ultimo método nesta classe é o *prepareRequest*, capaz de receber o tipo de request por parâmetro e devolver uma mensagem serializada, pronta para enviar por UDP.

![Serialize.svg](Serialize.svg)

## 4.2 Application

É no package Application onde encontramos o fluxo principal desta aplicação. Irei agora fazer um resumo do que foi implementado.

### 4.1.1 Server

Na classe Server existe um menu que permite escolher entre enviar um request Hello ou Reset.
Depois de uma escolha ser feita, são chamados métodos dentro da mesma classe que criam os Datagram Socket e Datagram Packet e definem um Timeout caso não exista resposta.

![HelloPrepare.svg](HelloPrepare.svg)

### 4.1.2 RequestController

Seguidamente aos Datagram's serem criados e no caso de ser um pedido Hello, esta classe percorre a lista de IP's disponível na aplicação e envia um pacote UDP para cada endereço presente nela. Depois do pacote ser enviado, a aplicação fica a aguardar durante 3 segundos por uma resposta e caso não haja a o Timeout termina a espera. 
No final é mostrada a lista de máquinas disponíveis no sistema e é questionado se quer enviar outro Hello request ou voltar para o menu. Este processo é repetido infinitamente com um intervalo de tempo definido no arranque.

Caso o pedido seja Reset o processo é semelhante, sendo que é pedido o IP da máquina para enviar o Reset e apenas é executado uma vez.

Ambos os pedidos terminam com uma chamada a um método capaz de processar a informação disponível, retornando uma instância de Machine, e adicionar o objeto á lista de máquinas disponíveis.
![HelloLoop.svg](HelloLoop.svg)

### 4.2.2 AvailableMachines

A estrutura de dados inicialmente escolhida era uma *LinkedList*, mas isso foi substituído por um *HashMap*. Com a chave do mapa representada por o ID do protocolo de comunicação é possível verificar facilmente que apenas pode existir uma máquina com esse mesmo ID. Caso o ID recebido já exista no nosso mapa, é verificado se os IP's são iguais. Se esta validação for verdadeira, significa que é uma máquina a responder novamente a um pedido. Se os IP's forem diferentes é imitido um aviso de erro e a funcionalidade termina.

Nesta classe também existe um método capaz de listar todas as máquinas disponíveis no sistema. Sempre que ele é invocado calcula a diferença de tempo para cada máquina e atualiza o seu estado.

![StatusUpdate.svg](StatusUpdate.svg)

# 5. Integração/Demonstração

A integração desta US foi bastante complexa. Está fortemente ligada á aplicação simuladora da máquina e muitos erros surgiram pela má manipulação de dados em ambas as aplicações.
Todo o desenvolvimento desta US é novidade o que apresenta dificuldades obvias não só no desenvolvimento, mas também no planeamento.

# 6. Observações
 
Face ao trabalho desenvolvido neste UC, sinto que cumpri perante os objetivos principais propostos e apresento uma solução válida. Existe muito espaço para melhoria, nomeadamente na organização das classes e das suas responsabilidades. Na aplicação dos conceitos e padrões de OOP também sinto que falhei em múltiplos aspetos. 

Penso que seja importante referir que existiram sérias dificuldades na interpretação do que era realmente pedido e como era suposto ser implementado. Estas dificuldades foram genéricas ao grupo e até mesmo a outros grupos e como tal apelo que no próximo sprint os enunciados sejam mais claros e explicativos.




**Aluno [1171857](../)** - UC2-3-1016 Receber pedidos RESET no simulador
=======================================

# 1. Requisitos

*Nesta secção o estudante deve indicar a funcionalidade desenvolvida bem como descrever a sua interpretação sobre a mesma e sua correlação e/ou dependência de/com outros requisitos.*

**US1016** Receber pedidos RESET no simulador

A interpretação feita deste requisito foi no sentido de simular o reset de uma maquina. Após receber um mensagem do tipo RESET, o simulador deve parar todas as atividades em segurança, e reiniciar o sistema. 

Regras de negócio (identificadas apenas através do forum para esclarecimento de requisitos):
- [Esclarecimento geral relativamente a User Story](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=31191#p41624)

# 2. Análise

*Neste secção o estudante deve relatar o estudo/análise/comparação que fez com o intuito de tomar as melhores opções de design para a funcionalidade bem como aplicar diagramas/artefactos de análise adequados.*

*Recomenda-se que organize este conteúdo por subsecções.*

Já existe uma classe de comunicação com o Sistema de Monitorização. Essa classe foi aplicada no sprint anterior é do tipo controller. Agora que penso melhor não foi a melhor opção utilizar o controller para a comunicação pois tem uma elavada carga de regras de negocios, e não é essa a função de um controller. 
Posto isto decidi criar um serviço que seria invocado pelo controller e ai sim teria toda a logica de negocio referente a comunicação com o Sistema de Monitorização. Este serviço irá receber dois tipos de mensagens, mensagem do tipo *HELLO*, e a mensagem do tipo *RESET*.

**Tipos de mensagens:**
* HELLO
* RESET

A interação entre o ator (nesta user Story será o Sistema de Monitorização) e a máquina é seguinte: 

1. **Sistema de Monitorização** envia mensagem **RESET**.
1. **Máquina** recebe mensagem.
1. **Máquina** procede a paragem das atividades de forma segura.
1. **Máquina** envia mensagem de **HELLO** ao **Sistema Central**.
1. **Sistema Central** responde a mensagem de **HELLO**.
1. **Máquina** responde à mensagem **RESET** (enviada pelo **Sistema de Monitorização**) com a resposta obtida à mensagem **HELLO** do **Sistema Central**.

No ponto **3.** faz referencia a *"paragem das atividades de forma segura"*. Esta paragem será feita atráves de uma variavel controlo partilhada entre threads. Esta variavel indica às threads se podem continuar com a sua atividade ou se têm de parar por um determinado tempo.
A variavel só é alterada na classe que recebe os pedidos de RESET, é da sua reponsabilidade parar/retomar a atividade da máquina.

# 3. Design

*Nesta secção o estudante deve descrever o design adotado para satisfazer a funcionalidade. Entre outros, o estudante deve apresentar diagrama(s) de realização da funcionalidade, diagrama(s) de classes, identificação de padrões aplicados e quais foram os principais testes especificados para validar a funcionalidade.*

*Para além das secções sugeridas, podem ser incluídas outras.*

## 3.1. Realização da Funcionalidade

*Nesta secção deve apresentar e descrever o fluxo/sequência que permite realizar a funcionalidade.*

![](DiagramaSequencia.svg)

## 3.2. Diagrama de Classes

*Nesta secção deve apresentar e descrever as principais classes envolvidas na realização da funcionalidade.*

![](DiagramaClasses.svg)

## 3.3. Padrões Aplicados
*Nesta secção deve apresentar e explicar quais e como foram os padrões de design aplicados e as melhores práticas*

Não foram aplicados nenhum padrão. Podiamos utilizar o padrão strategy se a programação fosse orientada a objectos, mas visto que é em C não podes criar um objecto generico que implementaria depois as diferentes interfaces neste caso mensagem de HELLO e mensagem de RESET.

## 3.4. Testes 
*Nesta secção deve sistematizar como os testes foram concebidos para permitir uma correta aferição da satisfação dos requisitos.*

Não foram gerados testes pois esta user story não tem qualquer influencia no Domain.

# 4. Implementação

*Nesta secção o estudante deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;*

*Recomenda-se que organize este conteúdo por subsecções.*

## Controller
![](Controller.svg)

## Service
![](Service.svg)

## Others

![](DoWhile.svg)

Estes exerto de codigo foi adicionado dentro de todos os ciclos infinitos. A variavel flag é a variavel de controlo entre threads. Atribuição do valor da variavel de controlo ao stop permite verificar se ainda está em pausa ou se pretende continuar.

# 5. Integração/Demonstração

*Nesta secção o estudante deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

O primeiro modo de integração seria o pthread cancel, que força a paragem de um thread. Este modo é errado pois uma threads pode estar a tentar alterar/aceder a alguma zona da memoria e é terminada de força não segura. Outra desvantagem é que ao terminarmos a threads, o objetivo seria iniciar a simulação outra vez, o que faria com que a simulação não conseguia responder ao pedido de RESET, bloqueando o Sistema de Monitorização, pois este espera por uma resposta.
Outra abordagem seria a nao utilização do *"do while"*, em que seria so utilizado um *while* com a variavel de controlo (*flag*). Nesta situação também sería uma má prática dado que varias threads estão a tentar ler um valor que pode estar a ser alterado por outra threads. Não permitiria controlar o acesso a variavel de controlo.
Por fim esta pareceu-nos a melhor implementação, foi discutida entre grupo e achamos que seria a melhor. 

# 6. Observações

*Nesta secção sugere-se que o estudante apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*

Considero que o trabalho realizado dá resposta ao requisto do cliente. 
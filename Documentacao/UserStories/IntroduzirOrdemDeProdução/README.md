**João Machado [1161884](../)** - UC4-1-2010 Introduzir uma ordem de produção
=======================================

# 1. Requisitos

UC4-1-2010 - Como Gestor de Produção, eu pretendo introduzir manualmente uma Ordem de Produção.

A interpretação feita desta US foi no sentido de um Gestor de Produção querer introduzir manualmente, através da consola, uma Ordem de Produção referente a um Produto para uma determinada quantidade.

### 1.1 Regras de Negócio

- O ID da Ordem de Produção é obrigatório e não nulo.
- Uma Ordem de Produção é sempre referente a um Produto com Ficha de Produção.
- Uma Ordem de Produção é referente a pelo menos um ID de Encomenda.
- A data Prevista de Execução não pode anteceder a data de Emissão.
- A Ordem de Produção deve ser sempre inicializada com o Estado Pendente.

# 2. Análise

Analisando o nosso Modelo de Domínio e o Caderno de Encargos, constatamos que a Ordem de Produção é uma Root Entity do agregado Ordens de Produção.

Este é um Agregado simples e muito semelhante a outros já implementados por mim e outros colegas de grupo, sendo constituído por dois Value-Object: ID Encomenda e ID Ordem de produção e como já referido o Root Entity Ordem de Produção.

Seguidamente irei apresentar uma análise mais aprofundada sobre esta US.

## 2.1 Estudo

A Entidade desta US, Ordem de Produção, tem como atributos Data de Emissão, representativa da data em que foi emitida a ordem, Data Prevista de Execução que simboliza uma data hipotética para a conclusão da Ordem e um Estado da Ordem de Execução que, como referido anteriormente, deve ser iniciado como "Pendente".

Externo ao nosso Agregado existe também a associação com o Value-Object Quantidade e o ID Fabrico do Produto.

*Atual*
![Modelo.svg](Modelo.svg)

## 2.2 Ordem de Produção

A fim de responder aos requisitos a classe Ordem de Produção foi implementada. Esta classe é uma Entity e tem o construtor encarregue de criar Ordens de Produção. A Ordem de Produção tem a responsabilidade de garantir que todos os seus atributos não são *null* e que a Data Prevista de Execução não antecede a Data de Emissão.

Como o Estado da Ordem de Execução apresenta valores predefinidos e inicialmente na opção "Pendente", decidi que será um Enum em que no construtor será forçado a opção Pendente sem ser necessário passar por parâmetro.

## 2.3 ID Ordem de Produção e ID Encomenda

Semelhante a outros value-object ID já implementados por mim, apenas terão de verificar se estão em concordância com a sintaxe defina pelo grupo referente aos ID (ex: 1 palavra, não maior que 10 caracteres, serem alfanuméricos).

O ID Ordem de Produção tem ainda que garantir que é um valor único.

# 3. Design

## 3.1. Realização da Funcionalidade

No diagrama seguinte apresento o fluxo lógico desta US.

![SD_ProductionOrder.svg](SD_ProductionOrder.svg) 

## 3.2. Diagrama de Classes

Para o diagrama de classes optei por incluir todos os packages do projeto com o intuito de centralizar o observador para esta US e representar as diversas partes que são utlizadas.

![CD_ProductionOrder.svg](CD_ProductionOrder.svg) 

## 3.3. Padrões Aplicados

### Padrão Repository

Este padrão foi utlizado para aceder á informação existente sobre Produtos assim como fazer a persistência da instância Ordem de Produção criada. É especialmente útil pois permite abstrair da implementação técnica da camada responsável por criar a persistência.

### Padrão Factory

Este padrão foi utilizado no caso específico da criação de uma Ordem de Produção. 
Neste caso, o sistema disponibiliza a possibilidade de instanciar um objeto do tipo Ordem de Produção através de uma Factory de OrdemProducaosFactory. A aplicação deste tipo de padrão, entre outras, apresenta a vantagem de permitir encapsular as regras do domínio.

## 3.4. Testes 

### Testes unitários

**Teste 1:** Verificar que não é possível criar uma instância da classe ProductionOrder com ID nulo.

    @Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowedOrderId() {

        ProductionOrder instance = new ProductionOrder(null, DATE_OF_ISSUE, EXPECTED_EXECUTION_DATE, PRODUCT_ID,
                orderId, QUANTITY);
    }
Este teste é repetido para todos os atributos individualmente.

**Teste 2:** Verificar que ao criar uma ProductionOrder o seu estado é "Pendente".

    @Test
    public void ensureProductionOrderStateIsPendente() {
        orderId.add(OrderID.valueOf("Order1"));
        ProductionOrder instance = new ProductionOrder(EXECUTION_ORDER_ID, DATE_OF_ISSUE, EXPECTED_EXECUTION_DATE, PRODUCT_ID,
                orderId, QUANTITY);
        assertEquals(instance.productionOrderState(), STATE);
    }

**Teste 3:** Garantir que a data Esperada de Execução não antecede a data de Emissão

    @Test(expected = IllegalArgumentException.class)
    public void ensureExpecetedExecutionDateIsAfterDateOfIssue() {

        LocalDate dateOfIssue = LocalDate.parse("05-05-2020", formatter);
        LocalDate expectedExecutionDate = LocalDate.parse("04-05-2020", formatter);

        ProductionOrder instance = new ProductionOrder(EXECUTION_ORDER_ID, dateOfIssue, expectedExecutionDate, PRODUCT_ID,
                orderId, QUANTITY);
    }

**Teste 4:** Semelhante a outras US, o value-object ProductionOrderID apresenta múltiplos testes para validar que está em concordância com os critérios definidos pelo o grupo relativo a ID.

Apresento dois como exemplo.

    @Test(expected = IllegalArgumentException.class)
    public void ensureIdentifierMustNotBeEmpty() {
        System.out.println("must have non-empty identifier");
        new ProductionOrderID("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureIdentifierMustNotHaveStrangeCharacters1() {
        System.out.println("identifier must not have strange characters (test1)");
        new ProductionOrderID("00-00");
    }

**Teste 5:** Para o OrderID os testes serão idênticos aos do ProductionOrderID e não vejo motivo para os demonstrar novamente.

# 4. Implementação

Nesta secção apresento excerto do código desenvolvido, representativo do culminar do processo de planeamento desenvolvido para esta UC.
Inspirado nas US previamente desenvolvidas e na aplicação ecafetaria, as classes implementadas foram desenvolvidas aplicando os conceitos lecionados em EAPLI, mantendo sempre em mente as boas práticas e os diversos padrões de POO.

## 4.1 Domain

Relativamente ao Domain esta US requer a criação de vários value-Object em cima referidos, nomeadamente: ProductionOrderID e OrderID. Adicionalmente é criado um Enum para guardar os diversos estados possíveis de uma Ordem de Execução. Para a construção correta do Objeto, a Entity garante que nenhum valor é nulo e que a Data de Execução é igual ou precede a Data de Emissão. 

Como o estado da Ordem de Execução inicialmente é sempre pendente, é escusado passar este atributo por parâmetro, mas sim definir esse estado sempre que uma instância é criada.

Referente aos Value-Object, estes são de caracter genérico a outros já implementados e não apresentam características dignas de serem mencionadas.

![Domain.svg](Domain.svg)

## 4.1 Controller

O Controller da US acumula as dependências necessárias que não se encontram dentro do Agregado. Isto significa que a UI apenas manda dados primitivos e é responsabilidade do Controller de criar as instâncias necessárias para passar ao Construtor da Entidade.

Um exemplo com algum realce é o método findProductByID. Este método é chamado pela UI, que envia uma String. É responsabilidade do Controller de recorrer ao serviço capaz de procurar Produtos pelo ID na persistência e guardar esse objeto. Como nesta US é obrigatório que o Produto tenha uma Ficha de Produção, no mesmo método faço essa validação, retornando um boolean permitindo a US continuar ou não na UI.

Outra responsabilidade do Controller é a formatação das Strings que representam datas. 

![Controller.svg](Controller.svg)

## 4.1 UI

Como é possível observar pelo excerto de código, a implementação da UI é bastante simplista. Optei por apenas permitir a escolha de ID de Produtos através do input correto do ID diretamente na consola. Esta escolha baseia-se puramente na falta de tempo e de maiores prioridades.

É responsabilidade do UI de apanhar as exceções geradas pela US e apresentar uma mensagem especifica.

Outra responsabilidade é a opção de serem adicionados vários ID de Encomenda, através de um ciclo while.

![UI.svg](UI.svg)

# 5. Integração/Demonstração

O desenvolvimento desta US foi direto e eficaz, com a prática já adquirida de outras US já desenvolvidas anteriormente a simplificar o processo.

# 6. Observações

Face ao trabalho desenvolvido neste UC, sinto que cumpri perante os objetivos propostos e apresento uma solução válida. Existe espaço para melhoria, nomeadamente nas alternativas sobre a escolha de Produtos.



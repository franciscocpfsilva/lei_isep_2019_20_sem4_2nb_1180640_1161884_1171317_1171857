**Aluno [1171317](../)** - US3010 Solicitar Envio de Configuração para uma Máquina

=======================================

# 1. Requisitos

_Nesta secção a equipa deve indicar a funcionalidade desenvolvida bem como descrever a sua interpretação sobre a mesma e sua correlação e/ou dependência de/com outros requisitos._

**US3010** - Como Gestor de Chão de Fábrica, eu pretendo solicitar o envio de uma determinada configuração para uma máquina.

#### (09/06/20)
O objetivo desta US, é o de oferecer, através da aplicação de **backoffice**, a funcionalidade de solicitação de envio de uma das configurações previamente associadas por composição a uma determinada máquina. Estas solicitações não terão resultados/consequencias imediatas, serão no entanto persistidas no nosso sistema enquanto aguardam processamento pela aplicação servidora, **Serviço de Comunicação com as Máquinas (SCM)**, e só então terá lugar o envio da configuração diretamente para a respetiva máquina através de um pedido CONFIG baseado em TCP.

Portanto, para já, para a realização desta US, prevê-se a criação de uma nova entidade no contexto da aplicação **base.core**, capaz de representar uma solicitação de envio de configuração, bem como o mecanismo de responsável por dár resposta aos pedidos persistidos na base de dados, no contexto da aplicação **base.scm**. Idealmente a aplicação servidora será capaz de oferecer esta funcionalidade paralelamente à recolha de mensagens geradas nas máquinas.

#### Criação de novo pseudo-conceito de domínio (09/06/20)
À imagem da abordagem usada na secção de requisitos da **US4002 - Recolher Mensagens Geradas nas Máquinas**, surge aqui também a necessidade de criar e persistir um novo conceito, que por sua vez será usado na solicitação de envio de configurações. Ora, esta tarefa inevitavelemnte precede a implementação da presente US. Assim sendo, foi criada uma nova **Task - Criação da classe RequestConfiguration e respetivo Repositório**, e foi-lhe atribuida a responsabilidade que o próprio nome sugere. No entanto, apenas o esqueleto das classes envolvidas será criado no contexto desta nova tarefa, enquanto que a consequente implementação e documentação será realizada nesta US.

#### Nova interpretação (12/06/20)
À luz de esclarecimentos feitos pelo Cliente, ao invés de criar uma nova entidade que permitisse a persistência dos pedidos para paralelo ou posterior precessamento, opta-se agora por estabelecer uma ligação baseada em TCP para comunicar com o SCM. Desta forma, numa realização bem sucedida desta US, os pedidos serão automaticamente enviados para o SCM (que deverá estar em execução) e daí reencaminhados para a respetiva máquina (que deverá estar também em execução).

#### Fórum para Esclarecimento de Requisitos (de suporte à US):
- **[Envio e receção de ficheiros de configuração](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=31409)**
- **[US1014 - Simulador receber um ficheiro de configuração](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=31198)**
- **[Ficheiro de Configuração do Simulador da Máquina](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=31119)**
- **[Solicitação de envio de uma determinada configuração para uma máquina](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=31100)**


## 1.1 Critérios de Aceitação/Observações

Nenhum.

## 1.2 Regras de Negócio

## 1.3 Dependências

Uma vez que esta US acontece no contexto da aplicação, **SCM**, verifica-se que a presente US, pedende diretamente da criação desta aplicação servidora, realizada na **US4001 - Importar Mensagens Existentes em Ficheiros de Texto**, e claro, das classes da camada de aplicação, implementadas na **US4002 - Recolher Mensagens Geradas nas Máquinas**, que eventualmente possam ser reutilizadas nesta US. Dependerá também da criação e persistencia do novo conceito representativo dos pedidos de configuração colocados pelo Gestor de Chão de Fábrica.

# 2. Análise

_Neste secção a equipa deve relatar o estudo/análise/comparação que fez com o intuito de tomar as melhores opções de design para a funcionalidade bem como aplicar diagramas/artefactos de análise adequados._

### Análise a (09-06-20)

Uma primeira ideia para resolver esta US, seria usando eventos. Estes seriam espoletados pelas solicitações de envios de configurações no **backoffice**, e resolvidos pelo **SCM**. No entanto optou-se por uma abordagem mais simplista, na qual a persisência será usada como elo de ligação entre as duas aplicações, não existindo qualquer comunicação entre as mesmas. Antecipa-se que que a aplicação servidora, assim que 'arranque', inicie também, a recolha de pedidos persistidos, bem como, a recolha de novos pedidos que possam eventualmente surgir na base de dados enquanto a **SCM** corre em 'background'.

Podemos, portanto dividir esta US em duas partes, cada uma a ser realizada em contextos de aplicações diferentes. Numa primeira instancia, teremos a **solicitação de envio de um ficheiro de configuração**, que resultará, sem bem sucedida, na criação de uma nova entrada na tabela de Pedidos Config.Este use case poder-se-á repetir indefinidamente. Por outro lado, temos o **envio concreto dos ficheiro para as máquinas** por uma ligação baseada em TCP. Ora, paralelamente à solicitação, ou em momentos distintos, teremos a aplicação SCM, que assim que 'arranque', procurará, periodicamente, encontrar entradas registadas na mesma tabela de Pedidos Config. A ideia é, que em intervalos de tempo predefinidos, o SCM faria uma pesquisa pela proxima entrada na tabela. Por cada entrada encontrada, essa será removida e a correspondente tentativa de envio do ficheiro será iniciada. Caso o envio não seja bem sucedido, o pedido seria novamente adicionado à tabela. 

Apesar de simplista esta abordagem usada, apresenta claras desvantagens. Não existe um _estado ocioso_ para os cenários em que a tabela possa estar vazia, ou que máquina a contactar esteja indisponivel/desligada. Neste último cenário, o envio é periodicamente tentado até que o ficheiro seja entrague.

#### Criação de novo pseudo-conceito de domínio
Este novo conceito será caracterizado por dois atributos, pelo o **ID do protocolo de comunicação**, que identificará a máquina no **SCM**, e pelo **ficheiro de configuração** que se pretende enviar, ambos já representados no nosso sistema como _value-objects_. 

Atributos da nova entidade _RequestConfiguration_:
- CommunicationProtocolID
- ConfigurationFile

### Análise a (12-06-20)

Continuaremos a dividir esta US em duas partes, no entanto, agora, a **solicitação de envio de um ficheiro de configuração**, resultará no envio de uma linha de texto contendo o ID da máquina e a identificação do ficheiro a enviar, através de uma ligação baseada em TCP. Já do lado do SCM, o processamento esta linha, resultará numa pesquisa no repositório das máquinas pela máquina mencionada, e no consequente estabelecimento de uma ligação novamente baseada em TCP com a mesma máquina. É nesta conexão que será finalmente enviado o ficheiro de configuração especificado.

Outro ponto de discussão faz-se aquando do cenário em que queremos executar a presente UC simultaneamente com a UC **US4002 Recolher mensagens geradas nas máquinas**. Aqui, incorriremos num conflito gerado em resultado de utilizarmos o mesmo porto (atribuido por grupo) na aplicação servidora SCM, para receber ao mesmo tempo pedidos do Backoffice e pedidos gerados pelas máquinas. Isto acontece uma vez que são usadas estruturas de dados distintas para representar os pedidos de ambas as aplicações. Tornar-se-á impossível distinguir a estrutura da mensagem até efetivamente a estrutura ser lida. Para resolver este conflito, optou-se por utilizar um segundo porto, que ficaria então exclusivo para os pedidos que chegam da aplicação Backoffice. O número do porto alternativo usado: 9999. 

## 2.1. Modelo de Domínio

# 3. Design

_Nesta secção a equipa deve descrever o design adotado para satisfazer a funcionalidade. Entre outros, a equipa deve apresentar diagrama(s) de realização da funcionalidade, diagrama(s) de classes, identificação de padrões aplicados e quais foram os principais testes especificados para validar a funcionalidade._

## 3.1. Realização da Funcionalidade

_Nesta secção deve apresentar e descrever o fluxo/sequência que permite realizar a funcionalidade._

### Diagramas de Sequência da presente US

#### No contexto da aplicação Backoffice:
![SD_US3010_RequestConfigFileToSendToMachine_part1](SD_US3010_RequestConfigFileToSendToMachine_part1.svg)

#### No contexto da aplicação SCM:
![SD_US3010_RequestConfigFileToSendToMachine_part2](SD_US3010_RequestConfigFileToSendToMachine_part2.svg)

A abordagem seguida neste diagrama é em tudo igual às abordagens usadas até então por outras UCs de adição/especificação de entidades no sistema. 

## 3.2. Diagrama de Classes

_Nesta secção deve apresentar e descrever as principais classes envolvidas na realização da funcionalidade._

![CD_US3010_RequestConfigFileToSendToMachine](CD_US3010_RequestConfigFileToSendToMachine.svg)

Este diagrama de classes procura encapsular todas as classes envolvidas na realização da presente US, apesar de implicarem duas aplicações distintas. 

A classe **SendRequestFactory** cria uma implementação da **interface SendRequest** de acordo com o tipo de pedido que procuramos enviar. No contexto do projeto SCM, criará uma instância de **SendRequestConfig**, para dar resposta ao pedido de configuração previamente enviado pelo Backoffice.  

A classe **MachineMessageTranslatorService**, aqui extendida a 'serviço', procura apenas encapsular as responsabilidades que envolvem a leitura e escrita de uma Mensagem de máquina para os DataInputStream e DataOutputStream, respetivamente. A ideia por detrás desta abordagem é a de reduzir a duplicação de código, uma vez que a tais responsabilidades são requeridas em mais do que uma classe.

**ReplyToRequestFromBackofficeServer** - Esta classe, representa o programa servidor responsável por 'escutar' e aceitar os pedidos de configuração provenientes do Backoffice. Ao mesmo tempo, a classe **ReplyToRequestFromBackofficeThread** ficará responsável por lidar com cada um desses pedidos. Por outro lado, a classe **SendRequestConfigToMachineClient** fica responsável por representar o 'lado' cliente da ligação que quer estabelecer com o simulador de máquinas como objetivo do envio concreto da mensagem de configuração. 

Já a classe **SendRequestConfigToMachineThread**, corresponde efetivamente à execução da presente US (inicia o controlador), no entanto sobre a forma de um sub-processo, que acreditamos ser do nosso interesse, que se venha realize paralelamente à US**4002**. Junto com a alocação de portos distintos, cada um para um tipos de mensagens distinta, esta simultaneidade torna-se possível. 

## 3.3. Padrões Aplicados

_Nesta secção deve apresentar e explicar quais e como foram os padrões de design aplicados e as melhores práticas_

**Protected Variation (GRASP)** Deparamo-nos com alguns pontos de variação. Nomeadamente o tipo de pedido a receber do Backoffice. Esta abordagem espelha a abordagem usada na US4002 para lidar com os diferentes tipos de pedidos que poderiam ser enviados pela máquina ao SCM. 
 
**Strategy (GoF)** Usado se existir um comportamento que deve/pode variar, sem implicar a alteração do objeto de dominio. Nesta UC, aplicado para permitir o uso de diferentes implementações concretas, de formas de lidar com os diferentes pedidos solicitados. Foi criada uma interface **SendRequest** que padroniza a assinatura dos comportamentos de envio.

**Service (DDD)** Honestamente, usado nesta US, talvez de forma demasiado lata. Isto é, na falta de melhor terminologia, ou até mesmo na falta de melhor atribuição de responsabilidades, esta designação foi aqui também usada, para encapsular comportamentos que se antecipavam vir a ser usados por mais do que uma classe. _Desacoplamento entre a funcionalidade da UC e o controlador do UC, e esta passa a ser atribuida a um serviço. este serviço fica então disponivel para ser reutilizado._

**Simple Factory (GoF)** Criada a classe **SendRequestFactory**, novamente, à imagem da abordagem usada na US**4002**, com a classe **ReplyToRequestFactory**, com a responsabilidade de criar a classe concreta que implementa os comportamentos (**template method**) de envio, especifico ao tipo de pedido recebido do Backoffice.

**Visitor (GoF)** Pseudo-aplicação deste padrão na camada de domínio, nomeadamente sob a forma de um formato genérico de representação textural do objecto _Machine_. A classe que implementa a interface _Visitor_ oferece meramente este formato de apresentação sem modificar a classe _Machine_.

## 3.4. Testes 

### Testes unitários

**Teste 1:** Verificar que não é possível criar uma instância da classe RequestConfiguration com o parametros a nulo.

**Teste 2:** Verificar que é possível criar uma instância da classe RequestConfiguration com parametros válidos.

### Testes funcionais


### Testes unitários

# 4. Implementação

_Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;_

# 5. Integração/Demonstração

_Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema._

Apesar de desenvolvida no contexto de uma nova aplicação, esta UC, integra e utiliza na sua realização o projeto **base.core** para reutilização de classes da camada de domínio pertinentes ao use case, e o projeto **base.persistence.impl** para o acesso à BD. 

# 6. Observações

_Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados._

Uniformizar definitivamente a estrutura das Mensagens. Verifica-se agora, que a estrutura das mensagens, definida no doc. relativo ao Protocolo de Comunicação, poderia ter sido aqui tambem adotada na comunicação entre o SCM e o Backoffice. Fez-se no entanto Numa primeira instancia procurou-se diminuir as dependencias entre as aplicações, nomeadamente a entidade mensagem definida no SCM. Idealmente, estas seriam iguais. 

Criação de uma nova entidade e respetivo repositório, bem como dos mecanismos responsáveis pela devida persistencia. No entanto, este esforço, algo inglorio, acabou por não ser aplicado de forma alguma na presente UC. Tento sido escolhida a comunicação por TCP.

Demasiado tempo gasto com o design o que levou a sucessivas reestruturações. 
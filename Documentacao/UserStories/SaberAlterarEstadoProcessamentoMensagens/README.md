**João Machado [1161884](../)** - UC4-2-3009 Saber e alterar estado do processamento de mensagens
=======================================

# 1. Requisitos

UC4-2-3009: Como Gestor de Chão de Fábrica, eu pretendo saber e alterar o estado (ativo/desativo) do processamento de mensagens de cada linha de produção bem como conhecer a última vez que o mesmo se realizou.

A interpretação feita deste requisito foi no sentido de ser possível ao Gestor de Chão de Fábrica poder, a qualquer momento, visualizar o estado do processamento de mensagens *Recorrente* (Ativo/Desativo). Também deve ser possível que o Gestor, se assim o entender, alterar este mesmo estado ativando o processamento *recorrente* das mensagens ou especificando um intervalo de tempo, pedir o processamento em bloco.
Sempre que for pedido um tipo de processamento os dados analisados devem ser refletidos no sistema e o Gestor deve ser notificado do sucesso da operação.

## 1.1 Regras de Negócio
- Só deve ser possível ativar o processamento de mensagens recorrente caso este esteja desativo.
- Se o estado de processamento de mensagens recorrente estiver ativo, não deve ser possível pedir o processamento em bloco.

## 1.2. Interpretação

Na aplicação Serviço de Processamento de Mensagens (SPM), para além do processamento realizado por blocos temporais e implementado na US 5001, deverá existir um processamento recorrente. A diferença entre ambos é que o primeiro é temporário, é executado uma vez e termina. Por sua vez, o segundo processa as mensagens existentes de x em x tempo, pelo que a aplicação deve estar constantemente a correr, sendo que no máximo é possível ativar/desativar o processamento recorrente para uma dada linha (US 3009).

![aplicacoes_existentes](aplicacoes_existentes.svg)

## 1.3. Dependências

- US 5001 - Processar mensagens em bloco para um intervalo de tempo;
- US 5002 - Processar mensagens de forma recorrente;

O desenvolvimento desta funcionalidade está a ser coordenado com os membros de equipa encarregue das US 5001 e 5002.

# 2. Análise

Inicialmente fiz o enquadramento desta UC com o modelo de domínio desenvolvido e com as dependências do mesmo já analisadas numa fase anterior. 

O processamento das mensagens já foi/está a ser implementado nas US 5001 e 5002. Do mesmo modo, os conceitos de domínio necessários, tais como a Execução das ordens de produção, as Produções, os Consumos, os Desvios, etc, já foram tratados nessas US. Eis o modelo de domínio que dá resposta ao processamento de mensagens.

![modelo_dominio_execucao_ordem_producao](modelo_dominio_execucao_ordem_producao.svg)

Tendo isto em conta, esta US serve como um *hub* de comunicação para ambas US, permitindo ao Gestor de Chão de Fábrica através da aplicação *backoffice* interagir com ambas de uma forma simples e sucinta.

Para dar resposta às US com quais o sistema vai interagir, foi criada uma nova aplicação SPM (Sistema de Processamento de Mensagens) em Java. Esta aplicação deve estar sempre em execução pelo que apresenta dificuldades especificas. Para o processamento recorrente, não será necessária uma interação direta entre as duas aplicações, sendo que o SPM recorre à *Base de Dados* para verificar quais as linhas de produção que possuem um estado de processamento como ativo e então efetuar o seu processamento. 

No caso do processamento de mensagens em bloco, são necessários vários parâmetros sendo estes o intervalo de tempo para qual se pretenda que o processamento seja referente e uma lista com os ID's das Linhas de produção.

O envio destes parâmetros apresenta uma dificuldade acrescida, pois como a SPM já se encontra em execução é necessário encontrar uma solução para enviar os mesmos. Foram escolhidas duas implementações distintas para a resolução deste problema sendo que uma utiliza uma comunicação através do protocolo TCP. A aplicação backoffice constrói um pacote TCP onde adiciona o intervalo de tempo e as linhas de produção, fazendo as validações necessárias e a SPM, que se encontra sempre à escuta de novas ligações, recebe este pacote e efetua o processamento.

Como esta solução apresenta complexidade acrescida na implementação do protocolo de comunicação TCP, outra opção foi pensada. Neste segundo caso não existirá comunicação através de TCP e não será usada a mesma instância da SPM, pelo o contrário, sempre que for necessário processar mensagens em bloco, o respetivo método da SPM é chamado diretamente pelo backoffice. Esta alternativa não respeita totalmente a condição de que a SPM tem de estar sempre a correr, possivelmente noutra máquina, pois corre a aplicação localmente, mas é uma solução viável e mais simplista de implementar tornando possível demonstrar a US num estado funcional.

Numa primeira fase será implementada a segunda opção devido a restrições de tempo e se existir a possibilidade, será substituída comunicação TCP.

# 3. Design

## 3.1. Realização da Funcionalidade

No contexto do que foi referido no capítulo anterior, irei agora apresentar o diagrama de fluxo lógico desta US, para a segunda solução.

No diagrama seguinte apresento o fluxo lógico desta US.

![SD_US3009](SD_US3009.svg)

## 3.2. Diagrama de Classes

Para o diagrama de classes optei por incluir todos os packages do projeto com o intuito de centralizar o observador para esta US e representar as diversas partes que são utlizadas.

![CD_US3009](CD_US3009.svg) 

## 3.3. Padrões Aplicados

### Padrão Repository
Este padrão foi utlizado para aceder á informação existente sobre as Linhas de Produção assim como fazer a persistência do Estado da mesma atualizado. É especialmente útil pois permite abstrair da implementação técnica da camada responsável por criar a persistência.

### Padrão Visitor
Este padrão foi utlizado para aceder á informação existente sobre as Linhas de Produção através da camada de apresentação, eliminando uma dependência adicional.

### Design
- **Single Responsibility Principle**
- **Tell, don't ask** 

## 3.4. Testes 

Esta US não possui camada de domínio pelo que não foi necessário a implementação de testes unitários.

Foram sim, aplicados e desenvolvidos testes funcionais para cada opção que o cliente possa escolher, garantindo um produto final sólido e com tratamento de erros.

# 4. Implementação

Nesta secção apresento excerto do código desenvolvido, representativo do culminar do processo de planeamento desenvolvido para esta UC.
Inspirado nas US previamente desenvolvidas na aplicação ecafetaria, as classes implementadas foram desenvolvidas aplicando os conceitos lecionados em EAPLI, mantendo sempre em mente as boas práticas e os diversos padrões de POO.

Irei dividir cada opção possível por tópicos e explicar mais detalhadamente.

## 4.1 View Production Lines State

Para a visualização dos estados de todas as *ProducitonLine* no sistema é necessário recorrer a uma entidade especifica, *ProductionLineProcessingState*. Esta entidade possui apenas dois atributos sendo eles o ID da Linha de Produção e o respetivo estado associado.

A classe de **UI** utiliza o **Controller** da *user story* para aceder ao respetivo repositório e através de um **Printer** mostrar para a consola o ID da Linha de Produção e o Estado.

## 4.2 Change Processing Messages State

Esta opção é direta, sendo apenas necessário introduzir um ID e um boolean (Y/N) representativo do estado. Aquando dos dados introduzidos, o *controller* é invocado com os dados *crude*, sendo da sua responsabilidade aceder à camada de persistência e verificar se o ID é valido. Se este caso se verificar, o estado da respetiva linha é modificado e, novamente o controller, acede à persistência e guarda a modificação.

## 4.3 Process Messages in Block

A particularidade desta funcionalidade reside na possibilidade de enviar vários ID's de Linha de produção. A solução implementada inicialmente requer a introdução de duas datas num formato específico e seguidamente é iniciado um ciclo onde é possível adicionar **pelo menos um** ID de linha de produção. Cada ID é validado utilizando o *controller* e quando o ciclo terminar, por input do utilizador, é invocado o respetivo método da aplicação SPM.

![controller](controller.svg) 

**UPDATE**: Relativamente a esta funcionalidade, foi adicionado o protocolo de comunicação por TCP. Com este protocolo implementado é possível aceder á instância da aplicação SPM que já se encontra em execução localmente ou noutra rede, introduzindo o IP manualmente.

# 5. Integração/Demonstração

O desenvolvimento desta US foi simples, todas as decisões já tinham sido tomadas previamente em equipa em principal foco com os responsáveis pelas US das quais esta é dependente.

Surgiu um dificuldade relativo a dependências cíclicas com o *maven* mas que rapidamente foi ultrapassada.

# 6. Observações

Face ao trabalho desenvolvido neste UC, sinto que cumpri perante os objetivos propostos e apresento uma solução válida. Existe espaço para melhoria, nomeadamente a implementação de um protocolo de comunicação TCP, entre a US 5001 e esta US, permitindo que seja utilizada a mesma instancia que já se encontra em execução para a US 5002.

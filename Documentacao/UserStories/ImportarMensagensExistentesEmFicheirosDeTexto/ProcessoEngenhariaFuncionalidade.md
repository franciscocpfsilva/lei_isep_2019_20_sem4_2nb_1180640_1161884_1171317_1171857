**Aluno [1171317](../)** - US4001 Importar Mensagens Existentes em Ficheiros de Texto
=======================================

# 1. Requisitos

_Nesta secção a equipa deve indicar a funcionalidade desenvolvida bem como descrever a sua interpretação sobre a mesma e sua correlação e/ou dependência de/com outros requisitos._

**US4001** - Como Serviço de Comunicação com as Máquinas (SCM), pretendo importar, de forma concorrente/paralela, as mensagens existentes nos ficheiros de texto presentes no diretório de entrada de forma a disponibilizar as mesmas para processamento.

O objetivo desta US, é o de oferecer a funcionalidade de importação, de forma concorrente (usando threads), de mensagens existentes em ficheiros e persisti-las na base de dados para posterior processamento (US5001). Este é um fluxo alternativo ao fluxo que envolve a recolha de mensagens (US4002) diretamente das máquinas, e as disponibiliza ao resto do sistema para processamento (US5001).

#### Criação de uma nova Aplicação servidora (27/05/20)
Ora, esta funcionalidade, a par da **US4002**, existirá no contexto de uma nova aplicação servidora a ser desenvolvida, o **Serviço de Comunicação com as Máquinas (SCM)**. Esta aplicação será implementada também no contexto da presente US, e fará uso dos modulos responsáveis pela lógica de negócio e pela persistencia.

#### Criação de novo pseudo-conceito de domínio
A nossa abordagem para realizar a persistência das mensagens implica também o uso do conceito _RawMessage_, que para todos os efeitos não fará parte do nosso domínio, e que à data da criação deste documento, não existe no nosso sistema. Surge então a necessidade de representar este conceito no nosso projeto, antes da iniciar a realização desta US. A responsabilidade de criar esta nova classe _RawMessage_, que encapsule o conceito de mensagem bruta, a par da criação do respetivo repositório, cai então sobre uma nova task, criada para este propósito, **Task - Criação da classe Mensagem e respetivo Repositório]**.

Será aqui feita também, ainda que abreviadamente, a documentação dos componentes do processo de engenharia relacionado com a criação e persistência deste novo conceito no nosso sistema, que se acharem mais pertinentes. 

#### Fórum para Esclarecimento de Requisitos (de suporte à US):
- **[Área de Comunicação](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=30244)**
- **[Tipo identificador mensagem](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=30561#p40822)**
- **[Importação de mensagens](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=30456#p40647)**

## 1.1 Critérios de Aceitação/Observações

- Este serviço deve corresponder a uma aplicação servidora usando threads em Java.
- Após importação, os ficheiros devem ser transferidos para um diretório de ficheiros processados.
- Ambos os diretórios devem ser definidos por configuração aquando da implantação do sistema.
- No mesmo diretório de entrada podem existir ficheiros com diferentes formatos e estruturas.

## 1.2 Regras de Negócio

Aplicáveis à classe _RawMessage_ (adaptadas do caderno de encargos):
- Código interno da máquina (alfanumérico). (não o código interno requerido pelo protocolo de comunicação)
- Um qualificador do tipo de mensagem.
- Data/hora em que a mensagem foi **gerada** (com exatidão máxima até ao segundo, i.e. 20190326151446).
- Considera-se que duas mensagens são iguais quando estas têm origem na mesma máquina e possuem o mesmo tipo de mensagem e a mesma data/hora de geração.

Aplicáveis à presente US:
- Pré-validação das mensagens:
    - Mensagem com formatos/estruturas desconhecidas
    - Mensagens com datas futuras (diferenças de dias e não de segundos)
    - Mensagens duplicadas (i.e. com os mesmos atributos base)
    - Todas estas mensagens devem ser rejeitadas. Contudo, deverá existir um log/registo das mesmas (e.g. em ficheiro de texto)

## 1.3 Dependências

À exceção da necessidade de um mecanismo de persistência previamente implementado, no nosso caso através da criação da classe _RawMessage_, esta US não possui quaisquer dependências com outras US.

# 2. Análise

_Neste secção a equipa deve relatar o estudo/análise/comparação que fez com o intuito de tomar as melhores opções de design para a funcionalidade bem como aplicar diagramas/artefactos de análise adequados._

## 2.1. Modelo de Domínio

### Representação do conceito Mensagem

![MD_Mensagens](MD_Mensagens.svg)

## 2.2. Estudo

A [US 2005 - Importar Produtos por Ficheiros CSV]() foi usada como referencia no estudo do mecanismo de importação e dos padrões usados.

# 3. Design

_Nesta secção a equipa deve descrever o design adotado para satisfazer a funcionalidade. Entre outros, a equipa deve apresentar diagrama(s) de realização da funcionalidade, diagrama(s) de classes, identificação de padrões aplicados e quais foram os principais testes especificados para validar a funcionalidade._

## 3.1. Realização da Funcionalidade

_Nesta secção deve apresentar e descrever o fluxo/sequência que permite realizar a funcionalidade._

#### Diagrama de Sequência da presente US

![SD_US4001_ImportRawMessageFromFile](SD_US4001_ImportRawMessageFromFile.svg)

O programa responsável pela importação inicia recebendo como parametro o tipo de formato de ficheiro esperado. Presentemente apenas o formato CSV está disponivel. De seguida, para cada ficheiro (cujo formato do nome é previamente definido e cuja extensão corresponde ao ID da maquina onde foi gerado) existente do diretório de entrada, é verificada a validade do ID contra as instancias armazenadas na BD. No presença de um ficheiro válido é iniciada uma thread que executa um serviço de importação de ficheiros utilizando o importador adequado.  

## 3.2. Diagrama de Classes

_Nesta secção deve apresentar e descrever as principais classes envolvidas na realização da funcionalidade._

#### Camada de "Domínio" e Persistencia, no contexto do conceito Mensagem

![CD_Task_CreateRawMessage](CD_Task_CreateRawMessage.svg)

#### Camada de Aplicação, "Domínio" e Persistencia, no contexto da presente US

![CD_US4001_ImportRawMessageFromFile](CD_US4001_ImportRawMessageFromFile.svg)

A classe **ProductImporterFactory** cria uma implementação da **interface ProductImporter** de acordo com o tipo de ficheiro que recebe. Para esta US, criará uma instância de ProductImporterCsv, mas quando surgir a necessidade de englobar mais tipos de ficheiros, teremos mais implementações da mesma interface.

A classe **ProductImporterService** corresponde ao serviço que contém a estrutura do algoritmo de importação.

A camada de UI tem dependência sobre a camada de domínio uma vez que vai receber do controlador uma lista dos produtos importados para mostrar ao utilizador.

## 3.3. Padrões Aplicados

_Nesta secção deve apresentar e explicar quais e como foram os padrões de design aplicados e as melhores práticas_

**Protected Variation (GRASP)** Apesar de neste caso termos uma implementação especifica para um formato, a aplicação deste padrão permite-nos acrescentar à medida do necessário, implementações para novos formatos sem incorrermos em qualquer alteração do codigo de negócio.
 
**Strategy (GoF)** Usado se existir um comportamento que deve/pode variar, sme implicar a alteração do objeto de dominio. Nesta UC, aplicado para permitir o uso de diferentes implementações concretas, especificas a um formato. Foi criada uma interface **RawMessageImporter** que padroniza a assinatura dos comportamentos de importação.

**Service (DDD)** Desacoplamento entre a funcionalidade da UC e o controlador do UC, e esta passa a ser atribuida a um serviço. este serviço fica então disponivel para ser reutilizado. 

**Simple Factory (GoF)** Criada a classe **RawMessagesImporterFactory**, com a responsabilidade de criar a classe concreta que implementa os comportamentos (**template method**) de importação especificos de um determinado formato.

## 3.4. Testes 

### Testes unitários

No o contexto da classe _RawMensage_

**Teste 1:** Verificar que não é possível criar uma instância da classe RawMessage com o valores a nulo.

**Teste 2:** Verificar que não é possível criar uma instância da classe RawMessage com um data de geração futura. (diferença em dias)

**Teste 3:** Verificar que é possível criar uma instância da classe RawMessage com parametros válidos (?)

### Testes funcionais

No o contexto desta US de importação

**Teste 1:** Verificar que importa mensagens através de um ficheiro válido.

**Teste 2:** Verificar que só importa mensagens válidas através de um ficheiro válido.

**Teste 3:** Verificar que cria um ficheiro de erros para apresentação das mensagens rejeitadas.


# 4. Implementação

_Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;_

Foram definidos e criados diretórios de entrada (**rawmessage_input**) e de saída (**rawMessage_output**) na raiz do projeto, os quais são destinados a armazenar ficheiros de mensagens à espera de validação, e ficheiros de mensagens validadas, prontas para processamento, respetivamente. Por cada ficheiro importado, é gerado um ficheiro de erros (e.g. **failedMessages.T3**) que será armazenado do diretório de entrada, e na presença de um ficheiro válido, a UC termina movendo-o para o diretório de saída.

# 5. Integração/Demonstração

_Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema._

Apesar de desenvolvida no contexto de uma nova aplicação, esta UC, integra e utiliza na sua realização o projeto **base.core** para reutilização de classes da camada de domínio pertinentes ao use case, e o projeto base.persistence.impl para o acesso à BD. 

# 6. Observações

_Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados._

Depois de alguma confusão no completo entendimento dos requisitos necessários e ter seguido por uma implemntação mais simples, foi feito um estudo sobre a possibilidade de implementar esta UC utilizando a infraestrutura Publisher/Subscriber disponibilisada pela framework de EAPLI. A ideia seria gerar um evento sempre que um novo ficheiro fosse adicionado ao diretorio de entrada, evento este que seria ouvido por algum 'watchdog' que se resposabilizaria de criar a thread que resolveria a importação. 
**João Cunha [1171857](../)** - UC2-2-3002 Especificar nova Linha de Produção
=======================================


# 1. Requisitos

UC2-2-3002: Como Gestor de Chão de Fábrica, eu pretendo especificar uma nova linha de produção.


A interpretação feita deste requisito foi no sentido de adicionar uma nova linha de produção e guarda-la no sistema. Esta User Stroy depende da existencia de algumas maquinas para poder associar maquinas a linha!
As regras de negócio associadas ao mesmo:

* [Não existe limite de máquinas numa linha de produção](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=29583#p39302)
* [Não existem "lugares" vagos entre maquinas](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=29583#p39328)
* [Existindo uma maquina numa dada posição, se inserir uma nova na mesma posição as máquinas posteriores tem de avançar 1 posição!](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=29583#p39328)

De notar que o ID da linha é unico e é da responsabilidade do utilizador de os introduzir.


# 2. Análise

Esta User Story irá ser dividida em duas task, uma será a criação da linha de produção (com os seus atributos) e a outra será associar maquinas a linha.
A linha de produção estará relacionada com o ID das máquinas, mas como as máquinas já são um *root entity*, a linha também será um *root entity*, que comunica com as máquinas através do ID.
A linha terá um ID unico, alfanumerico, e contem também um Sequencia, esta sequencia irá representar a associação da posição que ocupa, e que maquina se encontra nessa posição.
Para execução desta User Story será necessário uma interação com o utilizador. O utilizador em questão será o Gestor de Chão de Fábrica, que tem de inserir o value object que constroi a linha de produção (ID Linha de Produção). Para a outra task de associar máquinas a linha, terá que selecionar (das maquinas existente no sistema) as que pretende adicionar a linha de produção.

* Pode existir uma linha de produção sem maquinas associadas a sua sequencia!
* Restrição para o ID linha de produção:
    * Não serem vazios
    * Serem uma unica palavra (não conter espaços)
    * Não conter caracteres expeciais (e.g. *´?!_)
* Associação de uma **máquina A** a uma posição que já comtém uma **máquina B**, implica avançar uma posição na **maquina B**. Se a **máquina B** tiver máquinas posteriormente também terão de avançar uma posição.

É necessário fazer alterações ao nosso modelo de dominio, previamente ele foi consebido com a regra de que para uma linha existir teria de existir pelo menos uma máquina, atualmente é possivel existir a linha sem uma máquina. Também tinhamos o value-object Sequencia, que passou a ser um atributo. De um prespetiva de implementação uma simples Linked List consegue responder ao seguinte requisito:
>Existindo uma maquina numa dada posição, se inserir uma nova na mesma posição as máquinas posteriores tem de avançar 1 posição!

## Agregado de Linha de Produção Antigo

![](Agregado_Linha_de_Producao.png)

## Agregado de Linha de Produção com as alterações

![](Agregado_Linha_de_Producao_Atualizado.PNG)


# 3. Design

## 3.1. Realização da Funcionalidade

![](https://bitbucket.org/franciscocpfsilva/lei_isep_2019_20_sem4_2nb_1180640_1161884_1171317_1171857/raw/27ea6b72f70fb1226c6f5a64e90a182298a09a9d/Documentacao/AdicionarProduto/SD_AddProduct.svg)

#### Criar a linha de produção

Gestor de Chão de Fábrica seleciona que pretende especificar uma nova Linha de Produção. O programa solicitará a informação necessária para a especificação da Linha. Após o Gestor de Chão de Fábrica ter inserido a informação o programa tenta criar a nova Linha de Produção guardando-a no sistema.
Caso a informação seja inválida o programa emite uma mensagem de erro e não cria, nem guarda a nova Linha de Produção.

#### Associar máquinas a uma linha de produção

Gestor de Chão de Fábrica seleciona que pretende associar máquinas a uma Linha de Produção. O programa apresenta as Linhas de Produção do sistema. O Gestor de Chão de Fábrica seleciona a Linha de Produção que pretende. O programa apresenta as máquinas disponiveis. O Gestor de Chão de Fábrica seleciona a que pretende adicionar. O programa solicita em que posição prentede inserir. O Gestor de Chão de Fábrica insere a posição que a pretende. O programa associa a máquina à Linha de Produção e questiona se pretende associar mais máquinas. Se o Gestor de Chão de Fábrica pretender adicionar mais, repetem-se os passos anteriores a partir de "O programa apresenta as máquinas disponiveis".
Quando estiverem todas as máquinas pretendidas associadas, pelo Gestor de Chão de Fábrica, o programa guarda a sequencia e informa o sucesso da operação.

## 3.2. Diagrama de Classes

![](diagrama_de_classes.jpg)

#### Criar a linha de produção

![](SD_Linha_Producao.jpg)

A interação com o utilizador será da responsabilidade de classe *AddProductionLineUI* que vai solicitar os dados e guarda-los. De seguida a classe *AddProductionLineController* recebe os dados que a *AddProductionLineUI* guardou e irá solicitar a criação de um novo objecto *ProductionLine*. A classe *ProductionLine* será reponsável por validar se os dados estão corretos e por criar o objecto. De seguida a classe *AddProductionLineController* irá solicitar à interface *ProductionLinesRepository* que persista a informação do novo objecto linha de produção!

#### Associar máquinas a uma linha de produção

![](SD_Associar_Maquinas.jpg)

A interação com o utilizador será da responsabilidade de classe *AssociateMachineProductionLineUI* que vai solicitar os dados e guarda-los, e ainda apresentar listas de Linhas de produção e de Máquinas. 
De seguida a classe *AssociateMachineProductionLineController* irá solicitar Listas de informação a interface *MachineRepository* e/ou interface *ProductionLineRepository*. Irá também comunicar a classe *ProductionLine* para atualizar a sequencia da mesma.

## 3.3. Padrões Aplicados

Para tratar do ciclo de vida de um objecto *ProductionLine* foi só utilizador o padrão *Repositories*, para criar, não achei necessário utilizar o padrão *Factories* dada a baixa complexidade do objecto. 
Foi utilizado um service, para apresentar a lista de *Machines* disponiveis no repositorio. 

## 3.4. Testes 

**Teste 1:** Verificar que não é possível criar o value object *ProductionLineID* a null.

    @Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowed() {
        ProductionLineID instance = new ProductionLineID(null);
    }

**Teste 2:** Verificar que não é possível criar o value object *ProductionLineID* vazia.

    @Test(expected = IllegalArgumentException.class)
    public void ensureEmptyIsNotAllowed() {
        ProductionLineID instance = new ProductionLineID("");
    }

**Teste 3:** Verificar que não é possível criar o value object *ProductionLineID* não exeder os 15 caracteres.
    
    @Test(expected = IllegalArgumentException.class)
    public void productionLineIDDoesNotExceedFifteenInSize() {
        ProductionLineID instance = new ProductionLineID("ASDDSASAD0ASJDA019312");
    }

**Teste 4:** Verificar que não é possível criar o value object *ProductionLineID* é uma palavra unica (sem espaços).

    @Test(expected = IllegalArgumentException.class)
    public void productionLineIDIsNotASingleWord() {
        ProductionLineID instance = new ProductionLineID("A123 0123D ");
    }

**Teste 5:** Verificar que não é possível criar o value object *ProductionLineID* com caracteres especiais.

    @Test(expected = IllegalArgumentException.class)
    public void productionLineIDDoesNotContainSpecialCharacters() {
        ProductionLineID instance = new ProductionLineID("A123^0123D ");
    }

**Teste 4:** Verificar que não é possível criar a entity *ProductionLine* a null.

    @Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowedProductionLineID() {
        ProductionLine instance = new ProductionLine(null);
    }

**Teste 5:** Verificar que não é possível adicionar a lista *sequencia* uma *Machine* a null.

    @Test(expected = IllegalArgumentException.class)
    public void addNullMachineNotAllowed() {
        ProductionLine instance = new ProductionLine(new ProductionLineID("555"));
        instance.addMachineToSequence(0, null);
    }
    
**Teste 6:** Verificar que não é possível adicionar a lista *sequencia* uma posição negativa.

    @Test(expected = NumberFormatException.class)
    public void addNegativePositionNotAllowed() {
        ProductionLine instance = new ProductionLine(new ProductionLineID("555"));
        instance.addMachineToSequence(-12, new Machine(MACHINE_ID, SERIAL_NUMBER, 
                MACHINE_DESCRIPTION, INSTALLATION_DATE, BRAND, MODEL, COMMUNICATION_PROTOCOL_ID));
    }

**Teste 7:** Verificar que não é possível adicionar a lista *sequencia* duas vezes a mesma máquina.

    @Test
    public void addMachineAlreadyExistsInTheListNotAllowed() {
        ProductionLine instance = new ProductionLine(new ProductionLineID("555"));
        assertTrue(instance.addMachineToSequence(1, new Machine(MACHINE_ID, SERIAL_NUMBER, 
                MACHINE_DESCRIPTION, INSTALLATION_DATE, BRAND, MODEL, COMMUNICATION_PROTOCOL_ID)));
        assertFalse(instance.addMachineToSequence(0, new Machine(MACHINE_ID, SERIAL_NUMBER, 
                MACHINE_DESCRIPTION, INSTALLATION_DATE, BRAND, MODEL, COMMUNICATION_PROTOCOL_ID)));
    }

**Teste 8:** Caso de sucesso ao adicionar uma máquina à lista de *sequencia*.

    @Test
    public void addMachineSucessfully() {
        ProductionLine instance = new ProductionLine(new ProductionLineID("555"));
        assertTrue(instance.addMachineToSequence(1, new Machine(MACHINE_ID, SERIAL_NUMBER, 
                MACHINE_DESCRIPTION, INSTALLATION_DATE, BRAND, MODEL, COMMUNICATION_PROTOCOL_ID)));
    }
	
# 4. Implementação

## 4.1 Domain ##
![](domain.svg)

## 4.2 Controller ##
![](AddProductionLineController.svg)

## 4.3 UI ##

### 4.3.1 AddProductionLineUI ###
![](AddProductionLineUI.svg)

### 4.3.1 AssociateMachineProductionLineUI ###
![](AssociateMachineProductionLineUI.svg)

# 5. Integração/Demonstração

Em termos de integração com as funcionalidades já existentes, era relacionar a *ProductionLine* com as *Machine*. Foi criada uma lista de *Machine* dentro da production Line, que tem uma relação *OneToMany* para pressistir essa informação na base de dados. Foi utilizado também um service para apresentar as *Machine* disponiveis no repositorio. 


# 6. Observações

Esta está funcional, mas poderia estar melhor, atualmente ela apresenta todas as *Machine* do sistema para escolha do utilizador. Idialmente seria apresentar só as *Machine* disponiveis. 
Infelizmente não consegui executar o cenario ideal devido a falta de conhecimento de JPA e criação de TypedQuerys.




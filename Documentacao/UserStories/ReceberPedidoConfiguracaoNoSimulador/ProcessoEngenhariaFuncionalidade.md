# US 1014 - Receber ficheiros de configuração no simulador - Francisco Silva (1180640)

=======================================

# 1. Requisitos

_Nesta secção a equipa deve indicar a funcionalidade desenvolvida bem como descrever a sua interpretação sobre a mesma e sua correlação e/ou dependência de/com outros requisitos._

## 1.1. Descrição

Como Gestor de Projeto, eu pretendo que o simulador de máquina suporte a recepção de ficheiros de configuração.

## 1.2. Critérios de aceitação / Observações

- Deve ser concorrente (em C) com o resto da simulação da máquina e o estado deve ser partilhado entre threads.

## 1.3. Requisitos do protocolo de comunicação

O sistema central envia **pedidos CONFIG baseados em TCP** para as máquinas industriais. Caso o ID constante no pedido corresponda ao ID da máquina industrial deve ser retornado uma resposta ACK. Caso contrário, o pedido deve ser recusado e uma resposta NACK é retornada.

## 1.4. Esclarecimentos adicionais

- O simulador NÃO tem que assumir (no sentido de interpretar) as configurações recebidas.
- De acordo com a US 1014, o simulador tem apenas que receber um ficheiro de configuração que lhe seja enviado.
- Para demonstrar que recebeu corretamente esse ficheiro pode, por exemplo, persisti-lo localmente numa pasta.
- O conteúdo desses ficheiro é texto e a seu estrutura interna é irrelevante para o sistema em desenvolvimento.

## 1.5. Regras de negócio

- Resposta ACK deve ser devolvida caso o pedido seja válido (ID recebido = ID máquina simulada)
- Resposta NACK deve ser devolvida caso o pedido seja inválido (ID recebido != ID máquina simulada)

Nada mais a assinalar, dado que os conceitos de domínio necessários já foram criados na [US 1011 - Criar simulador de máquina](../CriarSimuladorMaquina/ProcessoEngenhariaFuncionalidade.md).

# 2. Análise

_Neste secção a equipa deve relatar o estudo/análise/comparação que fez com o intuito de tomar as melhores opções de design para a funcionalidade bem como aplicar diagramas/artefactos de análise adequados._

_Recomenda-se que organize este conteúdo por subsecções._

## 2.1. Sequência de passos

1. Simulador de máquina recebe pedido CONFIG.
2. Simulador verifica que o ID constante no pedido corresponde ao ID da máquina industrial.
3. Simulador cria um ficheiro numa pasta local com a configuração recebida.
4. Simulador devolve resposta ACK.

Ao contrário da US 1011, onde o simulador envia pedidos HELLO e MSG ao SCM, na US 1014 o simulador irá receber pedidos CONFIG do SCM. Deste modo, em vez de ser um cliente TCP, terá de passar a ser um servidor TCP para estar à escuta de pedidos provenientes do exterior.

## 2.2. Modelo de domínio

À semelhança da US 1011, usaremos o conceito Mensagem, assim como o Código de Mensagem (correspondente ao tipo do pedido).

Não existe necessidade de modificar o modelo de domínio atual.

![domain_model_machineSimulator](domain_model_machineSimulator.svg)

# 3. Design

_Nesta secção a equipa deve descrever o design adotado para satisfazer a funcionalidade. Entre outros, a equipa deve apresentar diagrama(s) de realização da funcionalidade, diagrama(s) de classes, identificação de padrões aplicados e quais foram os principais testes especificados para validar a funcionalidade._

_Para além das secções sugeridas, podem ser incluídas outras._

Para esta funcionalidade será necessário criar um servidor de comunicação com o SCM. Devemos aproveitar a Mensagem já existente no domínio, assim como o respetivo DTO para transferência de dados por rede.

## 3.1. Realização da Funcionalidade

_Nesta secção deve apresentar e descrever o fluxo/sequência que permite realizar a funcionalidade._

O servidor deve estar constantemente em execução, sendo que para cada pedido que recebe deve tratar esse pedido (ou delegar essa tarefa para alguém) e deve enviar uma resposta de volta.

![sequenceDiagram_configRequest](sequenceDiagram_configRequest.svg)

## 3.2. Diagrama de Classes

_Nesta secção deve apresentar e descrever as principais classes envolvidas na realização da funcionalidade._

Optou-se por criar uma camada de aplicação específica ao servidor para facilitar a navegação nos ficheiros do simulador.

![classDiagram_configRequest](classDiagram_configRequest.svg)

## 3.3. Padrões Aplicados

_Nesta secção deve apresentar e explicar quais e como foram os padrões de design aplicados e as melhores práticas_

- **Single Responsibility Principle** - Isolar responsabilidades ao máximo. Uma classe tem uma responsabilidade. Exemplos disso são as classes do servidor e de processar pedidos.
- **Tell, don't ask** - Por exemplo, ao processarmos um pedido, caso seja um pedido CONFIG, vamos pedir à classe que trata dos pedidos CONFIG para o processar e fazer o que tem a fazer, nomeadamente guardar essa configuração e criar uma mensagem de resposta. Isto surge associado ao isolamento de responsabilidades do padrão anterior.
- **Service** - Para processar um pedido, a solução adotada foi atribuir essa responsabilidade a um serviço da camada de aplicação, o qual é responsável por tratar a mensagem recebida.
- **DTO** - Para a mensagem. Permite a conversão da mensagem numa estrutura de dados adequada para envio pelos protocolos de redes.

O Interface Segregation Principle poderia ser usado para preparar o sistema para o processamento de pedidos de tipos diferentes (para além do CONFIG). Isto implicaria implementar algo semelhante a uma interface em C, o que é possível mas é complexo. Dada a complexidade, optou-se por não utilizar este padrão, sendo que a utilização de um SWITCH CASE é uma solução mais eficiente, apesar de obrigar no futuro a modificações acrescidas perante um novo tipo de pedido.

Não se preparou o sistema para guardar a configuração em ficheiros de vários tipos, uma vez que essa possibilidade não deve surgir no futuro.

## 3.4. Testes

_Nesta secção deve sistematizar como os testes foram concebidos para permitir uma correta aferição da satisfação dos requisitos._

Dadas as regras de negócio, os testes aplicados serão apenas testes funcionais.

**Teste 1:** Verificar que a resposta ACK é devolvida quando o pedido é válido (ID recebido = ID máquina simulada).

**Teste 2:** Verificar que a resposta NACK é devolvida quando o pedido é inválido (ID recebido != ID máquina simulada).

# 4. Implementação

_Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;_

_Recomenda-se que organize este conteúdo por subsecções._

A **configuração recebida é guardada num ficheiro de texto** dentro do diretório _config_files_, que por sua vez está dentro do diretório do simulador. O nome do ficheiro do texto segue a seguinte estrutura: _config\_{IDMAQUINA}.txt_

A implementação está em conformidade com o design, exceto num ponto. No diagrama de sequência, a responsabilidade de criar a estrutura _Message_ com o pedido recebido é atribuída ao ficheiro _TcpServer_. No entanto, durante a implementação, pareceu fazer mais sentido atribuir essa responsabilidade ao ficheiro _ProcessRequestService_. Deste modo, o _TcpServer_ apenas se preocupa com os pedidos e o seu conteúdo sob o formato de um DTO.

# 5. Integração/Demonstração

_Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema._

## 5.1. Integração

Nesta US, o simulador deve comunicar com o SCM. Para isso, foi necessário adotar uma estrutura comum da mensagem a enviar/receber, de modo a que ambas as aplicações saibam exatamente o formato e o tamanho dos dados. Esta estrutura estava já definida no enunciado "Protocolo de comunicação".

As decisões de design foram montadas por cima da US 1011 - Criar simulador de máquina, que tinha sido responsável pela criação da estrutura inicial da aplicação.

# 6. Observações

_Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados._

Teria sido interessante preparar melhor o sistema para receber pedidos de tipos diferentes no futuro, nomeadamente utilizando o Interface Segregation Principle.

Na US 1011 (criar o simulador), foram usados DTOs para a transmissão de dados. Isto funciona bem no sentido Mensagem -> DTO -> Envio dos dados. No sentido oposto, já não funciona tão bem, sendo que uma possibilidade teria sido o uso de um Serviço. Isto está a ser discutido nesta US, uma vez que se tornou masi notório ao termos de repetir os passos todos de extração de dados (extractVersion, extractCode...) no diagrama de sequência, quando isto poderia ser feito com o recurso apenas à chamada de um método no serviço.

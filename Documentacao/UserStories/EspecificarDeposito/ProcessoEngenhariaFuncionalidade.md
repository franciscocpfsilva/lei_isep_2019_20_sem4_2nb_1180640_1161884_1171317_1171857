# **Aluno [1180640](../)** - US3003 - Especificar um Depósito

# 1. Requisitos

_Nesta secção o estudante deve indicar a funcionalidade desenvolvida bem como descrever a sua interpretação sobre a mesma e sua correlação e/ou dependência de/com outros requisitos._

**US3003** Como Gestor de Chão de Fábrica, eu pretendo especificar a existência de um novo depósito.

Este requisito é direto ao assunto. Não parece haver muita margem para dúvidas de interpretação. O objetivo passa por criar um novo depósito e guardá-lo no sistema. Esta User Story (US) não depende das outras US do Sprint B, à exceção da que envolve a configuração do projeto.

De acordo com o caderno de encargos e com as respostas dadas no Fórum para Esclarecimento de Requisitos (perspetiva de cliente do sistema), contido na página de LAPR4, foram identificadas estas **regras de negócio**:

- Um depósito de matérias-primas e produtos caracteriza-se por um código alfanumérico e uma descrição;
- Código de um depósito deve ser único;
- [Código de um depósito é alfanumérico e não excede os 10 caracteres;](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=29293)
- [Descrição de um depósito não excede os 40 a 50 caracteres.](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=29293)

# 2. Análise

_Neste secção o estudante deve relatar o estudo/análise/comparação que fez com o intuito de tomar as melhores opções de design para a funcionalidade bem como aplicar diagramas/artefactos de análise adequados._

_Recomenda-se que organize este conteúdo por subsecções._

**O Modelo de Domínio (MD) existente à data deste texto permite dar resposta a este caso de uso**. Existe já uma _entity_ denominada "Depósito", com dois atributos: um identificador (código do depósito) e uma descrição. Esta _entity_ é a raiz do seu próprio agregado, pelo que é possível guardarmos o novo depósito criado diretamente através do repositório do agregado.

Atributos de Depósito:

- identificador -> _value-object_
- descrição -> _value-object_ (apesar de representado no MD como atributo simples; ver [aspetos base do projeto](../aspetos_base_do_projeto.md))

**Este caso de uso é muito semelhante ao caso de uso de criação de tipos de prato, existente no projeto exemplo ecafeteria**. Sendo assim, as decisões de Design foram baseadas nesse caso de uso. Foi também tido em conta o caso de uso de criação de pratos como exemplo de criação de uma entidade com atributos do tipo _value-object_.

A interação entre o ator (Gestor de Produção) e o sistema é muito simples, não merecendo por isso um diagrama explicador:

1. Gestor de Produção inicia caso de uso.
2. Consola pede os dados do depósito a criar.
3. Gestor de Produção introduz o código e a descrição do depósito.
4. Sistema cria o novo depósito (desde que válido) e mostra-o ao Gestor de Produção.

# 3. Design

_Nesta secção o estudante deve descrever o design adotado para satisfazer a funcionalidade. Entre outros, o estudante deve apresentar diagrama(s) de realização da funcionalidade, diagrama(s) de classes, identificação de padrões aplicados e quais foram os principais testes especificados para validar a funcionalidade._

_Para além das secções sugeridas, podem ser incluídas outras._

As **classes de domínio** desta funcionalidade estarão implementadas no package _eapli.base.storage.domain_ e consistirão em:

- StorageUnit
- StorageUnitID

A descrição do depósito será um _value-object_ mas fará uso da classe _Description_ da framework de EAPLI.

## 3.1. Realização da Funcionalidade

_Nesta secção deve apresentar e descrever o fluxo/sequência que permite realizar a funcionalidade._

![diagramaSequencia_USEspecificarDeposito](diagramaSequencia_USEspecificarDeposito.png)

## 3.2. Diagrama de Classes

_Nesta secção deve apresentar e descrever as principais classes envolvidas na realização da funcionalidade._

![diagramaClasses_USEspecificarDeposito](diagramaClasses_USEspecificarDeposito.png)

A dependência relativamente à classe Description da framework EAPLI não foi representada.

## 3.3. Padrões Aplicados

_Nesta secção deve apresentar e explicar quais e como foram os padrões de design aplicados e as melhores práticas_

- **Repository:** para a camada de persistência

## 3.4. Testes

_Nesta secção deve sistematizar como os testes foram concebidos para permitir uma correta aferição da satisfação dos requisitos._

**Teste 1:** Verificar que não é possível criar uma instância da classe StorageUnitID com valores nulos.

**Teste 2:** Verificar que não é possível que o código da classe StorageUnitID seja vazio.

**Teste 3:** Verificar que não é possível que o código da classe StorageUnitID tenha mais de 10 caracteres.

**Teste 4:** Verificar que é possível que o código da classe StorageUnitID tenha números.

**Teste 5:** Verificar que é possível que o código da classe StorageUnitID tenha letras maiúsculas.

**Teste 6:** Verificar que é possível que o código da classe StorageUnitID tenha letras minúsculas.

**Teste 7:** Verificar que não é possível que o código da classe StorageUnitID tenha caracteres estranhos (ex: '-', '/', '€').

**Teste 8:** Verificar que não é possível criar uma instância da classe StorageUnit com valores nulos.

**Teste 9:** Verificar que não é possível que a descrição da classe StorageUnit tenha mais de 40 caracteres.

Não é necessário verificar que a descrição do depósito não pode ser nula ou vazia. Esta verificação é feita já na framework EAPLI na classe de testes DescriptionTest.

# 4. Implementação

_Nesta secção o estudante deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;_

_Recomenda-se que organize este conteúdo por subsecções._

O tratamento das exceções foi feito na UI (o mais a montante possível). No caso de tentarmos guardar um depósito com um código já existente, estaríamos à espera que fosse lançada uma IntegrityViolationException (de acordo com a documentação da framework EAPLI para a interface DomainRepository). No entanto, isto não acontece, pelo que tivemos que fazer um tratamento extra para uma RollbackException.

# 5. Integração/Demonstração

_Nesta secção o estudante deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema._

Esta US não tem dependências sobre outras US do Sprint B.
Relativamente à UI, foi inserida num submenu relativo a funcionalidades de armazenamento. Neste momento, tem permissões para admin e poweruser, embora seja expectável que isto seja alterado.

# 6. Observações

_Nesta secção sugere-se que o estudante apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados._

O processo de engenharia pode ser aprimorado, nomeadamente a parte de Design. Sendo esta a minha primeira US implementada neste projeto, há alguns erros a corrigir. O diagrama de funcionalidades não precisa de ser tão extenso e pode haver mais explicação das decisões tomadas (e dos prós e contras das alternativas existentes). Como esta US é muito semelhante a algumas já existentes no projeto ecafeteria, é mais propícia a que nos sigamos pelo que já está implementado.

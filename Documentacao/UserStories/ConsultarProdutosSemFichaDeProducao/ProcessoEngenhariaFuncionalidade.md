**Aluno [1171317](../)** - US2003 - Consultar Produtos sem Ficha de Produção
=======================================

# 1. Requisitos

*Nesta secção o estudante deve indicar a funcionalidade desenvolvida bem como descrever a sua interpretação sobre a mesma e sua correlação e/ou dependência de/com outros requisitos.*

**US2003** Como Gestor de Produção, eu quero consultar os produtos que não têm Ficha de Produção definida.

Esta é primeira US de listagem simples. Permite listar todos os produtos sem ficha de produção. A responsabilidade da pesquisa foi atribuida ao repositório de Produtos, que é quem na verdade conhece os produtos. 

**Dependências**
Esta User Story depende diretamente da funcionalidade de _Adição de Produtos_ (US2006) e do bootstrap do mesmo contexto (US1006). Isto é, precisamos do conceito de negócio, _Produto_, definido e instanciado no nosso sistema, para que a listagem de produtos se realize. 

# 2. Análise

*Neste secção o estudante deve relatar o estudo/análise/comparação que fez com o intuito de tomar as melhores opções de design para a funcionalidade bem como aplicar diagramas/artefactos de análise adequados.*

*Recomenda-se que organize este conteúdo por subsecções.*

Discutiu-se o recipiente da reponsabilidade de implementar desta funcionalidade (listagem de produtos sem ficha), e neste sentido, especulou-se se a mesma deveria reacair diretamente sobre o controlador da UC ou seria realizado sob a forma de um serviço. 

Talvez seja exagerado ou até precipitado o uso de um serviço para resolver esta US, uma vez que não corresponde a uma funcionalidade que se veja a ser usada por várias outras UCs, no entanto, e imaginando pelo menos alguma utilidade no contexto da UC responsavel pela _Especificação de Ficha de Produção de Produto_, optou-se pelo serviço. Desta forma habilitamos a listagem a ser usada por qualquer controlador do sistema no futuro.

**NOTA:** No entanto, fica aqui registado, que caso se verifique que o serviço apenas é utilizado por esta US, incorriremos na devida correção, isto é, implementar a funcionalidade apenas no controlador da US, apagando o serviço. Foi também criada a classe _ProductPrinter_ que implementa a interface **Visitor**, desta forma termos um formato generico de apresentação de um produto sem modificarmos a classe _Product_.

# 3. Design

*Nesta secção o estudante deve descrever o design adotado para satisfazer a funcionalidade. Entre outros, o estudante deve apresentar diagrama(s) de realização da funcionalidade, diagrama(s) de classes, identificação de padrões aplicados e quais foram os principais testes especificados para validar a funcionalidade.*

*Para além das secções sugeridas, podem ser incluídas outras.*

## 3.1. Realização da Funcionalidade

*Nesta secção deve apresentar e descrever o fluxo/sequência que permite realizar a funcionalidade.*

O diagrama de sequencia que se segue procura emular, da perspectiva da listagem de objetos do repositorio através de da classe serviço, a funcionalidade de _List Dish Type_ do projeto _eCafetaria_.

![SD_US2003_ListProductsWithNoProductionSheet](SD_US2003_ListProductsWithNoProductionSheet.png)

## 3.2. Diagrama de Classes

*Nesta secção deve apresentar e descrever as principais classes envolvidas na realização da funcionalidade.*

![CD_US2003_ListProductsWithNoProductionSheet](CD_US2003_ListProductsWithNoProductionSheet.png)

## 3.3. Padrões Aplicados
*Nesta secção deve apresentar e explicar quais e como foram os padrões de design aplicados e as melhores práticas*

**Repository:** Aplicado à camada de persistencia, permite esconder os detalhes da persistencia da camada de dominio. Neste contexto, o repositório de Produtos é o responsável por conhecer a lista de Produtos, e no mesmo sentido, deverá ser responsável por listagens mais genéricas. 

**Visitor (GoF)** Pseudo-aplicação na camada de Domínio, nomeadamente sob a forma de um formato genérico de representação textural do objecto _Produto_. A classe que implementa a interface _Visitor_ oferece meramente este formato de apresentação sem modificar a classe _Produto_. No entanto, a classe 'visitada' não implementa (para já não se justifica) o metodo _accept(Visitor v)_ como ditaria o padrão, uma vez que não se perspetivam novas funcionalidades a acrescentar ao conceito _Produto_.

## 3.4. Testes 
*Nesta secção deve sistematizar como os testes foram concebidos para permitir uma correta aferição da satisfação dos requisitos.*

Testes funcionais: 

**Teste 1:** Verificar que consegue listar Produtos sem ficha de produção.

# 4. Implementação

*Nesta secção o estudante deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;*

*Recomenda-se que organize este conteúdo por subsecções.*

À semelhança de outras UC da mesma natureza, foram implementadas as classes tipicas necessárias à realização deste tipo de funcionalidade nas camadas apropriadas, apresentação, aplicação e persistência. Neste caso a camada de domínio não implica qualquer tipo de alteração ou introdução de novos conceitos.  

# 5. Integração/Demonstração

*Nesta secção o estudante deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

Neste secção destaca-se a associação do atributo _Production Sheet_ ao conceito _Produto_, sendo este necessário para a realização a listagem que este documento descreve. Este novo atributo, inicializado aqui a NULL, ficará a aguardar pela funcionalidade responsavel pela sua inicialização. 

Destaca-se também a implementação do método de pesquisa na classe concretas de repositório na tecnologia InMemory, implicou a criação do método _hasProductionSheet()_, na classe _Produto_, que devolve um booleano que verifica se a instância de produto têm uma ficha de produção.

# 6. Observações

*Nesta secção sugere-se que o estudante apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*

US de listagem simples, que procura servir de exemplo de implementação para futuras listagens.

**João Machado [1161884](../)** - UC3-3-6002 SMM enviar pedido de reinicialização para uma dada máquina

=======================================

# 1. Requisitos

- UC3-3-6002: Como SMM, eu pretendo enviar um pedido de reinicialização para uma dada máquina.

A interpretação desta US foi simples, sendo que é adicionada a possibilidade de ser enviado um pedido RESET para uma máquina.

Esta funcionalidade extra do Sistema de Monitorização de Máquinas (SMM) não deve afetar nem interromper os pedidos Hello que são iniciados automaticamente com o arranque da SMM.

 
### 1.1 Regras de Negócio

- O pedido RESET deve ser enviado a máquinas conhecidas do SMM.
- Deve ser realizado sem interromper do pedido HELLO.
- Deve atualizar o estado da máquina com a resposta ao pedido RESET.

# 2. Análise

Esta US não está contemplada no Modelo de Domínio desenvolvido no Sprint A, como tal, o processo de levantamento de requisitos tem de ser repetido.

Existe uma dependência direta com a US 1012 e 6002 (permitir que o simulador de máquina suporte pedidos de monitorização do seu estado) o que irá obrigar a que muitas decisões sejam tomadas em conjunto com o responsável dessa US.

## 2.1 Estudo

Um dos requisitos impostos por RCOMP é que o pedido RESET não interrompa o pedido HELLO que esteja a decorrer. A fim de respeitar este pormenor será necessário implementar uma thread em java que ira executar o pedido HELLO anteriormente implementado. 

Também será necessário desenvolver um menu para que seja possível mostrar as máquinas disponíveis no sistema e para enviar um pedido RESET.

Assim, sempre que a aplicação iniciar, automaticamente será criada e executada uma thread com o pedido HELLO e a aplicação irá mostrar um menu. Neste menu será possível escolher duas opções: Visualizar as Máquinas descobertas pelo SMM ou enviar um pedido RESET.

Para enviar o pedido reset será pedido o IP da máquina para qual se pretende enviar o pedido RESET. Depois de ser introduzido o IP, a aplicação irá criar uma mensagem com código correto e enviar um pacote UDP para o respetivo IP.

# 3. Design

## 3.1. Realização da Funcionalidade

No diagrama seguinte apresento o fluxo lógico desta US.

![SD_SMM.svg](SD_SMM.svg) 

## 3.2. Diagrama de Classes

Para o diagrama de classes optei por incluir todos os packages do projeto com o intuito de centralizar o observador para esta US e representar as diversas partes que são utlizadas.

![CD_SMM.svg](CD_SMM.svg) 

## 3.4. Testes 

### Testes unitários

**Teste 1:** Verificar que é possível enviar um pedido RESET.

**Teste 3:** Verificar que a resposta ao pedido RESET atualiza a máquina correta.

# 4. Implementação

Nesta secção apresento excerto do código desenvolvido, representativo do culminar do processo de planeamento desenvolvido para esta UC.
Inspirado nas US previamente desenvolvidas na aplicação ecafetaria, as classes implementadas foram desenvolvidas aplicando os conceitos lecionados em EAPLI, mantendo sempre em mente as boas práticas e os diversos padrões de POO.

## 4.1 Domain

Neste package é possível encontrar duas classes, Machine e Message. A classe Machine é responsável por criar instâncias com a informação recebida referente à máquina, nomeadamente o ID do protocolo de comunicação, a *raw data* recebida na mensagem, o IP da máquina, o seu estado e a hora que recebeu o último pacote. Este último campo é necessário para calcular se a máquina está num estado disponível ou indisponível. Isto é conseguido através da diferença entre essa hora guardada e a hora atual, sendo que se for maior do que o valor definido no arranque da aplicação o seu estado irá ser colocado a *Indisponível*.

Na classe Message existe um construtor para criar mensagens com os campos necessários. Também aqui existem dois métodos Serialized e Deserialized, responsáveis por criar um byte[] com os respetivos off-set para cada campo e por receber um byte[] e transformar em dados legíveis respetivamente. O Deserialized retorna uma instância de Machine criada com a informação que recebeu.

O último método nesta classe é o *prepareRequest*, capaz de receber o tipo de request por parâmetro e devolver uma mensagem serializada, pronta para enviar por UDP.

## 4.2 Application

É no package Application onde encontramos o fluxo principal desta aplicação. Irei agora fazer um resumo do que foi implementado.

### 4.1.2 SendRequest

Nesta classe foi adicionado o método *sendResetRequest*, em que inicialmente é pedido o IP da máquina ao qual se pretende enviar um pedido RESET. Seguidamente é utilizado o método acima referido, *prepareRequest*, para criar uma mensagem com código de RESET. É criado um socket e definido um *Time Out* concluído o processo com a chamada da classe responsável por enviar e receber os respetivos sockets.

Esta classe adiciona o IP e array de bytes ao socket a enviar, aguarda por uma resposta e se esta chegar, chama outro método capaz de processar a informação recebida e de atualizar a instância da máquina correta.

![reset.svg](reset.svg) 

# 5. Integração/Demonstração

A integração desta US foi bastante simples, pois aplicação já estava completamente funcional do sprint anterior e foi apenas necessário acrescentar algumas funcionalidades.

# 6. Observações
 
Face ao trabalho desenvolvido neste UC, sinto que cumpri perante os objetivos principais propostos e apresento uma solução válida. Existe muito espaço para melhoria, nomeadamente na organização das classes e das suas responsabilidades. Na aplicação dos conceitos e padrões de OOP também sinto que falhei em múltiplos aspetos. 




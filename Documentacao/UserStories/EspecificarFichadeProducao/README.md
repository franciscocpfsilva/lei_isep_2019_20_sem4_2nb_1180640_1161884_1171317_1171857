**João Machado [1161884](../)** - UC3-1-2004 Especificar Ficha de Produção
=======================================

# 1. Requisitos

UC3-1-2004: Como Gestor de Produção, eu pretendo especificar uma nova ficha de produção de um produto.

A interpretação feita deste requisito foi no sentido de um Gestor de Produção querer escolher um produto e especificar uma nova ficha de produção ao mesmo. O sistema deve ser capaz de fazer as validações necessárias aos dados introduzidos pelo Gestor, fazer a atualização do produto selecionado e informar se a operação foi efetuada com sucesso ou não.

### 1.1 Regras de Negócio
- O Produto ao qual se pretende associar uma ficha de Produção deve existir no sistema.
- Se o produto já tiver uma Ficha de Produção associada deve ser feito esse aviso-
- A Ficha de Produção deve uma quantidade standard e pelo menos um Item com a sua respetiva quantidade.
- A quantidade standard da Ficha de Produção e do Item devem de ser números positivos.


# 2. Análise

Inicialmente fiz o enquadramento desta UC com o modelo de domínio desenvolvido e com as dependências do mesmo já analisadas numa fase anterior. Dividi esta fase de análise em vários tópicos onde irei apresentar uma breve descrição para cada um deles.

## 2.1 Estudo

Numa Ficha de Produção deve ser possível mencionar Matérias-Primas bem como Produtos. No nosso Modelo de Domínio, para dar resposta a este critério foi criado o value-object Item que será visto em mais detalhe seguidamente.

Como uma Ficha de Produção tem de estar associada a um produto já existente, surgiu a questão de como pedir e apresentar os dados ao Gestor para ele indicar o produto que pretende escolher. Por razões de simplicidade apenas é pedido o ID do produto ao gestor e é verificado através de uma query á BD se esse produto existe.

Ao especificar a Ficha de Produção, também deve ser possível associar vários Item e como tal, este atributo está implementado como uma Lista. Durante a execução da US existe um Loop que permite então associar vários Item à mesma Ficha.



*Atual*
![MD_actual.png](MD_actual.png)

## 2.2 Ficha de Produção

A classe Ficha de Produção foi implementada dentro do Domínio de Produtos pois representa um value-object da Entity Root Produto. Este value-object consiste numa uma quantidade standard e uma lista de Item.
A Ficha de Produção também possui algumas validações, garantido que os atributos não são *null* e que a quantidade seja um número positivo.

*Versão Inicial*
A classe inicialmente é criada com nome e standard quantity e só posteriormente é possível assocar Item ao objeto.

*Versão actual*
Como em cima referido, inicialmente o object Ficha de Produção era criado por partes, primeiro o nome e uma quantidade e depois iterativamente eram adicionados Items á sua lista. Com esta implementação surgiu um erro de transação que, eu não tinha previsto quando desenvolvi o planeamento desta US. Depois de consultar com o Professor da PL de EAPLI foi sugerido que o objeto fosse criado completamente com apenas uma chamada. Para isto, primeiramente crio a lista de Item num processo iterativo e quando o Gestor estiver satisfeito, termina a fase de especificar Item e o objeto é criado. Com esta implementação foi possível ultrapassar o erro de transação.

## 2.3 Item

Esta classe visa responder a possibilidade de poder associar Matérias-Primas ou Produtos á Ficha de Produção.

Para suportar esta funcionalidade, esta classe contém três atributos. O RawMaterialID, ProductID e quantity. Como um Item só pode ser um dos dois ID (RawMaterialID e ProdcutID), foram implementados dois construtores em que um recebe um RawMaterialID e uma quantity e outro que recebe um ProdcutID e uma quantity.

## 2.4 UI

No UI é perguntado inicialmente o ID do Produto e após a validação do mesmo é necessário escolher se pretendemos adicionar um Produto ou Matéria-Prima à ficha de produção. Esta foi a maneira mais eficiente que encontrei para saber qual o construtor a chamar do Item. Esta escolha é feita dentro de um ciclo para poder serem associados vários Item á lista da Ficha de Produção. Assim que foram especificados todos os Item pretendidos, a Ficha de Produção é associado ao Produto e por sua vez este é atualizado na Base de Dados.

Devido às restrições de tempo escolhi apenas permitir que os Produtos ou Matérias-primas sejam procurados diretamente pelo seu ID. Em prática significa que o Gestor tem de saber o ID do produto que pretende associar uma ficha de produção e os ID's de ambos Produtos e Matérias-primas que constituem a Ficha de Produção.

# 3. Design

## 3.1. Realização da Funcionalidade

No diagrama seguinte apresento o fluxo lógico desta US.

![SD_productionSheet.png](SD_productionSheet.png) 

## 3.2. Diagrama de Classes

Para o diagrama de classes optei por incluir todos os packages do projeto com o intuito de centralizar o observador para esta US e representar as diversas partes que são utlizadas.

![CD_productionSheet.png](CD_productionSheet.png) 

## 3.3. Padrões Aplicados

### Padrão Repository
Este padrão foi utlizado para aceder á informação existente sobre Produtos e Matéria-Prima assim como fazer a persistência do Produto atualizado. É especialmente útil pois permite abstrair da implementação técnica da camada responsável por criar a persistência.

### Padrão Factory
Este padrão foi utilizado no caso específico da criação de uma Ficha de Produção. 
Neste caso específico, o sistema disponibiliza a possibilidade de instanciar um objeto do tipo Produto através de uma Factory de ProdutosFactory. A aplicação deste tipo de padrão, entre outras, apresenta a vantagem de permitir encapsular as regras do domínio. 

## 3.4. Testes 

### Testes unitários

Devido á simplicidade do código desta classe irei apenas demostrar um exemplo prático dos testes implementados e os restantes apenas terão uma breve descrição.

**Teste 1:** Verificar que não é possível criar uma instância da classe ProductionSheet com valores nulos.

    @Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowedProductionSheetName() {
        ProductionSheet instance = new ProductionSheet(null, STANDARD_QUANTITY,listOfItems);
    }

**Teste 2:** Verificar que é possível criar uma instância da classe ProductionSheet com os atributos corretos.

**Teste 3:** Verificar que não é possível criar uma instância da classe ProductionSheet com quantity negativa.

**Teste 4:** Verificar que inicialmente a lista de Items está vazia e que é possível adicionar novos objetos.

# 4. Implementação

Nesta secção apresento excerto do código desenvolvido, representativo do culminar do processo de planeamento desenvolvido para esta UC.
Inspirado nas US previamente desenvolvidas na aplicação ecafetaria, as classes implementadas foram desenvolvidas aplicando os conceitos lecionados em EAPLI, mantendo sempre em mente as boas práticas e os diversos padrões de POO.

## 4.1 Domain

Relativamente ao Domain esta US requer a criação do value-Object em cima referido Item. Para a construção correta do Objeto, ora com um ProdutoId ou RawMaterialId, estão implementados dois valueOf. Estes valueOf são invocados num *case* que determina se é um RawMaterial ou Product a serem adicionados.

![Domain.png](Domain.png)
![Domain2.png](Domain2.png)

## 4.1 Controller

Penso que seja importante referir que na primeira implementação, optei por criar diversos controllers neste US. O objetivo era ter um controller para cada serviço que iria precisar, permitindo assim que estes serviços pudessem ser aproveitados para outras US mas que pudessem ser chamados apenas pelo o respetivo controller. Esta escolha teve consequências evidentes, tornado a classe de UI numa espécie de controller "Hub", adicionado várias dependências extra e tornando o código menos claro. 
Depois de ser debatido entre o grupo e com o input do professor de EAPLI decidi seguir abordagem de apenas um controller que por sua vez iria fazer uso dos serviços diretamente. Diminuindo as dependências no UI e tornar o código mais legível.

Para encontrar o produto ao qual queremos adicionar uma Ficha de Produção, como mencionado anteriormente, existe um método validateProductByID que utiliza o método findProductByID para encontrar um Produto com um determinado ID retorna uma instância do Produto.

Quando pretendemos adicionar Produtos ou Matérias-Primas á lista de Item usamos os respetivos findAndAddByID, que tentam criar uma instância do objeto através do ID e se forem bem-sucedidos adicionam á lista.

Finalmente o método addProductionSheet cria uma instância de ProductionSheet, adiciona ao Produto desejado e persiste esse update.

![Controller.png](Controller.png)

## 4.1 UI

Devido a escolhas feitas no sprint A sobre como representar a possibilidade de ter Produtos e Matérias-Primas na nossa Aplicação, é necessário que o UI tenha alguma lógica de negócio associada. Nomeadamente a possibilidade de escolher se o gestor pretende adicionar uma Matéria-Prima ou um Produto. Esta funcionalidade está inserida dentro de um ciclo que pode ser repetido ou terminado quando o Gestor pretender.

![UI.png](UI.png)

# 5. Integração/Demonstração

O desenvolvimento desta US foi desafiante desde o início, muitas decisões tiveram de ser tomadas sem grande experiência deste tipo de implementação. Foi necessário durante o processo de estudo e análise consultar a aplicação ecafetaria e analisar como eram desenvolvidos os métodos de findById. A possibilidade de apresentar uma lista com os produtos e matérias-primas disponíveis neste US também foi considerada, igualmente a procura por descrições, mas devido á enorme restrição de tempo optei sempre pela implementação mais simplista, a procura por ID's.

# 6. Observações

Face ao trabalho desenvolvido neste UC, sinto que cumpri perante os objetivos propostos e apresento uma solução válida. Existe espaço para melhoria, nomeadamente nas alternativas sobre a escolha de Produtos ou Matérias-Primas.



**João Machado [1161884](../)** - UC2-1-1005 Bootstrap de Categoria de Matéria-Prima
=======================================


# 1. Requisitos

Como Gestor de Projeto, eu pretendo que a equipa proceda à inicialização (bootstrap) de algumas Categorias de Matérias-Primas.

A interpretação feita deste requisito foi no sentido de adicionar algumas Categorias de Matérias-Primas ao sistema para podermos demonstrar outras User Stories que dependem de destas.



# 2. Análise

Foi determinado pelo grupo que estas UC seriam utilizadas para efetuar Code Review aos UC á qual dependem. Neste caso fiz review ao UC Definir Categoria de Matérias-Primas.

Depois de ter analisado cuidadosamente como foi implementado o UC, comuniquei com o meu colega de grupo responsável as melhorias que poderia fazer.


## 2.1 Estudo

Uma Categoria de Matéria-Prima é composta por um ID e uma descrição.
Utilizando como exemplo os bootsrappers já implementados na framework, apenas limitei a replicar essa implementação modificando os parâmetros e o construtor a ser chamado.


# 3. Design

Como este UC é genérico a todos os elementos do grupo e não apresentar nenhuma curva de dificuldade tanto de desenvolvimento, planeamento ou implementação decidi não elaborar diagramas.

Apresento uma breve descrição do UC:

- No package eapli.base.infrastructure.bootstrapers é criada uma classe RawMaterialCategoryBootstrapper que instancia o Controller AddRawMaterialCategoryController que por sua vez chama o metodo addAddRawMaterialCategory. Este método faz a validação da role do user e instancia uma nova Categoria de Matérias-Primas.

- No mesmo package, na classe BaseBootstrapper é adicionado ao Actions[] action uma nova chamada para a classe desenvolvida para esta UC de bootstrapper;




# 5. Integração/Demonstração

Nesta UC, por não possuir dependências com implementações já presentes aplicação, não existiu grande dificuldade para a integração da mesma.


# 6. Observações

Face ao trabalho desenvolvido neste UC, sinto que cumpri perante os objetivos propostos e apresento uma solução válida.
w



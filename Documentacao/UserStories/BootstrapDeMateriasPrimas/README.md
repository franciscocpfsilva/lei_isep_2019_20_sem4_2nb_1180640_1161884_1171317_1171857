**João Machado [1161884](../)** - UC1-1-1004  Bootstrap de Matéria-Prima
=======================================


# 1. Requisitos

Como Gestor de Projeto, eu pretendo que a equipa proceda à inicialização (bootstrap) de algumas Matérias-Primas.

A interpretação feita deste requisito foi no sentido de adicionar algumas   Matérias-Primas ao sistema para podermos demonstrar que tanto o UC de adicionar categorias de Matérias-Primas como o UC de adicionar Matérias-Primas estão corretamente implementados.




# 2. Análise

Foi determinado pelo grupo que estas UC seriam utilizadas para efetuar Code Review aos UC á qual dependem. Apesar de ter sido eu quem também desenvolveu o UC Adicionar Matéria-Prima, o qual este depende, ele foi revisto por outro elemento do grupo, mas por falta de disponibilidade dos restantes elementos também implementei esta funcionalidade.

Após ter implementado as correções necessárias para o UC ao qual este depende, apenas foram necessárias nos testes, analisei como este tipo de UC foram implementados na framework.

Neste caso particular existe outra dependência com as Categorias de Matéria-prima, pois uma Matéria-Prima tem de ter uma Categoria associada. 

Isto implica ter de correr primeiramente o Bootstrap de Categorias de Matérias-Primas, seguidamente instanciar o Repositorio de Categorias e instanciar objetos do tipo Categoria de Matéria-Prima através de uma correspondência com os ID's.



## 2.1 Estudo

Uma Matéria-Prima é composta por uma Categoria, um ID, uma descrição, um tipo de unidade de medida e uma ficha técnica.


# 3. Design

Como este UC é genérico a todos os elementos do grupo e não apresentar nenhuma curva de dificuldade tanto de desenvolvimento, planeamento ou implementação decidi não elaborar diagramas.

Apresento uma breve descrição do UC:

- No package eapli.base.infrastructure.bootstrapers é criada uma classe RawMaterialBootstrapper. Esta classe conforme já referido em cima começa por criar instâncias de Categorias através da correspondência com ID's. Nesta fase também é criado um byte[] representativo de um ficheiro pdf com apenas o valor 1 associado.
-Seguidamente é chamado o método addRawMaterial, este responsável por chamar o Controller da nossa Entidade, com todos os parâmetros enumerados anteriormente necessários para a construção das Matérias-Primas

- No mesmo package, na classe BaseBootstrapper é adicionado ao Actions[] action uma nova chamada para a classe desenvolvida para esta UC de bootstrapper;




# 5. Integração/Demonstração

Nesta UC é necessário iniciar o Bootstrap de Categorias de Matérias-Primas, que na altura de implementação deste UC já estava desenvolvido e, portanto, não apresentou dificuldades.


# 6. Observações

Face ao trabalho desenvolvido neste UC, sinto que cumpri perante os objetivos propostos e apresento uma solução válida.

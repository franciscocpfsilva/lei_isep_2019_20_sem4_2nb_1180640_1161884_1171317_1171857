**João Machado [1161884](../)** - UC1-2-3001
=======================================


# 1. Requisitos

UC1-2-3001: Como Gestor de Chão de Fábrica, eu pretendo definir a existência de uma nova máquina.


A interpretação feita deste requisito foi no sentido de um Gestor de Chão de Fábrica querer adicionar uma nova máquina ao sistema. O sistema deve ser capaz de fazer a validação necessária dos dados introduzidos pelo Gestor e de informar se a operação foi efetuada com sucesso ou não.

### 1.1 Regras de Negócio
- ID de Máquina, Número de Série, Descrição,Data de Instalação, Marca, Modelo e ID de Protocolo de Comunicação são os atributos necessários nesta fase;
- ID de Máquina e Número de Série devem ser únicos sendo que o primeiro tem no máximo 10 caracteres o segundo 20;
- Descrição, Marca e Modelo apenas são limitados pelo seu tamanho máximo não exceder os 40 caracteres e não estarem vazios.
- ID de Protocolo de Comunicação é um número inteiro positivo entre 1 e 65535.
- Data de Instalação não deve de ser no futuro.



# 2. Análise

Inicialmente fiz o enquadramento deste UC com o modelo de domínio desenvolvido e com as dependências do mesmo já analisadas numa fase anterior. Dividi esta fase de análise em vários tópicos onde irei apresentar uma breve descrição para cada um deles.

## 2.1 Estudo

Para esta funcionalidade inicialmente a Máquina estaria dentro de uma Linha de Produção e iria mencionar a próxima na linha. Depois de melhor análise pelo grupo foi desenvolvida uma solução mais simples e que melhor representa o negócio. Uma Máquina passou a ter o seu próprio agregado, deixando de depender da Linha de Produção. Esta última é que iria, ao ser criada, especificar as máquinas que lhe pertenciam e a respetiva sequência.

*Antigo*
![MD_antigo.png](MD_antigo.png) 

*Actual*
![MD_actual.png](MD_actual.png)

Outra particularidade da Máquina é a possibilidade de associar ficheiros de configuração. Esta associação suscitou várias dúvidas entre elas, se seria feito no momento da especificação da máquina ou, posteriormente numa máquina já existente no nosso sistema. Outra duvida é referente á estrutura destes ficheiros e seria feita esta associação (por exemplo: se já existiam no sistema ou seriam importados).
Para responder a estas questões, o cliente foi questionado para fornecer mais detalhes e confirmei que para esta UC não seria implementada esta funcionalidade de associar ficheiros de configuração.


## 2.2 Máquina

A fim de satisfazer este UC, uma nova classe de domínio "Machine" foi desenvolvida. Esta classe contém todos os value-objects e atributos necessários, bem como as respetivas validações. Também foi implementada uma interface UI em consola para requisitar os dados necessários ao Gestor.
Como este conceito tem uma identidade própria, todos estes dados são persistidos no sistema através de uma classe Repository.

## 2.3 Value-Objects

Adicionalmente á classe Machine, duas classes adicionais, MachineID e CommunicationProtocolID, foram implementadas. MachineID é a aggregateRoot da nossa entidade e como *Primary Key* deve ser Unica e não pode ser repetida. O CommunicationProtocolID como especificado na parte III do caderno de encargos é um número inteiro positivo contido entre 1 e 65535, também único.

Número de Série, Descrição, Marca e Modelo também estão presentes, mas do tipo *Description*, uma classe da framework que já possui as validações necessárias. 

Relativamente á data de instalação da Máquina, a framework de eapli também já disponibiliza esta classe e as respetivas validações.


# 3. Design

## 3.1. Realização da Funcionalidade

No diagrama seguinte apresentamos o fluxo lógico com sucesso para este UC. Importante salientar que validações como: o número de série ser único, a descrição ter um limite de 40 caracteres entre outros, não estão representadas. Apesar de importantes, não apresentam grande valor para esta visualização e como tal optei por não as incluir e tornar o SD mais simples.

![SD_RegisterMachine.jpg](SD_RegisterMachine.png) 

## 3.2. Diagrama de Classes

Para o diagrama de classes optei por incluir todos os packages do projeto com o intuito de centralizar o observador para esta UC e representar as diversas partes que são utlizadas.

![CD_RegisterMachine.jpg](CD_RegisterMachine.png) 

## 3.3. Padrões Aplicados

### Padrão Repository

O padrão Repository foi aplicado para realizar a persistência da nova instância da entidade Máquina.Possui a vantagem de abstrair da camada de implementação da persistência pretendida.

### Padrão Factory

Apesar de não ser diretamente aplicado, este padrão é usado para o Repositório da classe Machine, através da framework de EAPLI já existente. A grande vantagem da aplicação deste padrão é a possibilidade de abstrair das funcionalidades existentes na persistência, sendo esta a responsável por implementar os diversos tipos de tecnologias convenientes e necessárias.

## 3.4. Testes 

### Testes unitários

Devido á simplicidade do código desta classe irei apenas demostrar um exemplo prático dos testes implementados e os restantes apenas terão uma breve descrição.


**Teste 1:** Verificar que não é possível criar uma instância da classe Machine com valores nulos.

	@Test
    public void ensureMachineWithIdSerialNumberDescriptionBrandModelAndCommunicationProtocolID() {
        new Machine(MACHINE_ID, SERIAL_NUMBER, MACHINE_DESCRIPTION, BRAND, MODEL, COMMUNICATION_PROTOCOL_ID);
        assertTrue(true);
    }

**Teste 2:** Verificar que não é possível criar uma instância da classe Machine com cada atributo singular a nulo.

**Teste 4:** Verificar que não é possível que o Numero de Série não excede os 20 caracteres.

**Teste 5:** Verificar que não é possível que a Descrição não exceda os 40 caracteres.

**Teste 6:** Verificar que não é possível que a Data de Instalação seja no futuro.

**Teste 7:** Verificar que , singularmente, que MachineID pode ter números e letras maiúsculas e minúsculas.

**Teste 8:** Verificar que não é possível criar uma instância da classe MachineID com caracteres especiais.

**Teste 9:** Verificar que não é possível criar uma instância da classe CommunicationProtocolID com id a nulo.

**Teste 10:** Verificar que não é possível criar uma instância da classe CommunicationProtocolID com id maior que 65535

**Teste 11:** Verificar que não é possível criar uma instância da classe CommunicationProtocolID com id menor que 1.

### Testes Funcionais
**Teste 1:** Verificar que ao introduzir uma nova máquina o número de sério ainda não existe no sistema.

- 1: Inicar o caso de uso, seleccionando no menu a opção de adicionar nova máquina
- 2: Introduzir o número de série
- 3: Neste momento o sistema deve fazer uma query á DB e verificar se este número de série já existe
- 4: Se não existir o número de série então o gestor pode continuar com o UC. Caso exista mensagem de erro deve ser apresenta a explicar que o número de série já se encontra registado.


# 4. Implementação

Nesta secção apresento excerto do código desenvolvido, representativo do culminar do processo de planeamento desenvolvido para esta UC.
Inspirado na classe Dish da aplicação ecafetaria, estas classes foram desenvolvidas aplicando os conceitos lecionados em EAPLI, mantendo sempre em mente as boas práticas e os diversos padrões de POO.


## 4.1 Domain

![domain.png](domain.png)

## 4.1 Controller

![controller.png](controller.png)

## 4.1 UI

![UI.jpg](UI.png)

# 5. Integração/Demonstração

Nesta UC, por não possuir dependências com implementações já presentes aplicação, não existiu grande dificuldade para a integração da mesma.
Contudo foi o primeiro contacto com JPA que teve os seus contratempos inerentes no processo de aprendizagem.


# 6. Observações

Face ao trabalho desenvolvido neste UC, sinto que cumpri perante os objetivos propostos e apresento uma solução válida. É importante referir que demorei mais tempo do que tinha inicialmente previsto não só pelas características de caracter novo da sintaxe do JPA, mas também com alguma falta de motivação pessoal.


**João Machado [1161884](../)** - UC1-1-2001
=======================================


# 1. Requisitos

UC1-1-2001: Como Gestor de Produção, eu pretendo adicionar uma matéria-prima ao catálogo.


A interpretação feita deste requisito foi no sentido de um Gestor de Produção querer adicionar uma nova Matéria-Prima ao sistema e que esta seja persistida. O sistema deve ser capaz de disponibilizar as Categorias de Matérias-primas já existentes, permitir escolar uma e posteriormente pedir os dados necessários, fazendo as validações necessárias dos mesmos. No final deve mostrar uma mensagem de sucesso assim como um breve resumo do que foi adicionado.

### 1.1 Regras de Negócio
-Uma Matéria-Prima é caracterizada por um código interno, uma descrição, uma categoria e uma ficha técnica. Também tem associada um tipo de Unidade de Medida.
- O ID deve ser único e não nulo.
- Descrição não deve exceder os 40 caracteres.
- A Categoria já deve existir no Sistema.
- A Ficha Técnica deve estar disponível em todos os sistemas, independente de quem use e em qual computador.
- A Unidade de Medida já deve existir no sistema.



# 2. Análise

Inicialmente fiz o enquadramento deste UC com o modelo de domínio desenvolvido e com as dependências do mesmo já analisadas numa fase anterior. Dividi esta fase de análise em vários tópicos onde irei apresentar uma breve descrição para cada um deles.

## 2.1 Estudo

Para a adição de uma Matéria-Prima, a Categoria de Matéria-Prima e Unidade de Medida da mesma já devem existir no nosso sistema e estarem disponíveis para serem selecionadas.

Como a Categoria de Matéria-Prima tem o seu próprio agregado, esta dependência não apresenta grandes dificuldades.

No caso da Unidade de Medida, esta é apenas um Value-Object que não pertence a nenhum agregado o que também torna a interação com este atributo simples.

A maior dificuldade desta UC diz respeito á Ficha Técnica pois, deve ser um ficheiro pdf, persistido na BD utilizando JPA. Este tipo de implementação nunca foi utilizado pelo grupo e apresenta dificuldades referente á sintaxe desconhecida assim como os próprios mecanismos para adicionar e aceder a ficheiros deste tipo.

![MD_actual.png](MD_actual.png)



## 2.2 Matéria-Prima

A fim de satisfazer este UC, uma nova classe de domínio "RawMaterial" foi desenvolvida. Esta classe contém todos os value-objects e atributos necessários, bem como as respetivas validações. Também foi implementada uma interface UI em consola para mostrar e requisitar os dados necessários ao Gestor.
Como este conceito tem uma identidade própria, todos estes dados são persistidos no sistema através de uma classe Repository.


## 2.3 Value-Objects

Adicionalmente á classe RawMaterial, a RawMaterialID foi implementada. RawMaterialID é a aggregateRoot da nossa entidade e como *Primary Key* deve ser única e não pode ser repetida. 

Descrição é do tipo *Description*, uma classe da framework que já possui as validações necessárias. 

A Categoria de Matéria-Prima já foi desenvolvida anteriormente e apenas é necessário mostrar as opções disponíveis e permitir que uma seja selecionada.

Similarmente, Unidade de Medida é um Value-object já implementado. Novamente é necessário mostrar as opções existentes e permitir que uma seja selecionada.


## 2.4 Ficha Técnica

Para a Ficha Técnica ponderei identificá-la como um Value-Object inicialmente, mas como face à inexistência de regras próprias apenas ficou identificada como um atributo simples. A validação que é feita é se o ficheiro realmente existe e se é possível ler e persistir na BD.


# 3. Design

## 3.1. Realização da Funcionalidade

No diagrama seguinte apresentamos o fluxo lógico com sucesso para este UC.

![SD_RawMaterial.jpg](SD_RawMaterial.png) 

## 3.2. Diagrama de Classes

Para o diagrama de classes optei por incluir todos os packages do projeto com o intuito de centralizar o observador para esta UC e representar as diversas partes que são utlizadas.

![CD_RawMaterial.jpg](CD_RawMaterial.png) 

## 3.3. Padrões Aplicados

### Padrão Repository

Este padrão foi utlizado para aceder á informação existente sobre Categorias de Matéria-Prima assim como fazer a persistência da Matéria-Prima criada. É especialmente útil pois permiti abstrair da implementação técnica da camada responsável por criar a persistência.

### Padrão Factory

Este padrão foi utilizado no caso específico da criação de uma RawMaterial. 
Neste caso específico, o sistema disponibiliza a possibilidade de instanciar um objeto do tipo RawMaterial através de uma Factory de RawMaterialFactory. A aplicação deste tipo de padrão, entre outras, apresenta a vantagem de permitir encapsular as regras do domínio. 


### Padrão Visitor

Como para este UC é necessário inicialmente listar Categorias de Matérias-Primas, que são entidades próprias, fiz uso deste padrão para tornar possível aceder indiretamente a esta Entidade e mostrar a informação que eu acho pertinente.

## 3.4. Testes 

### Testes unitários

Devido á simplicidade do código desta classe irei apenas demostrar um exemplo prático dos testes implementados e os restantes apenas terão uma breve descrição.


**Teste 1:** Verificar que não é possível criar uma instância da classe RawMaterial com valores nulos.

	@Test
    public void ensureRawMaterialWithIdDescriptionCategoryUnitandFile() {
        new Machine(RAWMATERIAL_ID, DESCRIPTION, CATEGORY, UNIT, FILE);
        assertTrue(true);
    }

**Teste 2:** Verificar que não é possível criar uma instância da classe RawMaterial com cada atributo singular a nulo.

**Teste 4:** Verificar que não é possível que o RawMaterialID não excede os 20 caracteres.

**Teste 5:** Verificar que não é possível que a Descrição não exceda os 40 caracteres.

**Teste 7:** Verificar que , singularmente, que RawMaterialID pode ter números e letras maiúsculas e minúsculas.

**Teste 8:** Verificar que não é possível criar uma instância da classe RawMaterialID com caracteres especiais.


### Testes Funcionais
**Teste 1:** Verificar que ao introduzir uma nova matéria-prima o id ainda não existe no sistema.

- 1: Inicar o caso de uso, seleccionando no menu a opção de adicionar nova matéria-prima
- 2: Introduzir os dados neessários
- 3: Neste momento o sistema deve fazer uma query á DB e verificar se este ID já existe pois deve ser unico
- 4: Se não existir o ID então o gestor pode continuar com o UC. Caso exista mensagem de erro deve ser apresenta a explicar que o ID já se encontra registado.


# 4. Implementação

Nesta secção apresento excerto do código desenvolvido, representativo do culminar do processo de planeamento desenvolvido para esta UC.
Inspirado na classe Dish da aplicação ecafetaria, estas classes foram desenvolvidas aplicando os conceitos lecionados em EAPLI, mantendo sempre em mente as boas práticas e os diversos padrões de POO.


## 4.1 Domain

![Domain.png](Domain.png)

## 4.1 Controller

![Controller.png](Controller.png)

## 4.1 UI

![UI.jpg](UI.png)

# 5. Integração/Demonstração

A integração desta UC teve alguns desafios nomeadamente, a implementação do padrão Visiter, algo que ainda não tinha utilizado. Apesar de não ser complexo neste caso, requer passos adicionais e a criação de mais classes que aumenta a complexidade da UC.
Também pelo facto de estar diretamente da UC de definir uma Categoria de Matérias-Primas dificultou o debug e teste durante a implementação, pois ambas as UC estavam a ser desenvolvidas simultaneamente.
Outra particularidade desta UC é o requisito do cliente para ser possível ler ficheiros em pdf. Esta funcionalidade revelou-se ser extremamente simples de implementar, mesmo assim, foi necessária alguma investigação.


# 6. Observações

Face ao trabalho desenvolvido neste UC, sinto que cumpri perante os objetivos propostos e apresento uma solução válida. Com a experiência adquirida na UC anterior, implementar esta UC foi significativamente mais rápido.


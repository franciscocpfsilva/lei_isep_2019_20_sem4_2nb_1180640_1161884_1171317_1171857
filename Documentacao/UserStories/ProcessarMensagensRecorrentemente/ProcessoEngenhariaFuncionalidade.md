# US 5002 - Processar mensagens de forma recorrente - 1180640

=======================================

# 1. Requisitos

_Nesta secção a equipa deve indicar a funcionalidade desenvolvida bem como descrever a sua interpretação sobre a mesma e sua correlação e/ou dependência de/com outros requisitos._

## 1.1. Descrição

Como Serviço de Processamento de Mensagens (SPM), pretendo efetuar o processamento das mensagens disponíveis no sistema.

## 1.2. Critérios de aceitação / Observações

- Neste sprint, pretende-se suportar o segundo modo de operação descrito abaixo.
- O processamento das mensagens deve ser feito por linha de produção de forma independente e em paralelo.
- O processamento pode estar ativo e/ou suspenso para uma ou mais linhas.
- Deve suportar dois modos de operação distintos:
    - As mensagens são processadas em bloco para um intervalo de tempo (e.g. das 11h45 às 12h00) especificado. Processa o bloco e termina.
    - As mensagens são processadas em bloco de forma recorrente em intervalos de tempo (e.g. 15m) a contar de uma dado momento (e.g. 11h00). Depois de processar um bloco, aguarda até ser oportuno processar o bloco seguinte.
- A recorrência deve ser implementada através da periodicidade de um thread.

## 1.3. Esclarecimentos adicionais

- Para uma dada linha de produção, de cada vez que o processamento recorrente atua, este processa todas as mensagens disponíveis que ainda não tenha sido processadas.
- Processamento ativo vs inativo:
    - Processamento Ativo: isto significa que o processamento recorrente de mensagens ocorre ciclicamente.
    - Processamento Inativo: não ocorre qualquer processamento recorrente. Neste caso, pode ser solicitado o (re)processamento para um determinado intervalo de tempo.
- Note-se que o processamento pode estar ativo para uma linha de produção e inativo para outra.

## 1.4. Interpretação

Na aplicação Serviço de Processamento de Mensagens (SPM), para além do processamento realizado por blocos temporais e implementado na US 5001, deverá existir um processamento recorrente. A diferença entre ambos é que o primeiro é temporário, é executado uma vez e termina. Por sua vez, o segundo processa as mensagens existentes de x em x tempo, pelo que a aplicação deve estar constantemente a correr, sendo que no máximo é possível ativar/desativar o processamento recorrente para uma dada linha (US 3009).

![aplicacoes_existentes](aplicacoes_existentes.svg)

## 1.5. Dependências

- US 5001 - Processar mensagens em bloco para um intervalo de tempo

O desenvolvimento desta funcionalidade está a ser coordenado com o membro de equipa encarregue da US 5001.

## 1.6. Regras de negócio

- Processamento é realizado paralelamente para cada linha de produção
- Processamento para cada linha pode estar ativo ou inativo

# 2. Análise

_Neste secção a equipa deve relatar o estudo/análise/comparação que fez com o intuito de tomar as melhores opções de design para a funcionalidade bem como aplicar diagramas/artefactos de análise adequados._

_Recomenda-se que organize este conteúdo por subsecções._

O processamento das mensagens já foi/está a ser implementado na US 5001. Do mesmo modo, os conceitos de domínio necessários, tais como a Execução das ordens de produção, as Produções, os Consumos, os Desvios, etc, já foram tratados na US 5001. Eis o modelo de domínio que dá resposta ao processamento de mensagens.

![modelo_dominio_execucao_ordem_producao](modelo_dominio_execucao_ordem_producao.svg)

Deste modo, **esta US deverá reaproveitar muito daquilo que já foi realizado na US 5001**, nomeadamente a classe ProcessMessageService, que tem o Template Method para processar uma mensagem, usando em conjunção o padrão Strategy, que permite responder aos diversos tipos de mensagem.

A aplicação SPM existente recebe pelo menos 3 parâmetros e corre automaticamente a funcionalidade de processamento de mensagens num dado intervalo de tempo.

Sendo assim, **poderemos criar um modo de execução da aplicação onde não são passados quaisquer parâmetros e o processamento recorrente começa a correr automaticamente para as linhas de produção cujo estado esteja ativo**. O estado de cada linha é controlado através da US 3009.

**Quando este modo da aplicação arrancar, devemos ter um estado _default_ para cada linha:** ativo, inativo ou o estado desde a última execução da aplicação. [Não há indicações explícitas do cliente sobre este assunto](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=30684#p41014). Posto isto, uma boa opção será começar, na primeira execução, com um estado inativo para todas as linhas de produção e, a partir daí, **persistir a informação do estado de cada linha** (sujeito a modificação pela US 3009) de modo a que se a aplicação for abaixo, tenhamos os últimos estados guardados e possamos restaurar a aplicação a partir daí. Esta persistência na base de dados facilita também o acesso e modificação dos estados em termos de implementação.

**ADENDA:** Inicialmente foi sugerido colocar uma UI nesta aplicação servidora, de modo a dar resposta à US 3009 - Saber e alterar o estado de processamento de cada linha de produção. No entanto, após discussão com o grupo, fez mais sentido colocar esta UI na aplicação backoffice (aplicação consola), pelo que a ideia de termos um menu foi esquecida. A comunicação entre a aplicação consola e esta aplicação servidora fica da responsabilidade da US 3009.

# 3. Design

_Nesta secção a equipa deve descrever o design adotado para satisfazer a funcionalidade. Entre outros, a equipa deve apresentar diagrama(s) de realização da funcionalidade, diagrama(s) de classes, identificação de padrões aplicados e quais foram os principais testes especificados para validar a funcionalidade._

_Para além das secções sugeridas, podem ser incluídas outras._

## 3.1. Realização da Funcionalidade

_Nesta secção deve apresentar e descrever o fluxo/sequência que permite realizar a funcionalidade._

Assumindo a estratégia falada no capítulo da Análise, poderemos correr a aplicação SPM sem lhe passar qualquer argumento. Esta iniciará o processamento recorrente. Para cada linha de produção, ver-se-á na persistência o estado de processamento atual. Se ativo, o processamento recorrente para essa linha começa a funcionar. Se não existir dados na persistência para essa linha, deverá ser criada uma entrada com o estado _default_ inativo.

O controller deste caso de uso será referente ao processamento recorrente corrido em background, e não contemplará nenhuma das opções da UI (isso é assunto da US 3009). Este controller deverá, para cada linha, criar uma thread responsável pelo processamento de mensagens, de modo a que este processamento seja paralelo e independente entre linhas de produção. A thread de cada linha deverá ser responsável pela periodicidade de processamento.

Cada thread terá de fazer uma verificação regular do estado de processamento da respetiva linha de produção, de modo a desativar o processamento caso o seu estado tenha passado de "ativo" a "inativo" por exemplo.

![sequenceDiagram_processMessagesRecurrently](sequenceDiagram_processMessagesRecurrently.svg)

![sequenceDiagram_processMessagesRecurrently_details](sequenceDiagram_processMessagesRecurrently_details.svg)

## 3.2. Diagrama de Classes

_Nesta secção deve apresentar e descrever as principais classes envolvidas na realização da funcionalidade._

![classDiagram_processMessagesRecurrently](classDiagram_processMessagesRecurrently.svg)

## 3.3. Padrões Aplicados

_Nesta secção deve apresentar e explicar quais e como foram os padrões de design aplicados e as melhores práticas_

- **Single Responsibility Principle**
- **Tell, don't ask**
- **Service:** Para facilitar o acesso ao estado de processamento de uma linha de produção, quer o estado já esteja persistido ou não.

Para além dos padrões usados regularmente: Controller, Repository; e dos padrões usados na US 5001: Template Method, Strategy.

## 3.4. Testes

_Nesta secção deve sistematizar como os testes foram concebidos para permitir uma correta aferição da satisfação dos requisitos._

Dado esta US não necessitar de implementar conceitos de negócio (já implementados na US 5001), não temos de realizar testes unitários.

Podemos no entanto realizar os seguintes testes funcionais.

**Teste funcional 1:** Verificar que o processamento é realizado paralelamente para cada linha de produção.

**Teste funcional 2:** Verificar que o processamento para cada linha pode estar ativo ou inativo.

# 4. Implementação

_Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;_

_Recomenda-se que organize este conteúdo por subsecções._

A implementação foi realizada de acordo com as decisões de Design, à exceção da questão da UI, discutida no capítulo de Integração e na "Adenda" do capítulo da Análise. A classe ProcessMessagesGeneralMode é responsável pela aplicação em modo sem argumentos. 

# 5. Integração/Demonstração

_Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema._

## 5.1. Integração

O desenvolvimento desta US foi coordenado com os membros responsáveis por realizarem a US 5001 e a US 3009.

- **Integração com a US 5001 (processamento por intervalo temporal):** Esta US reaproveitou muitas coisas da US 5001. No final, obtemos dois modos de correr a aplicação: i) com argumentos (relativo ao processamento por intervalo temporal da US 5001); ii) sem argumentos (corre o processamento recorrente da US 5002).
- **Integração com a US 3009 (saber e alterar o estado de processamento de cada linha, e pedir processamento por intervalo temporal):** Esta US foi pensada desde início tendo em conta a possibilidade de alterar o estado de processamento de cada linha de produção através da US 3009. Inicialmemte pensou-se em incorporar uma UI no SPM. No entanto, dado esta ser uma aplicação servidora, a opção de ter uma UI não seria a melhor (não é da sua responsabilidade), tendo-se passado esta UI para a aplicação consola (responsabilidade da US 3009).
    - **Estado de processamento:** A persistência na base de dados do estado de cada linha permite que a aplicação consola altere o estado sem depender da aplicação SPM. Por sua vez, o SPM tem de estar atento a alterações que possam surgir na base de dados, para refletir a mudança no estado de processamento. **O SPM já foi preparado para isto.**
    - **Pedir processamento por intervalo temporal:** Uma vez que corre indefinidamente, o modo para correr a aplicação sem argumentos poderá ser mais facilmente adaptado para correr um servidor numa thread própria que fique à escuta de pedidos de processamento por intervalo temporal. Quando um pedido surgir, o SPM deve chamar o(s) método(s) respetivo(s) a este tipo de processamento, que já foram por sua vez implementados na US 5001. **Esta thread e o respetivo servidor teriam de ser implementados de novo.**

![modos_para_correr_spm](modos_para_correr_spm.svg)

# 6. Observações

_Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados._

A questão da integração com a US 3009 poderia ter sido discutida em grupo mais cedo e mais a fundo. Tirando isso, sinto que o trabalho foi bem realizado, apesar de sentir que as decisões relacionadas com a organização da aplicação e o uso de threads não estão assentes em muitos padrões de design (para além do Single Responsibility Pattern), mas mais em bom senso. 

**João Cunha [1171857](../)** - UC3-1-2006 Adicionar um novo Produto
=======================================


# 1. Requisitos

UC3-1-2006: Como Gestor de Produção, eu pretendo adicionar um novo produto ao catálogo de produtos.

A interpretação feita deste requisito foi no sentido de adicionar um novo produto e guarda-lo no sistema. Dado que é possivel criar um novo produto sem Ficha de Produção, este não depende de nenhuma User Story.
No entanto existem algumas regras de negócio associadas ao mesmo:

* [Produtos sem Ficha de Produção](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=29380#p39103)
* [ID's de produto codigo alfanumerio não superior a 15 carateres](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=29283#p38897)
* [Descrição breve do produto não superior a 30 carateres](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=27390#p35711)
* [Produto tem associado uma unidade de medida! Produtos com diferentes unidades de medidas também tem de ter identificador diferente!](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=28756#p38049)

De notar que os dois ID's do produto são unicos e é da responsabilidade do utilizador de os introduzir.


# 2. Análise

Dado que esta User Story não depende de nenhuma outra do SPRINT B e como é um conceito importante para o negócio considero que produto é uma *entity*.
O produto está relacionado com uma ficha de produção, esta relação é necessária quando existe uma ordem de produção de um produto, pois um produto pode existir sem ter especificado uma ficha de produção, mas um produto não pode ser produzido sem ter associado uma ficha de produção! Visto que a ficha de produção só existe relacionada com um produto, defini que produto é um *root entity*.
O produto também tem associado uma *Unidade de Medida* unica. Se for necessario expecificar uma nova *Unidade de medida* para um produto é necessário criar um novo produto, gerar um novo identificador!
Para execução desta User Story será necessário uma interação com o utilizador. O utilizador em questão será o Gestor de Produção, que tem de inserir os varios atributos e/ou value objects que controem um produto (ID Fabrico, ID comercial, Desc breve, Desc completa).

* Pode existir produtos sem ficha de produção.
* Restrição para os ID's:
    * Não serem vazios
    * Não superior a 15 caracteres
    * Serem uma unica palavra (não conter espaços)
    * Não conter caracteres expeciais (e.g. *´?!_)
* Restrição para descrição breve, não superior a 30 caracteres

O nosso modelo de dominio só necessita de uma pequena alteração no values-object *Quantidade* pois ele era constituido por dois atributos, quantidade e unidade de medida! Dado que precisamos da relação *Produto* e *Unidade de Medida* iremos separar o value-object *Quantidade* em dois value-objects, conseguindo assim responder a exigencias desta User Story.

## Agregado de Produtos antigo

![](https://bitbucket.org/franciscocpfsilva/lei_isep_2019_20_sem4_2nb_1180640_1161884_1171317_1171857/raw/d3644623333a38fcdaf1f383fe4f4e7183e4882b/Documentacao/AdicionarProduto/Agregado_Produtos.PNG)

## Agregado de Produtos com as alterações

![](https://bitbucket.org/franciscocpfsilva/lei_isep_2019_20_sem4_2nb_1180640_1161884_1171317_1171857/raw/27ea6b72f70fb1226c6f5a64e90a182298a09a9d/Documentacao/AdicionarProduto/Agregado_Produtos_Atualizado.PNG)


# 3. Design

## 3.1. Realização da Funcionalidade

![](https://bitbucket.org/franciscocpfsilva/lei_isep_2019_20_sem4_2nb_1180640_1161884_1171317_1171857/raw/27ea6b72f70fb1226c6f5a64e90a182298a09a9d/Documentacao/AdicionarProduto/SD_AddProduct.svg)


O Gestor de Produção seleciona que pretende adicionar um novo Produto. O programa solicitará a informação necessária para a construção do produto. Após o Gestor de Produção ter inserido a informação o programa tenta criar o novo Produto guardando-o no sistema.
Caso a informação seja inválida o programa emite uma mensagem de erro e não cria, nem guarda o novo Produto!

## 3.2. Diagrama de Classes

![](https://bitbucket.org/franciscocpfsilva/lei_isep_2019_20_sem4_2nb_1180640_1161884_1171317_1171857/raw/d3644623333a38fcdaf1f383fe4f4e7183e4882b/Documentacao/AdicionarProduto/ClassDiagram.svg)


A interação com o utilizador será da responsabilidade de classe *AddProductUI* que vai solicitar os dados e guarda-los. De seguida a classe *AddProductController* recebe os dados que a *AddProductUI* guardou e irá solicitar a criação de um novo objecto *Product*. A classe *Product* será reponsável por validar se os dados estão corretos e por criar o objecto. De seguida a classe *AddProductController* irá solicitar à interface *ProductsRepository* que presista a informação do novo objecto produto!

## 3.3. Padrões Aplicados

Para tratar do ciclo de vida de um objecto *Product* foi só utilizador o padrão *Repositories*, para criar, não achei necessário utilizar o padrão *Factories* dada a baixa complexidade do objecto. 

## 3.4. Testes 

**Teste 1:** Verificar que não é possível criar uma instância da classe *Product* com valores nulos.

	@Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowed() {
        Product instance = new Product(null, null, null, null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowedFactoryID() {
        Product instance = new Product(null, business_ID, brief_desc, full_desc, 
                category, unit);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowedBusinessId() {
        Product instance = new Product(factory_ID, null, brief_desc, full_desc, 
                category, unit);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowedBriefDescription() {
        Product instance = new Product(factory_ID, business_ID, null, full_desc, 
                category, unit);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowedFullDescription() {
        Product instance = new Product(factory_ID, business_ID, brief_desc, null, 
                category, unit);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowedCategory() {
        Product instance = new Product(factory_ID, business_ID, brief_desc, full_desc, 
                null, unit);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowedUnitOfMeasurement() {
        Product instance = new Product(factory_ID, business_ID, brief_desc, full_desc, 
                category, null);
    }

**Teste 2:** Verificar que não é possível criar uma instância da classe *Product* com descrição breve superior a 30 carateres.

	@Test(expected = IllegalArgumentException.class)
    public void briefDescrNotExceedThirtyInSize() {
        brief_desc = Description.valueOf("RolhaA de qualidade superior a RolhaB");

        Product instance = new Product(factory_ID, factory_ID, brief_desc, full_desc, 
                category, unit);
    }

**Teste 3:** Verificar que não é possível criar uma instância da classe *Product* com a categoria vazia.

    @Test(expected = IllegalArgumentException.class)
    public void ensureEmptyIsNotAllowedCategory() {
        Product instance = new Product(FACTORY_ID, BUSINESS_ID, BRIEF_DESC, FULL_DESC,
                "", UNIT);
    }

**Teste 4:** Verificar que não é possível criar uma instância da classe *ProductID* com valores nulos.

	 @Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowed() {
        ProductID instance = new ProductID(null);
    }

**Teste 5:** Verificar que não é possível criar uma instância da classe *ProductID* com ID superior a 15 carateres.

	@Test(expected = IllegalArgumentException.class)
    public void factoryIDDoesNotExceedFifteenInSize() {
        ProductID instance = new ProductID("01239120ASJDA019312");
    }

**Teste 6:** Verificar que não é possível criar uma instância da classe *ProductID* com o ID não sendo uma palavra unica.

	@Test(expected = IllegalArgumentException.class)
    public void factoryIDIsNotASingleWord() {
        ProductID instance = new ProductID("A123 0123D ");
    }

**Teste 7:** Verificar que não é possível criar uma instância da classe *ProductID* com caracteres especiais.

	@Test(expected = IllegalArgumentException.class)
    public void factoryIDContainsSpecialCharacters() {
        ProductID instance = new ProductID("A123^0123D ");
    }

**Teste 8:** Verificar que não é possível criar uma instância da classe *ProductID* ID vazio.

    @Test(expected = IllegalArgumentException.class)
    public void ensureEmptyIsNotAllowed() {
        ProductID instance = new ProductID("");
    }
	
# 4. Implementação

## 4.1 Domain ##

![](https://bitbucket.org/franciscocpfsilva/lei_isep_2019_20_sem4_2nb_1180640_1161884_1171317_1171857/raw/27ea6b72f70fb1226c6f5a64e90a182298a09a9d/Documentacao/AdicionarProduto/Domain.PNG)

## 4.2 Controller ##

![](https://bitbucket.org/franciscocpfsilva/lei_isep_2019_20_sem4_2nb_1180640_1161884_1171317_1171857/raw/27ea6b72f70fb1226c6f5a64e90a182298a09a9d/Documentacao/AdicionarProduto/Controller.PNG)

## 4.3 UI ##

![](https://bitbucket.org/franciscocpfsilva/lei_isep_2019_20_sem4_2nb_1180640_1161884_1171317_1171857/raw/27ea6b72f70fb1226c6f5a64e90a182298a09a9d/Documentacao/AdicionarProduto/UI.PNG)

# 5. Integração/Demonstração

Nesta funcionalidade não houve necessidade de integrar com as restantes, pois ela não tem dependencias. 

# 6. Observações

Considero que o trabalho desenvolvido responde aos requisitos. O tempo de execução desta tarefa foi mais extenso, pois tive de procurar e perceber os métodos da framework, alguns problemas com JPA e avaliar a melhor maneira para implementar a tarefa. No entanto acho que esta demora é normal devido a ser a primeira tarefa, nas próximas já não existir tanta pesquisa e será mais implementação. Não significa que não vá existir pesquisa na mesma,  porque o nosso grupo optou, e bem, por começar pelas US sem dependências , as próximas já serão mais complexas nesse sentido. 




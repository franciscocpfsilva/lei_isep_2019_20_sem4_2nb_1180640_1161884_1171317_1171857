**João Cunha [1171857](../)** - UC1-2-1007 Bootstrap de Máquinas
=======================================


# 1. Requisitos

Como Gestor de Projeto, eu pretendo que a equipa proceda à inicialização (bootstrap) de algumas de máquinas.

A interpretação feita deste requisito foi no sentido de adicionar algumas máquinas ao sistema para podermos demonstrar outra User Stories que dependem de máquinas.

Sincronização com o colega que realizou a user story *Definir existência de uma nova maquina*.

# 2. Análise

Como esta User Story é um bootstrap a analise baseia-se em identificar dados de bootstrap são necessários.
Para a maquina são necessários os seguintos dados:
    * Machine ID
    * Serial Number
    * Machine Description
    * Installation Date
    * Brand
    * Model
    * Communication Protocol ID

# 3. Design

Dado que este bootstrap não depende de nenhuma outro, mas o bootstrap de uma linha de produção depende deste. 
Por isso a única restrição é que tem de ser executado primeiro este bootstrap.
	
# 4. Implementação

![](MachineBootstrapper.png)

# 5. Integração/Demonstração

Não foi necessário fazer nenhuma intregração. Só utilizei o *AddMachineController* para adicionar as novas máquinas!

# 6. Observações






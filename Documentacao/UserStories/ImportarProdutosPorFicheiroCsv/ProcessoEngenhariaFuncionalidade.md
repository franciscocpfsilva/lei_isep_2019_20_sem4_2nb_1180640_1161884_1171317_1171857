# Aluno 1180640 - US2005 - Importar Produtos através de ficheiro CSV

=======================================

# 1. Requisitos

_Nesta secção a equipa deve indicar a funcionalidade desenvolvida bem como descrever a sua interpretação sobre a mesma e sua correlação e/ou dependência de/com outros requisitos._

**US2005** Como Gestor de Produção, eu pretendo importar o catálogo de produtos através de um ficheiro de CSV.

## 1.1. Novos requisitos

**VARIABILIDADE DE TIPOS DE FICHEIROS**

O objetivo desta US passa por extrair informação de um ficheiro de produtos e guardá-la no sistema. Há um aspeto muito importante desta US, que consiste na **variabilidade de tipos de ficheiros** que poderemos encontrar no futuro. O próprio caderno de encargos faz referência a isto aquando da discussão da importação de informação em ficheiros de texto com diferentes formatos: "o sistema a desenvolver deve estar desde já preparado para lidar com esta variabilidade" (pág. 4). As decisões de Design devem ter em conta este requisito.

**[ERROS DE FORMATAÇÃO NO FICHEIROS DE PRODUTOS](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=29470#p39173)**

PERGUNTA: “Caso o ficheiro utilizado para importar produtos possua erros de formatação (e.g. número incorreto de atributos) qual deve ser o comportamento do sistema?”

RESPOSTA DO CLIENTE: “Deve ignorar essas linhas, colocando-as, por exemplo, num ficheiro de erros e no fim dá essa indicação ao utilizador. As linhas sem erros devem ser importadas. Já agora, no final da importação deveria ser apresentado ao utilizador um resumo quantitativo dos resultados da importação.“

O [formato do ficheiro de erros](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=29821#p39720) deve ser igual ao do ficheiro que contém os produtos a importar.

**[COMPORTAMENTO DO CASO DE USO PERANTE PRODUTOS REPETIDOS](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=29598)**

1. Sem dúvida, adicionar os novos produtos aos já existentes.
2. Questionar o utilizador, se em caso de algum produto já existir se pretende que a informação seja ou atualizada ou ignorada.
3. Não se pretende eliminar nenhum produto.

# 1.2. Regras de negócio

Não existem regras de negócio definidas para além daquilo que consta nos "Novos requisitos". Este tipo de regras não dará origem a testes unitários mas sim a testes funcionais.

## 1.3. Dependências

Esta US depende da US2006 “Adicionar um novo produto ao catálogo de produtos”. Para que possamos importar produtos, o conceito de produto já deve estar implementado no sistema, assim como algum modo de criar um produto. Esta dependência foi já identificada no planeamento e por isso mesmo a US2006 teve início antes desta US. No momento de escrita, a US2006 encontra-se quase finalizada. O desenvolvimento das duas US será ainda assim sincronizado entre os membros respetivos.

# 2. Análise

_Neste secção a equipa deve relatar o estudo/análise/comparação que fez com o intuito de tomar as melhores opções de design para a funcionalidade bem como aplicar diagramas/artefactos de análise adequados._

_Recomenda-se que organize este conteúdo por subsecções._

## 2.1. Modelo de Domínio

![MD_Produtos](MD_Produtos.png)

O MD existente à data permite dar resposta aos requisitos deste caso de uso. O conceito de Produto está já representado como uma _root-entity_, sendo portanto possível criar produtos e guardá-los no repositório do respetivo agregado. Os atributos existentes também vão de encontro à informação disponibilizada no [ficheiro exemplo de importação de produtos](https://moodle.isep.ipp.pt/mod/resource/view.php?id=193029).

## 2.2. Comportamento do caso de uso

Apesar desta US ser específica à importação de produtos através de ficheiros CSV, é inevitável não considerarmos a variabilidade de ficheiros que poderemos encontrar. Deste modo, a sequência de passos desta US deve também incluir a escolha do tipo de ficheiro.

1. Gestor de produção inicia o caso de uso.
2. Sistema mostra os tipos de ficheiros existentes.
3. Gestor de produção escolhe um tipo de ficheiro.
4. Sistema pede _path_ do ficheiro a ler e pergunta se quer atualizar ou ignorar as linhas com produtos repetidos.
5. Gestor de produção introduz os dados.
6. Sistema importa produtos e mostra resumo da operação.

## 2.3. Estudo/comparações realizadas

Foi feita a comparação desta US com a [US "Export Dishes" do projeto exemplo ecafeteria](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=29412). Existem muitas semelhanças entre ambas, sendo que a US exemplo também prepara o sistema para vários tipos de ficheiros.

# 3. Design

_Nesta secção a equipa deve descrever o design adotado para satisfazer a funcionalidade. Entre outros, a equipa deve apresentar diagrama(s) de realização da funcionalidade, diagrama(s) de classes, identificação de padrões aplicados e quais foram os principais testes especificados para validar a funcionalidade._

_Para além das secções sugeridas, podem ser incluídas outras._

## 3.1. Padrões Aplicados

_Nesta secção deve apresentar e explicar quais e como foram os padrões de design aplicados e as melhores práticas_

**Protected Variation (GRASP)** Dada a possibilidade de termos ficheiros de tipos diferentes, identificamos aqui um ponto de variação no nosso sistema e é portanto inevitável não pensar neste padrão, ou seja, a atribuição de responsabilidades de modo a criar uma interface estável à volta deste ponto.

**Template Method (GoF)** Apesar dos diversos tipos de ficheiros, a sequência de passos a realizar em cada um deles será a mesma: abrir o ficheiro, ler uma linha e importar o respetivo produto (repetido várias vezes) e fechar o ficheiro. Sendo assim, temos aqui um bom candidato para aplicar o padrão Template Method, que nos permite definir a estrutura de um algoritmo, dando liberdade na implementação de alguns passos do algoritmo.

**Strategy (GoF)** Finalmente, uma vez que queremos permitir ao cliente que escolha o tipo de ficheiro e, portanto, uma de várias alternativas ao algoritmo de importação, podemos resolver este problema com o padrão Strategy. Este padrão permite fazer muitas implementações da mesma interface e permitir que o cliente selecione uma. E portanto engloba na sua definição o Protected Variation, um padrão mais genérico.

**Outros** O padrão Strategy é uma excelente hipótese neste caso de uso, mas não resolve tudo. A interface usada neste padrão apenas pode ter os métodos relativos aos vários passos da importação de ficheiros, mas não define uma ordem desses passos. Para isso, podemos usar em conjunção o padrão Template Method. No entanto, põe-se a questão: em que classe fica o algoritmo de importação? e quem será responsável por gravar o produto na base de dados? A operação de negócio não parece fazer parte da responsabilidade de nenhum objeto de domínio, para além de que o domínio não deve ter dependências da camada de persistência (o que aconteceria no ato de gravar um produto). Aqui podemos aplicar o padrão **Service (DDD)**, sendo que este novo objeto ficará na camada de aplicação. Por outro lado, junto com o padrão Strategy, podemos aplicar o padrão **Simple Factory (GoF)** para resolver a manipulação de diferentes implementações da mesma interface (ou seja, métodos para ficheiros CSV, métodos para ficheiros JSON...).

### 3.1.1. Resumo dos padrões usados

- Protected Variation
- Template Method
- Strategy
- Service
- Simple Factory

## 3.2. Realização da Funcionalidade

_Nesta secção deve apresentar e descrever o fluxo/sequência que permite realizar a funcionalidade._

![diagramaSequencia_USImportarProdutos](diagramaSequencia_USImportarProdutos.png)

A decisão de atualizar ou ignorar os produtos que já existem pode ser passada desde a UI ao Controller e por sua vez ao Serviço através de um _boolean_. Consoante o valor deste _boolean_, o Serviço pode decidir o que fazer no método saveElement(). Ou chama um método saveElementIgnoringRepeated() ou saveElementUpdatingRepeated().

## 3.3. Diagrama de Classes

_Nesta secção deve apresentar e descrever as principais classes envolvidas na realização da funcionalidade._

![diagramaClasses_USImportarProdutos](diagramaClasses_USImportarProdutos.png)

A classe **ProductImporterFactory** cria uma implementação da **interface ProductImporter** de acordo com o tipo de ficheiro que recebe. Para esta US, criará uma instância de ProductImporterCsv, mas quando surgir a necessidade de englobar mais tipos de ficheiros, teremos mais implementações da mesma interface.

A classe **ProductImporterService** corresponde ao serviço que contém a estrutura do algoritmo de importação.

A camada de UI tem dependência sobre a camada de domínio uma vez que vai receber do controlador uma lista dos produtos importados para mostrar ao utilizador.

## 3.4. Testes

_Nesta secção deve sistematizar como os testes foram concebidos para permitir uma correta aferição da satisfação dos requisitos._

Não há testes unitários. Apenas testes funcionais.

**Teste funcional 1:** Verificar que importa produtos através de um ficheiro válido.

**Teste funcional 2:** Verificar que cria um ficheiro de erros para apresentação das linhas com erros de formatação.

# 4. Implementação

_Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;_

_Recomenda-se que organize este conteúdo por subsecções._

O package eapli.base.products.importing.application do projeto core contém as classes/interfaces necessárias para a implementação dos padrões Strategy e Template Method falados acima.

Este caso de uso lê um ficheiro, necessitando para isso que o utilizador lhe passe o caminho desse ficheiro. É também criado um ficheiro de erros que estará localizado no mesmo diretório do ficheiro anterior e terá o nome de "erros.csv" para ficheiros de tipo CSV.

A implementação da UI extende a classe AbstractListUI da framework EAPLI. Por isso mesmo, tem a estrutura de uma classe de listagem, o que visou facilitar a apresentação de um resumo dos produtos importados, um requisito do cliente.

Optou-se por não implementar a possibilidade de atualizar produtos já existentes, uma vez que isso traria uma complexidade acrescida a esta US, que por si só já é complexa. Deste modo, apenas são importados os produtos que ainda não existem na base de dados.

Outputs:

- **Resumo na consola:** apenas são mostrados os produtos importados
- **Ficheiro de erros:** contém as linhas do ficheiro original que tenham erros de formatação

Produtos já existentes na base de dados não são alterados nem apresentados no resumo da operação.

# 5. Integração/Demonstração

_Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema._

Este caso de uso foi realizado já a pensar com a possibilidade de podermos importar produtos através de outros tipos de ficheiro ou outros recursos. Para que isto aconteça, basta apenas criar uma implementação nova da interface ProductImporter para o tipo de ficheiro pretendido. Tudo o resto será reutilizado. Não existe sequer necessidade de criar um Controller ou uma UI.

Esta funcionalidade dependia da US2006 “Adicionar um novo produto ao catálogo de produtos”. E por isso mesmo fez uso do construtor e do repositório de produtos já criados. O método saveElement() da classe ProductImporterService contém alguma duplicação de código no que diz respeito à adição de um produto ao seu repositório. Poderia usar-se um serviço aqui, no entanto, dado o uso já do ProductImporterService, iríamos estar a criar dependências entre serviços, pelo que se optou por deixar esta pequena duplicação de código.

# 6. Observações

_Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados._

Esta US tinha algum nível de complexidade e foi notada alguma dificuldade em conciliar com alguns requisitos extra do cliente. Por isso mesmo, deixou-se cair a parte relativa à atualização de produtos relativos. Optou-se por uma abordagem mais simples, com o benefício de manter as coisas mais claras, evitar muitas misturas e evitar criar dependências a torto e a direito, até porque estamos numa fase de aprendizagem.

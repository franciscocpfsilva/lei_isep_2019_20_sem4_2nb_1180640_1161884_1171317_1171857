UC5-1-2013 Aplicar visualização a ficheiro XML

=======================================

# 1. Requisitos

- UC5-1-2013: Como Gestor de Produção, eu pretendo aplicar uma visualização/transformação (das várias disponíveis) a um ficheiro XML anteriormente gerado/exportado por mim (através do sistema).



### 1.1 Regras de Negócio
-Pelos menos 3 visualizações/transformações são requeridas: (i) para HTML, (ii) para JSON e (iii) texto (para leitura por humanos); sempre através de XSLT.
-O resultado/output deve ser outro ficheiro, mantendo o ficheiro original inalterado.


# 2. Análise

Esta US será desenvolvida em conjunto por todos os elementos do grupo, sendo que cada elemento deve desenvolver uma transformação para cada tipo de ficheiro ou auxiliar na mesma.

Como existem três formatos distintos, iremos abordar por secções cada um deles e explicar qual o objetivo da transformação e mostrar alguns dos resultados finais.

## 2.1 Estudo

No sprint C, foi desenvolvido um ficheiro XML que contem toda a informação do sistema. Este formato é extremamente "portátil" pois é independente de linguagens de programação ou plataformas.
Outra vantagem significativa é flexibilidade como é possível visualizar o seu conteúdo. Apesar do próprio formato XML ser legível para humanos existem outras maneiras de apresentar o seu conteúdo.
Seguidamente iremos abordar a visualização do ficheiro XML utilizando o XSLT para gerar documentos HTML, TXT e JSON.

# 3. Design

## 3.1. HTML

Mesmo não sendo um requisito para a US, o grupo decidiu ter em consideração a estética da página HTML. A fim de melhorar a apresentação dos dados vários scripts serão adicionados ao nosso ficheiro xslt já utilizados em LAPR1. Estes sripts são nomeadamente um bootstrap para embelezamento do site e jQuery API para permitir navegar pelas webpages sem haver necessidade de *refresh*.

A primeira transformação tem como objetivo representar todos os dados presentes no XML, organizados por separadores correspondentes aos agregados do sistema. Também será possível ter um somatório de todos os registos presentes por agregado, proporcionando assim uma visão geral dos dados presentes no sistema.

Outra transformação a desenvolver será referente às Ordens de Produção. O objetivo é poder filtrar as Ordens de Produção pelo seu estado e visualizar apenas as que se pretendem. Esta transformação irá fazer uso das variáveis em xslt.

A última transformação apresenta valor importante para o negócio, pois apenas irá mostrar os Produtos sem Ficha de Produção. Esta visualização pode ser importante sendo que uma Ordem de Execução só pode ser relativa a um produto com Ficha de Produção. Nesta transformação iremos usar algum tipo de combinação das funções *choose* e *when*.

## 3.2. TXT

Uma das transformações TXT vai continuar a incidir sobre os Produtos e a Ficha de Produção. No nosso sistema a Ficha de Produção pode conter Matérias-Primas ou produtos. Isto resulta em que um dos atributos ID do Value-Object Item, RawMaterialID ou ProductID, fique a null e o outro com um identificador referente a um destes Objetos. Como o objetivo é não visualizar o ID que está a null, esta transformação tem como objetivo fazer a verificação de qual ID não está a null e apenas mostrar esse.

Mudando para o agregado das Matérias-Primas e Categoria de Matérias-Primas, será adicionada uma transformação capaz de mostrar todas as Matérias-Primas apenas referentes a uma Categoria. Esta Categoria será guardada numa variável e seguidamente comparada com cada Matéria-Prima permitindo esse filtro.

Como existem pelo menos duas maneiras distintas de apresentar texto, esta última transformação apenas servirá para mostrar dados de uma maneira diferente das anteriores fazendo uso do método *substring* e *concat*. 



## 3.3. JSON

Similarmente há primeira transformação para HTML, também será desenvolvida uma transformação para JSON com o objetivo de mostrar toda a informação presente no XML de uma maneira mais organizada e visualmente agradável.



# 4. Implementação


# 5. Integração/Demonstração

# 6. Observações



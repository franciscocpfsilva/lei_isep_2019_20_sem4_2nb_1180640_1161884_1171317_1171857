<?xml version="1.0" encoding="ISO-8859-1" ?>
<xsl:stylesheet version="2.0" xmlns:t="http://my_namespace"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" indent="no"/>
   
    <xsl:template match="/">

        <xsl:for-each select="/t:Factory/t:Storage">
            <xsl:apply-templates select="t:StorageUnit" />
        </xsl:for-each>
       
    </xsl:template>

    

    <xsl:template match="t:StorageUnit">


        <xsl:text>  Storage Unit ID: </xsl:text>

        <xsl:value-of select="substring(concat(t:StorageUnitID/@identifier, '               '), 1, 10)" />

        <xsl:text> | Description: </xsl:text>

        <xsl:value-of select="substring(concat(t:Description/@description, '               '), 1, 28)" />
        
        <xsl:text>-------------------------------------------------------------- </xsl:text>
    
    </xsl:template>

</xsl:stylesheet>
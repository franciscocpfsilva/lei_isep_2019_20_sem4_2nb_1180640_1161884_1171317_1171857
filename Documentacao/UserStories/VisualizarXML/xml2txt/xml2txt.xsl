<?xml version="1.0" encoding="ISO-8859-1" ?>
<xsl:stylesheet version="2.0" xmlns:t="http://my_namespace"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" indent="no"/>

    <xsl:template match="/">
        <![CDATA[List of Existing products:  ]]><xsl:value-of select="count(t:Factory/t:Products/t:Product)" />
        <xsl:for-each select="/t:Factory/t:Products">
            <xsl:apply-templates select="t:Product" />
        </xsl:for-each>
    </xsl:template>


    <xsl:template match="t:Product">


    <![CDATA[Product number: ]]><xsl:value-of select="position()" />

    <![CDATA[Product ID: ]]><xsl:value-of select="t:ProductID/@id" />
    <![CDATA[Commercial ID: ]]><xsl:value-of select="t:CommercialID/@id" />
    <![CDATA[Short Description: ]]><xsl:value-of select="t:ShortDescription/@description" />
    <![CDATA[Description: ]]><xsl:value-of select="t:Description/@description" />
    <![CDATA[Unit Of Mesurement: ]]><xsl:value-of select="t:UnitOfMesurement/@Unit" />
    <![CDATA[Category: ]]><xsl:value-of select="t:Category" />

        <![CDATA[Production Sheet: ]]>
            <![CDATA[Standard Quantity: ]]><xsl:value-of select="t:ProductionSheet/t:StandardQuantity/t:Quantity/@Qvalue" />
            <![CDATA[Unit Of Mesurement: ]]><xsl:value-of select="t:ProductionSheet/t:StandardQuantity/t:UnitOfMesurement/@Unit" />

        <![CDATA[Items needed for production: ]]>
        <xsl:choose>
            <xsl:when test="t:ProductionSheet/t:Item/t:ProductID/@id ='null'">
            <![CDATA[Raw Material ID: ]]><xsl:value-of select="t:ProductionSheet/t:Item/t:RawMaterialID/@id" />
            </xsl:when>
            <xsl:otherwise>
            <![CDATA[Product ID: ]]><xsl:value-of select="t:ProductionSheet/t:Item/t:ProductID/@id" />
            </xsl:otherwise>
            </xsl:choose>
            <![CDATA[Quantity: ]]><xsl:value-of select="t:ProductionSheet/t:Item/t:Quantity/t:Quantity/@Qvalue" />
            <![CDATA[Unit Of Mesurement: ]]><xsl:value-of select="t:ProductionSheet/t:Item/t:Quantity/t:UnitOfMesurement/@Unit" />

    <![CDATA[**********************************************************]]>
    </xsl:template>

</xsl:stylesheet>
<?xml version="1.0" encoding="ISO-8859-1" ?>
<xsl:stylesheet version="2.0" xmlns:t="http://my_namespace"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" indent="no"/>
   



    <xsl:template match="/">
        <xsl:variable name="category" select="'2'"/>

        <xsl:for-each select="/t:Factory/t:RawMaterialCategories">
            <xsl:apply-templates select="t:RawMaterialCategory[t:RawMaterialCategoryID/@identifier=$category]" />
        </xsl:for-each>
       
        <xsl:for-each select="/t:Factory/t:RawMaterials">
            <xsl:apply-templates select="t:RawMaterial[t:RawMaterialCategoryID/@identifier=$category]" />
        </xsl:for-each>

    </xsl:template>

    <xsl:template match="t:RawMaterialCategory">
    <![CDATA[Raw Material Category:  ]]><xsl:value-of select="t:Description/@description" />
    <![CDATA[------------------------------------------------]]>
    </xsl:template>


    <xsl:template match="t:RawMaterial">
        
    <![CDATA[RawMaterial ID: ]]><xsl:value-of select="t:RawMaterialID/@identifier" />
    <![CDATA[Description: ]]><xsl:value-of select="t:Description/@description" />
    <![CDATA[Unit Of Measurement: ]]><xsl:value-of select="t:UnitOfMeasurement/@unit_of_measurement" />
    <![CDATA[RawMaterialCategory ID: ]]><xsl:value-of select="t:RawMaterialCategoryID/@identifier" />
    <![CDATA[TechnicalFile: ]]><xsl:value-of select="t:TechnicalFile/@file" />
    
    <![CDATA[**********************************************************]]>
    </xsl:template>

</xsl:stylesheet>
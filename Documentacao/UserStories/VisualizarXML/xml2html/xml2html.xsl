﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:t="http://my_namespace"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <head>
                <meta charset="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" />
                <title>Factory</title>
                <p></p>
            </head>
            <body>
                <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

                <!-- <Menus> -->
                <h1>Chão de Fábrica</h1>
                <p></p>
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="menu1-tab" data-toggle="tab" href="#menu1" role="tab" aria-controls="details" aria-selected="true">Factory Details</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="menu2-tab" data-toggle="tab" href="#menu2" role="tab" aria-controls="products" aria-selected="false">Products</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="menu3-tab" data-toggle="tab" href="#menu3" role="tab" aria-controls="categories" aria-selected="false">Raw Material Categories</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="menu4-tab" data-toggle="tab" href="#menu4" role="tab" aria-controls="rm" aria-selected="false">Raw Materials</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="menu5-tab" data-toggle="tab" href="#menu5" role="tab" aria-controls="storage" aria-selected="false">Storages</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="menu6-tab" data-toggle="tab" href="#menu6" role="tab" aria-controls="orders" aria-selected="false">Production Orders</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="menu7-tab" data-toggle="tab" href="#menu7" role="tab" aria-controls="orderEx" aria-selected="false">Production Orders in Execution</a>
                        </li>
                    </ul>
                </ul>

                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="menu1" role="tabpanel" aria-labelledby="menu1-tab">
                        <p></p>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Products</th>
                                    <th scope="col">Raw-Mater</th>
                                    <th scope="col">Machines</th>
                                    <th scope="col">Production Lines</th>
                                    <th scope="col">Production Orders</th>
                                    <th scope="col">Production Orders in Execution</th>
                                </tr>
                            </thead>
                            <tbody>
                                <xsl:for-each select="/">
                                    <xsl:apply-templates select="/t:Factory"/>
                                </xsl:for-each>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="menu2" role="tabpanel" aria-labelledby="menu2-tab">
                        <p></p>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">ProductID</th>
                                    <th scope="col">CommercialID</th>
                                    <th scope="col">ShortDescription</th>
                                    <th scope="col">Description</th>
                                    <th scope="col">UnitOfMesurement</th>
                                    <th scope="col">Category</th>
                                </tr>
                            </thead>
                            <tbody>
                                <xsl:for-each select="/">
                                    <xsl:apply-templates select="/t:Factory/t:Products"/>
                                    <xsl:apply-templates select="t:Product" />
                                </xsl:for-each>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="menu3" role="tabpanel" aria-labelledby="menu3-tab">
                        <p></p>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">RawMaterialCategoryID</th>
                                    <th scope="col">Description</th>

                                </tr>
                            </thead>
                            <tbody>
                                <xsl:for-each select="/">
                                    <xsl:apply-templates select="/t:Factory/t:RawMaterialCategories"/>
                                    <xsl:apply-templates select="t:RawMaterialCategory" />
                                </xsl:for-each>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="menu4" role="tabpanel" aria-labelledby="menu4-tab">

                        <p></p>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">RawMaterialID</th>
                                    <th scope="col">Description</th>
                                    <th scope="col">UnitOfMeasurement</th>
                                    <th scope="col">RawMaterialCategoryID</th>
                                    <th scope="col">TechnicalFile</th>

                                </tr>
                            </thead>
                            <tbody>
                                <xsl:for-each select="/">
                                    <xsl:apply-templates select="/t:Factory/t:RawMaterials"/>
                                    <xsl:apply-templates select="t:RawMaterial" />
                                </xsl:for-each>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="menu5" role="tabpanel" aria-labelledby="menu5-tab">
                        <p></p>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">StorageUnitID</th>
                                    <th scope="col">Description</th>

                                </tr>
                            </thead>
                            <tbody>
                                <xsl:for-each select="/">
                                    <xsl:apply-templates select="/t:Factory/t:Storage"/>
                                    <xsl:apply-templates select="t:StorageUnit" />
                                </xsl:for-each>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="menu6" role="tabpanel" aria-labelledby="menu6-tab">
                        <p></p>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">ProductionOrderID</th>
                                    <th scope="col">DateOfIssue</th>
                                    <th scope="col">ExpectedExecutionDate</th>
                                    <th scope="col">ProductID</th>
                                    <th scope="col">ProductionOrderState</th>
                                    <th scope="col">OrderId</th>
                                    <th scope="col">Quantity</th>
                                    <th scope="col">UnitOfMesurement</th>
                                </tr>
                            </thead>
                            <tbody>
                                <xsl:for-each select="/">
                                    <xsl:apply-templates select="/t:Factory/t:ProductionOrders"/>
                                    <xsl:apply-templates select="t:ProductionOrder" />
                                </xsl:for-each>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="menu7" role="tabpanel" aria-labelledby="menu7-tab">
                        <p></p>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">ProductionOrderExecutionID</th>
                                    <th scope="col">ProductionLineID</th>
                                    <th scope="col">TotalGrossTime</th>
                                    <th scope="col">TotalEfectiveTime</th>
                                    <th scope="col">ExecutionDate</th>
                                    <th scope="col" style="background-color:grey"></th>
                                    <th scope="col">Machine</th>
                                    <th scope="col">Gross Execution Time</th>
                                    <th scope="col">Efective Exeution Time</th>
                                    <th scope="col" style="background-color:grey"></th>
                                    <th scope="col">Item ID Deviaton</th>
                                    <th scope="col">Quantity</th>
                                    <th scope="col">Type</th>
                                    <th scope="col" style="background-color:grey"></th>
                                    <th scope="col">Consumption Storage Unit ID</th>
                                    <th scope="col">Item ID </th>
                                    <th scope="col">Quantity</th>
                                    <th scope="col">Type</th>
                                    <th scope="col" style="background-color:grey"></th>
                                    <th scope="col">Production Batch ID</th>
                                    <th scope="col">Storage Unit ID</th>
                                    <th scope="col">Production Item ID </th>
                                    <th scope="col">Quantity</th>
                                    <th scope="col">Type</th>
                                </tr>
                            </thead>
                            <tbody>
                                <xsl:for-each select="/">
                                    <xsl:apply-templates select="/t:Factory/t:ProductionOrdersExecution"/>
                                    <xsl:apply-templates select="t:ProductionOrderExecution" />
                                </xsl:for-each>
                            </tbody>
                        </table>
                    </div>

                </div>

            </body>
        </html>
    </xsl:template>
    <!-- <Links> -->
    <xsl:template match="t:Factory">
        <tr>

            <td>
                <xsl:for-each select=".">
                    <xsl:value-of select="count(t:Products/t:Product)"/>
                </xsl:for-each>
            </td>
            <td>
                <xsl:for-each select=".">
                    <xsl:value-of select="count(t:RawMaterials/t:RawMaterial)"/>
                </xsl:for-each>
            </td>
            <td>
                <xsl:for-each select=".">
                    <xsl:value-of select="count(t:Machinery/t:Machine)"/>
                </xsl:for-each>
            </td>
            <td>
                <xsl:for-each select=".">
                    <xsl:value-of select="count(t:ProductionLines/t:ProductionLine)"/>
                </xsl:for-each>
            </td>
            <td>
                <xsl:for-each select=".">
                    <xsl:value-of select="count(t:ProductionOrders/t:ProductionOrder)"/>
                </xsl:for-each>
            </td>
            <td>
                <xsl:for-each select=".">
                    <xsl:value-of select="count(t:ProductionOrdersExecution/t:ProductionOrderExecution)"/>
                </xsl:for-each>
            </td>

        </tr>
    </xsl:template>
    <xsl:template match="t:Product">
        <tr>

            <td>
                <xsl:value-of select="t:ProductID/@id" />
            </td>
            <td>
                <xsl:value-of select="t:CommercialID/@id" />
            </td>
            <td>
                <xsl:value-of select="t:ShortDescription/@description" />
            </td>
            <td>
                <xsl:value-of select="t:Description/@description" />
            </td>
            <td>
                <xsl:value-of select="t:UnitOfMesurement/@Unit" />
            </td>
            <td>
                <xsl:value-of select="t:Category" />
            </td>

        </tr>
    </xsl:template>

    <xsl:template match="t:RawMaterialCategory">
        <tr>

            <td>
                <xsl:value-of select="t:RawMaterialCategoryID/@identifier" />
            </td>
            <td>
                <xsl:value-of select="t:Description/@description" />
            </td>


        </tr>
    </xsl:template>
    <xsl:template match="t:RawMaterial">
        <tr>

            <td>
                <xsl:value-of select="t:RawMaterialID/@identifier" />
            </td>
            <td>
                <xsl:value-of select="t:Description/@description" />
            </td>
            <td>
                <xsl:value-of select="t:UnitOfMeasurement/@unit_of_measurement" />
            </td>
            <td>
                <xsl:value-of select="t:RawMaterialCategoryID/@identifier" />
            </td>
            <td>
                <xsl:value-of select="t:TechnicalFile/@file" />
            </td>

        </tr>
    </xsl:template>
    <xsl:template match="t:StorageUnit">
        <tr>

            <td>
                <xsl:value-of select="t:StorageUnitID/@identifier" />
            </td>
            <td>
                <xsl:value-of select="t:Description/@description" />
            </td>

        </tr>
    </xsl:template>
    <xsl:template match="t:ProductionOrder">
        <tr>
            <td>
                <xsl:value-of select="t:ProductionOrderID/@identifier" />
            </td>
            <td>
                <xsl:value-of select="t:DateOfIssue/@date" />
            </td>
            <td>
                <xsl:value-of select="t:ExpectedExecutionDate/@date" />
            </td>
            <td>
                <xsl:value-of select="t:ProductID/@identifier" />
            </td>
            <td>
                <xsl:value-of select="t:ProductionOrderState/@state" />
            </td>
            <td>
                <xsl:apply-templates select="t:OrderId/@identifier" />
            </td>
            <td>
                <xsl:value-of select="t:Quantity/@quantity" />
            </td>
            <td>
                <xsl:value-of select="t:Quantity/@unit" />
            </td>

        </tr>
    </xsl:template>
    <xsl:template match="t:ProductionOrderExecution">
        <tr>

            <td>
                <xsl:value-of select="t:ProductionOrderExecutionID/@identifier" />
            </td>
            <td>
                <xsl:value-of select="t:ProductionLineID/@identifier" />
            </td>
            <td>
                <xsl:value-of select="t:TotalGrossTime/@time" />
            </td>
            <td>
                <xsl:value-of select="t:TotalEfectiveTime/@time" />
            </td>
            <td>
                <xsl:value-of select="t:ExecutionDate/@date" />
            </td>
            <td style="background-color:grey">
            </td>
            <td>
                <xsl:apply-templates select="t:MachineActivity/@MachineID" />
            </td>
            <td>
                <xsl:apply-templates select="t:MachineActivity/@GrossExecutionTime" />
            </td>
            <td>
                <xsl:apply-templates select="t:MachineActivity/@EfectiveExeutionTime" />
            </td>
            <td style="background-color:grey">
            </td>
            <td>
                <xsl:apply-templates select="t:Deviation/@artigoID" />
            </td>
            <td>
                <xsl:apply-templates select="t:Deviation/@quantity" />
            </td>
            <td>
                <xsl:apply-templates select="t:Deviation/@unit" />
            </td>
            <td style="background-color:grey">
            </td>
            <td>
                <xsl:apply-templates select="t:Consumption/@StorageUnitID" />
            </td>
            <td>
                <xsl:apply-templates select="t:Consumption/@artigoID" />
            </td>
            <td>
                <xsl:apply-templates select="t:Consumption/@quantity" />
            </td>
            <td>
                <xsl:apply-templates select="t:Consumption/@unit" />
            </td>
            <td style="background-color:grey">
            </td>
            <td>
                <xsl:apply-templates select="t:Production/@loteID" />
            </td>
            <td>
                <xsl:apply-templates select="t:Production/@StorageUnitID" />
            </td>
            <td>
                <xsl:apply-templates select="t:Production/@artigoID" />
            </td>
            <td>
                <xsl:apply-templates select="t:Production/@quantity" />
            </td>
            <td>
                <xsl:apply-templates select="t:Production/@unit" />
            </td>

        </tr>

    </xsl:template>
    <!-- Chamar templte para separar -->
    <xsl:template match="t:OrderId/@identifier">
        <xsl:for-each select=".">
            <xsl:call-template name="addTrim"/>
        </xsl:for-each>
    </xsl:template>

    <!-- Separador -->
    <xsl:template name="addTrim">
        <xsl:value-of select="."/>
        <xsl:for-each select="."> 
            <xsl:text> , </xsl:text>
        </xsl:for-each>
    </xsl:template>

    <!-- Chamar templte para dar enter -->
    <!-- Machine activity -->
    <xsl:template match="t:MachineActivity/@MachineID">
        <xsl:for-each select=".">
            <xsl:call-template name="addLine"/>
        </xsl:for-each>
    </xsl:template>
    <xsl:template match="t:MachineActivity/@GrossExecutionTime">
        <xsl:for-each select=".">
            <xsl:call-template name="addLine"/>
        </xsl:for-each>
    </xsl:template>
    <xsl:template match="t:MachineActivity/@EfectiveExeutionTime">
        <xsl:for-each select=".">
            <xsl:call-template name="addLine"/>
        </xsl:for-each>
    </xsl:template>
    <!-- Deviation -->

    <xsl:template match="t:Deviation/@artigoID">
        <xsl:for-each select=".">
            <xsl:call-template name="addLine"/>
        </xsl:for-each>
    </xsl:template>
    <xsl:template match="t:Deviation/@StorageUnitID">
        <xsl:for-each select=".">
            <xsl:call-template name="addLine"/>
        </xsl:for-each>
    </xsl:template>
    <xsl:template match="t:Deviation/@quantity">
        <xsl:for-each select=".">
            <xsl:call-template name="addLine"/>
        </xsl:for-each>
    </xsl:template>
    <xsl:template match="t:Deviation/@unit">
        <xsl:for-each select=".">
            <xsl:call-template name="addLine"/>
        </xsl:for-each>
    </xsl:template>

    <!-- Consumo -->
    <xsl:template match="t:Consumption/@StorageUnitID">
        <xsl:for-each select=".">
            <xsl:call-template name="addLine"/>
        </xsl:for-each>
    </xsl:template>
    <xsl:template match="t:Consumption/@artigoID">
        <xsl:for-each select=".">
            <xsl:call-template name="addLine"/>
        </xsl:for-each>
    </xsl:template>
    <xsl:template match="t:Consumption/@quantity">
        <xsl:for-each select=".">
            <xsl:call-template name="addLine"/>
        </xsl:for-each>
    </xsl:template>
    <xsl:template match="t:Consumption/@unit">
        <xsl:for-each select=".">
            <xsl:call-template name="addLine"/>
        </xsl:for-each>
    </xsl:template>

    <!-- Produção -->
    <xsl:template match="t:Production/@loteID">
        <xsl:for-each select=".">
            <xsl:call-template name="addLine"/>
        </xsl:for-each>
    </xsl:template>
    <xsl:template match="t:Production/@StorageUnitID">
        <xsl:for-each select=".">
            <xsl:call-template name="addLine"/>
        </xsl:for-each>
    </xsl:template>
    <xsl:template match="t:Production/@artigoID">
        <xsl:for-each select=".">
            <xsl:call-template name="addLine"/>
        </xsl:for-each>
    </xsl:template>
    <xsl:template match="t:Production/@quantity">
        <xsl:for-each select=".">
            <xsl:call-template name="addLine"/>
        </xsl:for-each>
    </xsl:template>
    <xsl:template match="t:Production/@unit">
        <xsl:for-each select=".">
            <xsl:call-template name="addLine"/>
        </xsl:for-each>
    </xsl:template>

    <!--  enter -->
    <xsl:template name="addLine">
        <xsl:value-of select="."/>
        <xsl:for-each select=".">
            <p></p>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
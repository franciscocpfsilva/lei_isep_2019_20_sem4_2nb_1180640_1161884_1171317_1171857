<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:t="http://my_namespace"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">

        <html>
            <head>
                <meta charset="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" />
                <title>Factory</title>

                <p></p>
            </head>
            <body>
                <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

                <!-- <Menus> -->
                <h1>Products with no Production Sheet</h1>
                <p></p>
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="menu1-tab" data-toggle="tab" href="#menu1" role="tab" aria-controls="details" aria-selected="true">Factory Details</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="menu2-tab" data-toggle="tab" href="#menu2" role="tab" aria-controls="products" aria-selected="false">Products</a>
                        </li>
                    </ul>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="menu1" role="tabpanel" aria-labelledby="menu1-tab">
                        <p></p>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Products</th>
                                    <th scope="col">Raw-Mater</th>
                                    <th scope="col">Machines</th>
                                    <th scope="col">Production Lines</th>
                                    <th scope="col">Production Orders</th>
                                    <th scope="col">Production Orders in Execution</th>
                                </tr>
                            </thead>
                            <tbody>
                                <xsl:for-each select="/">
                                    <xsl:apply-templates select="/t:Factory"/>
                                </xsl:for-each>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="menu2" role="tabpanel" aria-labelledby="menu2-tab">
                        <p></p>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">ProductID</th>
                                    <th scope="col">CommercialID</th>
                                    <th scope="col">ShortDescription</th>
                                    <th scope="col">Description</th>
                                    <th scope="col">UnitOfMesurement</th>
                                    <th scope="col">Category</th>
                                </tr>
                            </thead>
                            <tbody>
                                <xsl:for-each select="/">
                                    <xsl:apply-templates select="/t:Factory/t:Products"/>
                                </xsl:for-each>
                            </tbody>
                        </table>
                    </div>
                </div>
            </body>
        </html>
    </xsl:template>
    <!-- <Links> -->
    <xsl:template match="t:Factory">
        <tr>

            <td>
                <xsl:for-each select=".">
                    <xsl:value-of select="count(t:Products/t:Product)"/>
                </xsl:for-each>
            </td>
            <td>
                <xsl:for-each select=".">
                    <xsl:value-of select="count(t:RawMaterials/t:RawMaterial)"/>
                </xsl:for-each>
            </td>
            <td>
                <xsl:for-each select=".">
                    <xsl:value-of select="count(t:Machinery/t:Machine)"/>
                </xsl:for-each>
            </td>
            <td>
                <xsl:for-each select=".">
                    <xsl:value-of select="count(t:ProductionLines/t:ProductionLine)"/>
                </xsl:for-each>
            </td>
            <td>
                <xsl:for-each select=".">
                    <xsl:value-of select="count(t:ProductionOrders/t:ProductionOrder)"/>
                </xsl:for-each>
            </td>
            <td>
                <xsl:for-each select=".">
                    <xsl:value-of select="count(t:ProductionOrdersExecution/t:ProductionOrderExecution)"/>
                </xsl:for-each>
            </td>

        </tr>
    </xsl:template>
    <xsl:template match="t:Product">
        <xsl:choose>
            <xsl:when test="t:ProductionSheet">
            </xsl:when>
            <xsl:otherwise>
                <tr>

                    <td>
                        <xsl:value-of select="t:ProductID/@id" />
                    </td>
                    <td>
                        <xsl:value-of select="t:CommercialID/@id" />
                    </td>
                    <td>
                        <xsl:value-of select="t:ShortDescription/@description" />
                    </td>
                    <td>
                        <xsl:value-of select="t:Description/@description" />
                    </td>
                    <td>
                        <xsl:value-of select="t:UnitOfMesurement/@Unit" />
                    </td>
                    <td>
                        <xsl:value-of select="t:Category" />
                    </td>

                </tr>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- Chamar templte para separar -->
    <xsl:template match="t:OrderId/@identifier">
        <xsl:for-each select=".">
            <xsl:call-template name="addTrim"/>
        </xsl:for-each>
    </xsl:template>

    <!-- PERGUNTAR AO PROF-->
    <!-- Separador -->
    <xsl:template name="addTrim">
        <xsl:value-of select="."/>
        <xsl:for-each select=".">
            <xsl:text> , </xsl:text>
        </xsl:for-each>
    </xsl:template>

</xsl:stylesheet>
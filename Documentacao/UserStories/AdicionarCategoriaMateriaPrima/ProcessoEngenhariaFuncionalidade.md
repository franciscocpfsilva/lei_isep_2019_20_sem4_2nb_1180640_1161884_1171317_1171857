**Aluno [1171317](../)** - US2002 - Definir uma Categoria de Matéria-prima
=======================================

# 1. Requisitos

*Nesta secção o estudante deve indicar a funcionalidade desenvolvida bem como descrever a sua interpretação sobre a mesma e sua correlação e/ou dependência de/com outros requisitos.*

**US2002** Como Gestor de Produção, eu pretendo definir uma nova categoria de matérias-primas.

Esta é umas das mais simples US, e permite a definição de uma nova categoria que por sua vez serão usadas na caracterização de novas matérias-primas. Esta US não depende de outros requisitos. 

Regras de negócio (identificadas apenas através do forum para esclarecimento de requisitos):
- Uma categoria de matéria-prima é caracterizada por um código alfanumérico e por uma descrição.
- [O código alfanumérico terá um máximo de 10 caracteres.](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=29285)
Regras de negócio (criadas por coerência com outras UCs):
- A descrição da Categoria de Matérias-primas poderá ter no máximo até 40 caracteres.

# 2. Análise

*Neste secção o estudante deve relatar o estudo/análise/comparação que fez com o intuito de tomar as melhores opções de design para a funcionalidade bem como aplicar diagramas/artefactos de análise adequados.*

*Recomenda-se que organize este conteúdo por subsecções.*

**O Modelo de Domínio (MD) (a 06/05/2020)**
O conceito Categoria de Matéria-prima é, no nosso Modelo de Domínio, representado como uma entidade pertencente ao agregado de Matérias-primas. Esta foi uma assunção que agora se verifica incorreta. Pela atual versão do MD, estaríamos a criar uma dependência entre a persistência de _Matérias-primas_ e _Categorias de Matérias-primas_. Assim sendo, incorremos na correção do MD da seguinte forma: ao invés de um agregado para Matérias-primas e Categorias, ficaremos com dois agregados, um para cada um dos conceitos.


**O Modelo de Domínio (MD) (a 09/05/2020)**

![RawMaterialCategory_and_RawMAterial_Aggregates](RawMaterialCategory_and_RawMAterial_Aggregates.png)

O conceito _Categoria de Matária-prima_ é agora _root-entity_ do seu próprio agregado. 

Atributos da Entidade Categoria de Matéria-prima:
- código identificador - value-object
- descrição - value-object

A Categoria de Matéria-prima será criada pelo controlador da US e persistida no repositório do agregado correspondente. À semelhança de outras entidades de domínio, o atributo descrição não é representado como um value-object, apesar de o ser.


# 3. Design

*Nesta secção o estudante deve descrever o design adotado para satisfazer a funcionalidade. Entre outros, o estudante deve apresentar diagrama(s) de realização da funcionalidade, diagrama(s) de classes, identificação de padrões aplicados e quais foram os principais testes especificados para validar a funcionalidade.*

*Para além das secções sugeridas, podem ser incluídas outras.*

## 3.1. Realização da Funcionalidade

*Nesta secção deve apresentar e descrever o fluxo/sequência que permite realizar a funcionalidade.*

O diagrama de sequencia que se segue procura honestamente emular a funcionalidade de _Register Dish Type_ do projeto _eCafetaria_, tal é o paralelismo estabelecido entre a _Categoria de Materias-primas_<->_Matéria-prima_ e _Tipo de Prato_<->_Prato_.

![SD_US2002_AddRawmaterialCategory](SD_US2002_AddRawmaterialCategory.png)

## 3.2. Diagrama de Classes

*Nesta secção deve apresentar e descrever as principais classes envolvidas na realização da funcionalidade.*

As classes de domínio para esta US podem ser encontradas no package _eapli.base.rawmaterials.domain_ do projeto base.core. Tal como estas, as restantes classes infracitadas, são representadas de forma minimalista, sem os atributos e métodos que as caracterizam.

![CD_US2002_AddRawmaterialCategory](CD_US2002_AddRawmaterialCategory.png)

## 3.3. Padrões Aplicados
*Nesta secção deve apresentar e explicar quais e como foram os padrões de design aplicados e as melhores práticas*

- Padrão **Repository:** aplicado à camada de persistência, permite esconder os detalhes da persistência da camada de doménio. 

## 3.4. Testes 
*Nesta secção deve sistematizar como os testes foram concebidos para permitir uma correta aferição da satisfação dos requisitos.*

**Teste 1:** Verificar que não é possível criar uma instância da classe RawMaterialCategoryID com o código a nulo.

**Teste 2:** Verificar que não é possível criar uma instância da classe RawMaterialCategoryID com o código vazio.

**Teste 3:** Verificar que não é possível criar uma instância da classe RawMaterialCategoryID com um código que exceda os 10 caracteres.

**Teste 4:** Verificar que não é possível criar uma instância da classe RawMaterialCategoryID com um código que tenha mais do que uma palavra.

**Teste 5:** Verificar que é possível criar uma instância da classe RawMaterialCategoryID com um código alfanumérico.

**Teste 6:** Verificar que não é possível que o código da classe RawMaterialCategoryID tenha caracteres especiais (ex: %, /, _, ?).

**Teste 7:** Verificar que não é possível criar uma instância da classe RawMaterialCategory com uma descriçáo que tenha mais do 40 caracteres.

**Teste 8:** Verificar que não é possível criar uma instância da classe RawMaterialCategory com valores nulos.

	@Test(expected = IllegalArgumentException.class)
		public void ensureRawMaterialCategoryIDCanNotNull() {
			System.out.println("category must have an identifier");
			new RawMaterialCategory(null, DESCRIPTION);
		}

# 4. Implementação

*Nesta secção o estudante deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;*

*Recomenda-se que organize este conteúdo por subsecções.*

Novamente aqui se faz referência à semelhança entre esta US e a US _RegisterDishType_ do projeto eCafetaria, e na forma como as classes criadas para implementar esta US em quase tudo emulam as classes que fazem a US análoga no projeto eCafetaria funcionar.

# 5. Integração/Demonstração

*Nesta secção o estudante deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

Aqui quizemos cumprir os requisitos pedidos enquanto mantemos a independência entre as funcionalidades desenvolvidas. Procuramos mantermo-nos fieis aos princípios de responsabilidade única e desacoplamento entre camadas. 

# 6. Observações

*Nesta secção sugere-se que o estudante apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*

Processos de engenharia talvez mais concisos e uso de designs genéricos transversais a várias User Stories. 



**Aluno [1171857](../)** - US5001 - Como Serviço de Processamento de Mensagens(SPM), pretendo efetuar o processamento das mensagens disponiveis no sistema.
=======================================

# 1. Requisitos

*Nesta secção o estudante deve indicar a funcionalidade desenvolvida bem como descrever a sua interpretação sobre a mesma e sua correlação e/ou dependência de/com outros requisitos.*

**US5001**  Como Serviço de Processamento de Mensagens(SPM), pretendo efetuar o processamento das mensagens disponiveis no sistema.

A interpretação feita deste requisito foi no sentido de processar as mensagem que se encontram no sistema. A informação obtida nas mensagens será armazenada/guardada na Execução de Ordem de Produção. A Execução de Ordem de Produção cotém varia informação desde tempos efetivos e brutos das maquinas, tempos efetivos e brutos totais, consumos, desvios, produção e lotes(não obrigatório). Esta User Story, depende da Ordem de Produção, sem uma ordem de produção valida o sistema gera uma notificação de Erro, este erro é classificado como impeditivo isto é termina o processamento de todas as mensagens! Ainda em relação aos erros, caso ocorra um erro, referente ao Produto e/ou depósito (A não existencia do mesmo) é gerada a notificação de erro, mas o sistema continuar a processar as restantes mensagens! Estas notificações serão tratadas posteriormente.

Regras de negócio (identificadas apenas através do forum para esclarecimento de requisitos):
- [O SPM necessita obrigatoriamente: do intervalo de tempo; as linhas de produção](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=30684#p41026)
- [Gerar notificações de erro e permitir o tratamento de erros impeditivos (ex: Ordem de produção não existir)](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=30857#p41231)

# 2. Análise

*Neste secção o estudante deve relatar o estudo/análise/comparação que fez com o intuito de tomar as melhores opções de design para a funcionalidade bem como aplicar diagramas/artefactos de análise adequados.*

*Recomenda-se que organize este conteúdo por subsecções.*

Dado a importancia da Execução da Ordem de Produção será uma *entity*, que estará relacionada com a Ordem de Produção e com uma Linha de Produção. Esta user story será uma Sistema de Processamento de Mensagens, não tem utilizador e será processada num determinado bloco de tempo pré-definido na inicialização da aplicação. 

* **Não podem ser null:**
    * Desvio;
    * Consumo;
    * Produção;
* **Não podem ser negativos:**
    * Tempo de execução bruto;
    * Tempo de execução efetivo;
* **Podem ser null:**
    * Lote;
    


O modelo de dominio responde ao fluxo da User Story, só é necessário adicionar um tipo ao desvio, para distinguir desvio de consumo e desvio de produção.
Foi necessário também adicionar uma relação entre a ordem de execução e a ficha de produção para a informação sobre calculos de desvios e produções serem validas mesmo que alguém no futuro altere a ficha de produção.

![](Domain_Model.svg)

# 3. Design

*Nesta secção o estudante deve descrever o design adotado para satisfazer a funcionalidade. Entre outros, o estudante deve apresentar diagrama(s) de realização da funcionalidade, diagrama(s) de classes, identificação de padrões aplicados e quais foram os principais testes especificados para validar a funcionalidade.*

*Para além das secções sugeridas, podem ser incluídas outras.*

## 3.1. Realização da Funcionalidade

*Nesta secção deve apresentar e descrever o fluxo/sequência que permite realizar a funcionalidade.*

![](DiagramaSequencia.svg)

Este diagrama de sequencia é referente a prespetiva geral desta User Story, cada tipo de processamento de mensagem irá implementar as suas proprias validações, atualizar a ordem de execução e persistir as atualizações na base de dados. No entanto a base esta representada no diagrama de sequencia. Um interface generico e depois são implementados os varios tipos processamentos.
O sistema recebe como parametros a data de inicio, a data de fim que pretende processar as mensagens, e uma lista de ID's de linha de produção. Esta lista cotem as linhas que irão processar as rawMessages que se encontram no sistema! 
Existem duas particularidades no sistema.
A primeira é, se não existir a ordem de produção referente a primeira mensagem de cada maquina, o sistema para o processamento de todas as mensagens para aquela linha de produção. Dado que uma ordem de execução só corresponde a uma Ordem de produção, e se esta nao existir não é possivel criar a ordem de execução, logo não podemos adicionar/atualizar a informação da ordem de produção.
A segunda particularidade, é que erros do tipo não existecia produtos, matérias primas e/ou depositos com os ID's fornecidos nas mensagens, o sistema altera o estado da mensage para erro, cria uma mensagem de Error, e continua a processar as proximas mensagens!

## 3.2. Diagrama de Classes

*Nesta secção deve apresentar e descrever as principais classes envolvidas na realização da funcionalidade.*

![](Diagrama_Classes.svg)

## 3.3. Padrões Aplicados
*Nesta secção deve apresentar e explicar quais e como foram os padrões de design aplicados e as melhores práticas*

**Protected Variation (GRASP)** Dada a possibilidade de existir vários tipos de mensagens, identificamos aqui um ponto de variação no nosso sistema e é portanto inevitável não pensar neste padrão, ou seja, a atribuição de responsabilidades de modo a criar uma interface estável à volta deste ponto.

**Template Method (GoF)** Apesar dos diversos tipos de mensagens, a sequência de passos a realizar em cada um deles será a mesma: ler uma *rawMessage*, verificar o Tipo de Mensagem e aplicar a alterações para cada tipo de mensagem. Sendo assim, temos aqui um bom candidato para aplicar o padrão Template Method, que nos permite definir a estrutura de um algoritmo, dando liberdade na implementação de alguns passos do algoritmo.

**Strategy (GoF)** Finalmente, uma vez que queremos permitir o processamento de vários tipos de mensagens, portanto, podemos resolver este problema com o padrão Strategy. Este padrão permite fazer muitas implementações da mesma interface e permitir que a aplicação selecione uma. E portanto engloba na sua definição o Protected Variation, um padrão mais genérico.

## 3.4. Testes 
*Nesta secção deve sistematizar como os testes foram concebidos para permitir uma correta aferição da satisfação dos requisitos.*

**ErrorMessage Class**

**Teste 1:** Verificar que não é possível criar uma instância de classe *ErrorMessage* com a RawMessage a null.

```
@Test(expected = IllegalArgumentException.class)
public void ensureRawMessageNullIsNotAllowed() {
        ErrorMessage instance = new ErrorMessage(null, ErrorType.NO_PROCT_OR_RAW_MATERIAL);
}
```

**Teste 2:** Verificar que não é possível criar uma instância de classe *ErrorMessage* com a ErrorType a null.

```
@Test(expected = IllegalArgumentException.class)
public void ensureErrorTypeNullIsNotAllowed() {
    rawMessage = new RawMessage("M750", MessageType.valueOf(attributes[POS_MSG_TYPE], datetime, line));
    instance = new ErrorMessage(rawMessage, null);
}
```

**ErrorType Class**

**Teste 1:** Verificar que não é possível criar uma instância de classe *ErrorType* a null.

```
@Test(expected = IllegalArgumentException.class)
public void ensureNullIsNotAllowed() {
    instance = new ErrorType(null);
}
```

**Teste 2:** Verificar que não é possível criar uma instância de classe *ErrorType* desconhecido.

```
@Test(expected = IllegalArgumentException.class)
public void unknowMessageTypeIsNotAllowed() {
    instance = new ErrorType("NoProductionLine");
}
```

**OrderProductionExecution Class**

**Teste 1:** Verificar que não é possível criar uma instância de classe *ProductionOrderID* a null.
```
@Test(expected = IllegalArgumentException.class)
public void ensureProductionOrderIDNullIsNotAllowed() {
    instance = new ProductionOrderExecution(null, "L123", date);
}
```
**Teste 2:** Verificar que não é possível criar uma instância de classe *ProductionLine* a null.
```
@Test(expected = IllegalArgumentException.class)
public void ensureProductionLineNullIsNotAllowed() {
    instance = new ProductionOrderExecution("PO103", null, date);
}
```

**Teste 3:** Verificar que não é possível criar uma instância de classe *ExecutionDate* com data futura.
```
@Test(expected = IllegalArgumentException.class)
public void ensureExecutionDateNullIsNotAllowed() {
    LocalDateTime date = LocalDateTime.parse("03-06-2030");
    instance = new ProductionOrderExecution("PO103", "L123" , date);
}
```

**Teste 4:** Verificar que não é possível criar uma instância de classe *ExecutionDate* a null.
```
@Test(expected = IllegalArgumentException.class)
public void ensureExecutionDateNullIsNotAllowed() {
    instance = new ProductionOrderExecution("PO103", "L123" , null);
}
```

**MachineActivity Class**

**Teste 1:** Verificar que não é possível criar uma instância de classe *MachineActivity* com *MachineID* null;

```
    @Test(expected = IllegalArgumentException.class)
    public void ensureMachineIdNullIsNotAllowed() {
        MachineActivity instance = MachineActivity.valueOf(null, startTime, mType);
    }
```

**Teste 2:** Verificar que não é possível criar uma instância de classe *MachineActivity* com *StartTime* null;

```
    @Test(expected = IllegalArgumentException.class)
    public void ensureStartTimeNullIsNotAllowed() {
        MachineActivity instance = MachineActivity.valueOf(machineID, null, mType);

    }
```

**Teste 3:** Verificar que não é possível criar uma instância de classe *MachineActivity* com *MessageType* null;

```
    @Test(expected = IllegalArgumentException.class)
    public void ensureMessageTypeNullIsNotAllowed() {
        MachineActivity instance = MachineActivity.valueOf(machineID, startTime, null);

    }
```

**Desviation Class**

**Teste 1:** Verificar que não é possível criar uma instância de classe *Desviation* com o *Item* a null.

```
    @Test(expected = IllegalArgumentException.class)
    public void ensureItemNullIsNotAllowed() {
        Desviation instance = Desviation.valueOf(null, PRODUCTION);
    }
```

**Teste 2:** Verificar que não é possível criar uma instância de classe *Desviation* com o *DesviationType* a null.

```
    @Test(expected = IllegalArgumentException.class)
    public void ensureDesviationTypeNullIsNotAllowed() {
        Desviation instance = Desviation.valueOf(item, null);
    }
```

**Consumption Class**

**Teste 1:** Verificar que não é possível criar uma instância de classe *Consumption* com *StorageUnitID* a null.

```
    @Test(expected = IllegalArgumentException.class)
    public void ensureStorageUnitIDNullIsNotAllowed() {
        Consumption instance = Consumption.valueOf(null, item);
    }
```

**Teste 2:** Verificar que não é possível criar uma instância de classe *Consumption* com *Item* a null.

```
    @Test(expected = IllegalArgumentException.class)
    public void ensureItemNullIsNotAllowed() {
        Consumption instance = Consumption.valueOf(storageID, null);
    }
```

**Production Class**

**Teste 1:** Verificar que não é possível criar uma instância de classe *Production* com *Item* a null.

```
    @Test(expected = IllegalArgumentException.class)
    public void ensureItemNullIsNotAllowed() {
        Production instance = Production.valueOf(lot, null);
    }
```

**Batch Class**

**Teste 1:** Verificar que não é possível criar uma instância de classe *Batch* com *id* a null.

```
     @Test(expected = IllegalArgumentException.class)
    public void ensureIDNullIsNotAllowed() {
        Batch instance = Batch.valueOf(null);
    } 
```

**EfectiveExecutionTime Class**

**Teste 1:** Verificar que não é possível criar uma instância de classe *EfectiveExecutionTime* com *MachineID* a null.


```
    @Test(expected = IllegalArgumentException.class)
    public void ensureMachineIDNullIsNotAllowed() {
        EfectiveExecutionTime instance = EfectiveExecutionTime.valueOf(null, lst);
    }
```

**Teste 2:** Verificar que não é possível criar uma instância de classe *EfectiveExecutionTime* com o mapa a null.

```
    @Test(expected = IllegalArgumentException.class)
    public void ensureHashMapNullIsNotAllowed() {
        EfectiveExecutionTime instance = EfectiveExecutionTime.valueOf(machineID, null);
    }
```

**GrossExecutionTime Class**

**Teste 1:** Verificar que não é possível criar uma instância de classe *GrossExecutionTime* com *MachineID* a null.

```
    @Test(expected = IllegalArgumentException.class)
    public void ensureListNullIsNotAllowed() {
        GrossExecutionTime instance = GrossExecutionTime.valueOf(null, machineID);
    }
```

**Teste 2:** Verificar que não é possível criar uma instância de classe *GrossExecutionTime* com a lista a null.

```
    @Test(expected = IllegalArgumentException.class)
    public void ensureMachineIDNullIsNotAllowed() {
        GrossExecutionTime instance = GrossExecutionTime.valueOf(lst, null);
    }
```

**ProductionOrderExecution Class**

**Teste 1:** Verificar que não é possível criar uma instância de classe *ProductionOrderExecution* com *ProductionOrderID* a null.

```
    @Test(expected = IllegalArgumentException.class)
    public void ensureProductionOrderIdNullIsNotAllowed() {
        ProductionOrderExecution instance = new ProductionOrderExecution(null,
                strartTime);
    }
```

**Teste 2:** Verificar que não é possível criar uma instância de classe *ProductionOrderExecution* com *generationDateTime* a null.

```
    @Test(expected = IllegalArgumentException.class)
    public void ensureGenerationDataTimeNullIsNotAllowed() {
        ProductionOrderExecution instance = new ProductionOrderExecution(productionOrderID,
                null);
    }
```

**Teste 3:** Verificar que não é possível adicionar a lista de *Production* uma instancia de *Production* a null.

```
    @Test(expected = IllegalArgumentException.class)
    public void ensureAddProductionNullIsNotAllowed() {
        ProductionOrderExecution instance = new ProductionOrderExecution(productionOrderID,
                startTime);
        instance.addToProduction(null);
    }
```

**Teste 4:** Verificar que não é possível adicionar a lista de *Chargeback* uma instancia de *Chargeback* a null.

```
    @Test(expected = IllegalArgumentException.class)
    public void ensureAddChargebackNullIsNotAllowed() {
        ProductionOrderExecution instance = new ProductionOrderExecution(productionOrderID,
                startTime);
        instance.addToChargeback(null);
    }
```

**Teste 5:** Verificar que não é possível adicionar a lista de *Consumption* uma instancia de *Consumption* a null.

```
    @Test(expected = IllegalArgumentException.class)
    public void ensureAddConsumptionNullIsNotAllowed() {
        ProductionOrderExecution instance = new ProductionOrderExecution(productionOrderID,
                startTime);
        instance.addToConsumption(null);
    }
```

**Teste 6:** Verificar que não é possível adicionar a lista de *ProductionDelivery* uma instancia de *ProductionDelivery* a null.

```
    @Test(expected = IllegalArgumentException.class)
    public void ensureAddProductionDeliveryNullIsNotAllowed() {
        ProductionOrderExecution instance = new ProductionOrderExecution(productionOrderID,
                startTime);
        instance.addToProductionDelivery(null);
    }
```

**Teste 7:** Verificar que não é possível adicionar a lista de *Desviation* uma instancia de *Desviation* a null.

```
    @Test(expected = IllegalArgumentException.class)
    public void ensureAddDesviationNullIsNotAllowed() {
        ProductionOrderExecution instance = new ProductionOrderExecution(productionOrderID,
                startTime);
        instance.addToDesviations(null);
    }
```

**Teste 8:** Verificar que não é possível adicionar a lista de *MachineActivity* uma instancia de *MachineActivity* a null.

```
    @Test(expected = IllegalArgumentException.class)
    public void ensureAddMachineActivityNullIsNotAllowed() {
        ProductionOrderExecution instance = new ProductionOrderExecution(productionOrderID,
                startTime);
        instance.addToMachineActivity(null);
    }
```

**Chargeback Class**

**Teste 1:** Verificar que não é possível criar uma instância de classe *Chargeback* com *StoragenUnitID* a null.

```
    @Test(expected = IllegalArgumentException.class)
    public void ensureStorageUnitIDNullIsNotAllowed() {
        Chargeback instance = Chargeback.valueOf(item, null);
    }
```

**Teste 2:** Verificar que não é possível criar uma instância de classe *Chargeback* com *Item* a null.

```
    @Test(expected = IllegalArgumentException.class)
    public void ensureItemNullIsNotAllowed() {
        Chargeback instance = Chargeback.valueOf(null, storageID);
    }
```

**ProductionDelivery Class**

**Teste 1:** Verificar que não é possível criar uma instância de classe *ProductionDelivery* com *StorageUnitID* a null.

```
    @Test(expected = IllegalArgumentException.class)
    public void ensureStorageUnitIDNullIsNotAllowed() {
        ProductionDelivery instance = ProductionDelivery.valueOf(production, null);
    }
```
**Teste 2:** Verificar que não é possível criar uma instância de classe *ProductionDelivery* com *Production* a null.

```
    @Test(expected = IllegalArgumentException.class)
    public void ensureProductionNullIsNotAllowed() {
        ProductionDelivery instance = ProductionDelivery.valueOf(null, storageID);
    }
```

# 4. Implementação

*Nesta secção o estudante deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;*

![](CreateListOfProductionLines.svg)

Esta classe foi criada com intuito de guarda a informação sobre as linhas de produção que se encontram ativas ou não. Contem um mapa de ProductionLineID e um boolean. O boolean representa o estado do processamento da linha Ativo/Desativo respetivamente true/false.

![](ChangeProductionLineStatus.svg)

O metodo changeProductionLineStatus, atualiza o estado de processamento da linha recebendo uma lista como paramentro, esta lista é recebida como um conjunto de argumentos no arranque da aplicação.

![](Thread.svg)

O metodo run é responsavel por criar uma thread para cada linha de produção!
De notar que são criadas threads para todas as linhas, mas como é possivel visualizar na imagem anterior, só as que tem o estado de processamento ativo é que realmente efetuam o processamento das mensagens!

![](ProcessMessageController.svg)

Inicialmente procura por todas as rawMessages que estao entre as datas fornecidas e que estejam no estado de *UNPROCESSED*.

![](MessageTypeFactory.svg)

Factory de mensagens types. Também é possivel observar as classes que implementam a interface ProcessaMessage.

![](InterfaceProcessMessage.svg)

Interface que processa as mensagens!
 
*Recomenda-se que organize este conteúdo por subsecções.*



# 5. Integração/Demonstração

*Nesta secção o estudante deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

Esta funcionalidade dependia das rawMessages. As rawMessages já estavam processadas e validados com informações importantes facilitando a intregação com o mesmo.


# 6. Observações

*Nesta secção sugere-se que o estudante apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*

Nesta User Story senti na pele a importancia de fazer um bom planeamento, pois era muito extensa e complexa. Dado que era muito extensa também poderia ter sido dividida entre o grupo.
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:t="http://www.dei.isep.ipp.pt/lprog"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/relatório">
        <html>

        <head>
            <meta charset="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
                integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
                crossorigin="anonymous" />
            <title>Relatório</title>
            <p></p>
        </head>

        <body>
            <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
                integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
                crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
                integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
                crossorigin="anonymous"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
                integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
                crossorigin="anonymous"></script>

            <!--ROOT-->
            <div id="relat">
                <xsl:apply-templates select="t:relatório" />
            </div>
        </body>

        </html>
    </xsl:template>

    <!-- templates -->
    <xsl:template match="t:relatório">
        <xsl:apply-templates select="t:páginaRosto" />
        <xsl:apply-templates select="t:corpo" />
        <xsl:apply-templates select="t:anexos" />
    </xsl:template>

    <xsl:template match="t:corpo">
        <div>
            <xsl:apply-templates select="t:introdução" />
            <xsl:apply-templates select="t:outrasSecções" />
            <xsl:apply-templates select="t:conclusão" />
            <xsl:apply-templates select="t:referências" />
        </div>
    </xsl:template>

    <xsl:template match="t:outrasSecções">
        <div>
            <xsl:apply-templates select="t:análise" />
            <xsl:apply-templates select="t:linguagem" />
            <xsl:apply-templates select="t:transformações" />
        </div>
    </xsl:template>

    <!-- paginarosto -->
    <xsl:template match="t:páginaRosto">
        <div align="center">
            <img>
            <xsl:attribute name="src">
                <xsl:value-of select="t:logotipoDEI" />
            </xsl:attribute>
            </img>
            <h1 align="center" class="display-3">
                <xsl:value-of select="t:tema" />
            </h1>
            <h3>
                <xsl:value-of select="t:disciplina/t:designação" />
            </h3>
            <h3>
                <xsl:value-of select="t:disciplina/t:anoCurricular" /> Ano
                <xsl:value-of select="t:disciplina/t:sigla" />
            </h3>
        </div>
        <div align="text-right">
            <h3>Realizado por:</h3>
            <xsl:for-each select="t:autor">
                <p>
                    <a href="mailto:#{generate-id(mail)}">
                        <xsl:value-of select="t:nome" />
                    </a>
                    -
                    <xsl:value-of select="t:número" />
                </p>
            </xsl:for-each>
        </div>
        <div align="right">
            <h3>
                <xsl:value-of select="t:data" />
                -
                <xsl:value-of select="t:turma" />
            </h3>
            <h3>
            </h3>
            <xsl:for-each select="t:professor">
                <xsl:value-of select="@sigla" />
                (
                <xsl:value-of select="@tipoAula" />)
                ;
            </xsl:for-each>
        </div>
    </xsl:template>

    <!--introdução-->
    <xsl:template match="t:introdução">
        <br></br>
        <br></br>
        <br></br>
        <h1 align="center" class="display-3">
            <xsl:value-of select="@tituloSecção" />
        </h1>
        <xsl:for-each select="./*">
            <xsl:choose>
                <xsl:when test="local-name()='parágrafo'">
                    <p>
                        <xsl:value-of select="." />
                    </p>
                </xsl:when>
                <xsl:when test="local-name()='listaItems'">
                    <xsl:for-each select="t:item">
                        <xsl:apply-templates select="." />
                    </xsl:for-each>
                </xsl:when>
                <xsl:when test="local-name()='subsecção'">
                    <p style="font-weight:bold">
                        <xsl:value-of select="." />
                    </p>
                </xsl:when>
                <xsl:otherwise></xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>

    <!--análise-->
    <xsl:template match="t:análise">
        <br></br>
        <br></br>
        <br></br>
        <h1 align="center" class="display-3">
            <xsl:value-of select="@tituloSecção" />
        </h1>
        <p>
            <xsl:value-of select="t:parágrafo" />
        </p>
    </xsl:template>

    <!-- linguagem -->
    <xsl:template match="t:linguagem">
        <br></br>
        <br></br>
        <br></br>
        <h1 align="center" class="display-3">
            <xsl:value-of select="@tituloSecção" />
        </h1>
        <p>
            <xsl:value-of select="t:parágrafo" />
        </p>
    </xsl:template>

    <!-- transformações -->
    <xsl:template match="t:transformações">
        <br></br>
        <br></br>
        <br></br>
        <h1 align="center" class="display-3">
            <xsl:value-of select="@tituloSecção" />
        </h1>
        <xsl:for-each select="./*">
            <xsl:choose>
                <xsl:when test="local-name()='parágrafo'">
                    <p>
                        <xsl:value-of select="." />
                    </p>
                </xsl:when>
                <xsl:when test="local-name()='listaItems'	">
                    <xsl:for-each select="t:item">
                        <xsl:apply-templates select="." />
                    </xsl:for-each>
                </xsl:when>
                <xsl:when test="local-name()='subsecção'">
                    <p style="font-weight:bold">
                        <xsl:value-of select="." />
                    </p>
                </xsl:when>
                <xsl:otherwise></xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>

    <!-- conclusão-->
    <xsl:template match="t:conclusão">
        <br></br>
        <br></br>
        <br></br>
        <h1 align="center" class="display-3">
            <xsl:value-of select="@tituloSecção" />
        </h1>
        <xsl:value-of select="." />
    </xsl:template>
    <xsl:template match="t:referências">
        <br></br>
        <h1 align="center" class="display-2">
            Referências
        </h1>
        <xsl:for-each select="t:refBibliográfica">
            <p>
                <xsl:value-of select="@idRef" /> :
                <xsl:value-of select="t:título" />
                <xsl:value-of select="t:dataPublicação" />
                <xsl:for-each select="t:autor">
                    <xsl:value-of select="." />
                </xsl:for-each>
            </p>
        </xsl:for-each>
        <xsl:for-each select="t:refWeb">
            <p>
                <xsl:value-of select="@idRef" /> :
                <xsl:value-of select="t:URL" />
            <p>
                <xsl:value-of select="t:descrição" />
                (
                <xsl:value-of select="t:consultadoEm" />)
            </p>

            </p>
        </xsl:for-each>
    </xsl:template>

    <!-- anexos -->
    <xsl:template match="t:anexos">
        <h3>
            <xsl:value-of select="@id" />
            -
            <xsl:value-of select="@tituloSecção" />
        </h3>
        <xsl:apply-templates select="t:bloco" />
    </xsl:template>
    
    <!--bloco-->
    <xsl:template match="t:bloco">
        <xsl:for-each select="t:paragráfo">
            <p>
                <xsl:value-of select="." />
            </p>
        </xsl:for-each>
        <xsl:for-each select="t:figura">
            <p>
                <img>
                <xsl:attribute name="src">
                    <xsl:value-of select="@src" />
                </xsl:attribute>
                </img>
                <xsl:value-of select="@descrição" />
            </p>
        </xsl:for-each>
    </xsl:template>

    <!--li-->
    <xsl:template match="t:item">
        <xsl:choose>
            <xsl:when test="current() != ''">
                <li>
                    <xsl:value-of select="." />
                </li>
            </xsl:when>
            <xsl:otherwise>
                <br></br>
                <br></br>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>
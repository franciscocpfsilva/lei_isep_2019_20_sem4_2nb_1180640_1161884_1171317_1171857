**João Machado [1161884](../)** - UC1-2-3004 Associar um ficheiro de configuração a uma máquina
=======================================

# 1. Requisitos

UC1-2-3004 - Como Gestor de Chão de Fábrica, eu pretendo associar um ficheiro de configuração a uma máquina.

A interpretação feita desta US foi a possibilidade de um Gestor de Chão De Fábrica poder associar um ficheiro de configuração (texto) a uma máquina já existente no sistema.

### 1.1 Regras de Negócio

- A máquina já deve estar criada no sistema.
- A descrição do ficheiro não deve ser nula nem ultrapassar os 40 caracteres.

# 2. Análise

De acordo com o Caderno de Encargos, deve ser possível associar á máquina um ou mais ficheiros de configuração complementados por uma breve descrição.
Também é referido que a manutenção dos mesmos é realizada fora deste projeto.

Durante o decorrer do Sprint C, o cliente em diversas ocasiões esclareceu que o conteúdo destes ficheiros não obedecia a nenhuma regra de negócio especifica.
Com esta informação, a US é baseada na pesquisa de uma Máquina através do ID onde após a validação da sua existência é pedido uma descrição breve do ficheiro e a caminho absoluto para o ficheiro.

## 2.1 Estudo

Este US é em muito semelhante há implementação da Ficha Técnica que desenvolvi no Sprint B sendo que, a maior diferença é ser necessário suportar vários Ficheiros.

A fim de suportar este requisito, irei criar um novo Value-Object que terá dois atributos, uma descrição e um byte[].

Na entidade Máquina, Root Entity do agregado, adiciono uma Lista do tipo do Value-Object criado, permitindo assim referenciar múltiplas instâncias de Ficheiros de Configuração e persistir as mesmas.

![](MD.svg)

## 2.2 Configuration Sheet

A implementação deste Value-Object será muito simples, semelhante a outros já implementados, que apenas terá como atributos uma Descrição e um byte[] ( array de bytes).

Esta classe irá garantir que ambos os atributos não são nulos e que no caso da descrição, esta não irá exceder os 40 caracteres.

Será necessário definir o atributo que irá conter a informação do ficheiro com anotação *@Lob*, algo aprendido noutra US, permitindo alocar mais espaço na coluna respetiva da persistência.

## 2.3 Machine

Na nossa Root Entity, Machine, já implementada noutra US, é necessário apenas acrescentar um atributo List do mesmo tipo do Value-Object e um método que permita acrescentar novas instâncias a esta lista. Este método em muito semelhante será a um *Set*.

# 3. Design

## 3.1. Realização da Funcionalidade

No diagrama seguinte apresento o fluxo lógico desta US.

![SD_AssociarFicheiroConfiguracao.svg](SD_AssociarFicheiroConfiguracao.svg) 

## 3.2. Diagrama de Classes

Para o diagrama de classes optei por incluir todos os packages do projeto com o intuito de centralizar o observador para esta US e representar as diversas partes que são utlizadas.

![CD_AssociarFicheiroConfiguracao.svg](CD_AssociarFicheiroConfiguracao.svg) 

## 3.3. Padrões Aplicados

### Padrão Repository

Este padrão foi utlizado para aceder á informação existente sobre Máquinas assim como fazer a persistência da instância Máquina modificada. É especialmente útil pois permite abstrair da implementação técnica da camada responsável por criar a persistência.

## 3.4. Testes 

### Testes unitários

Devido á simplicidade do código desta classe irei apenas demostrar um exemplo prático dos testes implementados e os restantes apenas terão uma breve descrição.

**Teste 1:** Verificar que inicialmente a lista de ficheiros está vazia e que é possível adicionar instancias de ConfigurationFile

    @Test
    public void addConfigurationFile() {
        byte file[] = new byte[]{1, 6, 3};
        
        assertTrue(configFiles.isEmpty());
        assertEquals(0, configFiles.size());

        configFiles.add(ConfigurationFile.valueOf(Description.valueOf("Modo broca 3mm"), file));

        assertFalse(configFiles.isEmpty());
        assertEquals(1, configFiles.size());

    }

**Teste 2:** Verificar que é possível criar uma Instância de ConfigurationFile

**Teste 3:** Verificar que não é possível criar uma Instância de ConfigurationFile com um dos valores a null.

**Teste 4:** Verificar que não é possível criar uma Instância de ConfigurationFile com a descrição a exceder os 40 caracteres em tamanho.

# 4. Implementação

Nesta secção apresento excerto do código desenvolvido, representativo do culminar do processo de planeamento desenvolvido para esta UC.
Inspirado nas US previamente desenvolvidas na aplicação ecafetaria, as classes implementadas foram desenvolvidas aplicando os conceitos lecionados em EAPLI, mantendo sempre em mente as boas práticas e os diversos padrões de POO.

## 4.1 Domain

Relativamente ao Domain esta US requer a criação do value-Object em cima referido ConfigurationFile. O construtor desta classe recebe um descrição e verifica se tem menos de 40 caracteres e recebe um array de bytes representativo da informação contida no ficheiro lido.

Na classe Machine, foi adicionado um novo atributo do tipo LinkedList. O meu objetivo era definir apenas como uma List e depois especificar o tipo no construtor, mas como é necessária a anotação *Lob* devido ao tamanho deste atributo, o JPA obriga a que seja declarado logo o tipo da List.

Adicionalmente foi implementado um método que recebe uma nova instância de ConfigurationFile e adiciona á LinkedList.

![Domain.svg](Domain.svg)

## 4.1 Controller

No package application implementei o Controller desta US que é facilmente resumido em 4 etapas.
Primeiramente procura e cria uma instância de Machine através do ID proveniente da UI.
Seguidamente cria uma instância de ConfigurationFile novamente com os parâmetros provenientes da UI. 
Adiciona este objeto ConfigurationFile á lista do objeto Machine e finalmente faz o *Update* e persiste o objeto Machine.

Também foi implementado um serviço especializado na procura de Máquinas por ID nesta US com o objetivo de potencial reutilização desta procura para outras US.

![Controller.svg](Controller.svg)

## 4.1 UI

Relativamente ao UI da US, adotei uma abordagem simples e eficiente tentado reduzir ao máximo as dependências com outras áreas da aplicação. 

A única validação presente nesta classe é referente ao ficheiro a ser lido, terminando a US caso não seja possível ler o ficheiro.

![UI.svg](UI.svg)

# 5. Integração/Demonstração

O processo de desenvolvimento foi bastante previsível, bastante semelhante a outros já implementados por mim. Não senti nenhuma dificuldade e foi possível integrar facilmente com as funcionalidades já existentes.

# 6. Observações

Face ao trabalho desenvolvido neste UC, sinto que cumpri perante os objetivos propostos e apresento uma solução válida.




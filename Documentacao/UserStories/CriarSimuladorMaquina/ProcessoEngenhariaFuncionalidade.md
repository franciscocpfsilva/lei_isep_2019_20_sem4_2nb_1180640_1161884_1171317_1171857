# Aluno 1180640 - US 1003 - Criar Aplicação para Simular uma Máquina

=======================================

# 1. Requisitos

_Nesta secção a equipa deve indicar a funcionalidade desenvolvida bem como descrever a sua interpretação sobre a mesma e sua correlação e/ou dependência de/com outros requisitos._

_Descrição da US:_ Como Gestor de Projeto, eu pretendo que a equipa desenvolva uma aplicação que simule o funcionamento de uma máquina, nomeadamente no envio de mensagens geradas por estas.

Esta US insere-se num de dois fluxos alternativos de integração de mensagens no sistema central. O primeiro fluxo a ser implementado consiste na importação de mensagens diretamente no sistema através de ficheiros de texto. Este fluxo é temporário e visa simplificar a geração de mensagens. No entanto, o segundo fluxo é aquele que mais se assemelha à realidade e é onde se insere esta US. Uma máquina gera mensagens, que são enviadas por um protocolo de comunicação e recebidas pelo sistema central, que faz o que quiser com elas. É aqui que entra o simulador de máquina: simula o envio de mensagens por parte de uma máquina.

## 1.1. Correlação com outras US

Pelo que foi dito anteriormente, esta US deve englobar a comunicação com o Serviço de Comunicação com as Máquinas (SCM), pelo menos da parte da máquina. Por SCM, entende-se a parte do sistema central que comunica com as máquinas. Esta US deve também sincronizar-se com as seguintes US:

- US 4002 - Recolher Mensagens geradas nas Máquinas, que corresponde à implementação da comunicação Máquina - SCM, mas do lado do sistema central;
- US 1012 - Suportar Pedidos de Monitorização de Estado no Simulador, uma vez que a comunicação com o Sistema de Monitorização das Máquinas (SMM) depende das respostas obtidas aquando da comunicação com o SCM. Para além disso, a sua implementação é realizada sobre o simulador, pelo que este precisa de estar implementado.

Isto pode ser visto no diagrama "Perspetiva Geral das Aplicações Existentes":

![aplicacoes_existentes](aplicacoes_existentes.svg)

## 1.2. Critérios de aceitação

- Desenvolvido em C (e usando threads).
- As mensagens a enviar são lidas de um ficheiro de texto.
- A identificação da máquina e cadência de envio são estipuladas por parâmetro.
- Deve contemplar cenários de erro que permitam aferir a resiliência do SCM.

Os cenários de erro referem-se a problemas de comunicação. Por exemplo, caso o simulador não consiga estabelecer comunicação com o SCM à primeira tentativa, não deve simplesmente desistir e terminar a aplicação, mas sim tentar outra vez passado algum tempo.

## 1.3. Outros requisitos / esclarecimentos

Esclarecimentos:

- Input: ficheiro de texto (cada linha corresponde a uma mensagem).
- Output: mensagens a enviar para o sistema central por um protocolo de comunicação (US 4002).
- [Ficheiro exemplo](https://moodle.isep.ipp.pt/mod/resource/view.php?id=193026) (o mesmo que para a US 4001)
- A execução de um simulador corresponde a uma e uma só máquina.
- Deve ser possível colocar em execução vários simuladores em simultâneo. Cada um com parâmetros diferentes. [(fórum)](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=30366#p40519)

# 1.4. Comunicação com o SCM

Estrutura de uma mensagem:

![messageStructure](messageStructure.svg)

Outros requisitos específicos à comunicação (ex: estrutura de uma mensagem, protocolos a usar) podem ser encontrados no protocolo de comunicação.

‌

# 2. Análise

_Neste secção a equipa deve relatar o estudo/análise/comparação que fez com o intuito de tomar as melhores opções de design para a funcionalidade bem como aplicar diagramas/artefactos de análise adequados._

_Recomenda-se que organize este conteúdo por subsecções._

## 2.1. Sequência de passos do simulador

1. Máquina inicia, recebendo por parâmetro os dados necessários
2. Máquina envia mensagem HELLO ao SCM
3. Máquina recebe e trata a resposta do SCM
4. Máquina lê ficheiro
5. Máquina envia mensagem MSG ao SCM
6. Máquina recebe e trata a resposta do SCM
7. Máquina espera algum tempo
8. Passos 5, 6 e 7 repetem-se enquanto houver mensagens para enviar

Parâmetros a passar ao simulador:

- ID da máquina a simular
- Cadência de envio de mensagens (ex: de 5 em 5 segundos)
- Ficheiro a ler para enviar as mensagens

O IP do SCM, necessário para a comunicação, pode ser automaticamente conhecido pelo simulador.

## 2.2. Modelo de Domínio

Uma vez que se trata de uma aplicação fora do sistema central, o modelo de domínio pelo qual nos seguimos não se aplica neste caso. Temos de criar um modelo de domínio próprio.

Pelo protocolo de comunicação, sabemos que uma mensagem, independentemente da origem e do protocolo usado, tem sempre a mesma estrutura, pelo que esta poderá ser encarada como um conceito de domínio.

Atributos da mensagem:

- Versão
- Código
- ID
- Tamanho dos dados
- Dados

O código da mensagem pode ser um dos seguintes:

- 0 - HELLO request
- 1 - MSG request
- 2 - CONFIG request
- 3 - RESET request
- 150 - ACK response
- 151 - NACK response

A própria máquina terá alguns atributos, nomeadamente o seu ID, mas dado o simulador ser para uma máquina apenas não parece haver necessidade de haver um conceito 'máquina' no modelo de domínio.

O modelo de domínio é portanto o seguinte:

![domain_model](domain_model_machineSimulator.svg)

# 2.3. Regras de negócio

Relativamente a uma mensagem:

- A versão do protocolo tem de estar dentro do intervalo [0; 255]
- O código da mensagem tem de ser um dos fornecidos
- O campo tamanho dos dados deve ser >= 0

# 3. Design

_Nesta secção a equipa deve descrever o design adotado para satisfazer a funcionalidade. Entre outros, a equipa deve apresentar diagrama(s) de realização da funcionalidade, diagrama(s) de classes, identificação de padrões aplicados e quais foram os principais testes especificados para validar a funcionalidade._

_Para além das secções sugeridas, podem ser incluídas outras._

## 3.1. Realização da Funcionalidade

_Nesta secção deve apresentar e descrever o fluxo/sequência que permite realizar a funcionalidade._

De acordo com os requisitos, não parece haver necessidade de momento para a existência de uma interface com o utilizador. Podemos facilmente meter o simulador a correr passando-lhe por parâmetro os dados necessários (ID da máquina, tempo de latência e nome do ficheiro).

Posto isto, a arquitetura da aplicação pode envolver a arquitetura em _horizontal slicing_ que é usada no sistema central (divisão nas camadas de aplicação, domínio e repositório). A diferença passa por substituir a parte do repositório pela parte da comunicação com o SCM. A divisão por funcionalidades (_vertical slicing_) não parece necessária dado o caráter simulador desta aplicação (não é uma aplicação que vise ser expandida em termos de funcionalidades).

No que diz respeito à importação e envio de mensagens, esta US pode usar a US 2005 - Importar Produtos através de ficheiro CSV como exemplo. No entanto, neste caso, em vez de lermos uma linha do ficheiro e a guardarmos no repositório como uma entidade de domínio, iremos ler uma linha do ficheiro e enviá-la como uma mensagem pelo protocolo TCP.

Neste aspeto, destacam-se os padrões Template Method e Strategy. Como estamos a implementar na linguagem de programação C e não existem interfaces, o padrão Strategy será de mais difícil aplicação. No entanto, o padrão Template Method pode ser aplicado, sendo que o algoritmo de leitura do ficheiro ficará definido num Serviço.

Para facilitarmos o processo de conversão de uma mensagem para uma estrutura de dados que possa ser enviada através do protocolo pretendido, a solução encontrada foi usar um DTO para a mensagem.

![sequenceDiagram_machineSimulator](sequenceDiagram_machineSimulator.svg)

## 3.2. Diagrama de Classes

_Nesta secção deve apresentar e descrever as principais classes envolvidas na realização da funcionalidade._

![classDiagram_machineSimulator](classDiagram_machineSimulator.svg)

O objetivo passou por manter a arquitetura em _horizontal slicing_ que é usada no sistema central. A divisão por funcionalidades (_vertical slicing_) não pareceu necessária dado o caráter simulador desta aplicação (não é uma aplicação que vise ser expandida em termos de funcionalidades).

## 3.3. Padrões Aplicados

_Nesta secção deve apresentar e explicar quais e como foram os padrões de design aplicados e as melhores práticas_

- **Single Responsibility Principl** - Isolar responsabilidades ao máximo. Uma classe tem uma responsabilidade. Exemplos disso são as classes de leitura do ficheiro CSV e de comunicação com o SCM.
- **Controller** - Usado com o objetivo de isolar comportamentos das várias US relacionadas com o simulador. Esta US terá o seu controlador, que coordenará os vários passos de execução. Desta forma, podemos até ter uma thread responsável pela execução deste caso de uso e outras threads responsáveis pela execução de outros casos de uso (ex: receção de pedidos de monitorização do SMM).
- **Template Method** - Algoritmo de leitura do ficheiro definido num Serviço. A implementação de cada um dos passos do algoritmo fica definida numa classe à parte.
- **Factory Method** - Para criar a estrutura Message e fazer as respetivas validações. No entanto, em C não conseguirmos impedir a criação de uma Message através da estrutura (sem usar o método de criação).
- **DTO** - Para a mensagem. Permite a conversão da mensagem numa estrutura de dados adequada para envio pelos protocolos de redes.

## 3.4. Testes

_Nesta secção deve sistematizar como os testes foram concebidos para permitir uma correta aferição da satisfação dos requisitos._

**Teste 1:** Verificar que não é possível criar uma instância da classe Message com uma versão do protocolo < 0 ou > 255.

**Teste 2:** Verificar que o código da mensagem existe.

**Teste 3:** Verificar que o campo tamanho dos dados não pode ser < 0.

# 4. Implementação

_Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;_

_Recomenda-se que organize este conteúdo por subsecções._

A implementação desta US foi realizada ao mesmo tempo que a implementação da US 1012, pelo que houve sincronização neste aspeto, nomeadamente nos ficheiros Message, MessageCode, MainClass e MachineAlgorithm. Houve comunicação constante com o membro de equipa responsável por essa US com o objetivo de acelerar a implementação e definir a melhor estratégia para a implementação do simulador. 

A estratégia adotada relativamente às threads foi a de ter 1 thread responsável pelo envio das mensagens ao Sistema Central (US 1011) e outra responsável pela comunicação com o Sistema de Monitorização (US 1012).

O tratamento de erros realizado passa por enviar um pedido ao Sistema Central, analisar a sua resposta e caso esta não seja ACK tentar enviar o pedido outra vez.

# 5. Integração/Demonstração

_Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema._

Esta US tem de ser integrada com a US 4002, que recolhe as mensagens geradas pelo simulador. Tratam-se de aplicações diferentes: uma implementada em C (simulador) e outra em Java (sistema de comunicação com as máquinas). Para que haja comunicação por protocolo TCP entre elas, o simulador (cliente) tem de conhecer o endereço IP do SCM (servidor), assim como o porto de acesso. Este último foi-nos sugerido num documento da unidade curricular de RCOMP, sendo o 31401 (turma 2NB, grupo 1). O endereço IP a utilizar neste momento consiste no localhost, uma vez que, à altura de escrita deste texto, o SCM ainda não estava funcional, pelo que o simulador foi testado em comunicação com uma aplicação servidora improvisada. Perante implementação do SCM, o endereço IP deverá ser alterado para possibilitar a comunicação através dos servidores do DEI.  

# 6. Observações

_Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados._

Esta US envolve o design de uma aplicação totalmente nova, aidna por cima implementada em C, o que dificulta a tarefa. Tendo isto em conta, penso que as decisões de design foram bem aplicadas. No entanto, o tempo despendido nesta US foi demasiado grande, dificultando o trabalho dos membros de equipa que dependiam desta US para avançar com outras US.

# Sprint Retrospective C

## Aspetos positivos

- Maioria das decisões tomadas em conjunto
- Levantamento de dependências facilitou muito a divisão das US
- Rápido planeamento

## Aspetos negativos / a melhorar

- Equipa demorou muito tempo a "arrancar" no sprint
- Parca comunicação, pelo menos em comparação com o sprint anterior

## O que vamos aplicar no próximo sprint

- Compromisso total
- Maior foco
# Sprint Retrospective B

## Aspetos positivos

- Muita comunicação entre os membros de equipa
- Maioria das decisões tomadas em conjunto
- Levantamento de dependências facilitou muito a divisão das US
- Logs das reuniões ajudaram a organizar o trabalho

## Aspetos negativos / a melhorar

- Planeamento muito demorado
- Equipa demorou muito tempo a "arrancar" no sprint
- Logs das reuniões podem ser feitos durante a reunião (vs no fim)

## O que vamos aplicar no próximo sprint

- Compromisso inicial maior
- Fazer logs das reuniões **durante** a reunião

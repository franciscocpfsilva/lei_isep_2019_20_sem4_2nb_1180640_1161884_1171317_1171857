RCOMP 2019-2020 Project - Sprint C review
=========================================
### Sprint master: 1171317 ###

# 1. Sprint's backlog #

| Task | Task Description | Student |
| :--: | :-- | :-- |
| US4002 | Development of the Central System application features regarding communications with Industrial Machines. | 1171317
| US1011 | Development of the Industrial Machines application regarding communications with the Central System. | 1180640
| US1012 | Development of the Industrial Machines application regarding communications with the Monitoring System. | 1171857
| US6001 | Development of the Monitoring System application regarding communications with Industrial Machines. | 161884
| US3008 | Development of the Monitoring System application regarding communications with WEB browsers. | --

# 2. Subtasks assessment #

The follows the assessment of each team member over his own task:

## US4002 - Development of a Java applicaton, using TCP protocol to comunicate with a machine #
### Implemented with some issues.  ###
Issues regarding the communication between the SCM and the machine simulator app through a remote connection, using DEI's servers.
Major changes from the previous official submitted version:
- Able to import to the database, instances of messages directly from the machines.
- Totally new architecture, that abides by the design requisites stablished in LAPR4.
- Still unable to send a Config request.

## US1011 - Development of a C application simualting a machine, using TCP protocol to communicate with the Central System #
The communication between this app and the SCM (Machine's Communication System) wasn't tested until the Sprint Review day.

On the Sprint Review day, while trying to run the app in DEI's servers, 2 changes were made:
- i) correction of the path of an 'include' file in header.h file (was giving compilation problems on the server, even though working fine in local machine);
- ii) inclusion of a makefile command to run app in DEI's servers (thread library was giving problems in server) - the command created is 'make runInServer'

Other than this, the US was implemented with no issues.

## US1012 - Development of a C application simulating a machine, using UDP protocol to be monitorize by the Monitoring System  #
### Totally implemented with no issues. ###
## US6001 - Development of a Java application, using UDP protocol to monitorize machines #
### Totally implemented with no issues. ###

Of each task, a set of subtasks was requested, they follow:
* Create a UDP/TCP packet.
* Converting text format messages to bytes.
* Error treatment.

Communication within US4002 and US1011 is not possible at this time. It's only possible to test each individual application communication using proxy applications.

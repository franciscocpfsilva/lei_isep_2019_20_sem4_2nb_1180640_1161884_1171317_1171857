RCOMP 2019-2020 Project - Sprint D review
=========================================
### Sprint master: 1171857 ###

# 1. Sprint's backlog #

| Task | Task Description | Student |
| :--: | :-- | :-- |
| US3010 | Development of the Central System application features regarding communications with Industrial Machines.| 1171317
| US1014 | Development of the Industrial Machines application regarding communications with the Central System. | 1180640
| US1016 | Development of the Industrial Machines application regarding communications with the Monitoring System. | 1171857
| US6002 | Development of the Monitoring System application regarding communications with Industrial Machines. | 1161884
| US3011 | Development of the Monitoring System application regarding communications with WEB browsers. | --
| US1013 | Development of the Central System application features regarding communications with Industrial Machines. (SSL/TLS)| --
| US1015 | Development of the Industrial Machines application regarding communications with the Central System. (SSL/TLS)| --

# 2. Subtasks assessment #

The follows the assessment of each team member over his own task:

##  US3010 - Development of the Central System application features regarding communications with Industrial Machines. #
### Totally implemented with no issues. ###

## US1014 - Development of a C application simualting a machine, using TCP protocol to communicate with the Central System #
### Totally implemented with no issues. ###

## US1016 - Development of a C application simulating a machine, using UDP protocol to be monitorize by the Monitoring System  #
### Totally implemented with no issues. ###

## US6002 - Development of a Java application, using UDP protocol to monitorize machines #
### Totally implemented with no issues. ###

## US1013 -  Ensure that communications with SCM and machine simulator are protected #
### Not implemented. ###

## US1015 - Ensure that communications with machine simulator and SCM are protected #
### Not implemented. ###

## US3011 - Requested machine reset using web dashboard #
### Not requested for groups of four ###

Of each task, a set of subtasks was requested, they follow:
* Create a UDP packet or a TCP connection.
* Converting text format messages to bytes.
* Error treatment.

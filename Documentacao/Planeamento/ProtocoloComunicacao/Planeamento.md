# Especificidades do protocolo de comunicação

## **Sprint C**

## Lista de tarefas

1. Development of the Central System application features regarding communications with Industrial Machines.
2. Development of the Industrial Machines application regarding communications with the Central System.
3. Development of the Industrial Machines application regarding communications with the Monitoring System.
4. Development of the Monitoring System application regarding communications with Industrial Machines.
5. Development of the Monitoring System application regarding communications with WEB browsers.

Uma vez que somos um grupo de 4 elementos, a tarefa 5. não deve ser realizada.

## Comparação com as User-Stories definidas em LAPR4

Perspetiva Geral das Aplicações Existentes:

![perspetiva_geral_aplicacoes](perspetiva_geral_aplicacoes.png)

De acordo com o diagrama acima, conseguimos ter uma noção da posição das tarefas de RCOMP no seio das User-Stories fornecidas.

As **tarefas 1 e 2** são referentes à comunicação entre o Serviço de Comunicação com as Máquinas (SCM) e o Simulador de Máquina (SM) (US4002), onde iremos aplicar o Protocolo 1 (ponto 2.2 no documento [PARTE III – Protocolo de Comunicação](https://moodle.isep.ipp.pt/pluginfile.php/323719/mod_resource/content/1/LEI-2019-20-Sem4-_Projeto_ProtocoloComunicacao.pdf)).

Por sua vez, as **tarefas 3 e 4** são referentes à comunicação entre o SM e o Serviço de Monitorização das Máquinas (SMM) (US6001), onde iremos aplicar o Protocolo 2 (ponto 2.3 no documento [PARTE III – Protocolo de Comunicação](https://moodle.isep.ipp.pt/pluginfile.php/323719/mod_resource/content/1/LEI-2019-20-Sem4-_Projeto_ProtocoloComunicacao.pdf)).

## Divisão de tarefas

|  Aluno Nr.  |  Nome do Aluno  | Tarefa atribuída | Sistemas envolvidos | User-Story |
| :---------: | :-------------: | :--------------: | :-----------------: | :--------: |
| **1161884** |  João Machado   |        4         |    **SMM** e SM     |   US6001   |
| **1171317** | Leonardo Coelho |        1         |    **SCM** e SM     |   US4002   |
| **1171857** |   João Cunha    |        3         |    **SM** e SMM     |   US1012   |
| **1180640** | Francisco Silva |        2         |    **SM** e SCM     |   US1011   |

As tarefas 1 e 2 serão dependentes uma da outra, pelo que os membros de equipa envolvidos deverão sincronizar alguns aspetos entre si. O mesmo se passa para as tarefas 3 e 4.

Os sistemas a negrito na tabela representam o sistema onde deverá ser implementada a aplicação de comunicação.

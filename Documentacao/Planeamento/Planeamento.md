# Projeto Integrador da LEI-ISEP Sem4 2019-20

# 1. Constituição do Grupo de Trabalho

O grupo de trabalho é constituído pelo estudantes identificados na tabela seguinte.

| Aluno Nr.   | Nome do Aluno   |
| ----------- | --------------- |
| **1161884** | João Machado    |
| **1171317** | Leonardo Coelho |
| **1171857** | João Cunha      |
| **1180640** | Francisco Silva |

# 2. Distribuição de Funcionalidades

A distribuição de requisitos/funcionalidades ao longo do período de desenvolvimento do projeto pelos elementos do grupo de trabalho realizou-se conforme descrito nas tabelas seguintes.

## Sprint B

Foi decidido que as User Stories (US) de Bootstrap serviriam como code review das US de criação, pelo que deveriam ser realizadas por um membro de equipa diferente daquele que realizou a US de criação. A US1004 (Bootstrap de matérias-primas) foi uma exceção, sendo que ainda assim a US de adicionar matérias-primas foi revista por um membro de equipa diferente.

| Aluno Nr.   | User-Stories                                        |
| ----------- | --------------------------------------------------- |
| **1161884** | US3001 - Adicionar uma máquina                      |
|             | US1005 - Bootstrap de Categorias de Matérias-primas |
|             | US2001 - Adicionar uma Matéria-prima                |
|             | US1004 - Bootsrap de Matérias-primas                |
| **1171317** | US2006 - Adicionar um produto                       |
|             | US1007 - Bootstrap de Máquinas                      |
|             | US3002 - Especificar uma Linha de produção          |
| **1171857** | US2002 - Adicionar uma categoria de matéria-prima   |
|             | US1007 - Bootstrap de Depósitos                     |
|             | US2003 - Consultar Produtos sem Ficha de produção   |
| **1180640** | US3003 - Adicionar um depósito                      |
|             | US1006 - Bootstrap de Produtos                      |
|             | US2005 - Importar Produtos através de ficheiro CSV  |

**Sprint Master:** 1180640

As seguintes US não foram atribuídas:

- US1008 - Bootstrap de Linhas de produção
- US2004 - Especificar Ficha de produção de um Produto

**Dependências das User-Stories**

![dependencias_sprintB](dependencias_sprintB.png)

## Sprint C

| Aluno Nr.   | User-Stories                                                        |
| ----------- | ------------------------------------------------------------------- |
| **1161884** | US2004 - Especificar Ficha de produção de um Produto **\***         |
|             | US6001 - Monitorizar estado de Máquinas                             |
|             | US2010 - Introduzir uma ordem de produção                           |
|             | US3004 - Associar um Ficheiro de Configuração a uma Máquina         |   
| **1171317** | US4001 - Importar mensagens existentes em ficheiros de texto        |
|             | US4002 - Recolher Mensagens geradas nas máquinas                    |
| **1171857** | US1012 - Suportar pedidos de monitorização de estado no simulador   |
| **1180640** | US1011 - Criar aplicação para simular uma máquina                   |

**Sprint Master:** 1171317

**\*** - US do backlog do Sprint B, agora aditadas ao conjunto de US do Sprint C, e finalmente atribuídas para realização.

Especificidades das tarefas relativas ao protocolo de comunicação podem ser encontradas [aqui](ProtocoloComunicacao/Planeamento.md). 

**Dependências das User-Stories**

![dependencias_sprintC](dependencias_sprintC.png)

## Sprint D

| Aluno Nr.   | User-Stories |
| ----------- | ------------ |
| **1161884** | UC4-2-3009 Saber e alterar estado do processamento de mensagens             |
|   | UC3-3-6002 SMM enviar pedido de reinicialização para uma dada máquina  |
|   | UC5-1-2013 Aplicar visualização a ficheiro XML|
|   | UC-5-1017 Relatório que descreva todas as aplicações XML, XSD, XSLT e XPATH |
| **1171317** | UC1-3-4002 Recolher mensagens geradas nas máquinas **\***             |
|   | UC2-3-3010 Envio de configuração para máquina  |
|   | UC5-1-2013 Aplicar visualização a ficheiro XML|
|   | UC-5-1017 Relatório que descreva todas as aplicações XML, XSD, XSLT e XPATH |
| **1171857** | UC1-4-5001 Efetuar o processamento de mensagens **\***         |
|   | UC2-3-1016 Receber pedidos RESET no simulador  |
|   | UC5-1-2013 Aplicar visualização a ficheiro XML|
|   | UC-5-1017 Relatório que descreva todas as aplicações XML, XSD, XSLT e XPATH |
| **1180640** | UC2-3-1014 Receção de ficheiros de configuração no simulador           |
|   | UC1-4-5002 Processar mensagens (recorrente) |
|   | UC5-1-2013 Aplicar visualização a ficheiro XML|
|   | UC-5-1017 Relatório que descreva todas as aplicações XML, XSD, XSLT e XPATH |

**\*** - US do backlog do Sprint C, agora aditadas ao conjunto de US do Sprint D, e finalmente atribuídas para realização.

**Sprint Master:** 1171857

**Dependências das User-Stories**

![dependencias_sprintD](dependencias_SPRINTD_v1.svg)
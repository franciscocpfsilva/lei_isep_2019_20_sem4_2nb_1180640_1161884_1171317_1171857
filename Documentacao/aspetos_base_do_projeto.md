# Aspetos gerais do projeto

1.  O **nome dos packages e das classes** deve estar em inglês. No projeto core, criar para cada agregado identificado três packages: _application_, _domain_ e _repositories_. A sua nomenclatura deve seguir o exemplo seguinte: _eapli.base.machinery.domain_.

2.  Os **javadocs** devem estar em inglês.

3.  Os **issues** devem seguir um dos seguintes formatos:

    a) Para User Story fornecida pelo cliente:

        UCX-Y-Z Descrição_sucinta_da_US, sendo:
            X o número do caso de uso no caderno de encargos;
            Y o número correspondente à área duncional da US;
            Z o número da US.

    b) Para Issue criado pela equipa:

        [Task] Descrição_sucinta_do_issue

    c) Para outros tipos de issues, definir o tipo correspondente na criação do issue (e.g. usar tipo Bug para correção de erros no projeto).

4.  Os **commits** devem estar associados a issue(s) e devem seguir a seguinte estrutura:

    a) AÇÃO_OPT #NUM Descrição_sucinta_do_commit, sendo:

        AÇÃO_OPT a ação a realizar ao issue na sequência do commit (e.g. fixes para resolver o issue) - é um parâmetro opcional;
        NUM o número do issue no Bitbucket;

        A mensagem dos commits deve estar em português.

5.  A **documentação** estará no mesmo repositório do projeto, dentro de uma pasta denominada documentation, documentacao ou semelhante. Para cada User Story haverá uma pasta respetiva com um ficheiro em markdown onde serão colocados os comentários relativos a requisitos, análise, design, implementação e integração/demonstração dessa US. A linguagem utilizada na documentação é o português.

# Arquitetura da aplicação

1. A aplicação usa o **padrão Layers**, estando a camada de Apresentação dependente da camada de Aplicação, esta por sua vez dependente das camadas de Domínio e Persistência/Repositório. A camada de Persistência depende da camada de Domínio, estando esta última mais isolada em termos de dependências.

2. A **camada de Apresentação** está implementada em projetos/módulos Maven próprios (ex: backoffice, user...).

3. O projeto **core** está dividido à superfície usando _vertical slicing_, sendo que cada fatia desta divisão corresponde a um agregado do modelo de domínio. Dentro de cada fatia, temos uma sub-divisão usando _horizontal slicing_, dando origem a três sub-fatias: Aplicação, Domínio e Repositório, cada uma correspondendo a um package diferente.

4. Uso do **padrão Repository**, o que em termos práticos dá origem a uma interface XptoRepository por agregado.

5. O **mecanismo de persistência** está implementado no projeto persistence.impl. Este projeto tem classes que implementam persistência i) em memória e ii) através de JPA para cada XptoRepository.

6. Uso do **padrão Abstract Factory** na persistência.

7. Tem como dependência a **framework eapli**.

# Aspetos relacionados com o Modelo de Domínio

1. Segue uma abordagem **Domain-Driven Design (DDD)**.

2. Para efeitos de simplificação, apenas foram representados os **_value-objects_** mais relevantes para a perceção do domínio (ex: IDs, Quantidade, Ficha de produção, etc). No entanto, alguns atributos simples terão regras de negócio próprias (como a limitação do número de caracteres) e poderão ser implementados como _value-objects_. Esta decisão deve estar mencionada na pasta da documentação, no capítulo da Análise ou Design do processo de engenharia do caso de uso respetivo.

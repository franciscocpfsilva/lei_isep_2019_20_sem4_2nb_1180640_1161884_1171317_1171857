# Sistema de recolha, processamento e gestão de informação de chão de fábrica

## Índice

- [Glossário](Modelo_Dominio/Glossario.md)
- [Modelo de Domínio](Modelo_Dominio/Modelo_Dominio.md)
- [Aspetos base do projeto](aspetos_base_do_projeto.md)
- [Planeamento dos Sprints](Planeamento/Planeamento.md)
- [Sprint Reviews](SprintReviews/PreparacaoReviews.md)

## User Stories

- Sprint B
    - [US 1004 - Bootstrap de Matérias-primas](UserStories/BootstrapDeMateriasPrimas/README.md)
    - [US 1005 - Bootstrap de Categorias de Matéria-prima](UserStories/BootstrapDeCategoriaMateriaPrima/README.md)
    - [US 1006 - Bootstrap de Produtos](FALTA/FALTA.md)
    - [US 1007 - Bootstrap de Máquinas](UserStories/BootstrapDeMaquinas/README.md)
    - _[US 1008 - Bootstrap de Linhas de Produção](FALTA/FALTA.md)_ - Não Implementada
    - [US 1009 - Bootstrap de Depósitos](FALTA/FALTA.md)
    - [US 2001 - Adicionar uma Matéria-Prima ao catálogo](UserStories/AdicionarMateriaPrima/README.md)
    - [US 2002 - Definir Categoria de Matérias-primas](UserStories/AdicionarCategoriaMateriaPrima/ProcessoEngenhariaFuncionalidade.md)
    - [US 2003 - Consultar Produtos sem Ficha de Produção](UserStories/ConsultarProdutosSemFichaDeProducao/ProcessoEngenhariaFuncionalidade.md)
    - _[US 2004 - Especificar Ficha de Produção](UserStories/EspecificarFichadeProducao/README.md)_ - Não Implementada
    - [US 2005 - Importar catálogo Produtos através de ficheiro CSV](UserStories/ImportarProdutosPorFicheiroCsv/ProcessoEngenhariaFuncionalidade.md)
    - [US 2006 - Adicionar novo Produto](UserStories/AdicionarProduto/README.md)
    - [US 3001 - Definir existência de uma nova maquina](UserStories/AdicionarMáquina/README.md)
    - _[US 3002 - Especificar nova Linha de Produção](UserStories/EspecificarNovaLinhaDeProducao/README.md)_ - Não Implementada
    - [US 3003 - Especificar Depósito](UserStories/EspecificarDeposito/ProcessoEngenhariaFuncionalidade.md)
- Sprint C
    - Backlog Sprint B
        - [US 2004 - Especificar Ficha de Produção](UserStories/EspecificarFichadeProducao/README.md)
        - [US 3002 - Especificar nova Linha de Produção](UserStories/EspecificarNovaLinhaDeProducao/README.md)
    - Backlog Sprint C
        - [US 1010 - Especificar documento XSD para validar XML com informação do sistema](UserStories/EspecificarXSD/shop_floor_info.xsd)
        - [US 1011 - Criar aplicação para simular uma máquina](UserStories/CriarSimuladorMaquina/ProcessoEngenhariaFuncionalidade.md)
        - [US 1012 - Suportar pedidos de monitorização de estado no simulador](UserStories/SuportarPedidosDeMonitorizacaoDeEstado/README.md)
        - [US 2007 - Exportar informação do sistema para ficheiro XML](UserStories/ExportarParaXML/shop_floor_info.xml)
        - [US 2010 - Introduzir uma ordem de produção](UserStories/IntroduzirOrdemDeProdução/README.md)
        - [US 3004 - Associar um Ficheiro de Configuração a uma Máquina](UserStories/AssociarFicheiroAMaquina/README.md)
        - [US 4001 - Importar mensagens existentes em ficheiros de texto](UserStories/ImportarMensagensExistentesEmFicheirosDeTexto/ProcessoEngenhariaFuncionalidade.md)
        - [US 4002 - Recolher Mensagens geradas nas máquinas](UserStories/RecolherMensagensGeradasNasMaquinas/ProcessoEngenhariaFuncionalidade.md)
        - [US 6001 - Monitorizar estado de Máquinas](UserStories/SistemaMonitorizacaoMaquinas/README.md)
- Sprint D
    - Backlog Sprint C
        - [US 5001 - Efetuar o processamento de mensagens (por intervalo temporal)](UserStories/SistemaProcessamentodeMensagens/README.md)
    - Backlog Sprint D
        - [US 1014 - Receção de ficheiros de configuração no simulador](UserStories/ReceberPedidoConfiguracaoNoSimulador/ProcessoEngenhariaFuncionalidade.md)
        - [US 1016 - Receber pedidos RESET no simulador](UserStories/SuportarPedidosDeReset/README.md)
        - [US 1017 - Elaborar relatório XML](UserStories/RelatorioXML/Relatorio.xml)
        - [US 2013 - Aplicar visualização a ficheiro XML](UserStories/VisualizarXML/README.md)
        - [US 3009 - Saber e alterar estado do processamento de mensagens](UserStories/SaberAlterarEstadoProcessamentoMensagens/README.md)
        - [US 3010 - Solicitar envio de configuração para uma máquina](UserStories/SolicitarEnvioDeConfiguracaoParaMaquina/ProcessoEngenhariaFuncionalidade.md)
        - [US 5002 - Processar mensagens de forma recorrente](UserStories/ProcessarMensagensRecorrentemente/ProcessoEngenhariaFuncionalidade.md)
        - [US 6002 - SMM enviar pedido de reinicialização para uma dada máquina](UserStories/EnviarPedidoResetSMM/README.md)

## Membros de equipa

- Francisco Silva – 1180640
- João Cunha – 1171857
- João Machado – 1161884
- Leonardo Coelho – 1171317

# Preparação da Sprint Review (Sprint D)

## Sequência de funcionalidades a demonstrar

Dar prioridade à integração entre as várias aplicações.

1. Overview aplicações e comunicação entre elas
2. SCM importa mensagens através de ficheiros
3. Iniciar SCM modo normal
4. Iniciar Simulador máquina e ver envio de mensagens para SCM
5. Abrir aplicação consola e adicionar ficheiro de configuração a uma máquina
6. Através da aplicação consola, enviar ficheiro de configuração para o simulador
7. Iniciar SMM e correr pedido RESET
8. Correr SPM
9. Na aplicação consola, pedir para processar por intervalo temporal
10. Alterar estado de uma linha e mostrar processamento recorrente (dependendo do tempo)

## Dados de suporte

Processamento por intervalo temporal: _'2019-03-26 15:15' '2019-03-26 15:30' P0003 P0004_

## Estado das User-Stories no final do Sprint

| User-Story (restantes do Sprint C)                                      | Estado       |
| ----------------------------------------------------------------------- | ------------ |
| US 2009 - Importar ordens de produção através de ficheiro CSV           | Não iniciada |
| US 2011 - Consultar ordens de produção num dado estado                  | Não iniciada |
| US 2012 - Consultar ordens de produção de uma encomenda                 | Não iniciada |
| US 3005 - Consultar notificações de erros por tratar                    | Não iniciada |
| US 3006 - Arquivar uma ou mais notificações de erros                    | Não iniciada |
| US 3007 - Consultar notificações de erros arquivadas                    | Não iniciada |
| US 3008 - Conhecer estado atual de máquinas                             | Não iniciada |
| US 4001 - Importar mensagens através de ficheiros de texto              | Concluída    |
| US 4002 - Recolher mensagens geradas nas máquinas                       | Concluída    |
| US 5001 - Efetuar o processamento de mensagens (por intervalo temporal) | Concluída    |

.

| User-Story (Sprint D)                                                | Estado       |
| -------------------------------------------------------------------- | ------------ |
| US 1013 - Proteger comunicação entre SCM e máquinas                  | Não iniciada |
| US 1014 - Receção de ficheiros de configuração no simulador          | Concluída    |
| US 1015 - Proteger comunicação entre máquinas e SCM                  | Não iniciada |
| US 1016 - Receber pedidos RESET no simulador                         | Concluída    |
| US 1017 - Elaborar relatório XML                                     | Concluída    |
| US 2013 - Aplicar visualização a ficheiro XML                        | Concluída    |
| US 3009 - Saber e alterar estado do processamento de mensagens       | Concluída    |
| US 3010 - Solicitar envio de configuração para uma máquina           | Concluída    |
| US 3011 - Solicitar reinicialização de uma máquina                   | Não iniciada |
| US 5002 - Processar mensagens de forma recorrente                    | Concluída    |
| US 6002 - SMM enviar pedido de reinicialização para uma dada máquina | Concluída    |
| US 9002 - Elaborar apresentação sobre o nosso sistema                | Concluída    |

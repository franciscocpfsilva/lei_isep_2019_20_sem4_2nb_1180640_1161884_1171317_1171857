# Preparação da Sprint Review (Sprint C)

## Sequência de funcionalidades a demonstrar

1. Importar mensagens através de ficheiros de texto (US 4001)
2. Simulador de máquina com envio e receção de mensagens no Sistema de Comunicação com as Máquinas (USs 1011 e 4002)
3. Pedidos de monitorização e resposta aos pedidos no simulador (USs 6001 e 1012)
4. Introduzir uma ordem de produção (US 2010)
5. Associar ficheiro de configuração a máquina (US 3004)

Do sprint anterior (caso sobre tempo):

6. Especificar ficha de produção de um produto (US 2004)
7. Especificar uma linha de produção (US 3002)

## Dados de suporte

1. ...
2. ...
3. ...
   ...

## Estado das User-Stories no final do Sprint

| User-Story (restantes do Sprint B)                    | Estado       |
| ----------------------------------------------------- | ------------ |
| US 1008 - Bootstrap de linhas de produção             | Não iniciada |
| US 2004 - Especificar ficha de produção de um produto | Concluída    |
| US 3002 - Especificar uma linha de produção           | Concluída    |

.

| User-Story (Sprint C)                                         | Estado       |
| ------------------------------------------------------------- | ------------ |
| US 1010 - Especificar documento XSD                           | Em curso     |
| US 1011 - Criar simulador de máquina                          | Em review    |
| US 1012 - Suportar pedidos de monitorização no simulador      | Em review    |
| US 2007 - Exportar informação toda para ficheiro XML          | Em curso     |
| US 2009 - Importar ordens de produção através de ficheiro CSV | Não iniciada |
| US 2010 - Introduzir uma ordem de produção                    | Concluída    |
| US 2011 - Consultar ordens de produção num dado estado        | Não iniciada |
| US 2012 - Consultar ordens de produção de uma encomenda       | Não iniciada |
| US 3004 - Associar um ficheiro de configuração a uma máquina  | Concluída    |
| US 3005 - Consultar notificações de erros por tratar          | Não iniciada |
| US 3006 - Arquivar uma ou mais notificações de erros          | Não iniciada |
| US 3007 - Consultar notificações de erros arquivadas          | Não iniciada |
| US 3008 - Conhecer estado atual de máquinas                   | Não iniciada |
| US 4001 - Importar mensagens através de ficheiros de texto    | Em curso     |
| US 4002 - Recolher mensagens geradas nas máquinas             | Em curso     |
| US 5001 - Processar mensagens                                 | Não iniciada |
| US 6001 - Monitorizar estado de máquinas                      | Em review    |

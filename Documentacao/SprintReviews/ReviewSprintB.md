# Preparação da Sprint Review (Sprint B)

## Sequência de funcionalidades a demonstrar

1. Adicionar categoria de matéria-prima
2. Adicionar matéria-prima
3. Adicionar produto
4. Importar produtos
5. Consultar produtos sem ficha de produção
6. Adicionar depósito
7. Adicionar máquina
8. Especificar linha de produção

## Dados de suporte
1. Categoria de matéria-prima: Loging como PM (Product Manager) > RMC2002(id), Plástico 
2. Matéria-prima: Loging como PM (Product Manager), 6 (Textil) ,L001,Atacador Preto L, 3 (Metro), Camino para Ficheiro PDF (Mac: /Users/aTuaConta/Desktop/ficheiro.pdf)
3. Produto: Loging como PM (Product Manager), 10 (Procuts) , 1 (Add Product) , XTO5020 (Factory ID), QOS7020 (Commercial ID), RolhaA (Brief Description), Rolha de qualidade A (Full Description), AS-SP (Category), 1 (UN)
4. Ficheiro de importação de produtos: Produtos.csv (sacar do moodle) (meter num diretório acessível, p.e. Desktop, porque o path do ficheiro terá de ser fornecido ao sistema) (ficheiro de erros será criado no mesmo diretório)
5. --
6. Depósito: DE089, Depósito principal
7. Máquina: Loging como SFM (Shop Floor Manager), M001,AX2002, Maquina de Corte a Laser, 20-02-2020, YAMAHA, LaserCutter3000,10.
8. Linha de produção:

## Estado das User-Stories no final do Sprint

| User-Story                                           | Estado       |
| ---------------------------------------------------- | ------------ |
| US1003 - Configurar estrutura do projeto             | Concluída    |
| US1004 - Bootstrap de matérias-primas                | Concluída    |
| US1005 - Bootstrap de categorias de matérias-primas  | Concluída    |
| US1006 - Bootstrap de produtos                       | Concluída    |
| US1007 - Bootstrap de máquinas                       | Concluída    |
| US1008 - Bootstrap de linhas de produção             | Não iniciada |
| US1009 - Bootstrap de depósitos                      | Concluída    |
| US2001 - Adicionar uma matéria-prima                 | Concluída    |
| US2002 - Definir uma categoria de matérias-primas    | Concluída    |
| US2003 - Consultar produtos sem ficha de produção    | Em review    |
| US2004 - Especificar ficha de produção de um produto | Não iniciada |
| US2005 - Importar produtos através de ficheiro CSV   | Concluída    |
| US2006 - Adicionar um produto                        | Concluída    |
| US3001 - Definir uma máquina                         | Concluída    |
| US3002 - Especificar uma linha de produção           | Em curso     |
| US3003 - Especificar um depósito                     | Concluída    |
| US9001 - Elaborar apresentação para o cliente        | Concluída    |

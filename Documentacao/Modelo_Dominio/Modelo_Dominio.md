# Modelo de Domínio

Adotamos uma perspetiva incremental no que diz respeito ao modelo de domínio. Em primeiro lugar, chegamos a consenso sobre quais as entidades que devem estar no modelo (e quais se relacionam com quais), sendo que para isso dividimos o modelo por áreas funcionais de acordo com o que é referido no caderno de encargos. Posteriormente, acrescentamos sobre estas entidades os respetivos value-objects. Finalmente, identificamos os agregados e respetivas entidades-raizes e fazemos os devidos ajustes necessários.

## Índice

[TOC]

## Discussão dos conceitos e relações do domínio: vantagens e desvantagens das diferentes abordagens

Neste capítulo, o nosso objetivo passa por seguir o caderno de encargos e identificar os conceitos relevantes para o modelo de domínio, assim como representar as relações entre eles. Estas relações podem ser representadas de diferentes formas, pelo que iremos discutir as vantagens e desvantagens de cada uma, de modo a escolhermos a que mais se adequa ao sistema que estamos a desenvolver.

### a) Linhas de produção e Máquinas

Na página 2 do enunciado, temos o seguinte:

“O sistema a desenvolver visa ser implantado em unidades industriais. Uma **unidade industrial** compreende a existência de uma ou mais fábricas, sendo cada **fábrica** dedicada/especializada na produção de um **catálogo de produtos**. Contudo, para efeitos de projeto, pode-se considerar apenas a existência de uma fábrica. Com vista à produção desses **produtos**, a fábrica possui um conjunto muito diversificado de **máquinas** organizadas de forma sequencial em **linhas de produção**.”

Após a leitura deste parágrafo, talvez a solução que nos pareca mais intuitiva seja esta (Solução 1):

![Linha_producao_e_Maquina_v1](Conceitos_e_relacoes/Linha_producao_e_Maquina_v1.png)

No entanto, esta solução implica duas relações de um para muitos. Sempre que possível, devemos tentar simplificar estas relações (tal como as de muitos para muitos). E neste caso, podemos fazê-lo de duas maneiras.

Uma delas (Solução 2) passa por seguir exatamente o que o enunciado diz. Ou seja, a fábrica, em vez de possuir linhas de produção, vai possuir máquinas, e cada máquina está inserida numa só linha de produção.

![Linha_producao_e_Maquina_v2](Conceitos_e_relacoes/Linha_producao_e_Maquina_v2.png)

A outra solução (Solução 3) passa por manter as linhas de produção associadas à fábrica, mas referenciar, para cada linha de produção, apenas a primeira máquina da sequência, sendo que esta por sua vez iria referir a máquina seguinte da sequência, e por aí em diante até atingirmos a última máquina da linha. Uma linha de produção consiste numa sequência ordenada de máquinas. Esta solução permite representarmos facilmente a sua ordem.

![Linha_producao_e_Maquina_v3](Conceitos_e_relacoes/Linha_producao_e_Maquina_v3.png)

Vantagens e desvantagens de cada uma:

| **_Solução_** | **_Vantagens_** | **_Desvantagens_** |
|:--------------|:-------------------|:-------------------|
| **1** | i) Mais intuitiva | i) Duas relações de um para muitos |
| **2** | i) Apenas uma relação de um para muitos | i) Linha de produção não conhece as suas máquinas (importante mais à frente devido às ordens de produção) |
| **3** | i) Apenas uma relação de um para muitos; ii) Fácil de representar a ordem das máquinas ||

### b) Mensagens e Processamento 

Na página 3 do enunciado, temos o seguinte:

"Relativamente ao processamento das mensagens, pretende-se que este seja resiliente a **falhas** e potencie o seu tratamento/resolução. As falhas podem ter várias causas. De momento, o sistema a desenvolver deve focar-se nas falhas relacionadas com **menção a informação em falta no sistema**"

Após a leitura deste parágrafo, ouve um debate sobre como seria representada este conceito no nosso modelo de domínio. Inicialmente foi feito o levantamento de todo o tipo de mensagens como conceitos próprios pois cada um deles apresenta características distintas. Como o objetivo é representar conceitos importantes e não entidades do tipo "Sistema" o conceito falha foi adicionado de maneira a representar o processo de tratamento e resolução relativamente a falhas com a menção de informação.

Esta representação esperamos nós, menciona uma área importante do nosso domínio, mas que por si só não é explicita e está sempre a ser alvo de analise.

![mensagem_falha](Conceitos_e_relacoes/mensagem_falha.png)

### c) Ordens de produção

Este é um conceito muito importante no nosso modelo de domínio. Segundo a página 3 do enunciado: 
>“Uma **ordem de produção** é sempre referente a um **produto** e a uma (ou mais) **encomenda(s)** recebida(s) pela empresa."
>
>"Para tal, também se pode assumir que numa **linha de produção** está em execução apenas uma ordem de produção de cada vez.”

Podemos representar esta relação da seguinte maneira:

![Ordem_producao](Conceitos_e_relacoes/Ordem_producao.png)

Tendo em conta que o nosso sistema é focado no controlo da produção e da execução, parece fazer mais sentido ser a ordem de produção a referenciar a linha de produção (relação de um para um), em vez do inverso (relação de um para muitos). A ordem de produção quer conhecer a linha de produção para obter os dados que necessita (ex: máquinas usadas e respetivos tempos de execução). Por sua vez, no nosso sistema não interessa à linha de produção saber as suas ordens de produção. Não faz parte do nosso intuito coordenar as máquinas na sua atividade, queremos sim recolher os seus dados. 

Relativamente às ordens de produção, há outros três grandes tópicos que devemos abordar: a) os tempos de execução (brutos e efetivos) tanto da ordem como das respetivas máquinas onde esta foi executada; b) os lotes e os produtos resultantes; c) os consumos e desvios de matérias-primas.

Para ficarmos mais esclarecidos, vejamos a página 3 do enunciado:

>“No âmbito deste sistema, relativamente a cada **ordem de produção**, é importante saber-se (i) se a mesma já se encontra (ou não) em execução; (ii) quando é que se iniciou e concluiu a sua execução; (iii) a linha de produção e as máquinas onde a mesma decorreu; (iv) tempo bruto de execução (i.e. todo o tempo decorrido) e tempo efetivo de execução (i.e. não considera paragens devido a falhas ocorridas); (v) detalhe de tempos (brutos e efetivos) por máquina; (vi) os consumos reais das matérias-primas envolvidas na produção do produto em causa bem como potenciais desvios relativamente à lista de matérias-primas constante na sua ficha de produção (bill of materials); (vii) os lotes e respetivas quantidades de produto resultantes.”

#### **Ordens de produção e respetivos tempos de execução**

Neste sub-capítulo, vamos abordar os pontos i)-v). 

Para vermos se a ordem já se encontra ou não em execução - **ponto i)** -, podemos ter um atributo “Estado da ordem”. Isto é suportado pelo caso de uso “Criar ordens de produção”, onde temos o seguinte (página 8): 

>“Por omissão, uma ordem de produção assume o estado de “pendente”. (...) b. Uma ordem de produção posse assumir outros estados: i. “Em Execução” ou “Execução Parada Temporariamente” ou “Concluída” (...); ii. “Suspensa”.” 

Para o **ponto ii)** (saber quando se iniciou e concluiu a execução), precisaríamos à primeira vista de dois atributos: “Data de execução” e “Data de fim”. No entanto, dada a existência de tempos de execução, podemos não colocar a “Data de fim”, uma vez que somando os tempos de execução à data de início, obtemos o momento em que a ordem de produção chegou ao fim. Por isso, ficamos com o atributo “Data de execução”.

O **ponto iii)** já está resolvido, uma vez que a associação entre a ordem conhece a linha de produção onde esta foi/será executada. A partir da linha de produção também conhecemos as respetivas máquinas.

Por sua vez, os **pontos iv) e v)** são relativos aos tempos de execução. Um aborda os tempos totais da ordem e outro aborda os tempos relativos a cada máquina. Sendo assim, se tivermos os tempos de cada máquina, basta-nos somá-los e obtemos os tempos totais, pelo que o ponto iv) fica resolvido através do ponto v). A grande discussão passa agora a ser a forma como vamos representar os tempos por máquina no nosso modelo de domínio.

Eis as soluções a que chegamos:

![Ordem_producao_e_tempos_execucao](Conceitos_e_relacoes/Ordem_producao_e_tempos_execucao.png)

A **solução 1** é talvez a mais intuitiva. A máquina teria e saberia os seus tempos de execução (bruto e efetivo) relativos a cada ordem. No entanto, se virmos bem, não fará muito sentido que uma máquina tenha de se preocupar com os tempos de execução relativos a uma ordem, quando muito provavelmente essa máquina vai executar centenas ou milhares de ordens no seu ciclo de vida. É uma informação que diz respeito à ordem de produção e não à máquina.

Sendo assim, pensamos noutra solução, a **solução 2**. Aqui, a ordem de produção possui vários tempos de execução, sendo que cada tempo está associado à respetiva máquina. É uma solução mais equilibrada. No entanto, não parece ser da responsabilidade do tempo de execução de saber que deve ter uma máquina associada. O tempo seria apenas um conceito. E tempo não tem nada a ver com máquina.

Posto isto, chegamos à **solução 3**: a criação de um conceito intermediário que suporte os tempos de execução e ao mesmo tempo a referência à máquina respetiva. Trata-se do conceito “Atividade” na imagem. Uma ordem tem várias atividades, tendo cada uma os detalhes de tempo de uma máquina. A **solução 4** é semelhante, com a diferença de que os tempos de execução são atributos em vez de conceitos. Por agora, podem ser apenas atributos.

Portanto, as soluções 3 e 4 serão neste momento as mais adequadas.

#### **Unidade Industrial e Fábrica**

O conceito Unidade Industrial e Fábrica numa fase inicial do projeto foram identificadas como Entidades que tinha o seu valor para o negócio. A justificação que nos motivou neste sentido era baseada na possibilidade de existência de mais fábricas na nossa Unidade Industrial e então seria importante enquadrar o nosso Modelo de Domínio na sua totalidade lógica.

Contudo, após uma análise mais aprofundada no caderno de encargos concluímos que esse não era o objetivo da aplicação a desenvolver e que mesmo com a existência de mais fábricas na Unidade Industrial, o Modelo de Domínio não seria homogéneo e iria sofrer alterações. Consequentemente foram retiradas estas entidades tornando as relações mais simples e com menos dependências.



## Divisão por áreas funcionais

...

## Modelo completo


![Domain_Model](Domain_Model.svg)

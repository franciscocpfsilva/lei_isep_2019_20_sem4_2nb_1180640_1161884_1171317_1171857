# Glossário

| **_Termo_** | **_Descrição_** |                                       
|:------------|:----------------|
| **Chão de Fábrica** | Expressão genérica usada para designar (parte ou o todo) dos elementos (e.g. máquinas, funcionários, atividades) que participam nas tarefas produtivas de uma indústria, por contraponto com os elementos dedicados à gestão e administração do processo. |
| **Depósito** | Corresponde a um local onde são armazenados os produtos e as matérias-primas. Estes locais são comuns a todas as linhas de produção existentes na fábrica. |
| **Fábrica** | É um local estruturado em linhas de produção com vista ao fabrico de um ou mais produtos. |
| **Ficha de Produção** | Corresponde à lista de matérias-primas e respetivas quantidades usadas para produzir uma quantidade standard (e.g. 1 tonelada; 100 unidades) de um dado produto. |
| **Linha de Produção** | Organização sequencial de um conjunto de máquinas. |
| **Lote** | Corresponde a uma característica atribuída a um conjunto de exemplares de um produto. |
| **Máquina** | É um equipamento produtivo capaz de realizar operações com vista a produzir um produto. |
| **Matéria-Prima** | Corresponde a um material e/ou produto usado no processo de fabrico de um ou mais produtos. |
| **Mensagem (no âmbito de Máquina)** | Corresponde a um conjunto de dados gerado pela máquina e devidamente estruturado de acordo com um determinado tipo de mensagem. |
| **Movimento de Stock** | Corresponde à informação que regista a saída ou entrada de uma determinada quantidade de matéria-prima ou produto (em conformidade com uma unidade de medida) num determinado depósito em consequência da execução de uma dada ordem de produção. |
| **Ordem de Produção** | Documento em que se autoriza/solicita a produção de um produto numa determinada quantidade (a pretendida) através de um conjunto de matérias-primas e respetivas quantidades (de referência). |
| **Produto** | Corresponde a um item que uma fábrica é capaz de produzir. Nalguns casos, um produto pode ser utilizado como matéria-prima para a produção de outro produto. |
| **Tempo Bruto (de execução)** | Corresponde à diferença entre a data/hora indicada numa mensagem de “Fim de Atividade” (e.g. 10h13) e a data/hora indicada numa outra mensagem de “Inicio de Atividade” (e.g. 10h00). Neste exemplo, o tempo bruto seria de 13 minutos. |
| **Tempo Efetivo (de execução)** | Corresponde ao tempo bruto (de execução) subtraído do somatório do tempo despendido em paragens. O tempo de paragem corresponde à diferença entre a data/hora indicada numa mensagem de “Retoma de Atividade” (e.g. 10h09) e a data/hora indicada numa outra mensagem de “Paragem Forçada” (e.g. 10h07). Neste exemplo, o tempo de paragem seria de 2 minutos. Assim, considerando um tempo bruto de 13 minutos, o tempo efetivo seria de 11 minutos. |
| **Tipo de Mensagem (no âmbito de Máquina)** | Corresponde a um valor que permite determinar/classificar que género de conteúdo e estrutura consta de uma mensagem. |
| **Unidade de Medida** | Alguns exemplos são: (i) unidades; (ii) quilogramas; (iii) metro cúbico. |
| **Unidade Industrial** | Refere-se uma entidade e local dedicado à transformação de matérias-primas em produtos. |
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.scm;

import base.app.scm.rawmessages.importer.application.FileFormat;
import base.app.scm.rawmessages.importer.application.ImportRawMessageFromFileController;
import base.app.scm.server.application.ReplyToRequestController;
import base.app.scm.server.application.Server;
import java.io.IOException;

/**
 *
 * @author leona
 */
public class Main {
    
    public static void main(final String[] args) {
        
        if (args.length == 1){
            try{
                System.out.println("** SCM importe from file is running... **");
                
//                FileFormat format = checkFileFormat(args[0]);
                FileFormat format = FileFormat.valueOf(args[0]);
                
                ImportRawMessageFromFileController controller = new ImportRawMessageFromFileController();
                
                controller.startImport(format);
                
            }catch (IllegalArgumentException e){
                System.out.println("Error: " + e.getMessage());
            }
            
        }else if (args.length == 0){
            
            try{
                System.out.println("Accepting TCP connections (IPv6/IPv4). Use CTRL+C to terminate the server.");
                  
//                TcpSrvScm server = new TcpSrvScm();
//                server.startListeningToRequests();

//                ScmServerController controller = new ScmServerController();
//                controller.startListeningToRequests();

                Server server = new Server();
                server.start();

            }catch (IOException ioe){
                System.out.println("Error: " + ioe.getMessage());
            }
        }
    }
}

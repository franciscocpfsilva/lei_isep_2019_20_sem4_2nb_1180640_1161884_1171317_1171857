/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.scm.rawmessages.importer.application;

import eapli.base.rawmessage.domain.RawMessage;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author 
 */
public interface RawMessageFromFileImporter {
    
    /**
     * Initiate the import process. The implementation should open the underlying resource (e.g., file) and read the
     * "document start"/"header" for the respective format. It should also create 
     * a resource from the same type for error notification.
     *
     * @param file
     * @throws java.io.IOException
     */
    void begin(File file) throws IOException;
    
    /**
     * Read one single element from the resource. Returns an instance of RawMessage created from 
     * the information read. If unable create a RawMessage then returns null and 
     * prints that line into the error file.
     * 
     * @return the rawMessage read or null if unable to create it
     */
    RawMessage readElementFromFile();
    
    /**
     * Read one single element from a string passed as argument. Returns an instance of RawMessage created from 
     * the information read. If unable create a RawMessage then returns null and 
     * prints that line into the error file.
     * 
     * @param rawData
     * @return the rawMessage read or null if unable to create it
     */
    RawMessage readElementFromRawData(String rawData);
    
    /**
     * Check if there is still a new element to be read
     * 
     * @return  true, if there is a new element
     *          false, otherwise
     */
    boolean hasNext();

    /**
     * Indicates that there are no more elements to import. The implementation should create any "document closing"
     * element it might need and close the underlying resource.
     */
    void end();

    /**
     * Gives the importer implementation a chance to cleanup in case some exception has occurred.
     */
    void cleanup();
}

package base.app.scm.rawmessages.importer.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.rawmessage.domain.RawMessage;
import eapli.base.rawmessage.repositories.RawMessagesRepository;
import eapli.framework.domain.repositories.IntegrityViolationException;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.RollbackException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.exception.ConstraintViolationException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author
 */
public class RawMessageFromFileImporterService {
    
    private static final Logger LOGGER = LogManager.getLogger(RawMessageFromFileImporterService.class);
    private final RawMessagesRepository rawMessagesRepository = PersistenceContext.repositories().rawMessages();
    
    public Iterable<RawMessage> importRawMessages(File file, RawMessageFromFileImporter importer) throws IOException {
        
        List<RawMessage> messagesList = new LinkedList<>();
        
        try {
            
            importer.begin(file);
            
            while(importer.hasNext()) {
                
                RawMessage msg = importer.readElementFromFile();
                System.out.println("[@_FLAG 2]: hasNext: " + file.getName());
                if (msg != null && saveElement(msg)) {
                    messagesList.add(msg);
                }
            }
            importer.end();
            return messagesList;
        } catch(IndexOutOfBoundsException iob){
            LOGGER.error("Error while reading line: " + iob.getMessage());
            importer.cleanup();
            return null;
        }catch (final IOException e) {
            LOGGER.error("Error while importing", e);
            throw e;
        } finally {
            importer.cleanup();
        }
    }
    
    private boolean saveElement(RawMessage rawMessage) {
        
        try {
            
            try { 
                RawMessage tmp = this.rawMessagesRepository.findRawMessageBy(rawMessage.fromMachine(), 
                                                                rawMessage.messageType(), 
                                                                rawMessage.generatedAtDateTime());
            } catch (NoResultException nre){
                if (!this.rawMessagesRepository.contains(rawMessage)){
                    this.rawMessagesRepository.save(rawMessage);
                    return true;           
                }
            }
            LOGGER.error("Error: Message already existes in the DB");
            return false;
        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            return false;
        } catch (final RollbackException e) {
            if (!( e.getCause() instanceof PersistenceException 
                    && e.getCause().getCause() instanceof ConstraintViolationException ) ) {
                
                LOGGER.error("Problem importing message from machine: " + rawMessage.fromMachine(), e);
            }
            return false;
        }
    }
    
    public void importRawMessageFromRawData(String rawData, RawMessageFromFileImporter importer) throws IOException {
        
        RawMessage msg = importer.readElementFromRawData(rawData);
        if (msg != null && saveElement(msg))
            System.out.println("Successfully imported new Message!");
        else
            System.out.println("Unable to import Message!");
        importer.end();
        importer.cleanup();
    }
}

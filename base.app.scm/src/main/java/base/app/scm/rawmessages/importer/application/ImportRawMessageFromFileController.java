/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.scm.rawmessages.importer.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.machinery.domain.MachineID;
import eapli.base.machinery.repositories.MachinesRepository;
import eapli.base.rawmessage.domain.RawMessage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 *
 * @author leona
 */
public class ImportRawMessageFromFileController {
    
    private final String INPUT_DIR_PATH = "rawmessage_input";
    private final String OUTPUT_DIR_PATH = "rawmessage_output";
    private final String MAIN_FILE_NAME = "Mensagens.";
    private final String FILE_NAME_SEPARATOR = "\\.";
    
    private final RawMessageFromFileImporterFactory factory = new RawMessageFromFileImporterFactory();
    private final MachinesRepository machineRepository = PersistenceContext.repositories().machines();
    
    public void startImport(FileFormat format){
        
        final File inputFolder = new File(INPUT_DIR_PATH);
        final File outputFolder = new File(OUTPUT_DIR_PATH);
        
        System.out.println("INPUT_DIR_PATH: " + inputFolder.getAbsolutePath());
        for (final File fileEntry : inputFolder.listFiles()) {
            
            if (fileEntry.isFile() & fileEntry.getName().startsWith(MAIN_FILE_NAME)) {
                
                try{
                    
                    if( !machineRepository.findByID(new MachineID(fileEntry.getName().split(FILE_NAME_SEPARATOR)[1])).isPresent() )
                        throw new IllegalArgumentException("File from unknown Machine: " + fileEntry.getName());
                    
                    ImportRawMessageFromFileThread new_thread = new ImportRawMessageFromFileThread(fileEntry, format);
                    Thread task = new Thread(new_thread);
//                    new_thread.run();
                    task.start();
                    
//                    moveFile(fileEntry);
                    
                }catch(IllegalArgumentException | IllegalStateException iae){
                    System.out.println("Error: " + iae.getMessage());
                }
            }
        }
    }
    
    private int listMessages(final Iterable<RawMessage> importedRawMessages){
        int count = 0;
        for (RawMessage msg : importedRawMessages){
/* ------ Apagar */
            System.out.printf("nova msg: \tID: %s\tType: %s\tDate: %s\n", msg.fromMachine(), msg.messageType().name(), msg.generatedAtDateTime());
            count++;
        } return count;
    }
    
    private void moveFile(final File file) {
        Path result = null;
        try {
            final String src = file.getPath();
            final String dest = String.format("%s/%s", OUTPUT_DIR_PATH, file.getName());
            
            result =  Files.move(Paths.get(src), Paths.get(dest));
        } catch (IOException e) {
           System.out.println("Exception while moving file: " + e.getMessage());
        }
        if(result != null) {
           System.out.println("File moved successfully. ("+ file.getName() +")");
        }else{
           System.out.println("File movement failed.");
        }  
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.scm.rawmessages.importer.application;

import java.io.File;
import java.io.IOException;

/**
 *
 * @author
 */
public class ImportRawMessageFromFileThread implements Runnable{

    private final RawMessageFromFileImporterService service = new RawMessageFromFileImporterService();
    private final File file;
    private final RawMessageFromFileImporter importer;
    private final RawMessageFromFileImporterFactory factory = new RawMessageFromFileImporterFactory();
    
    public ImportRawMessageFromFileThread(File file, FileFormat format){
       this.file = file;
       this.importer = factory.build(format);
    }
    
    @Override
    public void run() {
         try{
            service.importRawMessages(file, importer);
        }catch (IOException e){
            System.out.println("Error while importing (Thread): " + e.getMessage());
        }        
    }
    
}

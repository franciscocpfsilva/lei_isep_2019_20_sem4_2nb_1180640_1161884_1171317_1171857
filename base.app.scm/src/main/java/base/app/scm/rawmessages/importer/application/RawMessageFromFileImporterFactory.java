/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.scm.rawmessages.importer.application;

/**
 *
 * @author 
 */
public class RawMessageFromFileImporterFactory {
    
    public RawMessageFromFileImporter build(FileFormat format) {
        if (format == null) {
            throw new IllegalStateException("Unknown format");
        }
        
        switch (format) {
        case CSV:
            return new CsvRawMessageFromFileImporter();
        }
        throw new IllegalStateException("Unknown format");
    }
}

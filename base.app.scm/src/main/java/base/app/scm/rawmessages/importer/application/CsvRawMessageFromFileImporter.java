/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.scm.rawmessages.importer.application;

import eapli.base.machinery.domain.MachineID;
import eapli.base.rawmessage.domain.MessageType;
import eapli.base.rawmessage.domain.RawMessage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author 
 */
public class CsvRawMessageFromFileImporter implements RawMessageFromFileImporter{

    private final String INPUT_DIR_PATH = "../rawmessage_input";
    private final static String ERROR_FILE_NAME = "failedMessages.";
    private final static String FILE_NAME_SEPARATOR= "\\.";
    private final static String FILE_SEPARATOR = ";";
    private final static int MINIMUM_EXPECTED_NUMBER_OF_ATTRIBUTES = 3;
    private final static int EXPECTED_DATE_SIZE = 14;
    
    private final static int POS_MACHINE_ID = 0;
    private final static int POS_MSG_TYPE = 1;
    private final static int POS_DATE = 2;
    
    private Scanner scanner; // for the file to be read
    private PrintWriter stream; // for the file where the errors will be printed
    
    private final Set<String> listType = new HashSet<>();
    private String str_machine = "";
    
    @Override
    public void begin(File fileToImport) throws IOException {
        
        str_machine = fileToImport.getName().split(FILE_NAME_SEPARATOR)[1];
        
        String errorFileNamefull = String.format("%s%s", ERROR_FILE_NAME, str_machine);
        String errorFilePath = ( (fileToImport.getParent() == null) ? INPUT_DIR_PATH : fileToImport.getParent() + "/" ) 
                                + errorFileNamefull;
        
        scanner = new Scanner(new FileInputStream(fileToImport));
        stream = new PrintWriter(new FileWriter(errorFilePath));
        
        for(MessageType msgType : MessageType.values()){
            listType.add(msgType.name());
        }
    }
    
    @Override
    public boolean hasNext() {
        return scanner.hasNext();
    }
    
    @Override
    public void end() {
        scanner.close();
        stream.close();
    }

    @Override
    public void cleanup() {
         if (scanner != null) {
            scanner.close();
        }
        if (stream != null) {
            stream.close();
        }
    }

    @Override
    public RawMessage readElementFromRawData(String line) {
        
        String[] attributes = line.split(FILE_SEPARATOR);
       
/* Validation */
        if (attributes.length < MINIMUM_EXPECTED_NUMBER_OF_ATTRIBUTES ) {
            stream.println(line + " << element with not enough attributes");
            return null;
        }else if (!listType.contains(attributes[POS_MSG_TYPE])) {
            stream.println(line + " << unknown message type");
            return null;
        }else if (attributes[2].length() != EXPECTED_DATE_SIZE) {
            stream.println(line + " << Expected full date and time");
            return null;
        }
        
        RawMessage rawMessage;
        try {
            
            // expected date format: 20190326151640
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
            LocalDateTime datetime = LocalDateTime.parse(attributes[POS_DATE], formatter);
//            byte[] rawData = line.getBytes();
                
           
            MachineID machineID = new MachineID(attributes[POS_MACHINE_ID]);

/*-- Checks if machine id of each message matches the file extension */            
            if (!str_machine.equals(attributes[POS_MACHINE_ID])){
                stream.println(line + " << Machine ID doesn't match");
                return null;
            }
    
            rawMessage = new RawMessage(machineID, 
                                        MessageType.valueOf(attributes[POS_MSG_TYPE]), 
                                        datetime, 
                                        line);
            
            return rawMessage;
            
        } catch (IllegalArgumentException e) {
            stream.println(line);
            return null;
        }
    }

    @Override
    public RawMessage readElementFromFile() {
        String line = this.scanner.nextLine();
        return readElementFromRawData(line);
    }
}

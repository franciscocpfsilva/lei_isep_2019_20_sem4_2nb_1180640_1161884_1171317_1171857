/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.scm.server.application;

import base.app.scm.server.domain.CodeTypeToSend;
import java.io.IOException;

/**
 *
 * @author leona
 */
public class SendRequestConfigToMachineController{

    private final ReplyToRequestFromBackofficeServer replyToBackofficeServer = new ReplyToRequestFromBackofficeServer();
    private final SendRequestFactory factory = new SendRequestFactory();
    
    
    public void sendRequestConfigToMachine(CodeTypeToSend code){
        try{
            System.out.println("Accepting TCP connections (IPv6/IPv4). Use CTRL+C to terminate the server.");
            
            final SendRequest request = factory.build(code);
            replyToBackofficeServer.listenToRequests(request);

        }catch (IOException ioe){
            System.out.println("Error: " + ioe.getMessage());
        }
    }
}

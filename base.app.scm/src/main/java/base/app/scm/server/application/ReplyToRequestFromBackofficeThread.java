/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.scm.server.application;

import eapli.base.machinery.application.FindMachineByIDService;
import eapli.base.machinery.domain.ConfigurationFile;
import eapli.base.machinery.domain.Machine;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

/**
 *
 * @author leona
 */
public class ReplyToRequestFromBackofficeThread implements Runnable{

    private final SendRequestConfigToMachineClient client = new SendRequestConfigToMachineClient();
    private final FindMachineByIDService machineService = new FindMachineByIDService();
    private final Socket socket;
    private DataOutputStream sOut;
    private DataInputStream sIn;
    private final SendRequest sender;
    private static final String SEPARATOR = ";";
    private static final int POS_MACHINE = 0;
    private static final int POS_FILE = 1;
    
    
    public ReplyToRequestFromBackofficeThread(Socket cliSock, SendRequest sender){
        this.socket=cliSock;
        this.sender=sender;
    }
    
    @Override
    public void run() {
        
        InetAddress clientIP;

        clientIP=socket.getInetAddress();
        System.out.println(">> New request CONFIG from Backoffice: " + clientIP.getHostAddress() + ", port number " + socket.getPort());
        try {
            sOut = new DataOutputStream(socket.getOutputStream());
            sIn = new DataInputStream(socket.getInputStream());
            
/* -- Read request from Backoffice */   
            int length = sIn.readInt();
            String message ="";
            for (int i=0; i<length; i++)
                message = message + (char) sIn.read();

/* -- Reply to Backoffice */
            String reply ="-- SCM server --";
            sOut.writeInt(reply.length());
            sOut.writeBytes(reply);
            
/* -- Send request to Machine Simulator */
            String[] array_message = message.split(SEPARATOR);
            Machine machine = machineService.findByID(array_message[POS_MACHINE]).get();
            
            for (ConfigurationFile file : machine.configurationFiles()){
                if (array_message[POS_FILE].equals(file.configurationFileDescription().toString())){
                    client.sendRequest(sender, Integer.parseInt(machine.communicationProtocolID().toString()), file);
                }
            }
/* -- Terminate connection */
            System.out.println("<< Backoffice: " + clientIP.getHostAddress() + ", port number: " + socket.getPort() + " disconnected\n");
            socket.close();
            
        } catch(IllegalArgumentException iae){
            System.out.println("Error: " + iae.getMessage());
        } catch(IOException ex) { 
            System.out.println("IOException: " + ex.getMessage()); 
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.scm.server.application;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author leona
 */
public class ReplyToRequestFromBackofficeServer {
    
    private ServerSocket sock;
    
    public void listenToRequests(SendRequest request)throws IOException {
        
        Socket cliSock;
        
        try {
            sock = new ServerSocket(Server.DEFAULT_PORT_N); 
        } catch(IOException ex) {
            System.out.println("Failed to open server socket");
//            System.exit(1);
        }

        while(true) {
            cliSock=sock.accept();
            Thread newThread = new Thread(new ReplyToRequestFromBackofficeThread(cliSock, request));
            newThread.start();
        }    
    }
}

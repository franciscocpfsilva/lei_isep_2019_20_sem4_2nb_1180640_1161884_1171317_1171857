/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.scm.server.application;

import java.io.IOException;

/**
 *
 * @author leona
 */
public class ReplyToRequestController {
     
    private final ReplyToRequestServer service = new ReplyToRequestServer();
    
    public void startListeningToRequests() throws IOException{
        service.listenToRequests();
        
    }
    
}
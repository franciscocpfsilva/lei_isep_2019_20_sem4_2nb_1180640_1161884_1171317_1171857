/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.scm.server.application;

import base.app.scm.rawmessages.importer.application.CsvRawMessageFromFileImporter;
import base.app.scm.rawmessages.importer.application.FileFormat;
import base.app.scm.rawmessages.importer.application.RawMessageFromFileImporter;

/**
 *
 * @author leona
 */
public class MachineMessageFromRawDataImporterFactory {
    
     public MachineMessageFromRawDataImporter build(FileFormat format) {
        if (format == null) {
            throw new IllegalStateException("Unknown format");
        }
        
        switch (format) {
        case CSV:
            return new MachineMessageFromRawDataImporterCsv();
        }
        throw new IllegalStateException("Unknown format");
    }
}

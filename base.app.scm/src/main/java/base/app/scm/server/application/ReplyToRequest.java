/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.scm.server.application;

import base.app.scm.server.domain.MachineMessage;
import java.io.DataOutputStream;
import java.net.InetAddress;
import java.util.Map;

/**
 *
 * @author leona
 */
public interface ReplyToRequest {
    
    void begin(DataOutputStream sOut, InetAddress clientIP);
    
    void sendReply(MachineMessage message);
}

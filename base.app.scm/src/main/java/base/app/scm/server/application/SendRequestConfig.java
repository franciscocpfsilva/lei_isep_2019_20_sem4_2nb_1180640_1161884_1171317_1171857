/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.scm.server.application;

import base.app.scm.server.domain.CodeTypeToSend;
import base.app.scm.server.domain.MachineMessage;
import eapli.base.communication.scm.domain.CommunicationProtocol;
import eapli.base.machinery.domain.ConfigurationFile;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author leona
 */
public class SendRequestConfig implements SendRequest{
    
//    private final ManageCommunicationProtocolService service = new ManageCommunicationProtocolService();
    private final MachineMessageTranslatorService translator = new MachineMessageTranslatorService();
    private DataOutputStream sOut;
//    private InetAddress clientIP;
//    private MachineMessage message;
    private CommunicationProtocol protocol;
            
    @Override
    public void begin(DataOutputStream sOut, CommunicationProtocol protocol) {
        System.out.println("Send CONFIG request");
        this.sOut = sOut;
        this.protocol = protocol;
//        this.clientIP = clientIP;
//        this.message = new MachineMessage( (char) 1, CodeTypeToSend.Config, this.protocol.communicationProtocolID().communicationProtocolID(), 0, "");
    }

    @Override
    public void sendRequest(Object obj) {
        
        ConfigurationFile file = (ConfigurationFile) obj;
        
        String rawData = new String(file.configurationFile());
        MachineMessage message = new MachineMessage( (char) 1, CodeTypeToSend.Config, 
                this.protocol.communicationProtocolID().communicationProtocolID(), 
                rawData.length(),
                rawData);

        try {
            translator.writeMessage(sOut, message);
            System.out.println("Request message: " + message);
        } catch (NullPointerException npe){
            npe.printStackTrace(System.out);
        } catch (IOException ex) {
            Logger.getLogger(ReplyToRequestHello.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}

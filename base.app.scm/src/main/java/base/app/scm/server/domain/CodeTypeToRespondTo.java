/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.scm.server.domain;

/**
 *
 * @author leona
 */
public enum CodeTypeToRespondTo {
    Hello(0),
    Msg(1),
    Ack(150),
    Nack(151);
    
    public final int code;
    
    private CodeTypeToRespondTo(int code){
        this.code=code;
    }
}

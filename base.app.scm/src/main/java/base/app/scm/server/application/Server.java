/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.scm.server.application;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author leona
 */
public class Server {
    
    static ServerSocket sock;
    static final int PORT_N=31401;
    static final int DEFAULT_PORT_N=9999;
    
//    private final ManageCommunicationProtocolService service = new ManageCommunicationProtocolService();
    private final ReplyToRequestController replyToRequestsController = new ReplyToRequestController();
   
    public void start() throws IOException{
        
        Thread newThread = new Thread(new SendRequestConfigToMachineThread());
        newThread.start();
        
//        Thread newThread = new Thread(new SendRequestConfigToMachineController());
//        newThread.start();
            
        replyToRequestsController.startListeningToRequests();
    }
    
//    public void listenToRequests()throws IOException {
//        
//        Socket cliSock;
//        service.importAllCommunicationProtocolsFromMachineRepo();
//        
//        try {
//            sock = new ServerSocket(Server.PORT_N); 
//            listenToRequests
//        } catch(IOException ex) {
//            System.out.println("Failed to open server socket");
//            System.exit(1);
//        }
//
//        while(true) {
//            cliSock=sock.accept();
//            Thread newThread = new Thread(new ReplyToRequestThread(cliSock));
//            newThread.start();
//        }    
//    }
}

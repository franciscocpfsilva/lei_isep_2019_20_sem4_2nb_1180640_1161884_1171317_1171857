/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.scm.server.application;

import eapli.base.machinery.domain.MachineID;
import eapli.base.rawmessage.domain.MessageType;
import eapli.base.rawmessage.domain.RawMessage;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author leona
 */
public class MachineMessageFromRawDataImporterCsv implements MachineMessageFromRawDataImporter{
    
    private final static String FILE_SEPARATOR = ";";
    private final static int MINIMUM_EXPECTED_NUMBER_OF_ATTRIBUTES = 3;
    private final static int EXPECTED_DATE_SIZE = 14;
    
    private final static int POS_MACHINE_ID = 0;
    private final static int POS_MSG_TYPE = 1;
    private final static int POS_DATE = 2;
    
    private final Set<String> listType = new HashSet<>();

    @Override
    public void begin() throws IOException {
        
        for(MessageType msgType : MessageType.values()){
            listType.add(msgType.name());
        }
    }

    @Override
    public RawMessage readElementFromRawData(String line) {
        
        String[] attributes = line.split(FILE_SEPARATOR);
       
/* Validation */
        if (attributes.length < MINIMUM_EXPECTED_NUMBER_OF_ATTRIBUTES ) {
            return null;
        }else if (!listType.contains(attributes[POS_MSG_TYPE])) {
            return null;
        }else if (attributes[2].length() != EXPECTED_DATE_SIZE) {
            return null;
        }
        
        RawMessage rawMessage;
        try {
            
            // expected date format: 20190326151640
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
            LocalDateTime datetime = LocalDateTime.parse(attributes[POS_DATE], formatter);
           
            MachineID machineID = new MachineID(attributes[POS_MACHINE_ID]);
    
            rawMessage = new RawMessage(machineID, 
                                        MessageType.valueOf(attributes[POS_MSG_TYPE]), 
                                        datetime, 
                                        line);
            
            return rawMessage;
            
        } catch (IllegalArgumentException e) {
            System.out.println("Error: " + e.getMessage());
            return null;
        }
    }
    
}

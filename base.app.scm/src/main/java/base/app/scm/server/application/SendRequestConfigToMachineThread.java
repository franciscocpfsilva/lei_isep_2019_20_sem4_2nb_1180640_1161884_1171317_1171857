/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.scm.server.application;

import base.app.scm.server.domain.CodeTypeToSend;

/**
 *
 * @author leona
 */
public class SendRequestConfigToMachineThread implements Runnable{

    private final SendRequestConfigToMachineController controller = new SendRequestConfigToMachineController();
    
    @Override
    public void run() {
        controller.sendRequestConfigToMachine(CodeTypeToSend.Config);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.scm.server.application;

import eapli.base.communication.scm.application.ManageCommunicationProtocolService;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author leona
 */
public class ReplyToRequestServer {
    
    private ServerSocket sock;
    
    private final ManageCommunicationProtocolService service = new ManageCommunicationProtocolService();
    
    public void listenToRequests()throws IOException {
        
        Socket cliSock;
        service.importAllCommunicationProtocolsFromMachineRepo();
        
        try {
            sock = new ServerSocket(Server.PORT_N); 
            
        } catch(IOException ex) {
            System.out.println("Failed to open server socket");
//            System.exit(1);
        }

        while(true) {
            cliSock=sock.accept();
            Thread newThread = new Thread(new ReplyToRequestThread(cliSock));
            newThread.start();
        }    
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.scm.server.application;

import base.app.scm.server.domain.CodeTypeToSend;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author leona
 */
public class SendRequestFactory {
    
    private final String PATH_TO_SEND_REQUEST = "base.app.scm.server.application.SendRequest";
    
    public SendRequest build(CodeTypeToSend type) {
        if (type == null) {
            throw new IllegalStateException("Unknown type");
        }
        
        Class<?> clazz;
        try {
            clazz = Class.forName(PATH_TO_SEND_REQUEST + type.name());
            return (SendRequest) clazz.newInstance();
            
        }  catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(ReplyToRequestFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        throw new IllegalStateException("Error while creating reply class.");
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.scm.server.application;

import base.app.scm.rawmessages.importer.application.RawMessageFromFileImporterService;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.rawmessage.domain.RawMessage;
import eapli.base.rawmessage.repositories.RawMessagesRepository;
import eapli.framework.domain.repositories.IntegrityViolationException;
import java.io.IOException;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.RollbackException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.exception.ConstraintViolationException;

/**
 *
 * @author leona
 */
public class MachineMessageFromRawDataImporterService {
    
    private static final Logger LOGGER = LogManager.getLogger(RawMessageFromFileImporterService.class);
    private final RawMessagesRepository rawMessagesRepository = PersistenceContext.repositories().rawMessages();
    
    private boolean saveElement(RawMessage rawMessage) {
        
        try {
            
            try { 
                RawMessage tmp = this.rawMessagesRepository.findRawMessageBy(rawMessage.fromMachine(), 
                                                                rawMessage.messageType(), 
                                                                rawMessage.generatedAtDateTime());
            } catch (NoResultException nre){
                if (!this.rawMessagesRepository.contains(rawMessage)){
                    this.rawMessagesRepository.save(rawMessage);
                    return true;           
                }
            }
            LOGGER.error("Error: Message already exists in the DB");
            return false;
        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            return false;
        } catch (final RollbackException e) {
            if (!( e.getCause() instanceof PersistenceException 
                    && e.getCause().getCause() instanceof ConstraintViolationException ) ) {
                
                LOGGER.error("Problem importing message from machine: " + rawMessage.fromMachine(), e);
            }
            return false;
        }
    }
    
    public void importRawMessageFromRawData(String rawData, MachineMessageFromRawDataImporter importer) throws IOException {
        
        RawMessage msg = importer.readElementFromRawData(rawData);
        if (msg != null && saveElement(msg))
            System.out.println("Successfully imported new Message!");
        else
            System.out.println("Unable to import Message!");
    }
}

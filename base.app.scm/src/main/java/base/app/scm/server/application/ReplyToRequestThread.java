/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.scm.server.application;

import base.app.scm.server.domain.MachineMessage;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

/**
 *
 * @author leona
 */
public class ReplyToRequestThread implements Runnable{

        
    private final ReplyToRequestFactory replyFactory = new ReplyToRequestFactory();
//    private final ReplyToRequestService replyService = new ReplyToRequestService();
    private final MachineMessageTranslatorService translator = new MachineMessageTranslatorService();
//    private final Map<Integer, String> machines;
    
    private final Socket socket;
    private DataOutputStream sOut;
    private DataInputStream sIn;
    
    public ReplyToRequestThread(Socket cliSock){
//    public ServerReplyThread(Socket cliSock){
        this.socket=cliSock;
//        this.machines = machines;
    }
    
    @Override
    public void run() {
        InetAddress clientIP;

        clientIP=socket.getInetAddress();
        System.out.println(">> New machine connection from: " + clientIP.getHostAddress() + ", port number " + socket.getPort());
        
        try {
            sOut = new DataOutputStream(socket.getOutputStream());
            sIn = new DataInputStream(socket.getInputStream());
            
/* -- Read request message */   
            MachineMessage message;
            try{
                message = translator.readMessage(sIn);
            }catch(IllegalArgumentException iae){
                System.out.println("Error: " + iae.getMessage());
                message = null;
            }
/* -- Reply to that message code specifically  */
            if (message!=null){
                final ReplyToRequest replyToRequest = replyFactory.build(message.codeTypeToRespondTo());
                replyToRequest.begin(sOut, clientIP);
                replyToRequest.sendReply(message);
            }else
                translator.writeMessage(sOut, message);
            
/* -- Terminate connection */
            System.out.println("<< Machine " + clientIP.getHostAddress() + ", port number: " + socket.getPort() + " disconnected\n");
            socket.close();
            
        } catch(IllegalArgumentException iae){
            System.out.println("Error: " + iae.getMessage());
        } catch(IOException ex) { 
            System.out.println("IOException: " + ex.getMessage()); 
        }
    }
    
}

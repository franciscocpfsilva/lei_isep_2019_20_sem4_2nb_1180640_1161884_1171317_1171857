/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.scm.server.application;

import eapli.base.rawmessage.domain.RawMessage;
import java.io.File;
import java.io.IOException;

/**
 *
 * @author leona
 */
public interface MachineMessageFromRawDataImporter {
     /**
     * Initiate the import process. The implementation should open the underlying resource (e.g., file) and read the
     * "document start"/"header" for the respective format. It should also create 
     * a resource from the same type for error notification.
     *
     * @throws java.io.IOException
     */
    void begin() throws IOException;
    
    /**
     * Read one single element from a string passed as argument. Returns an instance of RawMessage created from 
     * the information read. If unable create a RawMessage then returns null and 
     * prints that line into the error file.
     * 
     * @param rawData
     * @return the rawMessage read or null if unable to create it
     */
    RawMessage readElementFromRawData(String rawData);
    
}

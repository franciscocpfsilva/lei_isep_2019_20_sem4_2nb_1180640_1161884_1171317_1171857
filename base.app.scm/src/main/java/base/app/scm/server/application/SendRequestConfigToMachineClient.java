/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.scm.server.application;

import base.app.scm.server.domain.MachineMessage;
import eapli.base.communication.scm.application.ManageCommunicationProtocolService;
import eapli.base.communication.scm.domain.CommunicationProtocol;
import eapli.base.machinery.domain.ConfigurationFile;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 *
 * @author leona
 */
public class SendRequestConfigToMachineClient {
    
    private final ManageCommunicationProtocolService service = new ManageCommunicationProtocolService();
    private final MachineMessageTranslatorService translator = new MachineMessageTranslatorService();
    private final BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
   
    static InetAddress serverIP;
    static Socket sock;
    private DataOutputStream sOut;
    private DataInputStream sIn;
    
    public void sendRequest(SendRequest request, int protocolID, ConfigurationFile configFile){
       
        try{
            CommunicationProtocol protocol = service.findByID(protocolID);
            System.out.println("\n>> Trying to send CONFIG file to Machine: " + protocol.ipAddress() + ", ID number: " + protocol.identity());

            if (protocol.ipAddress().isEmpty()){
                System.out.println("Error: Machine still has not ip address assigned to it.");
                return;
            }

            try { 
                serverIP = InetAddress.getByName(protocol.ipAddress()); 
            }catch(UnknownHostException ex) {
                System.out.println("Invalid client specified: " + protocol.ipAddress());
                return;
            }

            try { 
                sock = new Socket(serverIP, Server.DEFAULT_PORT_N); 
            }catch(IOException ex) {
                System.out.println("Error: " + ex.getMessage());
                return;
            }

            sOut = new DataOutputStream(sock.getOutputStream());
            sIn = new DataInputStream(sock.getInputStream());


            request.begin(sOut, protocol);
            request.sendRequest(configFile);

            final MachineMessage machineMessage = translator.readMessage(sIn);
            System.out.println("<< Reply from machine: " + machineMessage.toString());

            sock.close();
//                MachineMessage msg = translator.readMessage(sIn);
//                System.out.println("Received responce: " + msg.toString());

        }catch(NumberFormatException nfe){
            System.out.println("Illegal communication protocol ID -> range (1-65535)");
        }catch(IOException ioe){
            System.out.println("IOException: " + ioe.getMessage()); 
        } 
    }
}

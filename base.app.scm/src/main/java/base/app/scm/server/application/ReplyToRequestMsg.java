/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.scm.server.application;

import base.app.scm.server.domain.CodeTypeToRespondTo;
import base.app.scm.server.domain.MachineMessage;
import base.app.scm.rawmessages.importer.application.FileFormat;
import eapli.base.communication.scm.application.ManageCommunicationProtocolService;
import eapli.base.communication.scm.domain.CommunicationProtocol;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author leona
 */
public class ReplyToRequestMsg implements ReplyToRequest{

    private final ManageCommunicationProtocolService protocolService = new ManageCommunicationProtocolService();
    private final MachineMessageTranslatorService translator = new MachineMessageTranslatorService();
    private DataOutputStream dataOutputStream;
    private final MachineMessageFromRawDataImporterFactory factory = new MachineMessageFromRawDataImporterFactory();
    private final MachineMessageFromRawDataImporterService service = new MachineMessageFromRawDataImporterService();
    private InetAddress clientIP;
    
    @Override
    public void begin(DataOutputStream sOut, InetAddress clientIP) {
        System.out.println("Reply to MSG request");
        this.dataOutputStream = sOut;
        this.clientIP = clientIP;
    }

    @Override
    public void sendReply(MachineMessage msg) {
        
        CommunicationProtocol protocol = protocolService.findByID(msg.machineId());
        MachineMessage message;
        if ( protocol !=null ){
            if (protocol.ipAddress().equals(clientIP.getHostAddress())){
                message = new MachineMessage( (char) msg.version(), CodeTypeToRespondTo.Ack, msg.machineId(), msg.dataLength(), msg.rawData());
            /* Add new Message */
                    MachineMessageFromRawDataImporter importer = factory.build(FileFormat.CSV);
                    try{
                        importer.begin();
                        service.importRawMessageFromRawData(msg.rawData(), importer);
                    }catch (IOException e){
                        System.out.println("Error while importing message to DB: " + e.getMessage());
                    }  
            } else {
                System.out.println(String.format("Error: IP address doesn't match! \n\tIp address (DB):: %s\n\tIp address (new): 5s", protocol.ipAddress(), this.clientIP.getHostAddress()));
                message = new MachineMessage( (char) msg.version(), CodeTypeToRespondTo.Nack, msg.machineId(), 0,"");
            }
        } else {
            System.out.println("Error: Machine unknown to the data base!" + msg.machineId());
            message = new MachineMessage( (char) msg.version(), CodeTypeToRespondTo.Nack, msg.machineId(), 0,"");
        }
        
        try {
            translator.writeMessage(dataOutputStream, message);
            System.out.println("Replied message: " + message);
        } catch (IOException ex) {
            Logger.getLogger(ReplyToRequestHello.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    

    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.scm.server.application;

import base.app.scm.server.domain.CodeTypeToRespondTo;
import base.app.scm.server.domain.MachineMessage;
import eapli.base.communication.scm.application.ManageCommunicationProtocolService;
import eapli.base.communication.scm.domain.CommunicationProtocol;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author leona
 */
public class ReplyToRequestHello implements ReplyToRequest{

    private final ManageCommunicationProtocolService service = new ManageCommunicationProtocolService();
    private final MachineMessageTranslatorService translator = new MachineMessageTranslatorService();
    private DataOutputStream dataOutputStream;
    private InetAddress clientIP;
    
    @Override
    public void begin(DataOutputStream sOut, InetAddress clientIP) {
        System.out.println("Reply to HELLO request");
        this.dataOutputStream = sOut;
        this.clientIP = clientIP;
    }

    @Override
    public void sendReply(MachineMessage msg) {
        
        CommunicationProtocol protocol = service.findByID(msg.machineId());
        MachineMessage message;
        if ( protocol !=null){
            protocol.addIpAddress(clientIP.getHostAddress());
            service.importCommunicationProtocol(protocol);
            message = new MachineMessage( (char) msg.version(), CodeTypeToRespondTo.Ack, msg.machineId(), 0, "");
        } else {
            System.out.println("Error: Machine unknown to the data base! (" + msg.machineId() + ")");
            message = new MachineMessage( (char) msg.version(), CodeTypeToRespondTo.Nack, msg.machineId(), 0,"");
        }
        
        try {
            translator.writeMessage(dataOutputStream, message);
            System.out.println("Replied message: " + message);
        } catch (IOException ex) {
            Logger.getLogger(ReplyToRequestHello.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.scm.server.application;

import base.app.scm.server.domain.CodeTypeToRespondTo;
import base.app.scm.server.domain.MachineMessage;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author leona
 */
public class MachineMessageTranslatorService {
    
    
    public  MachineMessage readMessage(DataInputStream sIn) throws IOException {
/*-- Read Version   - 1 byte*/
        char version = (char) sIn.read();
/*-- Read Code      - 1 byte*/
        char code = (char) sIn.read();
        CodeTypeToRespondTo codeType;
        codeType = getCodeTypeByVal( (int) code);
        
/*-- Read Code      - 2 bytes*/
        int id = readBytes(2, sIn);
/*-- Read Data Length   - 2 bytes*/
        int data_length = readBytes(2, sIn);
/*-- Read Raw Data   - data_length bytes*/
        String raw_data = readBytesToString(data_length, sIn);

//            MachineMessage message = new MachineMessage(version, code, id, data_length, raw_data);
        MachineMessage message = new MachineMessage(version, codeType, id, data_length, raw_data);
        return message;
    }
    
    private CodeTypeToRespondTo getCodeTypeByVal(int codeVal){
        for (CodeTypeToRespondTo code : CodeTypeToRespondTo.values())
            if (code.code == codeVal)
                return code;
        throw new IllegalArgumentException("Unknown message code (" + codeVal + ")" );
    }
    
    private int readBytes(int n, DataInputStream sIn) throws IOException{
        int res=0, f=1;
        for (int i=0; i<n; i++){
            res=res + f*sIn.read();
            f=f*256;
        } return res;
    }
    
    private String readBytesToString(int n, DataInputStream sIn) throws IOException{
        String str = "";
        for (int i=0; i<n; i++){
            str = str + (char) sIn.read();
        } return str;
    }

    public void writeMessage(DataOutputStream sOut, MachineMessage message) throws IOException {
        if (message != null){
            sOut.write((byte) message.version());
//            sOut.write((byte) message.codeTypeToRespondTo().code);
            sOut.write((byte) (message.codeTypeToRespondTo()== null ? message.codeTypeToSend().code : message.codeTypeToRespondTo().code));
            writeShortToBytes(2, sOut, (int) message.machineId());
            writeShortToBytes(2, sOut, (int) message.dataLength());
            writeStringToBytes(message.dataLength(), sOut, message.rawData());
        }else {
    /* -- Default error message */
            String err = "Unexpexted error occurred";
            sOut.write((byte) 0);
            sOut.write((byte) CodeTypeToRespondTo.Ack.code);
            writeShortToBytes(2, sOut, 0);
            writeShortToBytes(2, sOut, err.length());
            writeStringToBytes(err.length(), sOut, err);
        }
    }
    
    private void writeShortToBytes(int n, DataOutputStream sOut, int num) throws IOException{
        for (int i=0; i<n; i++){
            sOut.write( (byte) (num%256));
            num /= 256;
        }
    }
    
    private void writeStringToBytes(int n, DataOutputStream sOut, String str) throws IOException{
        for (int i=0; i<n; i++){
            sOut.write( (byte) str.charAt(i));
        }
    }
}

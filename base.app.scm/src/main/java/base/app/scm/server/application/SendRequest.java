/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.scm.server.application;

import eapli.base.communication.scm.domain.CommunicationProtocol;
import java.io.DataOutputStream;

/**
 *
 * @author leona
 */
public interface SendRequest {
    
    void begin(DataOutputStream sOut, CommunicationProtocol protocol);
    
    void sendRequest(Object obj);
}

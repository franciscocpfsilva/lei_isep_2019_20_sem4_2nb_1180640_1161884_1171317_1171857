/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.scm.server.domain;

/**
 *
 * @author leona
 */
public enum CodeTypeToSend {
    Config(2),
    Ack(150),
    Nack(151);
    
    public final int code;
    
    private CodeTypeToSend(int code){
        this.code=code;
    }
}

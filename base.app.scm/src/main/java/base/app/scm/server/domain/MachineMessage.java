/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base.app.scm.server.domain;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
/**
 *
 * @author leona
 */
public class MachineMessage {
    
    private char version;
    private CodeTypeToRespondTo codeTypeToRespondTo;
    private CodeTypeToSend codeTypeToSend;
    private int id;
    private int data_length;
    private String raw_data;
    
    public static final int SHORT_RANGE = 65535;
    public static final int CHAR_RANGE = 255;
    
    public static final int VERSION_BYTE_N = 1;
    public static final int CODE_BYTE_N = 1;
    public static final int ID_BYTE_N = 2;
    public static final int DATA_LENGTH_BYTE_N = 2;

//    AvailableMachines av = new AvailableMachines();

    /**
     * Constructor for creating a Message
     *
     * @param version Version a char with value of the version
     * @param codeType
     * @param id id short the Comunication Protocol ID of the machine
     * @param data_length data_length short containg the lenth of the raw data
     * string
     * @param raw_data raw_data String with the text of the message
     */
    public MachineMessage(char version, CodeTypeToRespondTo codeType, int id, int data_length, String raw_data) {
        setForResponce(version, codeType, id, data_length, raw_data);
    }
    
    public MachineMessage(char version, CodeTypeToSend codeType, int id, int data_length, String raw_data) {
        setForRequest(version, codeType, id, data_length, raw_data);
    }
    
    private void setForResponce(char version, CodeTypeToRespondTo codeType, int id, int data_length, String raw_data){
        if (codeType==null)
            throw new IllegalArgumentException("Code type can't be null");
        this.codeTypeToRespondTo = codeType;
        this.codeTypeToSend = null;
        setParameters(version, id, data_length, raw_data);
    }
    
    private void setForRequest(char version, CodeTypeToSend codeType, int id, int data_length, String raw_data){
        if (codeType==null)
            throw new IllegalArgumentException("Code type can't be null");
        this.codeTypeToRespondTo = null;
        this.codeTypeToSend = codeType;
        setParameters(version, id, data_length, raw_data);
    }
    
    private void setParameters(char version, int id, int data_length, String raw_data){
        this.version = version;
        this.id = setIntAsUnsignedShort(id);
        this.data_length = setIntAsUnsignedShort(data_length);
        this.raw_data = raw_data;
    }
    
//    private void setParameters(char version, CodeTypeToRespondTo codeType, int id, int data_length, String raw_data){
//        if (codeType==null)
//            throw new IllegalArgumentException("Code type can't be null");
//        this.version = version;
//        this.codeTypeToRespondTo = codeType;
//        this.id = setIntAsUnsignedShort(id);
//        this.data_length = setIntAsUnsignedShort(data_length);
//        this.raw_data = raw_data;
//    }
    
    private int setIntAsUnsignedShort(int val){
        if (val < 0 || val > SHORT_RANGE)
            throw new IllegalArgumentException("Number out of range (0-65535)");
        return val;
    }

    public char version(){
        return this.version;
    }
    public CodeTypeToRespondTo codeTypeToRespondTo(){
        return this.codeTypeToRespondTo;
    }
    
    public CodeTypeToSend codeTypeToSend(){
        return this.codeTypeToSend;
    }
    
    public int machineId(){
        return this.id;
    }
    public int dataLength(){
        return this.data_length;
    }
    public String rawData(){
        return this.raw_data;
    }
    
    @Override
    public String toString() {
        return String.format("[version=%d] [code=%d] [ID=%d] [RawLength=%d] [RawData=%s]",  
                (int) this.version, 
                this.codeTypeToRespondTo == null ? this.codeTypeToSend.code : this.codeTypeToRespondTo.code, 
                this.id, 
                this.data_length, 
                this.raw_data);
    }
    
}
